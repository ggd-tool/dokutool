export const environment = {
  production: true,
  userProfileURL: 'https://Example.com/auth/realms/Dokutool/account/#/personal-info',
  keycloakURL: 'https://Example.com/auth',
  keycloakRealm: 'Dokutool',
  keycloakClient: 'dokutool-frontend',
  apiServer: 'https://Example.com',
};
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BedienungsanleitungComponent } from './bedienungsanleitung.component';

describe('BedienungsanleitungComponent', () => {
  let component: BedienungsanleitungComponent;
  let fixture: ComponentFixture<BedienungsanleitungComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BedienungsanleitungComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BedienungsanleitungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

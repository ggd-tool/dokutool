import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ExportPopupComponent } from '../export-popup/export-popup.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  loaded = false;
  public isLoggedIn = false;
  public userProfile: KeycloakProfile | null = null;

  constructor(private readonly keycloak: KeycloakService, private dialog: MatDialog) { }

  async ngOnInit() {
    // Nutzerprofil laden
    this.isLoggedIn = await this.keycloak.isLoggedIn();

    if (this.isLoggedIn) {
      this.userProfile = await this.keycloak.loadUserProfile();
      this.loaded = true;
    }
  }

  // Dailog um Zeitraum für export festzulegen
  openExportDialog() {
    // Dialog konfigurieren
    const dialogExportConfig = new MatDialogConfig();
    dialogExportConfig.autoFocus = false;
    dialogExportConfig.width = "80%";
    // Dialog öffnen -> Siehe Dialog in KategoriePopupComponent
    const dialogRef = this.dialog.open(ExportPopupComponent, dialogExportConfig);
  }

  nameToShow(): String {
    // Name im Frontend anzeigen
    // Wenn kein Name angegeben username nehmen
    var name = "";
    (this.userProfile.firstName != "") ? name = this.userProfile.firstName : name = this.userProfile.username;
    return name;
  }

}

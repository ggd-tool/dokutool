import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { RepeatComponent } from '../../general/repeat/repeat.component';
import { FormControl, FormGroup } from '@angular/forms';
import { TypenService } from '../../../services/typen/typen.service';
import { MassnahmenService } from '../../../services/massnahmen/massnahmen.service';
import { PresetsService } from '../../../services/presets/presets.service';
import { InputBase } from '../../../inputs/input-base';
import { InputControlService } from '../../../services/input-control/input-control.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DropdownInput } from '../../../inputs/input-dropdown';
import { TextboxInput } from '../../../inputs/input-textbox';
import { CheckboxInput } from '../../../inputs/input-checkbox';
import { RadioInput } from '../../../inputs/input-radio';
import { MultiDropdownInput } from '../../../inputs/input-multidropdown';
import { DatepickerInput } from '../../../inputs/input-datepicker';
import { RangeInput } from '../../../inputs/input-range';
import { SliderInput } from '../../../inputs/input-slider';
import { NumberInput } from '../../../inputs/input-number';
import { TextareaInput } from '../../../inputs/input-textarea';
import { ArrayInput } from '../../../inputs/input-array';
import { InputSection } from 'src/app/inputs/input-section';
import { ReplaySubject } from 'rxjs';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { startWith } from 'rxjs/operators';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { UbersichtService } from 'src/app/services/ubersicht/ubersicht.service';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-forms-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss'],
  providers: [InputControlService],
})

export class DynamicFormComponent implements OnInit {

  // cosmetic for the Parent "dialogflow.component"
  @Input() Dialogflow_showHeader: string = 'true'
  @Input() Dialogflow_expanded: string = 'false'
  @Input() Dialogflow_anzeigen: string = "true"

  // important variables for the Parent "dialogflow.component"
  @Input() Dialogflow_Typ: string;
  @Input() Dialogflow_Preset_ID: any = {}
  @Input() Dialogflow_navigate: any = "true"
  @Output() Dialogflow_save = new EventEmitter<boolean>();

  @Input() inputs: InputBase<string>[] = [];
  allInputs: any[] = [];
  currentForm: FormGroup;
  forms: any = [];
  disabled: boolean = true;

  step: number = -1;
  step_sector: number = 0;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  payLoad: string;
  typ: string;
  massnahmeDurchfuehren: boolean = false;

  presetControl = new FormControl();
  presets: any[] = [];
  allPresets: any = [];
  showPreset: boolean = false;

  wiederholen: any[] = [];
  bearbeiten: boolean = false;
  anzeigen: boolean = false;
  disableButtons: boolean = false;
  lastId: string;

  link: string;
  preURL: string;

  loaded: boolean = false;
  repeatButton: boolean = false;
  service: any;

  trackedInputKeys: string[] = [];
  refWarning: any = [];

  @ViewChild('presetInput') presetInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  public presetFilterControl: FormControl = new FormControl();
  public filteredPresets: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(private _snackBar: MatSnackBar, private ref: ChangeDetectorRef, private ics: InputControlService, private route: ActivatedRoute, private typenService: TypenService, private massnahmenService: MassnahmenService, private presetsService: PresetsService,
    private dialog: MatDialog, private ubersichtService: UbersichtService, private router: Router, private location: Location) { }

  ngAfterContentChecked() {
    this.ref.detectChanges();
  }

  ngOnInit() {
    // Routenparameter auslesen - Zeigen den Typen an und welche Aktion benötigt wird
    this.route.params.subscribe((params) => {
      this.link = "/tool/";
      this.typ = params.typ;
      //Variablen müssen immer für anzeigen und bearbeiten resettet werden
      this.bearbeiten = false;
      this.loaded = false;
      this.anzeigen = false;
      this.disableButtons = false;
      this.wiederholen = [];
      this.presets = [];
      //falls aus anzeige mehr Dialoge hinzugefügt wurden und zurück gegangen wird wieder auf 1 setzen und step zum ausklappen resetten
      if (this.forms.length > 1) {
        this.forms = [this.forms[0]];
        this.step = 0;
      }
      // Formular des Typen vom Server laden
      // service für Typen und Maßnahmen switchen
      // Links für bearbeiten und wiederholen button setzen
      switch (this.route.data["_value"].routeName) {
        case "TypVerwalten":
          this.service = this.typenService;
          this.link += "typen/verwalten/" + this.typ;
          break;
        case "MaßnahmeVerwalten":
          this.service = this.massnahmenService;
          this.massnahmeDurchfuehren = params.action === 'ablegen';
          this.repeatButton = true;
          this.link += "maßnahmen/verwalten/" + this.typ;

          if(this.Dialogflow_Typ){
            this.typ = this.Dialogflow_Typ
          }
          break;
        case "PresetVerwalten":
          this.service = this.presetsService;
          this.link += "presets/verwalten/" + this.typ;
          break;  
      }

      this.route.queryParams.subscribe(queryParams => {
        // Überprüfen ob eine Maßnahme oder Objekt bearbeitet oder angezeigt werden soll
        if (queryParams.bearbeiten === "true") {
          this.bearbeiten = true;
          this.disableButtons = true;
        }
        if (queryParams.anzeigen === "true") {
          this.anzeigen = true;
          this.disableButtons = true;
        }
        //Falls kein Form vorhanden ist Component einmal builden
        if (this.forms.length === 0) {
          this.forms = [];
          this.buildComponent();
        }
      });
    });
  }

  buildComponent() {
    // mögliche Presets über Service laden
    this.presetsService.getAllFormName("Preset_" + this.typ).subscribe(data => {
      this.allPresets = data;
      if (this.allPresets !== null) {
        if (this.allPresets.length > 0) {
          this.showPreset = true;
        }
        //Filter zuweisen
        this.presetFilterControl.valueChanges.pipe(startWith('')).subscribe(() => {
          this._filterPresets();
        })
      }
    });

    this.service.getForm(this.typ).subscribe(data => {
      // Speicherurl der API speichern
      this.preURL = data["url"];
      // Einzelne Sektionen zu Formular umwandeln
      data['data'].forEach(section => {
        let tmpInputs = [];
        // Einzelnen Inputs einer Sektion verarbeiten
        section.inputs.forEach(input => {
          // Key formatierung basierend auf der gleichen Funktion wie im create Script
          input.key = this.ubersichtService.formatForDatabase(input.key);
          switch (input.type) {
            case "text":
              tmpInputs.push(new TextboxInput({
                key: input.key,
                label: input.label,
                default: input.default,
                min: input.min,
                required: input.required,
                hint: input.hint,
              }));
              break;
            case "dropdown":
              tmpInputs.push(new DropdownInput({
                key: input.key,
                default: input.default,
                dependFor: input.dependFor,
                dependLoader: input.dependLoader,
                label: input.label,
                options: input.options,
                required: input.required,
                hint: input.hint,

              }));
              if (input.ref) {
                this.trackedInputKeys.push(input.key);
              }
              break;
            case "user-dropdown":
              tmpInputs.push(new DropdownInput({
                key: input.key,
                label: input.label,
                loadUser: true,
                hint: input.hint
              }));
              break;
            case "checkbox":
              tmpInputs.push(new CheckboxInput({
                key: input.key,
                default: input.default,
                label: input.label,
                required: input.requiered,
              }));
              break;
            case "radio":
              tmpInputs.push(new RadioInput({
                key: input.key,
                default: input.default,
                label: input.label,
                options: input.options,
                required: input.required,
                hint: input.hint,
              }));
              break;
            case "multidropdown":
              tmpInputs.push(new MultiDropdownInput({
                key: input.key,
                default: input.default,
                label: input.label,
                dependsOn: input.dependsOn,
                dependFor: input.dependFor,
                dependLoader: input.dependLoader,
                singleDepend: input.singleDepend,
                options: input.options,
                required: input.required,
                hint: input.hint,
              }));
              if (input.ref) {
                this.trackedInputKeys.push(input.key);
              }
              break;
            case "datepicker":
              tmpInputs.push(new DatepickerInput({
                key: input.key,
                default: input.default,
                label: input.label,
                required: input.required,
              }));
              break;
            case "number":
              tmpInputs.push(new NumberInput({
                key: input.key,
                default: input.default,
                label: input.label,
                min: input.min,
                max: input.max,
                required: input.required,
                hint: input.hint,
              }));
              break;
            case "array":
              tmpInputs.push(new ArrayInput({
                key: input.key,
                default: input.default,
                label: input.label,
                options: input.options,
              }));
              break;
            case "textarea":
              tmpInputs.push(new TextareaInput({
                key: input.key,
                default: input.default,
                label: input.label,
                min: input.min,
                max: input.max,
                required: input.required,
                hint: input.hint,
                placeholder: input.placeholder
              }));
              break;
            case "rangeSlider":
              tmpInputs.push(new RangeInput({
                key: input.key,
                label: input.label,
                options: input.options,
                value: input.value,
                highValue: input.highValue,
              }))
              break;
            case "singleSlider":
              tmpInputs.push(new SliderInput({
                key: input.key,
                label: input.label,
                options: input.options,
                value: input.value,
              }))
              break;
          }
        });

        // Inputsektion letztendlich generieren
        this.inputs.push(new InputSection({
          label: section.label,
          hint: section.hint,
          inputs: tmpInputs
        }));
      });
      if (this.inputs !== []) {
        // Alle Inputs zusammenfassen - benötigt um sie zu einer Formgroup machen zu können
        this.inputs.forEach(section => {
          section.inputs.forEach(input => {
            this.allInputs.push(input);
          });
        });
        this.createForm();
        this.route.queryParams.subscribe(queryParams => {
          // Überprüfen ob id vorhanden und daten wieder in Dialog geladen werden müssen
          if (queryParams.id) {
            this.ics.findById(this.typ, queryParams.id).subscribe(async data => {
              //data für fillData formatieren
              let dataArray = [data];
              data = this.ubersichtService.formatForTable(dataArray, this.allInputs);
              this.lastId = data[0]._id;
              data = data[0];
              this.fillData(data, true);
            })
          } else {
            this.loaded = true;
          }
        });
      }
    });
  }

  //Form für AllInputs generieren 
  createForm(type?: string, title?: string) {
    this.step++;
    // Formgroups erstellen
    this.currentForm = this.ics.toFormGroup(this.allInputs);
    if (type && title) {
      this.forms.push({ form: this.currentForm, type: type, title: title, id: this.randomString(16) });
    } else {
      let name = this.typ;
      if (this.service === this.presetsService) {
        name = this.typ.split("Preset_")[1];
      }
      this.forms.push({ form: this.currentForm, type: "default", title: name, id: this.randomString(16) });
    }
    this.onChanges();
  }

  // Wenn sich in einem Formular etwas ändert
  onChanges(): void {
    // Alle Ref Dropdowns durchgehen
    this.trackedInputKeys.forEach(trackedInputKey => {
      // Alle Ref Felder in Forms überwachen
      this.forms.forEach(formObject => {
        formObject.form.get(trackedInputKey).valueChanges.subscribe(val => {
          if (val !== null) {
            if (this.forms.length > 0) {
              this.forms.forEach(otherFormObject => {
                if (Array.isArray(val)) {
                  if (otherFormObject !== formObject) {
                    if (this.refWarning[formObject.id] !== undefined) {
                      if (this.refWarning[formObject.id][otherFormObject.id] !== undefined) {
                        let refWarningPointer = this.refWarning[formObject.id][otherFormObject.id];
                        // Überprüfen ob alte Überschneidung weg ist
                        if (refWarningPointer.includes(trackedInputKey)) {
                          if (!otherFormObject.form.get(trackedInputKey).value.find(x => val.includes(x))) {
                            refWarningPointer.splice(refWarningPointer.indexOf(trackedInputKey), 1);
                            this.removeWarningInCss(formObject.id, otherFormObject.id, trackedInputKey);
                          }
                        }
                      }
                    }
                    // Auf neue Überschneidung prüfen
                    if (otherFormObject.form.get(trackedInputKey).value.find(x => val.includes(x))) {
                      if (this.refWarning[formObject.id] === undefined) {
                        this.refWarning[formObject.id] = [];
                      }
                      if (this.refWarning[formObject.id][otherFormObject.id] === undefined) {
                        this.refWarning[formObject.id][otherFormObject.id] = [];
                      }
                      this.refWarning[formObject.id][otherFormObject.id].push(trackedInputKey);
                      this.setWarningInCss(formObject.id, otherFormObject.id, trackedInputKey);
                    }
                  }
                } else {
                  if (otherFormObject !== formObject) {
                    if (this.refWarning[formObject.id] !== undefined) {
                      if (this.refWarning[formObject.id][otherFormObject.id] !== undefined) {
                        let refWarningPointer = this.refWarning[formObject.id][otherFormObject.id];
                        // Überprüfen ob alte Überschneidung weg ist
                        if (refWarningPointer.includes(trackedInputKey)) {
                          if (otherFormObject.form.get(trackedInputKey).value !== val) {
                            refWarningPointer.splice(refWarningPointer.indexOf(trackedInputKey), 1);
                            this.removeWarningInCss(formObject.id, otherFormObject.id, trackedInputKey);
                          }
                        }
                      }
                    }
                    // Auf neue Überschneidung prüfen
                    if (otherFormObject.form.get(trackedInputKey).value === val) {
                      if (this.refWarning[formObject.id] === undefined) {
                        this.refWarning[formObject.id] = [];
                      }
                      if (this.refWarning[formObject.id][otherFormObject.id] === undefined) {
                        this.refWarning[formObject.id][otherFormObject.id] = [];
                      }
                      this.refWarning[formObject.id][otherFormObject.id].push(trackedInputKey);
                      this.setWarningInCss(formObject.id, otherFormObject.id, trackedInputKey);
                    }
                  }
                }
              });
            }
          }
        });
      });
    });
  }

  setWarningInCss(id1, id2, key) {
    const warnColor = '3px solid rgba(255,153,0,0.6)';
    const warnBorder = "1px 1px 4px 2px rgba(255,153,0,0.6)";
    (document.querySelector('#' + id1) as HTMLElement).style.border = warnColor;
    (document.querySelector('#' + id2) as HTMLElement).style.border = warnColor;
    (document.querySelector('#' + id1 + key) as HTMLElement).style["box-shadow"] = warnBorder;
    (document.querySelector('#' + id2 + key) as HTMLElement).style["box-shadow"] = warnBorder;
  }

  removeWarningInCss(id1, id2, key) {
    const resetColor = "";
    const resetBorder = "0px 0px 0px 0px rgba(0,0,0,0.51)";
    (document.querySelector('#' + id1) as HTMLElement).style.border = resetColor;
    (document.querySelector('#' + id2) as HTMLElement).style.border = resetColor;
    (document.querySelector('#' + id1 + key) as HTMLElement).style["box-shadow"] = resetBorder;
    (document.querySelector('#' + id2 + key) as HTMLElement).style["box-shadow"] = resetBorder;
  }

  //ausgewähltes Preset anwenden
  async onPreset(preset) {
    this.ics.findById("Preset_" + this.typ, preset.key).subscribe(data => {
      //preset für fillData formatieren
      data = [data];
      data = this.ubersichtService.formatForTable(data, this.allInputs);
      data = data[0];
      this.fillData(data, false);
    })
  }

  //Presets löschen
  remove(preset): void {
    //Preset wieder in Auswahl hinzufügen damit es erneut angewendet werden kann
    this.allPresets.push(preset);

    let index = this.presets.indexOf(preset);
    if (index >= 0) {
      this.presets.splice(index, 1);
    }
    //Form mit gelöschtem Preset entfernen, falls kein Form mehr vorhanden neues generieren
    this.forms.forEach((element, index) => {
      if (element.type === "Preset" && element.title === preset.name) {
        this.forms.splice(index, 1);
        return;
      }
    });
    if (this.forms.length === 0) {
      this.createForm()
    }
    this.step--;
  }

  //Preset in auswahl hinzufügen
  selected(): void {
    this.loaded = false;
    for (let i = 0; i < this.presetControl.value.length; i++) {
      if (!this.presets.includes(this.presetControl.value[i])) {
        this.presets.push(this.presetControl.value[i]);
        this.onPreset(this.presetControl.value[i])
      }
    }

    if (this.presets.length > this.presetControl.value.length) {
      for (let i = 0; i < this.presets.length; i++) {
        if (!this.presetControl.value.includes(this.presets[i])) {
          for (let j = 0; j < this.forms.length; j++) {
            if (this.forms[j].title === this.presets[i].value) {
              this.forms.splice(j, 1);
              this.presets.splice(i, 1);
              if (this.forms.length === 0) {
                this.createForm();
                this.loaded = true;
                return;
              }
            }
          }
        }
      }
      this.loaded = true;
    }
  }

  //Presetnamen die dem Filter entsprechen suchen
  _filter(value: string): string[] {
    if (typeof value === "string") {
      const filterValue = value.toLowerCase();
      let matchingResults: any[] = [];
      this.allPresets.forEach(preset => {
        if (preset.name.toLowerCase().indexOf(filterValue) >= 0) {
          matchingResults.push(preset)
        }
      });
      return matchingResults;
    }
  }

  async save() {
    //spinner fürs speichern setzen
    this.loaded = false;
    let navigateLink: string = '/tool/ubersicht/';
    let alertTextSingle: string;
    let alertTextMultiple: string;
    let functionName: string;
    switch (this.route.data["_value"].routeName) {
      case "TypVerwalten":
        navigateLink += 'typen/' + this.typ;
        alertTextSingle = "Objekt";
        alertTextMultiple = "Objekte";
        functionName = "updateSpecificTyp";
        break;
      case "MaßnahmeVerwalten":
        navigateLink += '/maßnahmen/' + this.typ;
        alertTextSingle = "Maßnahme";
        alertTextMultiple = "Maßnahmen";
        functionName = "updateSpecificMassnahme";
        break;
      case "PresetVerwalten":
        navigateLink += 'presets/' + this.typ;
        alertTextSingle = "Preset";
        alertTextMultiple = "Presets";
        functionName = "updateSpecificPreset";
        break;
    }

    let formsWithError = [];
    let formsWithoutError = [];

    let tmpthis = this;

    let afterSaveRequest = () => {
      // Wenn es keine Error Forms gibt Erfolgsmeldung machen und zur Übersicht leiten
      if (formsWithError.length === 0) {
        if (!tmpthis.bearbeiten) {
          if (tmpthis.forms.length > 1) {
            tmpthis.openSnackBar(`${alertTextMultiple} erfolgreich gespeichert`);
          } else {
            tmpthis.openSnackBar(`${alertTextSingle} erfolgreich gespeichert`);
          }
        } else {
          tmpthis.openSnackBar(`${alertTextSingle} erfolgreich aktualisiert`);
        }
        if(this.Dialogflow_navigate == "true"){
          tmpthis.router.navigate([navigateLink]);
        }else{
          /// tell Parent Component "Maßnahme" is saved
          this.Dialogflow_save.emit(true);
        }
        
      } else {
        // User darauf hinweisen, dass es Fehler gab
        if (!tmpthis.bearbeiten) {
          if (tmpthis.forms.length > 1) {
            alert(`${formsWithoutError.length} ${alertTextMultiple} erfolgreich gespeichert. Noch angezeigte ${alertTextMultiple} weisen einen Fehler auf.`);
          } else {
            alert(`${alertTextSingle} weist Fehler auf und konnte deswegen nicht gespeichert werden.`);
          }
        } else {
          tmpthis.openSnackBar(`${alertTextSingle} erfolgreich aktualisiert`);
        }
        // Alle Forms löschen die keine Errors haben
        formsWithoutError.forEach(formWithoutError => {
          let index = tmpthis.forms.indexOf(formWithoutError);
          tmpthis.forms.splice(index, 1);
          tmpthis.step--;
        });
        // Fehler Felder markieren und error anzeigen
        formsWithError.forEach(formWithError => {
          for (let input in formWithError.error.errors) {
            let inputerror = formWithError.error.errors[input];
            if (inputerror.kind === "ObjectId") {
              formWithError.form.form.controls[inputerror.path].setErrors({ 'objectId': true });
            } else {
              formWithError.form.form.controls[inputerror.path].setErrors({ 'misc': true });
            }
          }
        });
      }
    };

    await this.forms.forEach(async element => {
      // Alert anzeigen und auf die jeweilige Übersicht leiten
      try {
        if (!this.bearbeiten) {
          await this.service.post(this.preURL, element.form.value).subscribe(data => {
            if (!data['_id']) {
              formsWithError.push({
                form: element,
                error: data
              });
            } else {
              formsWithoutError.push(element);
            }
            // Alle Forms wurden gespeichert
            if (formsWithError.length + formsWithoutError.length === this.forms.length) {
              afterSaveRequest();
            }
          });
        } else {
          await this.service[functionName](this.preURL, this.lastId, element.form.value).subscribe(data => {
            if (!data['_id']) {
              formsWithError.push({
                form: element,
                error: data
              });
            } else {
              formsWithoutError.push(element);
            }
            // Form wurde gepeichert
            if (formsWithError.length + formsWithoutError.length === this.forms.length) {
              afterSaveRequest();
            }
          });
        }
      } catch (error) {
        console.log(error)
      }
    });
    this.loaded = true;
  }

  //alert der bei Save aufgeruden wird
  openSnackBar(message: string) {
    this._snackBar.open(message, "schließen", {
      duration: 2000,
      panelClass: 'successSnackbar',
      verticalPosition: 'top'
    });
  }

  //Form wieder löschen
  removeForm(form) {
    let index = this.forms.indexOf(form);
    this.forms.splice(index, 1);
    this.step--;

    if (this.forms.length === 0) {
      this.createForm();
    }
  }

  // Dialog um vorangegangene Maßnahmen zu wiederholen
  openRepeat(typ) {
    // Dialog konfigurieren
    const dialogRepeatConfig = new MatDialogConfig();
    dialogRepeatConfig.autoFocus = true;
    dialogRepeatConfig.width = "80%";
    dialogRepeatConfig.data = {
      "massnahme": typ,
    }
    // Dialog öffnen -> Siehe Dialog in RepeatComponent
    const dialogRef = this.dialog.open(RepeatComponent, dialogRepeatConfig);
    // Wenn der Dialog geschlossen wird ausgewählter Maßnahme in das Formular schreiben
    dialogRef.afterClosed().subscribe(data => {
      this.loaded = false;
      this.fillData(data, true);
    });
  }

  // Das Formular mit Daten füllen
  async fillData(data, repeat: boolean) {
    let excludeBemerkung: boolean = false;
    if (data != undefined) {
      let form;
      //Titel der Card für wiederholen, bearbeiten und Preset anpassen
      if (this.bearbeiten || this.anzeigen) {
        let title = "";
        switch (this.route.data["_value"].routeName) {
          case "TypVerwalten":
            title = data.name;
            break;
          case "MaßnahmeVerwalten":
            title = "Maßnahme vom " + this.ubersichtService.showDate(data.datum);
            break;
          case "PresetVerwalten":
            title = "Preset " + data.name;
            break;
        }
        form = this.forms[0].form;
        this.forms[0].title = title;
        this.forms[0].type = "bearbeiten";
      } else if (repeat) {
        let title;
        if (!data.datum.value) {
          title = "Maßnahme vom " + this.ubersichtService.showDate(data.datum) + " wiederholen";
        } else {
          title = "Maßnahme vom " + this.ubersichtService.showDate(data.datum.value) + " wiederholen";
        }
        if (this.presets.length === 0 && this.wiederholen.length === 0) {
          form = this.forms[0].form;
          this.forms[0].title = title;
          this.forms[0].type = "wiederholen";
        } else {
          this.createForm("wiederholen", title);
          form = this.forms[this.forms.length - 1].form;
        }
      } else {
        excludeBemerkung = true;
        //für jedes Preset form erneut erstellen
        if (this.presets.length + this.wiederholen.length !== this.forms.length) {
          this.createForm("Preset", this.presets[this.presets.length - 1].value);
          form = this.forms[this.forms.length - 1].form;
        } else {
          form = this.forms[0].form;
          this.forms[0].type = "Preset";
          this.forms[0].title = this.presets[this.presets.length - 1].value;
        }
      }
      let dependFields = [];
      for (let property in data) {
        if (!(excludeBemerkung && property === 'bemerkung')) {
          if (form.get(property)) {
            if (data[property] !== null && data[property] !== undefined && data[property].length !== 0) {
              // Für dropdowns sowie datepicker werden extra Funktionen benötigt
              if (typeof data[property] === 'object') {
                if (data[property][0] !== undefined) {
                  // Wenn multidropdown key extrahieren und in neues Array schreiben
                  let tmpOptions = []
                  data[property].forEach(option => {
                    //Laden falls dependsOn
                    this.inputs.forEach(section => {
                      section.inputs.forEach(async input => {
                        if (input.key === property) {
                          if (input.dependsOn) {
                            //dürfen erst zum schluss geladen werden
                            if (!dependFields.includes(input)) {
                              dependFields.push(input);
                            }
                          }
                          if (input.controlType !== "range") {
                            if (option._id) {
                              tmpOptions.push(option._id)
                            }
                          }
                        }
                      });
                    });
                    if (option._id === undefined) {
                      if (option.key === undefined) {
                        tmpOptions.push(option);
                      } else {
                        tmpOptions.push(option.key);
                      }
                    }
                  });
                  form.get(property).setValue(tmpOptions);
                } else {
                  if (data[property].type === "datepicker") {
                    // datepicker aktuelles Datum einfügen
                    form.get(property).setValue(new Date());
                  } else {
                    //user-dropdown
                    if (data[property].FIRST_NAME) {
                      form.get(property).setValue(data[property]);
                    }
                    // Normales Dropdown einfügen
                    else {
                      if (data[property]['key'] !== undefined) {
                        form.get(property).setValue(data[property]['key']);
                      } else {
                        form.get(property).setValue(data[property]['_id']);
                      }
                    }
                  }
                }
              } else {
                //Ausnahme für Range slider, die vorher <x> - <x> formatiert sind
                this.inputs.forEach(section => {
                  section.inputs.forEach(async input => {
                    if (input.key === property) {
                      if (input.controlType === 'range') {
                        let split = data[property].split(" - ");
                        data[property] = [split[0], split[1]];
                      }
                    }
                  });
                });
                // Jedes andere Feld normal einfügen
                form.get(property).setValue(data[property]);
              }
            }
          }
          this.wiederholen.push(data);
        }
      }
      //depends dürfen erst zuletzt geladen werden, damit auch abhängigkeitsfeld befüllt
      for (let i = 0; i < dependFields.length; i++) {
        await form !== [];
        let formatedValue = form.value[await this.ubersichtService.formatForDatabase(dependFields[i].dependsOn.split(".")[0])];
        if (formatedValue !== undefined) {
          //soll nur einmal ausgeführt werden, damit nicht für jede Option ein request gefeuert wird
          this.ics.loadDependsOn(formatedValue, dependFields[i].dependsOn).subscribe(async dependData => {
            this.ics.setDependValues("", dependFields[i].controlType, dependFields[i].key, dependData, true);
            //darf bei anzeige nicht enabled werden
            if (!this.anzeigen) {
              await form.controls[dependFields[i].key].enable();
            }
            if (dependFields[i].singleDepend === true) {
              this.singleDependChange(dependFields[i].key, dependData, form);
            }
          });
        }
      }
    }
    //Timeout damit Smoother und kein Flackern einzelner Felder
    setTimeout(() => {
      this.loaded = true;
    }, 60);
  }

  //Disabled alle anderen optionen einer Gruppe falls eine ausgewählt
  singleDependChange(inputKey: string, options: any, form: FormGroup) {
    let selectedValue = form.value[inputKey];
    if (selectedValue) {
      for (let i = 0; i < selectedValue.length; i++) {
        let key = selectedValue[i];
        options.forEach(option => {
          if (option.keys.includes(key)) {
            option.data.forEach(element => {
              if (element.key !== selectedValue[i]) {
                element.disabled = true;
              }
            });
          }
        });
      }
    }
  }

  // Zufälligen String erzeugen
  randomString(length): string {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  // Form valid State überprüfen für Speichernbutton -> true = INVALID / false = VALID
  checkValid(): boolean {
    let disable: boolean = false;
    this.forms.forEach(element => {
      if (element.form.status === "INVALID") {
        disable = true;
      }
    });
    if (!disable) {
      return false;
    } else {
      return true;
    }
  }

  toggleEdit() {
    this.router.navigateByUrl(this.link + '/bearbeiten').then(() =>
      window.location.reload()
    );
  }

  backOnePage() {
    this.location.back();
  }

  //zurück zur jeweiligen Übersicht
  backUbersicht() {
    switch (this.route.data["_value"].routeName) {
      case "TypVerwalten":
        this.router.navigate(['/tool/ubersicht/typen/' + this.typ]);
        break;
      case "MaßnahmeVerwalten":
        this.router.navigate(['/tool/ubersicht/maßnahmen/' + this.typ]);
        break;
      case "PresetVerwalten":
        this.router.navigate(['/tool/ubersicht/presets/' + this.typ]);
        break;
    }
  }

  //Presets filtern
  _filterPresets() {
    let filterValue = this.presetFilterControl.value;
    if (!filterValue) {
      this.filteredPresets.next(this.allPresets.slice());
      return;
    } else {
      filterValue = filterValue.split(" ").join("").toLowerCase();
    }
    this.filteredPresets.next(
      this.allPresets.filter(preset => preset.value.split(" ").join("").toLowerCase().indexOf(filterValue) >= 0)
    );
  }

  /* 
    the functions below are needed for the dialogflow.component
    dialogflow.compoenent can control this component with this functions
  */ 
  // if the @input() Variable in the Parent Component is changed, load the new Forms
  ngOnChanges(changes) {
    let Clean_Up_Component = ()=>{
      this.forms = []
      this.inputs = []
      for(let control in this.currentForm.controls){          
        this.currentForm.removeControl(control)
      }
      this.allInputs = []
      this.trackedInputKeys = []

      this.refWarning = [] //????

      this.presets = []

      this.step_sector = 0
    }

    if(changes.Dialogflow_Typ){
      if (!changes.Dialogflow_Typ.firstChange) {
        if(this.typ != this.Dialogflow_Typ && 
                        this.Dialogflow_Typ && 
                        this.forms.length != 0){
          Clean_Up_Component()            
          this.ngOnInit()
        }      
      }
    }

    if(changes.Dialogflow_Preset_ID && changes.Dialogflow_Preset_ID.currentValue != "" && changes.Dialogflow_Preset_ID.currentValue != changes.Dialogflow_Preset_ID.previousValue){
      this.forms = []
      this.presetControl.setValue([this.Dialogflow_Preset_ID])
      this.presets = []
      this.selected()
    }

    // for selecting a preset and changing it back to ""
    if(changes.Dialogflow_Preset_ID && changes.Dialogflow_Preset_ID.currentValue == "" && !changes.Dialogflow_Preset_ID.firstChange){
      this.presetControl.setValue([""])
      this.presets = []
      Clean_Up_Component()            
      this.ngOnInit()
    }
    
    
  }
  // changes the expanded mat-accordion
  setStep(index: number) {
    this.step_sector = index;
  }
  // copied from dynamic-input.component
  async checkForDepend(input_key:string) {
    for(const section of this.inputs){
      for(const [index, input] of section.inputs.entries()){
        if(input.key != input_key){
          continue
        }
        for (let i = 0; i < input.dependLoader.length; i++) {
          let depend = input.dependLoader[i];
          let dependFor = input.dependFor[i];
          let formatedValue = await this.ubersichtService.formatForDatabase(depend.split(".")[0].toLocaleLowerCase());
          input.dependFor[i] = await this.ubersichtService.formatForDatabase(dependFor);
  
          formatedValue = this.forms[0].form.value[formatedValue];
          if (depend && this.forms[0].form.value[input.key].length !== 0) {
            if (formatedValue !== "") {
              this.ics.loadDependsOn(formatedValue, depend).subscribe(async data => {
                let dependValues = this.forms[0].form.value[dependFor];
                dependValues = [];
                await this.ics.setDependValues(input.dependFor[i], input.controlType, input.key, data, false, dependValues);
                if (dependValues.length > 0) {
                  this.forms[0].form.get(dependFor).setValue(dependValues);
                }
                //Falls Value nicht mehr im Dropdown verfügbar aus select löschen
                await this.ics.removeFromDependDropdown(dependFor, dependValues);
              })
            };
            if (!this.anzeigen) {
              this.forms[0].form.controls[dependFor].enable();
            }
          } else {
            //Falls alle abgewählt werden eventuell ausgewählte option wieder leeren
            this.forms[0].form.get(dependFor).setValue("");
            this.forms[0].form.controls[dependFor].disable();
          }
        }
      }
    }
  }

}

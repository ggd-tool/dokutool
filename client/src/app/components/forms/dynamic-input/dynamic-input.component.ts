import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { InputControlService } from 'src/app/services/input-control/input-control.service';
import { UbersichtService } from 'src/app/services/ubersicht/ubersicht.service';
import { InputBase } from '../../../inputs/input-base';

import { NgxMatDatetimePicker, NgxMatDateAdapter } from '@angular-material-components/datetime-picker';
import { Observable, ReplaySubject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';

@Component({
  selector: 'app-forms-dynamic-input',
  templateUrl: './dynamic-input.component.html',
  styleUrls: ['./dynamic-input.component.scss']
})
export class DynamicInputComponent implements OnInit {
  @Input() input: InputBase<string>;
  @Input() form: FormGroup;
  //um Inputs bei Anzeige zu disabeln 
  @Input() anzeigen: boolean;
  // Für Arrays
  @Input() arrayDepend: Object;

  selectAll: boolean = false;
  disableDependsOn: boolean = true;
  filteredOptions: Observable<any[]>;
  filteredUsers: Observable<any[]>;
  objectId = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
  allUsers: any[] = [];
  clearButton = false;
  valueToSet;
  userProfile: KeycloakProfile | null = null;

  public multiFilterControl: FormControl = new FormControl();
  public filteredObjectsMulti: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  // Alle Inputs die ein mat-form-field tag benötigen
  matFormFieldInputs = ['textbox', 'dropdown', 'user-dropdown', 'multidropdown', 'number', 'textarea'];

  constructor(private readonly keycloak: KeycloakService, private ics: InputControlService, private ubersichtService: UbersichtService, private dateAdapter: NgxMatDateAdapter<any>) { }

  async ngOnInit() {
    this.dateAdapter.setLocale("de");
    this.input.key = await this.ubersichtService.formatForDatabase(this.input.key);
    //Überprüfen ob Feld wegen dependsOn oder nur Anzeige disabled werde muss
    if (!this.anzeigen && this.input.dependsOn !== undefined && this.form.value[this.ubersichtService.formatForDatabase(this.input.dependsOn.split(".")[0])].length === 0) {
      this.form.controls[this.input.key].disable();
    }
    else if (this.anzeigen) {
      if (this.input.controlType === 'range' || this.input.controlType === 'slider') {
        this.input.options = Object.assign({}, this.input.options, { disabled: true })
      } else {
        this.form.controls[this.input.key].disable();
      }
    } else {
      this.form.controls[this.input.key].enable();
    }
    if (this.input.loadUsers) {
      this.getAllUsers();
    }
    if (this.input.options.length > 0) {
      this.filteredOptions = this.form.controls[this.input.key].valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    }
    if (this.input.controlType === 'multidropdown') {
      this.clearButton = true;
      this.valueToSet = [];
      if (!this.input.dependsOn) {
        this.multiFilterControl.valueChanges.pipe(startWith('')).subscribe(() => {
          this._filterMulti();
        })
      }
    }
    if (this.input.controlType === 'dropdown') {
      this.clearButton = true;
      this.valueToSet = "";
    }
  }

  ngOnChanges() {
    this.ngOnInit();
  }

  //filter um in dropdowns suchen zu können
  _filter(value: string) {
    const filterValue = value.split(" ").join("").toLowerCase();
    //Wenn schon eins ausgewählt wieder alle anzeigen 
    if (value.match(this.objectId)) {
      return this.input.options.filter(option => option.key.indexOf(filterValue) == 0);
    }
    return this.input.options.filter(option => option.value.split(" ").join("").toLowerCase().indexOf(filterValue) >= 0);
  }

  _filterUsers(value: string) {
    if (typeof value === 'string') {
      const filterValue = value.split(" ").join("").toLowerCase();
      return this.allUsers.filter(user => (user['FIRST_NAME'].toLowerCase() + user['LAST_NAME'].toLowerCase()).indexOf(filterValue) >= 0);
    } else {
      return this.allUsers.filter(user => (user['FIRST_NAME'].toLowerCase() + user['LAST_NAME'].toLowerCase()).indexOf(((value['FIRST_NAME'] as string).toLowerCase() + (value['LAST_NAME'] as string).toLowerCase())) == 0);
    }
  }

  _filterMulti() {
    let filterValue = this.multiFilterControl.value;
    if (!filterValue) {
      this.filteredObjectsMulti.next(this.input.options.slice());
      return;
    } else {
      filterValue = filterValue.split(" ").join("").toLowerCase();
    }
    this.filteredObjectsMulti.next(
      this.input.options.filter(option => option.value.split(" ").join("").toLowerCase().indexOf(filterValue) >= 0)
    );
  }

  // Für ein Multiselect
  // Wählt im übergebenen Multiselect alle Optionen aus oder ab
  selectToggle(key, options, depend: boolean) {
    if (!this.selectAll) {
      let optionsKeys = [];
      options.forEach(option => {
        optionsKeys.push(option.key);
      });
      this.form.get(key).setValue(optionsKeys);
      this.selectAll = true
    } else {
      this.form.get(key).setValue([]);
      this.selectAll = false;
    }
    if (depend) {
      this.checkForDepend();
    }
  }

  //lädt Values zu vom depend abhängigen Feld und disabled bzw. enabled dieses
  async checkForDepend() {
    for (let i = 0; i < this.input.dependLoader.length; i++) {
      let depend = this.input.dependLoader[i];
      let dependFor = this.input.dependFor[i];
      let formatedValue = await this.ubersichtService.formatForDatabase(depend.split(".")[0].toLocaleLowerCase());
      this.input.dependFor[i] = await this.ubersichtService.formatForDatabase(dependFor);
      formatedValue = this.form.value[formatedValue];
      if (depend && this.form.value[this.input.key].length !== 0) {
        if (formatedValue !== "") {
          this.ics.loadDependsOn(formatedValue, depend).subscribe(async data => {
            let dependValues = this.form.value[dependFor];
            dependValues = [];
            await this.ics.setDependValues(this.input.dependFor[i], this.input.controlType, this.input.key, data, false, dependValues);
            if (dependValues.length > 0) {
              this.form.get(dependFor).setValue(dependValues);
            }
            //Falls Value nicht mehr im Dropdown verfügbar aus select löschen
            this.ics.removeFromDependDropdown(dependFor, dependValues);
          })
        };
        if (!this.anzeigen) {
          this.form.controls[dependFor].enable();
        }
      } else {
        //Falls alle abgewählt werden eventuell ausgewählte option wieder leeren
        this.form.get(dependFor).setValue("");
        this.form.controls[dependFor].disable();
      }
    }
  }

  //Disabled alle anderen optionen einer Gruppe falls eine ausgewählt
  singleDependChange() {
    let selectedValue = this.form.value[this.input.key];
    if (selectedValue.length === 0) {
      this.input.options.forEach(option => {
        option.data.forEach(element => {
          element.disabled = false;
        });
      });
      return;
    }
    let values = [];
    for (let i = 0; i < selectedValue.length; i++) {
      let key = selectedValue[i];
      this.input.options.forEach(option => {
        if (option.keys.includes(key)) {
          option.data.forEach(element => {
            values.push(option.group);
            if (element.key !== selectedValue[i]) {
              element.disabled = true;
            }
          });
        }
      });
    }

    // check ob group disabled aber nicht mehr in auswahl 
    let checked = [];
    this.input.options.forEach((option, index) => {
      option.data.forEach(element => {
        if (element.disabled) {
          checked.push({ group: option.group, index: index })
          return;
        }
      });
    });

    checked.forEach(element => {
      if (!values.includes(element.group)) {
        this.input.options[element.index].data.forEach(option => {
          option.disabled = false;
        });
      }
    });
  }

  async getAllUsers() {
    this.userProfile = await this.keycloak.loadUserProfile();
    this.ics.getAllUsers().subscribe(data => {
      this.allUsers = data as any;
      let currentUser = this.allUsers.find(user => user['USERNAME'] === this.userProfile.username);
      this.form.controls[this.input.key].setValue(currentUser);
      this.filteredUsers = this.form.controls[this.input.key].valueChanges.pipe(
        startWith(''),
        map(value => this._filterUsers(value))
      );
    });
  }

  //Autocomplete Values richtig anzeigen
  displayUser(user): string {
    try {
      return user['FIRST_NAME'] + ' ' + user['LAST_NAME'];
    } catch (err) {
      return ""
    }
  }

  displayDropdown(key: string): string {
    for (let i = 0; i < this.input.options.length; i++) {
      if (this.input.options[i].key === key) {
        return this.input.options[i].value;
      }
    }
  }

  resetDropdown() {
    this.form.get(this.input.key).setValue(this.valueToSet);
    if (this.input.dependFor) {
      this.checkForDepend();
    }
  }
}

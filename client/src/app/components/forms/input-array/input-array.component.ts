import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { InputBase } from 'src/app/inputs/input-base';
import { DropdownInput } from 'src/app/inputs/input-dropdown';
import { NumberInput } from 'src/app/inputs/input-number';
import { TextboxInput } from 'src/app/inputs/input-textbox';
import { InputControlService } from 'src/app/services/input-control/input-control.service';
import { UbersichtService } from 'src/app/services/ubersicht/ubersicht.service';

@Component({
  selector: 'input-array',
  templateUrl: './input-array.component.html',
  styleUrls: ['./input-array.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputArrayComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: InputArrayComponent,
      multi: true
    }
  ]
})
export class InputArrayComponent implements OnInit, ControlValueAccessor {
  // Input Values zur generierung (siehe: dynamic-input.component)
  @Input() key: String;
  @Input() options: any[];
  //um Inputs bei Anzeige zu disabeln 
  @Input() anzeigen: boolean;
  // Falls man Objekte laden muss. So weiß man auch in diesem Kontext zu welchem Objekt das Array gehört
  @Input() arrayDepend: Object;

  // Custom-Input Funktionen 
  onChange = (_: any) => { };
  onTouch = (_: any) => { };

  // Form-Variablen
  allInputs: InputBase<string>[] = [];
  form: FormGroup;

  // Das Array an sich
  value = [];
  // Die Datenquelle der Tabelle
  dataSource = new MatTableDataSource<Object>();

  constructor(private ics: InputControlService, private ubersichtService: UbersichtService) { }

  ngOnInit(): void {
    // Einzelnen Inputs eine Arrays verarbeiten
    this.options.forEach(input => {
      // Key formatierung basierend auf der gleichen Funktion wie im create Script
      input.key = this.ubersichtService.formatForDatabase(input.key);
      switch (input.type) {
        case "text":
          this.allInputs.push(new TextboxInput({
            key: input.key,
            label: input.label,
            min: input.min,
            required: input.required,
            hint: input.hint,
          }));
          break;
        case "number":
          this.allInputs.push(new NumberInput({
            key: input.key,
            label: input.label,
            min: input.min,
            max: input.max,
            required: input.required,
            hint: input.hint,
          }));
          break;
        case "dropdown":
          this.allInputs.push(new DropdownInput({
            key: input.key,
            label: input.label,
            options: input.options,
            required: input.required,
            hint: input.hint,
          }));
          break;
      }
    });
    if (this.allInputs !== []) {
      // Eine Formgroup für das Array machen
      this.form = this.ics.toFormGroup(this.allInputs);
    }
    this.setDisabledState(this.anzeigen);
  }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.form.disable() : this.form.enable();
  }

  // Objekt zum Array hinzufügen
  addValue() {
    if (this.form.valid) {
      this.value.push(this.form.value);
      this.form.reset();
      this.emitChanges();
    }
  }

  // Objekt an der Stelle pos aus dem Array löschen
  deleteValue(pos) {
    this.value.splice(pos, 1);
    this.emitChanges();
  }

  // Tabelle aktualisieren und die onChange-Funktion ausführen
  emitChanges() {
    this.refreshShownValues();
    this.onChange(this.value);
  }

  // value ändern / überschreiben
  writeValue(val: any) {
    if (val) {
      // Hier jetzt das Objekt nochmal laden und die value richtig setzen
      if (val[0]._id) {
        this.ics.getObjectWithObjectinArray(this.fixedEncodeURIComponent(this.ubersichtService.formatForFiles(this.arrayDepend['typ'])), this.arrayDepend["key"], val[0]._id).subscribe(data => {
          this.value = data[this.arrayDepend["key"]];
          this.emitChanges();
        });
      } else {
        this.ics.getObjectWithObjectinArray(this.fixedEncodeURIComponent(this.ubersichtService.formatForFiles(this.arrayDepend['typ'])), this.arrayDepend["key"], val[0]).subscribe(data => {
          this.value = data[this.arrayDepend["key"]];
          this.emitChanges();
        });
      }
    }
  }

  // Neue onChange Funktion
  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  // Neue onTouch Funktion
  registerOnTouched(fn: any) {
    this.onTouch = fn;
  }

  validate(control: AbstractControl): ValidationErrors {
    const isValid = this.value && this.value.length >= 1;
    return isValid ? null : { invalidArray: isValid };
  }

  // Tabelle aktualisieren
  refresh(shownValue) {
    this.dataSource.data = shownValue;
  }

  // shownValues aktualisieren
  refreshShownValues() {
    // Alle Refs finden
    let allRefs = [];
    this.options.forEach(option => {
      if (option.ref) {
        allRefs.push({
          "ref": option.ref,
          "key": option.key
        });
      }
    });
    // Alle Ids durch name ersetzen
    // Kopie von this.value erstellen
    let tmpShownValue = [];
    for (let j = 0; j < this.value.length; j++) {
      tmpShownValue[j] = {};
      for (let property in this.value[j]) {
        tmpShownValue[j][property] = this.value[j][property];
      }
    }
    // Alle Referenzierten IDs zu Namen übersetzen
    for (let i = 0; i < allRefs.length; i++) {
      for (let j = 0; j < this.value.length; j++) {
        tmpShownValue[j][allRefs[i].key] = this.options.find(input => input.key === allRefs[i].key).options.find(option => option.key === this.value[j][allRefs[i].key]).value;
      }
    }
    this.refresh(tmpShownValue);
  }

  // Die am Anfang übergebenen Optionen/Eigenschaften des Arrays zu Tabellen-Spalten-Keys formatieren
  arrayOptionsToCols(options) {
    let out = [];
    options.forEach(option => {
      out.push(option.key);
    });
    //Buttons nicht in Tabelle anzeigen bei Anzeige Dialog
    if (!this.anzeigen) {
      out.push("actions");
    }
    return out;
  }

  fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
      return '%' + c.charCodeAt(0).toString(16);
    });
  }

}

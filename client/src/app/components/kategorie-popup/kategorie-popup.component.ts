import { Color, ColorAdapter } from '@angular-material-components/color-picker';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { UbersichtService } from 'src/app/services/ubersicht/ubersicht.service';

@Component({
  selector: 'app-kategorie-popup',
  templateUrl: './kategorie-popup.component.html',
  styleUrls: ['./kategorie-popup.component.scss']
})
export class KategoriePopupComponent implements OnInit {
  form: FormGroup;
  loaded: boolean = false;
  kategorien: any = [];
  name: string;
  pickedColor: string = "#283593";
  textColor: string = "#000000";

  displayedColumns: string[] = ['name', 'color', 'actions'];

  public color: ThemePalette = 'primary';
  private default: Color = new Color(40, 53, 147);

  @Output() updatedKategorien: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('formDirective') private formDirective: NgForm;

  constructor(private ubersichtService: UbersichtService) { }

  ngOnInit(): void {
    // Form erstellen
    this.form = new FormGroup({
      name: new FormControl("", [Validators.required]),
      color: new FormControl(this.default, [Validators.required])
    });
    this.form.controls['name'].valueChanges.subscribe(() => {
      this.name = this.form.get('name').value;
    })
    this.form.controls['color'].valueChanges.subscribe(() => {
      this.pickedColor = this.form.get('color').value;
    })
    this.loadTable();
    this.updatedKategorien.emit(false);
  }

  loadTable() {
    this.ubersichtService.getKategorien().subscribe(data => {
      if (data) {
        this.kategorien = data;
        this.loaded = true;
      }
    });
  }

  addValue() {
    if (this.form.valid) {
      let newKategorie = {
        name: this.form.value.name,
        color: this.form.value.color.hex,
        textColor: this.textColor
      }
      this.ubersichtService.postKategorie(newKategorie).subscribe(data => {
        if (data) {
          this.updatedKategorien.emit(true);
          this.loadTable();
          //reset
          this.form.reset();
          //Bei Angular Material muss ebenfalls FormDirective zurückgesetzt werden, damit Errors resettet werden
          this.formDirective.resetForm({ name: "", color: this.default });
        }
      });
    }
  }

  deleteValue(id) {
    let confirmation = confirm("Wollen Sie diese Kategorie wirklich löschen?");
    if (confirmation) {
      this.ubersichtService.deleteKategorie(id).subscribe(data => {
        if (data) {
          this.updatedKategorien.emit(true);
          this.loadTable();
        }
      });
    }
    return;
  }

  //Switch Textcolor zwischen Schwarz und weiß
  toggleTextColor() {
    this.textColor = this.textColor === "#000000" ? "#ffffff" : "#000000";
  }

}

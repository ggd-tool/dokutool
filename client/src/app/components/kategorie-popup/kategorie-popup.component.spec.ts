import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KategoriePopupComponent } from './kategorie-popup.component';

describe('KategoriePopupComponent', () => {
  let component: KategoriePopupComponent;
  let fixture: ComponentFixture<KategoriePopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KategoriePopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KategoriePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

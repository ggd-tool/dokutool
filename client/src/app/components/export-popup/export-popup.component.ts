import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UbersichtService } from 'src/app/services/ubersicht/ubersicht.service';
import { saveAs } from 'file-saver';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-export-popup',
  templateUrl: './export-popup.component.html',
  styleUrls: ['./export-popup.component.sass']
})
export class ExportPopupComponent implements OnInit {

  zeitraum = new FormGroup({
    start: new FormControl(),
    ende: new FormControl()
  });

  loaded: boolean = true;

  constructor(public dialogRef: MatDialogRef<ExportPopupComponent>, private ubersichtService: UbersichtService) { }

  ngOnInit(): void {
  }

  async exportExcel() {
    this.loaded = false;
    await this.ubersichtService.excelExport(this.zeitraum.value).subscribe(data => {
      this.loaded = true;
      //speichern Dialog öffnen und Exportdialog schließen
      const blob = new Blob([data], { type: 'application/octet-stream' });
      saveAs(blob, `Export(${this.formatDate(this.zeitraum.value.start)}-${this.formatDate(this.zeitraum.value.ende)}).zip`)
      this.dialogRef.close();
    })
  }

  //Datum als string in deutsches Format formatieren
  formatDate(date) {
    //Month +1 weil js monate von 0 zählt
    let monat = date.getMonth() + 1;
    let tag = date.getDate();
    let jahr = date.getFullYear();
    if (tag < 10) {
      tag = `0${tag}`;
    }
    if (monat < 10) {
      monat = `0${monat}`;
    }
    return `${tag}.${monat}.${jahr}`;
  }
}

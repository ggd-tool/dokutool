import { Component, OnInit, ViewChild, ViewChildren, ComponentRef } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ReplaySubject } from 'rxjs';
import { startWith, take } from 'rxjs/operators';

import * as RecordRTC  from 'recordrtc';

import { DialogflowService } from '../../services/dialogflow/dialogflow.service';
import { MassnahmenService } from '../../services/massnahmen/massnahmen.service'
import { PresetsService } from '../../services/presets/presets.service'

import { InputBase } from '../../inputs/input-base';

import { DynamicFormComponent } from '../forms/dynamic-form/dynamic-form.component'

@Component({
  selector: 'app-dialogflow',
  templateUrl: './dialogflow.component.html',
  styleUrls: ['./dialogflow.component.scss']
})

export class DialogflowComponent implements OnInit {

  constructor(
    private dialogflowService: DialogflowService, 
    private _formBuilder: FormBuilder, 
    private massnahmenService: MassnahmenService, 
    private presetsService: PresetsService,
    ) { }

  async ngOnInit() {

    this.deleteAllContexts()
    this.createContexts = ["select_massnahme"]


    /// fill with all "Maßnahmen" (and "Presets")
    this.all_Massnahmen = []
    this.massnahmenService.getAllObjects().pipe(take(1)).subscribe((data)=>{
      
      if(!(data instanceof Array) || data.length == 0){
        this.all_Massnahmen = []
        return
      }

      var massnahmen: any = data
      massnahmen.forEach(element => {
        this.all_Massnahmen.push({
          name: element.name,
          empty: false
        })
      });

      /// Sort all Maßnahmen alphabetically
      this.all_Massnahmen.sort((a,b) => a['name'] > b['name'] ? 1 : -1)

      /// assign "Maßnahme" Filter
      this.massnahmeFilterControl.valueChanges.pipe(startWith('')).pipe(take(1)).subscribe(() => {
        let filterValue = this.massnahmeFilterControl.value;
        if (filterValue == "" || !filterValue || filterValue == null) {
          this.filteredMassnahme.next(this.all_Massnahmen.slice());
          return;
        } else {
          filterValue = filterValue.split(" ").join("").toLowerCase();
        }
        this.filteredMassnahme.next(
          this.all_Massnahmen.filter(element => {
            let tmp_element: any = element
            let name:any = tmp_element.name
            return name.split(" ").join("").toLowerCase().indexOf(filterValue) >= 0
          })
        );
      });

      /// assign "Preset" Filter
      this.presetFilterControl.valueChanges.pipe(startWith('')).pipe(take(1)).subscribe(() => {
        let filterValue = this.presetFilterControl.value;
        if (filterValue == "" || !filterValue || filterValue == null) {
          this.filteredPresets.next(this.all_Presets.slice());
          return;
        } else {
          filterValue = filterValue.split(" ").join("").toLowerCase();
        }
        this.filteredPresets.next(
          this.all_Presets.filter(element => {
            let tmp_element: any = element
            if(tmp_element instanceof Object){
              return tmp_element["value"].split(" ").join("").toLowerCase().indexOf(filterValue) >= 0
            }else{
              return tmp_element.split(" ").join("").toLowerCase().indexOf(filterValue) >= 0
            }
          })
        );
      });
    })

    /// check the Validation from the Stepper/Inputs
    this.firstStepController = this._formBuilder.group({
      massnahme: ['', Validators.required],
    });
    this.secondStepController = this._formBuilder.group({
      preset: [''],
    });
    this.thirdStepController = this._formBuilder.group({
      empty: ['', Validators.required],
    });

  }

  ngOnDestroy(): void{
    if(this.comp){
      this.comp.destroy(); 
    }
  }

  @ViewChildren('dynamicFormComponent') dynamicFormComponent;
  comp: ComponentRef<DynamicFormComponent>
  Dialogflow_typ: string
  Dialogflow_action: string

  @ViewChildren('selectInput') selectInput;

  massnahmeFilterControl: FormControl = new FormControl();
  all_Massnahmen: object[] = [];
  filteredMassnahme: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  selected_Massnahme: string = ""

  presetFilterControl: FormControl = new FormControl();
  all_Presets: string[] = [];
  filteredPresets: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  selected_preset: string

  inputs: InputBase<string>[] = [];

  @ViewChild('stepper') stepperView

  firstStepController: FormGroup;
  secondStepController: FormGroup;
  thirdStepController: FormGroup;

  skipPresetStepper:boolean = false  

  // Request vom User an Dialogflow. Durch variable binding wird die Frage vom User gespeichert
  querytext: string = "";
  
  // Für den RecorderRTC
  public_recorder: any; 
  public_stream: any;
  recorder_einstellungen: any = {
    type: 'audio',
    recorderType: RecordRTC.StereoAudioRecorder,
    mimeType: 'audio/wav',
    disableLogs: true,
    numberOfAudioChannels: 1,
  }
  status: any;
  recordingstarted: string;

  // Der Context Array wird hier als String zwischengespeichert.
  createContexts: String[] = []
  deleteContexts: String[] = []

  // allow Experimental Intents
  slide_Experimental_Intents:boolean = false

  // Der ganze Dialog soll in diesen Array gespeichert werden und im HTML ausgelesen werden
  dialog: any = {
    user:       "",
    dialogflow: ""
  }

  massnahme_parameter: any = {}
  /**/
  
  // Stepper and form functions
  async MassnahmeChange(value){
    this.selected_Massnahme = value
    let massnahme = value

    /// Get all Presets from assigned Maßnahme
    this.presetsService.getAllFormName("Preset_" + massnahme).pipe(take(1)).subscribe((data)=>{
      let tmp_presets: any = data
      /// check if its null or empty
      if(!(tmp_presets instanceof Array) || tmp_presets.length == 0 || !tmp_presets){
        this.all_Presets = []
        this.filteredPresets.next([])

        /// skip Preset Stepper 
        this.skipPresetStepper = true

      }else{
        /// put it inside "all_Presets"
        this.all_Presets = tmp_presets
        this.filteredPresets.next(tmp_presets.slice())

        /// skip Preset Stepper 
        this.skipPresetStepper = false
      }

    })
    this.selected_preset = ""
  }
  onMassnahmeChange(event){
    this.MassnahmeChange(event.value)
    this.Dialogflow_send_text_request(event.value, false)
  }
  onPresetChange(event){
    this.selected_preset = event.value
    this.stepperView.next()
  }
  save(){
    this.deleteAllContexts()
    this.secondStepController.controls.preset.setValue("")
    this.stepperView.selectedIndex = 0
  }

  // RecorderRTC Functionen 
  async recordingStart() {
    // navigator.mediaDevices.getUserMedia() can only be used, when the Website is with HTTPS secured.
    // if its not, then navigator.mediaDevices.getUserMedia() is undefined
    try{
      if(this.public_recorder == undefined || this.public_stream == undefined){
        this.public_stream = await navigator.mediaDevices.getUserMedia({ audio: true});
        this.public_recorder = new RecordRTC(this.public_stream, this.recorder_einstellungen);
      }else{
        if(this.public_stream.getAudioTracks()[0].readyState == "ended"){
          this.public_stream = await navigator.mediaDevices.getUserMedia({ audio: true});
          this.public_recorder = new RecordRTC(this.public_stream, this.recorder_einstellungen);
        }
      }
    }catch(e){
      console.log(e.name + ": " + e.message);
    }
    
    try{
      switch(this.public_recorder.getState()){
        case "recording":
          this.public_recorder.pauseRecording();
          this.status = "gestoppt"
          break;
        case "paused":
          this.public_recorder.resumeRecording();
          this.status = "recording"
          break;
        default:
          this.public_recorder.startRecording();
          this.status = "recording"
          break;
      }
      this.recordingstarted = this.public_recorder.getState()
    }catch(e){
      console.log(e.name + ": " + e.message);
    }
  }
  recordingAbort(){
    try{
      if(this.public_recorder){
        if(this.public_recorder.getState() == "paused" || this.public_recorder.getState() == "recording"){
          this.public_recorder.reset()
          this.public_stream.getTracks().forEach(function(track) {
            track.stop();
          });

          this.recordingstarted = this.public_recorder.getState()
          this.status = "abgebrochen"

        }
      }
      
    }catch(e){
      console.log(e.name + ": " + e.message);
      this.status = "Fehler!";
    }
  }
  
  // Dialogflow Debug Functionen
  async getcontext(){
    await this.dialogflowService.showContext().pipe(take(1)).subscribe(data=>{
      console.log(data)
    })
  }
  async deleteContext(name){    
    let request: Array<number>
    if(typeof name == "object"){
      request = name
    }else{
      request = [name]
    }

    await this.dialogflowService.deleteContext(request).pipe(take(1)).subscribe((data)=>{
      //this.getcontext()
    })
  }
  async createContext(name: string, lifespanCount: number = 5, parameters: object = undefined){
    await this.dialogflowService.createContext({
      'context': {
        name: name,
        lifespanCount: lifespanCount,
        parameters: parameters
      },
    }).pipe(take(1)).subscribe((data)=>{
      //this.getcontext()
    });
  }
  showSessionEntities(){
    this.dialogflowService.showEntities().pipe(take(1)).subscribe(d => console.log(d))
  }
  
  // play Dialogflow Audio 
  play_byteArray(byteArray){
      // from https://stackoverflow.com/questions/56426744/how-to-play-the-output-audio-from-dialogflow-int-array

      var arrayBuffer = new ArrayBuffer(byteArray.length);
      var bufferView = new Uint8Array(arrayBuffer);
      for (let i = 0; i < byteArray.length; i++) {
        bufferView[i] = byteArray[i];
      }
      let audioContext = new AudioContext();
      audioContext.decodeAudioData(
        arrayBuffer,
        function(buffer) {
          play(buffer);
        }.bind(this)
      );
  
    var play = (buf) => {
      // Create a source node from the buffer
      let context = new AudioContext();
      var source = context.createBufferSource();
      source.buffer = buf;
      // Connect to the final output node (the speakers)
      source.connect(context.destination);
      // Play immediately
      source.start(0);
    }
  }
  // Dialogflow Functionen
  async deleteAllContexts(){
    await this.dialogflowService.deleteAllContexts().pipe(take(1)).subscribe(data=>{
    })
  }
  getCurrentStepper(){
    var switchSelection = [0,1,2,3]
    const selectedIndex = this.stepperView.selectedIndex 

    if(this.skipPresetStepper){
      switchSelection = [0, null, 1, 2]
    }

    switch(selectedIndex){
      case switchSelection[0]:
        this.deleteContexts = ["select_preset", "customize_massnahme", "fertig"]
        this.createContexts = ["select_massnahme"]
        break;  
      case switchSelection[1]:
        this.deleteContexts = ["select_massnahme", "customize_massnahme", "fertig"]
        this.createContexts = ["select_preset"]
        break;
      case switchSelection[2]:
        this.deleteContexts = ["select_massnahme", "select_preset"]
        this.createContexts = ["customize_massnahme", "fertig"]
        break;
      case switchSelection[3]:
        this.deleteContexts = ["select_massnahme", "select_preset", "customize_massnahme"]
        this.createContexts = ["fertig"]
        break;
      default:
        this.deleteContexts = ["select_massnahme", "select_preset", "customize_massnahme", "fertig"]
        this.createContexts = []
        console.log("Error: Stepper is not defined")
        break;
    }
  }
  Dialogflow_send_audio_request(){
    // Audio aufnahme:
    // https://github.com/muaz-khan/RecordRTC
    
    // Audio nach Nodejs übermitteln:
    // Im Frontend werden die Daten in FormData gespeichert und übersendet
    // Im Backend werden die Daten durch Multer abgefangen und bereit gestellt. (Multer ist in pre-routes.js zu finden)
    // Mit https://www.npmjs.com/package/multer 

    // Die UserID/SessionID taucht im Response von Dialogflow nach User nur im Context auf
    // Die UserID/SessionID taucht im WebhookRequest von Dialogflow nach externen Server in der Variable SessionID auf.
    // https://cloud.google.com/dialogflow/es/docs/fulfillment-webhook#webhook_request

    try{
      var audio_request = new FormData();

      this.public_recorder.stopRecording(() => {
        this.status = "bitte warten"

        audio_request.append("audio", this.public_recorder.getBlob())
        this.public_recorder.reset()
        this.recordingstarted = this.public_recorder.getState()
        this.public_stream.getTracks().forEach(function(track) {
          track.stop();
        });
        
        

        this.getCurrentStepper()
        if (this.createContexts) {
          audio_request.append("createContexts", JSON.stringify(this.createContexts))
        }
        if (this.deleteContexts) {
          audio_request.append("deleteContexts", JSON.stringify(this.deleteContexts))
        }

        if(this.slide_Experimental_Intents){
          audio_request.append("allowExperimentalIntents", 'true')
        }

        this.dialog.user =        "[Audio gesendet]"
        this.dialog.dialogflow  = "[Anfrage wird bearbeitet]"

        this.dialogflowService.UseAgent_audio(audio_request).pipe(take(1)).subscribe(result=>{
          this.handling_result(result)
        })
      });
    }catch(e){
      this.status = "fehler"
      this.dialog.user = "[unerwarteter Fehler: Benachrichtigen sie den IT-Support]"
      console.log(e.name + ": " + e.message);
      if(this.public_recorder.getState() == "paused" || this.public_recorder.getState() == "recording"){
        this.public_recorder.reset()
      }
      this.public_stream.getTracks().forEach(function(track) {
        track.stop();
      });
    }
  }
  Dialogflow_send_text_request(querytext = this.querytext, handling_result = true){
    if(querytext == null || querytext == undefined || querytext == ""){
      //hier eine Warnung, dass das Feld ausgefüllt werden soll.
      return 
    }
    try{

      this.getCurrentStepper()
      var text_request = {
        "querytext": querytext,
        "createContexts": this.createContexts,
        "deleteContexts": this.deleteContexts
      }

      if(this.slide_Experimental_Intents){
        text_request["allowExperimentalIntents"] = 'true'
      }
      
      this.dialog.user        = querytext
      this.dialog.dialogflow  = "[Anfrage wird bearbeitet]"
      
      this.querytext = null;
      
      this.dialogflowService.UseAgent_text(text_request).pipe(take(1)).subscribe(result=>{
        if(handling_result){
          this.handling_result(result)
        }else{
          this.handling_dialog(result)
          this.stepperView.next()
        }
      })
    }
    catch(e){
      console.log(e.name + ": " + e.message);
      this.status = "Fehler!";
    }
  }
  handling_dialog(result){
    /// get Answer from Dialogflow and show it to the User
    this.dialog.user = result.queryText
    this.dialog.dialogflow = result.fulfillment
  }
  handling_result(result){
    try{
      const parameters = result.parameters
      const allRequiredParamsPresent = result.allRequiredParamsPresent
      const intent = result.intent
      
      this.handling_dialog(result)

      /// play audio
      if(result['outputAudio'] && result['outputAudio'].data.length > 0){
        this.play_byteArray(result['outputAudio'].data)
      }

      if(allRequiredParamsPresent){
        switch(intent){
          case "select_massnahme":
            //Parameter überpüfen und fehler senden
            if(parameters.selected_massnahme == null || parameters.selected_massnahme == undefined || parameters.selected_massnahme == ""){
              console.error("Etwas stimmt nicht. Sie haben anscheinend keine Maßnahme ausgewählt. Versuchen sie es nochmal.")
              this.deleteAllContexts()
              this.massnahme_parameter = undefined
              return
            }

            /// get selected "Maßnahme"
            var massnahme_from_Context = parameters.selected_massnahme

            this.firstStepController.controls.massnahme.setValue(massnahme_from_Context)
            this.MassnahmeChange(massnahme_from_Context)

            if(massnahme_from_Context != "" && massnahme_from_Context){
              this.stepperView.next()
            }

            break;     
          case "customize_massnahme":
            break;
          case "fertig":
            this.dynamicFormComponent.last.save()
            break;
          
          default:
            const change_preset = async (selected_preset)=>{
              if(this.all_Presets.length > 0){
                for (let one_Preset of this.all_Presets) {
                  const preset_tmp = one_Preset
                  if( preset_tmp["key"] == selected_preset ){
                    this.selected_preset = preset_tmp
                    this.secondStepController.controls.preset.setValue(preset_tmp)
                  }
                }
              }
            }
            const change_input = () => {
              const forms_keys = Object.keys(this.dynamicFormComponent.last.forms[0].form.controls) 
              const parameters_keys = Object.keys(parameters) 
              
              if(parameters){
                for(let key of parameters_keys){
                  let updated_key = this.CheckAndUpdateNameForDatabase(key)
                  /// check if the parameter exist in the form
                  if(forms_keys.includes(updated_key)){
                    /// check which type is the parameter
                    /// and set the new Value for the Input
                    switch(typeof parameters[key]){
                      case "number":
                      case "string":
                        this.dynamicFormComponent.last.forms[0].form.get(updated_key).setValue(parameters[key])
                        break;
                      case "object":
                        if(parameters[key].number){
                          this.dynamicFormComponent.last.forms[0].form.get(updated_key).setValue(parseFloat(parameters[key].number))
                        }else if(parameters[key].amount){
                          this.dynamicFormComponent.last.forms[0].form.get(updated_key).setValue(parseFloat(parameters[key].amount))
                        }else if(parameters[key].von && parameters[key].bis){
                          let von = parseFloat(parameters[key].von)
                          let bis = parseFloat(parameters[key].bis)
                          if(von < bis){
                            this.dynamicFormComponent.last.forms[0].form.get(updated_key).setValue([von, bis])
                          }else{
                            this.dynamicFormComponent.last.forms[0].form.get(updated_key).setValue([bis, von])
                          }
                        }else if(Array.isArray(parameters[key])){
                          this.dynamicFormComponent.last.forms[0].form.get(updated_key).setValue(parameters[key])
                        }else if(parameters[key].value){
                          this.dynamicFormComponent.last.forms[0].form.get(updated_key).setValue(parseFloat(parameters[key].value))
                        }
                        break;
                    }

                    /// Get the Input from allInputs
                    const foundInput = this.dynamicFormComponent.last.allInputs.find( ({ key }) => key === updated_key );
                    if(foundInput){
                      /// check if it has any dependencies
                      if(foundInput.dependFor){
                        /// set the new Values for the dependencies
                        this.dynamicFormComponent.last.checkForDepend(foundInput.key)
                      }
                    }
                  }
                }
              }
            }

            let split_intent = intent.split("--")
            let intent_second_id = split_intent[1]
            
            switch(intent_second_id){
              case "PRESET":
                var selected_preset
                var selected_massnahme

                if(parameters.selected_massnahme){
                  selected_massnahme = parameters.selected_massnahme
                }else{
                  console.error("couldn't find selected Massnahme")
                  //maybe an error Hint/popup
                }

                if(parameters["Preset_" + selected_massnahme]){
                  selected_preset = parameters["Preset_" + selected_massnahme]
                }else{
                  console.error("couldn't find selected Preset")
                  //maybe an error Hint/popup
                }

                change_preset(selected_preset).then(()=>{
                  if(this.selected_preset != "" && this.selected_preset){
                    this.stepperView.next()
                  }
                  /// setTimeout is needed because "forms" from the child Component is to slow
                  setTimeout(()=>{
                    change_input()
                  }, 500)
                })
                break
              case "M-P-C":
                /// get selected Massnahme and Preset
                var selected_preset
                var selected_massnahme
                if(parameters.selected_massnahme){
                  selected_massnahme = parameters.selected_massnahme
                }else{
                  console.error("couldn't find selected Massnahme")
                  //maybe an error Hint/popup
                }

                if(parameters["Preset_" + selected_massnahme]){
                  selected_preset = parameters["Preset_" + selected_massnahme]
                }else{
                  console.error("couldn't find selected Preset")
                  //maybe an error Hint/popup
                }

                /// select the selected Massnahme
                this.firstStepController.controls.massnahme.setValue(selected_massnahme)
                this.selected_Massnahme = selected_massnahme
                this.MassnahmeChange(selected_massnahme).then(()=>{
                  /// setTimeout is needed because of *ngIf="!skipPresetStepper" in the Stepper
                  /// It needs some time befor it can move to the third Stepper
                  setTimeout(()=>{
                    this.stepperView.next()
                    /// select in HTML the selected Preset
                    change_preset(selected_preset).then(()=>{
                      setTimeout(()=>{
                        this.stepperView.next()
                        /// setTimeout is needed because "forms" from the child Component is to slow
                        setTimeout(()=>{
                          change_input()
                        }, 500)
                      }, 300)
                    })
                  }, 300)
                });
                break;
              case "S":
                change_input()
                /// get in which sector the Input Field is 
                let intent_third_id = split_intent[2]
                /// move to the Sector
                this.dynamicFormComponent.last.step_sector = parseInt(intent_third_id)
                break;
              case "M":
                /// get selected Massnahme
                var selected_massnahme
                if(parameters.selected_massnahme){
                  selected_massnahme = parameters.selected_massnahme
                }else{
                  console.error("couldn't find selected Massnahme")
                  //maybe an error Hint/popup
                }

                /// select the selected Massnahme
                this.firstStepController.controls.massnahme.setValue(selected_massnahme)
                this.selected_Massnahme = selected_massnahme
                
                this.MassnahmeChange(selected_massnahme).then(()=>{
                  /// setTimeout is needed because of *ngIf="!skipPresetStepper" in the Stepper
                  /// It needs some time befor it can move to the third Stepper
                  setTimeout(()=>{
                    /// setTimeout is needed because "forms" from the child Component is to slow
                    setTimeout(()=>{
                      let step = this.stepperView._steps.last._stepper._steps.length
                      if(step == 3){
                        this.stepperView.next()
                        this.stepperView.next()
                      }else if(step == 2){
                        this.stepperView.next()
                      }
                      change_input()
                    }, 500)
                  }, 300)
                });
                break;
            }
            break;
        }
      }
    }catch(e){
      console.log(e);
      console.log(e.name + ": " + e.message);
      this.status = "Fehler!";
      this.dialog.user       = "[Error: Ein unerwarteter Fehler ist aufgetreten]"
      this.dialog.dialogflow = "[Error: Ein unerwarteter Fehler ist aufgetreten]"
    }
  }

  // etc
  CheckAndUpdateNameForDatabase(name){
    name = name.replace(/ /g, "_") //' '
    name = name.replace(/\//g, "_") //'/'
    name = name.replace(/-/g, "_") //'-'
    name = name.replace(/\(/g, "") //(
    name = name.replace(/\)/g, "") //)

    name = name.replace(/\%/g, "") //%
    name = name.replace(/\°/g, "0") //°
    name = name.replace(/\²/g, "2") //²
    name = name.replace(/\³/g, "3") //³

    return name.toLowerCase()
  }
  
}

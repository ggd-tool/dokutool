import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';

import { saveAs } from 'file-saver';

import { ImportService } from 'src/app/services/import/import.service';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.sass']
})
export class ImportComponent implements OnInit {

  constructor(private ImportService: ImportService) { }

  loaded: boolean = true;
  fileToUpload: File | null = null;

  success: Array<Object>
  errors: Array<Object>

  ngOnInit(): void {
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  importExcel() {
    if(this.fileToUpload == undefined || this.fileToUpload == null){
      alert("Sie haben keine Datei ausgewählt")
      return
    }
    this.ImportService.ImportExcel(this.fileToUpload).pipe(take(1)).subscribe(data => {
      // do something, if upload success
      this.success = data.success

      let tmpErrorsArray = []
      for(let key in data.errors){
        tmpErrorsArray.push({
          name : key,
          data: data.errors[key]
        })
      }
      this.errors = tmpErrorsArray
    }, error => {
      console.log(error);
    });
  }

  getTemplate(){
    this.ImportService.getExcelTemplate().pipe(take(1)).subscribe(data => {
      // retrieve the original buffer of data
      //const buff = Buffer.from(data['blob'], "base64");
      //const blob = new Blob([buff], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });

      // Buffer needs to be installed. This Project uses an older Version of Angular and Typescript which dont has Buffer.
      // Buffer throws an "Cannot find name 'BigInt'" Error inside the Dependency of Buffer. (Restart sometimes ng Serve and it works again)
      // The code below replaces the Job of Buffer.from()
      const byteCharacters = atob(data['blob']);
      const byteArrays = [];

      for (let offset = 0; offset < byteCharacters.length; offset += 512) {
        const slice = byteCharacters.slice(offset, offset + 512);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      const blob = new Blob(byteArrays, { type: 'application/octet-stream' });
      saveAs(blob, "Spreadsheet_Template.xlsx")
    }, error => {
      console.log("error");
      console.log(error);
    });
  }
}

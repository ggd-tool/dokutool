import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerwaltenComponent } from './verwalten.component';

describe('VerwaltenComponent', () => {
  let component: VerwaltenComponent;
  let fixture: ComponentFixture<VerwaltenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerwaltenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerwaltenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-verwalten',
  templateUrl: './verwalten.component.html',
  styleUrls: ['./verwalten.component.sass']
})
export class VerwaltenComponent implements OnInit {

  action: string;
  object: string;
  title: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    // Abfrage um Überschrift korrekt anzuzeigen
    this.route.params.subscribe(params => {
      this.action = params.action;
      this.object = params.typ;
    });
    this.route.url.subscribe((url) => {
      switch (url[1].path) {
        case "typen":
          this.title = "Objekt";
          break;
        case "presets":
          this.title = "Preset";
          break;
        case "maßnahmen":
          this.title = "Maßnahme";
          break;
      }
    });
  }

}

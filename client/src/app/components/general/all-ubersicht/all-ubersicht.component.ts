import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MassnahmenService } from 'src/app/services/massnahmen/massnahmen.service';
import { PresetsService } from 'src/app/services/presets/presets.service';
import { TypenService } from 'src/app/services/typen/typen.service';
import { UbersichtService } from 'src/app/services/ubersicht/ubersicht.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { KategoriePopupComponent } from '../../kategorie-popup/kategorie-popup.component';
import { FormControl } from '@angular/forms';
import { ReplaySubject } from 'rxjs';
import { startWith } from 'rxjs/operators';

@Component({
  selector: 'app-all-ubersicht',
  templateUrl: './all-ubersicht.component.html',
  styleUrls: ['./all-ubersicht.component.scss']
})
export class AllUbersichtComponent implements OnInit {
  @Input() favoritesOnly: boolean = false;
  @Input() type: string;
  loaded = false;
  allObjects: any = [];
  objectsToShow: any = [];
  service;
  title: string;
  newLinkParams: [string, string] = ["", ""];
  favoritesValue: string;
  allKategorien: any = [];
  filterValue: string = "";
  valueToShow: string;
  favorites: any = [];

  public kategorieControl: FormControl = new FormControl();
  public kategorieFilterControl: FormControl = new FormControl();
  public filteredKategorien: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  mappedKategorien;

  constructor(private dialog: MatDialog, private typenService: TypenService, private ubersichtsService: UbersichtService, private route: ActivatedRoute, private presetsService: PresetsService, private maßnahmenService: MassnahmenService) { }

  ngOnInit(): void {
    this.route.url.subscribe((url) => {
      let valueToSwitch;
      if (this.type) {
        valueToSwitch = this.type;
      } else {
        valueToSwitch = url[1].path;
      }
      switch (valueToSwitch) {
        case "typen":
          this.service = this.typenService;
          this.title = "Objekte";
          this.newLinkParams[0] = "typen";
          this.newLinkParams[1] = "erstellen";
          this.favoritesValue = "object";
          break;
        case "presets":
          this.service = this.presetsService;
          this.title = "Presets";
          this.newLinkParams[0] = "presets";
          this.newLinkParams[1] = "anlegen";
          this.favoritesValue = "preset";
          break;
        case "maßnahmen":
          this.service = this.maßnahmenService;
          this.title = "Maßnahmen";
          this.newLinkParams[0] = "maßnahmen";
          this.newLinkParams[1] = "ablegen";
          this.favoritesValue = "massnahme";
          break;
      }
    });
    // Lade alle Dialoge vom Server und zeige diese an
    this.service.getAllObjects().subscribe(data => {
      //alphabetisch sortieren 
      (data as any).sort((a, b) => a.name.localeCompare(b.name));
      this.allObjects = data;
      this.objectsToShow = data;
      //Kategorien laden
      this.ubersichtsService.getKategorien().subscribe(data => {
        this.allKategorien = data;
        this.kategorieFilterControl.valueChanges.pipe(startWith('')).subscribe(() => {
          this._filterKategorien();
        });
      })
      // Favoriten des Nutzers laden und diese zu den Status zu den jeweiligen Maßnahmen hinzufügen
      this.ubersichtsService.getAllFavs(this.favoritesValue).subscribe(favs => {
        this.favorites = favs;
        this.objectsToShow.forEach(element => {
          if (this.favorites.includes(element._id)) {
            element.favorite = true;
          } else {
            element.favorite = false;
          }
        });
        this.loaded = true;
      });
    });
  }

  // Dialog um Kategorien zu verwalten
  openKategorien() {
    // Dialog konfigurieren
    const dialogKategorieConfig = new MatDialogConfig();
    dialogKategorieConfig.autoFocus = false;
    dialogKategorieConfig.width = "80%";
    // Dialog öffnen -> Siehe Dialog in KategoriePopupComponent
    const dialogRef = this.dialog.open(KategoriePopupComponent, dialogKategorieConfig);
    let updatedKategorien: boolean;
    dialogRef.componentInstance.updatedKategorien.subscribe(data => {
      updatedKategorien = data;
    });
    dialogRef.afterClosed().subscribe(() => {
      if (updatedKategorien) {
        this.ubersichtsService.getKategorien().subscribe(data => {
          this.allKategorien = data;
          this._filterKategorien();
        });
      }
    })
  }

  //Kategorien filtern
  _filterKategorien() {
    let filterValue = this.kategorieFilterControl.value;
    if (!filterValue) {
      this.filteredKategorien.next(this.allKategorien.slice());
      return;
    } else {
      filterValue = filterValue.split(" ").join("").toLowerCase();
    }
    this.filteredKategorien.next(
      this.allKategorien.filter(kategorie => kategorie.name.split(" ").join("").toLowerCase().indexOf(filterValue) >= 0)
    );
  }

  //geladene Items nach Namen Filtern
  applyFilter() {
    this.loaded = false;
    this.valueToShow = this.filterValue;
    let filter = this.filterValue.split(" ").join("").toLowerCase();
    let kategorienFilter = this.kategorieControl.value;
    this.objectsToShow = this.allObjects.filter(obj => {
      if (filter === "" && kategorienFilter !== null) {
        if (kategorienFilter.length > 0) {
          for (let i = 0; i < kategorienFilter.length; i++) {
            if (obj.kategorien.includes(kategorienFilter[i])) {
              return obj;
            };
          };
        };
      } else {
        let show = false;
        if (obj.name.split(" ").join("").toLowerCase().indexOf(filter) >= 0) {
          show = true;
        }
        if (kategorienFilter !== null) {
          kategorienFilter.forEach(kategorie => {
            if (obj.kategorien.includes(kategorie)) {
              show = true;
            };
          });
        }
        if (show) {
          return obj;
        }
      }
    });
    this.loaded = true;
  }

  //filter zurücksetzen
  resetFilter() {
    this.loaded = false;
    this.filterValue = "";
    this.kategorieControl.setValue(null);
    this.applyFilter();
  }

  mapKategorien() {
    this.mappedKategorien = this.kategorieControl.value.map(kategorie => this.allKategorien.find(kat => kat._id === kategorie));
  }

  //Wenn Kategorie in KategorieDD aktualisiert wurde muss es auch in der Übersicht aktualisiert werden, damit richtig gefiltered werden kann
  updateItem(item: any) {
    this.allObjects.find((obj, index) => {
      if (obj._id === item._id) {
        return this.allObjects[index] = item;
      }
    })
  }

}

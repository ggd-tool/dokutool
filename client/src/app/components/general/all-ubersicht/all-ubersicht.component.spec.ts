import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllUbersichtComponent } from './all-ubersicht.component';

describe('AllUbersichtComponent', () => {
  let component: AllUbersichtComponent;
  let fixture: ComponentFixture<AllUbersichtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllUbersichtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllUbersichtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

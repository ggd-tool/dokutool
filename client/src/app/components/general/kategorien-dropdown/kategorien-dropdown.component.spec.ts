import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KategorienDropdownComponent } from './kategorien-dropdown.component';

describe('KategorienDropdownComponent', () => {
  let component: KategorienDropdownComponent;
  let fixture: ComponentFixture<KategorienDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KategorienDropdownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KategorienDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ReplaySubject } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { UbersichtService } from 'src/app/services/ubersicht/ubersicht.service';

@Component({
  selector: 'app-kategorien-dropdown',
  templateUrl: './kategorien-dropdown.component.html',
  styleUrls: ['./kategorien-dropdown.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class KategorienDropdownComponent implements OnInit, OnChanges {

  constructor(private ubersichtsService: UbersichtService) { }

  @Input() kategorien;
  @Input() allKategorien;
  @Input() typ;
  @Input() id;
  @Output() changedKategorien: EventEmitter<any> = new EventEmitter<any>();

  mappedKategorien;
  changedValues;

  public kategorieControl: FormControl = new FormControl();
  public kategorieFilterControl: FormControl = new FormControl();
  public filteredKategorien: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  ngOnInit(): void {
    this.kategorieFilterControl.valueChanges.pipe(startWith('')).subscribe(() => {
      this._filterKategorien();
    });
    //Kategorien eines Feldes zuweisen
    this.kategorieControl.setValue(this.kategorien);
    this.mappedKategorien = this.kategorien.map(kategorie => this.allKategorien.find(kat => kat._id === kategorie));
  }

  ngOnChanges(changes: SimpleChanges) {
    this.allKategorien = changes.allKategorien.currentValue;
    //Save für wenn eine Kategorie gelöscht wurde
    for (let j = 0; j < this.kategorien.length; j++) {
      let remover = true;
      for (let i = 0; i < this.allKategorien.length; i++) {
        if (this.kategorien[j] === this.allKategorien[i]._id) {
          remover = false
        }
      }
      if (remover) {
        this.kategorien.splice(j, 1);
        j--;
      }
    }
    //Kategorien neu zuweisen
    this.kategorieControl.setValue(this.kategorien);
    this.mappedKategorien = this.kategorien.map(kategorie => this.allKategorien.find(kat => kat._id === kategorie));

    this._filterKategorien();
  }

  //Kategorien filtern
  _filterKategorien() {
    if (this.allKategorien) {
      let filterValue = this.kategorieFilterControl.value;
      if (!filterValue) {
        this.filteredKategorien.next(this.allKategorien.slice());
        return;
      } else {
        filterValue = filterValue.split(" ").join("").toLowerCase();
      }
      this.filteredKategorien.next(
        this.allKategorien.filter(kategorie => kategorie.name.split(" ").join("").toLowerCase().indexOf(filterValue) >= 0)
      );
    }
  }

  // Eine Kategorie hinzufügen
  toggleKategorie(): void {
    let kategorien = this.kategorieControl.value;
    this.ubersichtsService.updateKategorien(this.id, this.typ, kategorien).subscribe(data => {
      //Kategorien neu Mappen
      this.kategorien = (data as any).kategorien;
      this.mappedKategorien = this.kategorien.map(kategorie => this.allKategorien.find(kat => kat._id === kategorie));
      this.changedValues = data;
    })
  }

  //Wenn Componente geschlossen wird veränderte Values übergeben
  emitChanges() {
    if (this.changedValues) {
      this.changedKategorien.emit(this.changedValues);
    }
  }

}

import { Component, ComponentFactoryResolver, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { MassnahmenService } from 'src/app/services/massnahmen/massnahmen.service';
import { PresetsService } from 'src/app/services/presets/presets.service';
import { TypenService } from 'src/app/services/typen/typen.service';
import { UbersichtService } from 'src/app/services/ubersicht/ubersicht.service';

@Component({
  selector: 'app-einzel-ubersicht',
  templateUrl: './einzel-ubersicht.component.html',
  styleUrls: ['./einzel-ubersicht.component.scss']
})
export class EinzelUbersichtComponent implements OnInit {

  constructor(private ubersichtService: UbersichtService, private typenService: TypenService, private presetsService: PresetsService, private massnahmenService: MassnahmenService, private route: ActivatedRoute, private router: Router) { }
  loaded: boolean = false;
  service: any;
  dialogName: string;
  newLink: string;
  newLinkEnding: string;
  typ: string;
  allInputs = [];
  filterValue: string = "";
  filterValueToShow: string;

  //paginatroOptions
  pageSize: number = 20;
  pageIndex: number = 1;
  totalItems: number;

  objects: any = [];
  displayedColums: { key: string, label: string, type: any }[] = [];
  displayedColumsKeys: string[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  async ngOnInit() {
    // Lade alle Dialoge vom Server und zeige diese an
    await this.route.params.subscribe(async (params) => {
      this.typ = params.typ;
      this.dialogName = params.object;
      this.newLink = "/tool/";
      switch (params.typ) {
        case "typen":
          this.service = this.typenService;
          this.newLink += "typen/verwalten/" + this.dialogName;
          this.newLinkEnding = "/erstellen";
          break;
        case "presets":
          this.service = this.presetsService;
          this.newLink += "presets/verwalten/" + this.dialogName;
          this.newLinkEnding = "/anlegen";
          break;
        case "maßnahmen":
          this.service = this.massnahmenService;
          this.newLink += "maßnahmen/verwalten/" + this.dialogName;
          this.newLinkEnding = "/ablegen";
          break;
      }
      await this.ubersichtService.getObjects(this.dialogName, this.typ, this.pageSize, this.pageIndex).subscribe((data: { items: [], total: number }) => {
        this.formatData(data, true);
        setTimeout(() => {
          this.objects.paginator = this.paginator;
        });
      });
    });
  }

  formatData(data, initial) {
    this.allInputs = [];
    this.totalItems = data.total;
    let bestdata = this.makeDataGreatAgain(data.items);
    this.service.getForm(this.dialogName).subscribe(form => {
      // Variablen richtig formatieren
      form['data'].forEach(section => {
        section.inputs.forEach(input => {
          this.allInputs.push(input);
        });
      });
      let formatObjects = this.ubersichtService.formatForTable(bestdata, this.allInputs);
      form['data'].forEach(section => {
        section['inputs'].forEach(input => {
          // Key wie in db formatieren
          input.key = this.ubersichtService.formatForDatabase(input.key);
          if (initial) {
            this.displayedColumsKeys.push(input.key);
            this.displayedColums.push(input);
          }
          // Optionkeys zu Values
          if (input.type === 'dropdown' && 'options' in input) {
            formatObjects.forEach(formatObject => {
              input.options.forEach(option => {
                if (option.key === formatObject[input.key]) {
                  formatObject[input.key] = option;
                }
              });
              // Wenn im Dropdown nicht ausgewählt worden ist
              if (formatObject[input.key] === undefined || formatObject[input.key] === null) {
                formatObject[input.key] = '';
              }
            });
          } else if (input.type === 'datepicker') {
            // datepicker values richtig formatieren
            formatObjects.forEach(formatObject => {
              formatObject[input.key] = {
                "type": "datepicker",
                "value": formatObject[input.key]
              }
            });
          } else if (input.type === 'multidropdown') {
            formatObjects.forEach(formatObject => {
              if (input.options !== 'ref') {
                input.options.forEach(option => {
                  formatObject[input.key].forEach((key, i) => {
                    if (key === option.key) {
                      formatObject[input.key][i] = option;
                    }
                  });
                });
              }
              // Wenn im Dropdown nicht ausgewählt worden ist
              if (formatObject[input.key] === undefined || formatObject[input.key] === null) {
                formatObject[input.key] = '';
              }
            });
          }
        });
      });
      if (initial) {
        // Kommentar und Nutzer entfernen
        if (this.typ === 'maßnahmen' || this.typ === 'presets') {
          this.displayedColumsKeys = this.displayedColumsKeys.slice(0, -2);
        }
        // Action Buttons hinzufügen
        this.displayedColumsKeys.push("actions");
        this.displayedColums.push({
          'key': 'actions',
          'label': 'Aktionen',
          'type': "buttons"
        });
      }
      this.objects = new MatTableDataSource(formatObjects);
      this.loaded = true;
    });
  }

  // Ein Datum richtig formatieren
  showDate(dateString) {
    return this.ubersichtService.showDate(dateString);
  }

  makeDataGreatAgain(data): Object {
    let out = [];
    for (let i = 0; i < data.length; i++) {
      out.push(JSON.parse(JSON.stringify(data[i])));
    }
    return out;
  }

  async applyFilter() {
    this.loaded = false;
    this.filterValueToShow = this.filterValue;
    if (this.filterValue === "") {
      await this.ubersichtService.getObjects(this.dialogName, this.typ, this.pageSize, this.pageIndex).subscribe((data: { items: [], total: number }) => {
        //data nach fetch neu formatieren und zuweisen
        this.formatData(data, false);
      })
    } else {
      this.ubersichtService.getAllItems(this.dialogName, this.typ).subscribe(items => {
        let bestdata = this.makeDataGreatAgain(items);
        this.service.getForm(this.dialogName).subscribe(form => {
          let formatObjects = this.ubersichtService.formatForTable(bestdata, this.allInputs);
          form['data'].forEach(section => {
            section['inputs'].forEach(input => {
              // Key wie in db formatieren
              input.key = this.ubersichtService.formatForDatabase(input.key);
              // Optionkeys zu Values
              if (input.type === 'dropdown' && 'options' in input) {
                formatObjects.forEach(formatObject => {
                  input.options.forEach(option => {
                    if (option.key === formatObject[input.key]) {
                      formatObject[input.key] = option;
                    }
                  });
                  // Wenn im Dropdown nicht ausgewählt worden ist
                  if (formatObject[input.key] === undefined) {
                    formatObject[input.key] = '';
                  }
                });
              } else if (input.type === 'datepicker') {
                // datepicker values richtig formatieren
                formatObjects.forEach(formatObject => {
                  formatObject[input.key] = {
                    "type": "datepicker",
                    "value": formatObject[input.key]
                  }
                });
              }
            });
          });
          let filters = this.filterValue.split(",");
          formatObjects = (formatObjects as any).filter(data => {
            for (let j = 0; j < filters.length; j++) {
              let filter = filters[j].split(" ").join("").toLowerCase();
              for (let property in data) {
                let item = data[property];
                if (item) {
                  if (typeof item === "object") {
                    if (item[0] !== undefined) {
                      for (let i = 0; i < item.length; i++) {
                        let value = item[i]
                        //ausnahme für Slider
                        if (value.value) {
                          if (value.value.split(" ").join("").toLowerCase().indexOf(filter) >= 0) {
                            return data;
                          }
                        } else {
                          if (value.toString().trim().toLowerCase().indexOf(filter) >= 0) {
                            return data;
                          }
                        }
                      };
                    } else {
                      if (item.type === 'datepicker') {
                        let formatDate = this.showDate(item.value);
                        if (formatDate.indexOf(filter) >= 0) {
                          return data;
                        }
                      } else {
                        if (item.value.trim().toLowerCase().indexOf(filter) >= 0) {
                          return data;
                        }
                      }
                    }
                  } else {
                    if (property !== "_id" && property !== "createdAt" && property !== "updatedAt" && property !== "__v") {
                      if (JSON.stringify(item).split(" ").join("").toLowerCase().indexOf(filter) >= 0) {
                        return data;
                      }
                    }
                  }
                }
              }
            }
          })
          //paginator anpassen
          let itemsToShow = [];
          let limiter = this.pageSize * this.pageIndex;
          if (formatObjects.length < limiter) {
            limiter = formatObjects.length;
          }
          for (let i = this.pageSize * (this.pageIndex - 1); i < limiter; i++) {
            itemsToShow.push(formatObjects[i]);
          }
          this.objects = new MatTableDataSource(itemsToShow);
          this.totalItems = formatObjects.length;
          this.loaded = true;
        })
      });
    }
  }

  resetFilter() {
    this.loaded = false;
    this.filterValue = "";
    this.applyFilter();
  }

  isStickyBegin(colum: string) {
    var stickyColums = ["name", "datum"];
    return stickyColums.includes(colum);
  }

  isStickyEnd(colum: string) {
    var stickyColums = ["actions"];
    return stickyColums.includes(colum);
  }

  onPaginate(pageEvent: PageEvent) {
    this.loaded = false;
    this.pageSize = pageEvent.pageSize;
    this.pageIndex = pageEvent.pageIndex + 1;
    this.ubersichtService.getObjects(this.dialogName, this.typ, this.pageSize, this.pageIndex).subscribe((data: { items: [], total: number }) => {
      //data nach fetch neu formatieren und zuweisen
      this.formatData(data, false);
    });
  }

  backUbersicht() {
    switch (this.typ) {
      case "typen":
        this.router.navigate(['/tool/typen/übersicht']);
        break;
      case "maßnahmen":
        this.router.navigate(['/tool/maßnahmen/übersicht']);
        break;
      case "presets":
        this.router.navigate(['/tool/presets/übersicht']);
        break;
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EinzelUbersichtComponent } from './einzel-ubersicht.component';

describe('EinzelUbersichtComponent', () => {
  let component: EinzelUbersichtComponent;
  let fixture: ComponentFixture<EinzelUbersichtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EinzelUbersichtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EinzelUbersichtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

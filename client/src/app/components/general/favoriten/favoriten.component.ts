import { Component, Input, OnInit } from '@angular/core';
import { UbersichtService } from 'src/app/services/ubersicht/ubersicht.service';

@Component({
  selector: 'app-favoriten',
  templateUrl: './favoriten.component.html',
  styleUrls: ['./favoriten.component.sass']
})
export class FavoritenComponent implements OnInit {
  @Input() isFavOnInit: boolean;
  @Input() favType: string;
  @Input() item: any;

  currentState = false;
  itemId = "";

  constructor(private ubersichtsService: UbersichtService) { }

  ngOnInit(): void {
    (this.isFavOnInit) ? this.currentState = true : this.currentState = false;
    this.itemId = this.item._id;
  }

  clicked() {
    this.currentState = !this.currentState;
    this.ubersichtsService.updateDialogFav(this.favType, this.itemId, this.currentState).subscribe();
  }

}

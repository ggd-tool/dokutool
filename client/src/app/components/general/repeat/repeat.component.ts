import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MassnahmenService } from 'src/app/services/massnahmen/massnahmen.service';
import { UbersichtService } from 'src/app/services/ubersicht/ubersicht.service';

@Component({
  selector: 'app-repeat',
  templateUrl: './repeat.component.html',
  styleUrls: ['./repeat.component.scss']
})
export class RepeatComponent implements OnInit {
  loaded = false;
  prevMassnahmen;
  // Alle Informationen zu einer Spalte auch type
  tableCols: any = [];
  // lediglich die keys der spalten
  tableColsKeys = [];

  @ViewChild(MatSort) sort: MatSort;

  constructor(private massnahmenService: MassnahmenService, private dialogRef: MatDialogRef<RepeatComponent>, @Inject(MAT_DIALOG_DATA) private data, private ubersichtService: UbersichtService) { }

  async ngOnInit() {
    // Die letzten vorangegangen Maßnahmen laden
    let formatedName = await this.fixedEncodeURIComponent(await this.ubersichtService.formatForFiles(this.data.massnahme).toLowerCase());
    this.massnahmenService.getAllPrevMassnahmen(formatedName).subscribe(massnahmen => {
      // Das Form laden
      this.massnahmenService.getForm(this.data.massnahme).subscribe(form => {
        // Variablen richtig formatieren
        let allInputs = [];
        form['data'].forEach(section => {
          section.inputs.forEach(input => {
            allInputs.push(input);
          });
        });
        let formatMassnahmen = this.ubersichtService.formatForTable(massnahmen, allInputs);
        // Erstellungsspalte hinzufügen
        this.tableColsKeys.push("createdAt");
        this.tableCols.push({
          'key': 'createdAt',
          'label': 'Erstellt',
          'type': 'create'
        });
        // Alle Inputs durchgehen -> welche formatierung wird für welche value benötigt
        form['data'].forEach(section => {
          section['inputs'].forEach(input => {
            // Key wie in db formatieren
            input.key = this.ubersichtService.formatForDatabase(input.key);
            // Informationen der Spalten abspeichern
            this.tableColsKeys.push(input.key);
            this.tableCols.push(input);
            // Optionkeys zu Values
            if (input.type === 'dropdown' && 'options' in input) {
              formatMassnahmen.forEach(formatMassnahme => {
                input.options.forEach(option => {
                  if (option.key === formatMassnahme[input.key]) {
                    formatMassnahme[input.key] = option;
                  }
                });
              });
            } else if (input.type === 'datepicker') {
              // datepicker values richtig formatieren
              formatMassnahmen.forEach(formatMassnahme => {
                formatMassnahme[input.key] = {
                  "type": "datepicker",
                  "value": formatMassnahme[input.key]
                }
              });
            }
          });
        });
        // Kommentar und Nutzer entfernen

        this.tableColsKeys = this.tableColsKeys.slice(0, -2);

        // Den übernehmen Button hinzufügen
        this.tableColsKeys.push("actions");
        this.tableCols.push({
          'key': 'actions',
          'label': 'Übernehmen',
          'type': 'buttons'
        });

        // die formatierten Daten konvertieren für die Tabelle
        this.prevMassnahmen = new MatTableDataSource(formatMassnahmen);
        setTimeout(() => {
          this.prevMassnahmen.sort = this.sort;
        });
        // Loadingspinner deaktivieren
        this.loaded = true;
      });
    });
  }

  // Ein Datum richtig formatieren
  showDate(dateString) {
    return this.ubersichtService.showDate(dateString);
  }

  fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
      return '%' + c.charCodeAt(0).toString(16);
    });
  }
}

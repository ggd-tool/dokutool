import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './app.authguard';
import { DashboardComponent } from './components/dashboard/dashboard.component';

//General
import { EinzelUbersichtComponent } from './components/general/einzel-ubersicht/einzel-ubersicht.component';
import { AllUbersichtComponent } from './components/general/all-ubersicht/all-ubersicht.component';
import { VerwaltenComponent } from './components/general/verwalten/verwalten.component';

//Dialogflow
import { DialogflowComponent } from './components/dialogflow/dialogflow.component';

//Excel Import
import { ImportComponent } from './components/import/import.component';

//Bedinungsanleitung 
import { BedienungsanleitungComponent } from './components/bedienungsanleitung/bedienungsanleitung.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/tool/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'tool/dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    data: { roles: ['visitor'] }
  },
  {
    path: 'tool/maßnahmen/übersicht',
    component: AllUbersichtComponent,
    canActivate: [AuthGuard],
    data: { roles: ['visitor'] }
  },
  {
    path: 'tool/maßnahmen/verwalten/:typ/:action',
    component: VerwaltenComponent,
    canActivate: [AuthGuard],
    data: { roles: ['worker'], routeName: "MaßnahmeVerwalten" }
  },
  {
    path: 'tool/typen/übersicht',
    component: AllUbersichtComponent,
    canActivate: [AuthGuard],
    data: { roles: ['visitor'] }
  },
  {
    path: 'tool/typen/verwalten/:typ/:action',
    component: VerwaltenComponent,
    canActivate: [AuthGuard],
    data: { roles: ['worker'], routeName: "TypVerwalten" }
  },
  {
    path: 'tool/presets/übersicht',
    component: AllUbersichtComponent,
    canActivate: [AuthGuard],
    data: { roles: ['visitor'] }
  },
  {
    path: 'tool/presets/verwalten/:typ/:action',
    component: VerwaltenComponent,
    canActivate: [AuthGuard],
    data: { roles: ['worker'], routeName: "PresetVerwalten" }
  },
  {
    path: 'tool/ubersicht/:typ/:object',
    component: EinzelUbersichtComponent,
    canActivate: [AuthGuard],
    data: { roles: ['worker'] }
  },
  {
    path: 'tool/import',
    component: ImportComponent,
    canActivate: [AuthGuard],
    data: { roles: ['worker'] }
  },
  {
    path: 'tool/spracheingabe',
    component: DialogflowComponent,
    canActivate: [AuthGuard],
    data: { roles: ['worker'], routeName: "MaßnahmeVerwalten" }
  },
  {
    path: 'bedienungsanleitung',
    component: BedienungsanleitungComponent,
    canActivate: [AuthGuard],
    data: { roles: ['visitor'] }
  },
  {
    path: '**',
    redirectTo: '/tool/dashboard',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

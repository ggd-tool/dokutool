export const links = [
    {
        "link": "/tool/dashboard",
        "label": "Dashboard"
    },
    {
        "link": "/tool/presets/übersicht",
        "label": "PLONK_SIDEBAR_PRESETS"
    },
    {
        "link": "/tool/maßnahmen/übersicht",
        "label": "PLONK_SIDEBAR_ACTIONS"
    },
    {
        "link": "/tool/typen/übersicht",
        "label": "PLONK_SIDEBAR_OBJECTS"
    },
    {
        "link": "/tool/import",
        "label": "Import"
    },
    {
        "link": "/tool/spracheingabe",
        "label": "Spracheingabe"
    }
];
export const bottom_links = [
    {
        "link": "/bedienungsanleitung",
        "label": "Bedienungsanleitung"
    }
]
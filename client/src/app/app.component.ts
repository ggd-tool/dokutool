import { Component } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { environment } from '../environments/environment';
import {TranslateService} from '@ngx-translate/core';
import { links, bottom_links } from './links';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  opened: false;
  title = 'client';
  // Lädt die User Profil URL aus der environment-Datei
  userProfileURL = environment.userProfileURL;

  sidebarLinks = links;
  last_sidebarLink = bottom_links;

  constructor(private keycloakService: KeycloakService,public translate: TranslateService) {
    translate.setDefaultLang('de');
    translate.use(localStorage.getItem('PLONK_lang'));
  }

  // Keycloak Logout Funktion
  async doLogout() {
    await this.keycloakService.logout();
  }

  // Sidebar Sachen
  showLink(link) {
    console.log(link);
  }

  lang(lang) {
    localStorage.setItem('PLONK_lang', lang);
    this.translate.use(lang);
  }
}

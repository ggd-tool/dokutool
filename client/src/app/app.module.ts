import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { NgxFlagsModule } from 'ngx-flags';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
// Extra Material Angular Componenten (Datepicker, Colorpicker und Stepper)
import { NgxMatMomentModule, NgxMatMomentAdapter, NGX_MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular-material-components/moment-adapter';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NGX_MAT_DATE_FORMATS, NgxMatDateAdapter, NgxMatDateFormats } from '@angular-material-components/datetime-picker';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MAT_COLOR_FORMATS, NgxMatColorPickerModule, NGX_MAT_COLOR_FORMATS } from '@angular-material-components/color-picker';
import { MatStepperModule } from '@angular/material/stepper';

//Directives
import { GridColsDirective } from './directives/grid-cols/grid-cols.directive';

//Dashboard
import { DashboardComponent } from './components/dashboard/dashboard.component';

//Forms
import { InputArrayComponent } from './components/forms/input-array/input-array.component';
import { DynamicInputComponent } from './components/forms/dynamic-input/dynamic-input.component';
import { DynamicFormComponent } from './components/forms/dynamic-form/dynamic-form.component';

//General
import { EinzelUbersichtComponent } from './components/general/einzel-ubersicht/einzel-ubersicht.component';
import { FavoritenComponent } from './components/general/favoriten/favoriten.component';
import { AllUbersichtComponent } from './components/general/all-ubersicht/all-ubersicht.component';
import { VerwaltenComponent } from './components/general/verwalten/verwalten.component';
import { RepeatComponent } from './components/general/repeat/repeat.component';


//Paginator
import { getGermanPaginatorIntl } from './german-paginator-intl';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { KategoriePopupComponent } from './components/kategorie-popup/kategorie-popup.component';
import { KategorienDropdownComponent } from './components/general/kategorien-dropdown/kategorien-dropdown.component';

import { environment } from '../environments/environment';
import { ExportPopupComponent } from './components/export-popup/export-popup.component';

//Dialogflow
import { DialogflowComponent } from './components/dialogflow/dialogflow.component';
import { BedienungsanleitungComponent } from './components/bedienungsanleitung/bedienungsanleitung.component';
import { ImportComponent } from './components/import/import.component';

/**
 * Diese Funktion initialisiert Keycloak
 * Es müssen die jeweiligen Parameter zur Verbindung mit dem Keycloak Server angegeben werden.
 * Alle Dateien im Assets Ordner werden nicht von Keycloak geschützt.
 * Hier wird auch die 'silent-check-sso.html' eingebunden.
 * Mehr Infos: https://www.npmjs.com/package/keycloak-angular
 * @param keycloak 
 */
function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: environment.keycloakURL,
        realm: environment.keycloakRealm,
        clientId: environment.keycloakClient,
      },
      bearerExcludedUrls: ['/assets'],
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri:
          window.location.origin + '/assets/silent-check-sso.html',
      },
    });
}

// DE Zeitanzeige
const CUSTOM_MOMENT_FORMATS: NgxMatDateFormats = {
  parse: {
    dateInput: "l, LT"
  },
  display: {
    dateInput: "DD.MM.YYYY HH:mm",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "D. MMMM YYYY",
    monthYearA11yLabel: "MMMM YYYY",
  }
};

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DynamicInputComponent,
    DynamicFormComponent,
    GridColsDirective,
    RepeatComponent,
    EinzelUbersichtComponent,
    InputArrayComponent,
    FavoritenComponent,
    AllUbersichtComponent,
    VerwaltenComponent,
    KategoriePopupComponent,
    KategorienDropdownComponent,
    DialogflowComponent,
    ExportPopupComponent,
    BedienungsanleitungComponent,
    ImportComponent,
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    AppRoutingModule,
    KeycloakAngularModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxSliderModule,
    NgxMatMomentModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatColorPickerModule,
    MatStepperModule,
    TranslateModule.forRoot({
      defaultLanguage: 'de',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgxFlagsModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    }, DatePipe,
    {
      provide: MatPaginatorIntl, useValue: getGermanPaginatorIntl()
    },
    { provide: MAT_COLOR_FORMATS, useValue: NGX_MAT_COLOR_FORMATS },
    { provide: NGX_MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: NGX_MAT_DATE_FORMATS, useValue: CUSTOM_MOMENT_FORMATS },
    { provide: NgxMatDateAdapter, useClass: NgxMatMomentAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
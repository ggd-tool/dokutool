import { InputBase } from './input-base';

export class RangeInput extends InputBase<string> {
  controlType = 'range';
}
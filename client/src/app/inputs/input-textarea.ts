import { InputBase } from './input-base';

export class TextareaInput extends InputBase<string> {
  controlType = 'textarea';
}
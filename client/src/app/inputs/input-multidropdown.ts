import { InputBase } from './input-base';

export class MultiDropdownInput extends InputBase<string> {
  controlType = 'multidropdown';
}
import { InputBase } from './input-base';

export class DatepickerInput extends InputBase<string> {
  controlType = 'datepicker';
}
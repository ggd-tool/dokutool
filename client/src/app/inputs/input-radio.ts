import { InputBase } from './input-base';

export class RadioInput extends InputBase<string> {
  controlType = 'radio';
}
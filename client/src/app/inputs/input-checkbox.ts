import { InputBase } from './input-base';

export class CheckboxInput extends InputBase<string> {
  controlType = 'checkbox';
}
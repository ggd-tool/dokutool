export class InputBase<T> {
  value: T;
  key: string;
  label: string;
  required: boolean;
  controlType: string;
  type: string;
  options: any[];
  hint: string;
  min: string;
  max: string;
  inputs: InputBase<T>[];
  dependsOn: string;
  dependFor: [string];
  dependLoader: [string];
  singleDepend: boolean;
  placeholder: string;
  default: string;
  loadUsers: boolean;
  highValue: string;

  constructor(options: {
    value?: T;
    key?: string;
    label?: string;
    required?: boolean;
    controlType?: string;
    type?: string;
    options?: any[];
    hint?: string;
    min?: string;
    max?: string;
    inputs?: InputBase<T>[];
    dependsOn?: string;
    dependFor?: [string];
    dependLoader?: [string];
    singleDepend?: boolean;
    placeholder?: string;
    default?: string;
    loadUser?: boolean;
    highValue?: string;
  } = {}) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.required = !!options.required;
    this.controlType = options.controlType || '';
    this.type = options.type || '';
    this.options = options.options || [];
    this.hint = options.hint;
    this.min = options.min || '0';
    this.max = options.max;
    this.inputs = options.inputs;
    this.dependsOn = options.dependsOn;
    this.dependFor = options.dependFor;
    this.dependLoader = options.dependLoader;
    this.singleDepend = options.singleDepend;
    this.placeholder = options.placeholder;
    this.default = options.default;
    this.loadUsers = options.loadUser || false;
    this.highValue = options.highValue;
  }
}

import { InputBase } from './input-base';

export class ArrayInput extends InputBase<string> {
  controlType = 'array';
}
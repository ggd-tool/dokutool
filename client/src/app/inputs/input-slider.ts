import { InputBase } from './input-base';

export class SliderInput extends InputBase<string> {
  controlType = 'slider';
}
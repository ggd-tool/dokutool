import { InputBase } from './input-base';

export class InputSection extends InputBase<string> {
  controlType = 'section';
}
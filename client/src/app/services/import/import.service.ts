import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImportService {

  node_url = environment.apiServer;

  constructor(private http: HttpClient) { }

  ImportExcel(excelToImport: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('excel', excelToImport, excelToImport.name);
    return this.http.post(this.node_url + '/api/pre/excel/import', formData);
  }

  getExcelTemplate(){
    return this.http.post(this.node_url + '/api/pre/excel/template',{
      responseType: 'blob' as 'json'
    });
  }

}

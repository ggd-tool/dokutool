import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PresetsService {

  node_url = environment.apiServer;

  constructor(private http: HttpClient) { }

  getAllFormName(presetName){
    //Lädt alle presets zu einem bestimmten Namen
    return this.http.get(this.node_url + '/api/pre/presets/preset/' + presetName);
  }

  getForm(typ) {
    // Formular/Dialog eines bestimmten Typen
    return this.http.get(this.node_url + '/api/pre/presets/' + typ);
  }

  getAllObjects() {
    // Alle Typen die es überhaupt gibt
    return this.http.get(this.node_url + '/api/pre/presets');
  }

  post(url, data) {
    // Einen neues Objekt speichern
    return this.http.post(this.node_url + "/api" + url, data);
  }

  updateSpecificPreset(dialog, id, preset) {
    // Ein Preset updaten
    return this.http.put(this.node_url + "/api/" + dialog + "/" + id, preset);
  }
}
import { TestBed } from '@angular/core/testing';

import { UbersichtService } from './ubersicht.service';

describe('UbersichtService', () => {
  let service: UbersichtService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UbersichtService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

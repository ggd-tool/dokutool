import { DatePipe } from '@angular/common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TypenService } from '../typen/typen.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UbersichtService {

  node_url = environment.apiServer;
  items: any;

  constructor(private http: HttpClient, private datepipe: DatePipe, private typenService: TypenService) { }

  // Nutzer fügt hinzu und löscht einen Favoriten
  updateDialogFav(type, id, status) {
    return this.http.post(this.node_url + '/api/pre/updateFav', { type: type, id: id, status: status });
  }

  // Alle Favoriten eines Nutzers zu Presets oder Maßnahmen oder Typen vom Server holen
  getAllFavs(type) {
    var params = new HttpParams().set('type', type);
    return this.http.get(this.node_url + '/api/pre/allFavs', { params })
  }

  getObjects(object, typ, itemsPerPage, currentPage) {
    // Formular/Dialog eines bestimmten Typen
    const queryParams = `?pageSize=${itemsPerPage}&page=${currentPage}`;
    return this.http.get(this.node_url + '/api/pre/ubersicht/' + typ + "/" + object + queryParams);
  }

  getAllItems(object, typ) {
    // Formular/Dialog eines bestimmten Typen
    return this.http.get(this.node_url + '/api/pre/ubersicht/' + typ + "/" + object);
  }

  getKategorien() {
    return this.http.get(this.node_url + '/api/pre/kategorien');
  }

  postKategorie(kategorie) {
    return this.http.post(this.node_url + '/api/pre/kategorien', kategorie);
  }

  deleteKategorie(id) {
    return this.http.delete(this.node_url + '/api/pre/kategorien/' + id);
  }

  //label fuer die Datenbank zu LowerCase umwandeln
  //leerzeichen durch _ ersetzen
  //Punkte entfernen
  formatForDatabase(attribute) {
    attribute = attribute.toString().toLowerCase();
    if (attribute.includes(" ")) {
      attribute = attribute.split(" ").join("_");
    }
    if (attribute.includes("-")) {
      attribute = attribute.split("-").join("_");
    }
    if (attribute.includes(",")) {
      attribute = attribute.split(",").join("");
    }
    if (attribute.includes("%")) {
      attribute = attribute.split("%").join("");
    }
    if (attribute.includes("‰")) {
      attribute = attribute.split("‰").join("");
    }
    if (attribute.includes(".")) {
      attribute = attribute.split(".").join("");
    }
    if (attribute.includes("(")) {
      attribute = attribute.split("(").join("");
    }
    if (attribute.includes(")")) {
      attribute = attribute.split(")").join("");
    }
    if (attribute.includes("/")) {
      attribute = attribute.split("/").join("_");
    }
    if (attribute.includes("²")) {
      attribute = attribute.split("²").join("2");
    }
    if (attribute.includes("³")) {
      attribute = attribute.split("³").join("3");
    }
    if (attribute.includes("°")) {
      attribute = attribute.split("°").join("");
    }
    return attribute;
  }

  // Objekte zu Namen konvertieren für Table
  formatForTable(objects, inputs) {
    objects.forEach(async object => {
      for (let property in object) {
        if (object[property] !== null && object[property] !== undefined) {
          let input = inputs.find(element => this.formatForDatabase(element.key) === property);
          if (input) {
            if (this.formatForDatabase(input.key) === property) {
              //.type für übersicht / .controlType für anzeigen o. bearbeiten
              switch ((input.type === "") ? input.controlType : input.type) {
                case 'multidropdown':
                  if (input.dependsOn) {
                    let depends = input.dependsOn.split(".");
                    let current = object;
                    for (let i = 0; i < depends.length; i++) {
                      if (current !== undefined) {
                        if (!current.length) {
                          current = current[this.formatForDatabase(depends[i])];
                        } else {
                          let tmp = []
                          current.forEach(element => {
                            let currentValue = element[this.formatForDatabase(depends[i])];
                            if (currentValue.length) {
                              currentValue.forEach(el => {
                                tmp.push(el)
                              });
                            } else {
                              tmp.push(currentValue);
                            }
                          });
                          current = tmp;
                        }
                      }
                    }
                    let values = [];
                    if (current !== undefined) {
                      for (let i = 0; i < object[property].length; i++) {
                        for (let j = 0; j < current.length; j++) {
                          if (current[j]._id === object[property][i]) {
                            values.push({ key: current[j]._id, value: current[j].name });
                          }
                          if (current[j] === object[property][i]) {
                            await this.typenService.getForm(depends[depends.length - 2]).subscribe(data => {
                              (data as any).data.forEach(section => {
                                section.inputs.forEach(inp => {
                                  if (inp.type === 'multidropdown') {
                                    inp.options.forEach(option => {
                                      if (option.key === current[j]) {
                                        values.push(option);
                                      }
                                    });
                                  }
                                });
                              });
                            });
                          }
                        }
                      }
                    }
                    object[property] = values;
                  }
                  else {
                    //referenzierte Multidropdowns
                    if (object[property][0] !== undefined) {
                      if (object[property][0]._id) {
                        let tmpNames = [];
                        object[property].forEach(element => {
                          tmpNames.push({
                            "key": element._id,
                            "value": element['name']
                          });
                        });
                        object[property] = tmpNames;
                      }
                    }
                    // Nicht referenzierte Multidropdowns 
                    else {
                      let tmpNames = [];
                      for (let i = 0; i < object[property].length; i++) {
                        for (let j = 0; j < input.options.length; j++) {
                          if (object[property][i] === input.options[j].key) {
                            tmpNames.push(input.options[j]);
                          }
                        }
                      };
                      object[property] = tmpNames;
                    }
                  }
                  break;
                case 'dropdown':
                  if (object[property]['_id']) {
                    object[property] = {
                      "key": object[property]['_id'],
                      "value": object[property]['name']
                    };
                  }
                  break;
                case 'rangeSlider':
                  object[property] = object[property][0] + " - " + object[property][1];
                  break;
                case 'user-dropdown':
                  object[property] = {
                    "key": object[property],
                    "value": object[property]['FIRST_NAME'] + " " + object[property]['LAST_NAME']
                  };
                  break;
                case 'checkbox':
                  //darf für anzeige nicht geändert werden
                  if (!input.controlType) {
                    (object[property]) ? object[property] = 'Ja' : object[property] = 'Nein';
                  }
                  break;
                case 'radio':
                  object[property] = input.options.find(option => option.key === object[property]);
                  break;
                case 'array':
                  let tmpNames = [];
                  // Überprüfen ob es ein definierendes Objekt gibt
                  let hasOtherObject = false;
                  let otherObject = "";
                  for (let arrayProperty in object[property][0]) {
                    if (typeof object[property][0][arrayProperty] === 'object') {
                      hasOtherObject = true;
                      otherObject = arrayProperty;
                      break;
                    }
                  };
                  object[property].forEach(element => {
                    if (element['name']) {
                      // Hat definierenden Namen
                      tmpNames.push({
                        "key": element._id,
                        "value": element['name']
                      });
                    } else if (hasOtherObject) {
                      // Hat definierendes Objekt
                      tmpNames.push({
                        "key": element._id,
                        "value": element[otherObject]['name']
                      });
                    } else {
                      // Failsafe
                      tmpNames.push({
                        "key": element._id,
                        "value": ""
                      });
                    }
                  });
                  object[property] = tmpNames;
                  break;
              }
            }
          }
        }
      }
    });
    return objects;
  }

  // Ein Datum richtig formatieren
  showDate(dateString) {
    try {
      return this.datepipe.transform(dateString, 'dd.MM.yyyy, HH:mm');
    } catch {
      return "Kein Datum vorhanden"
    }
  }

  //einen Dialog updaten (z.B. für Kategorien => item)
  updateKategorien(id, typ, kategorien) {
    // Eine Maßnahmendialog updaten
    return this.http.put(this.node_url + "/api/pre/" + typ + "/update/" + id, { kategorien: kategorien });
  }

  formatForFiles(objectName: string) {
    objectName = objectName.toString();
    if (objectName.includes(" ")) {
      objectName = objectName.split(" ").join("_");
    }
    if (objectName.includes(".")) {
      objectName = objectName.split(".").join("");
    }
    if (objectName.includes("(")) {
      objectName = objectName.split("(").join("");;
    }
    if (objectName.includes(")")) {
      objectName = objectName.split(")").join("");;
    }
    if (objectName.includes("-")) {
      objectName = objectName.split("-").join("");;
    }
    return objectName;
  }

  //Daten in Excel für bestimmten Zeitraum exportieren
  excelExport(zeitraum) {
    var params = new HttpParams().set('start', zeitraum.start);
    params = params.append('ende', zeitraum.ende);
    return this.http.get(this.node_url + '/api/pre/export', {
      params, responseType: 'arraybuffer', headers: {
        'Content-Type': 'application/json'
      }
    });
  }
}

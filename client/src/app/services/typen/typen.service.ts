import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TypenService {

  node_url = environment.apiServer;

  constructor(private http: HttpClient) { }

  getForm(typ) {
    // Formular/Dialog eines bestimmten Typen
    return this.http.get(this.node_url + '/api/pre/objects/' + typ);
  }

  getAllObjects() {
    // Alle Typen die es überhaupt gibt
    return this.http.get(this.node_url + '/api/pre/objects');
  }

  post(url, data) {
    // Einen neues Objekt speichern
    return this.http.post(this.node_url + "/api" + url, data);
  }

  getSpecific(typ, id) {
    // Ein bestimmtest Objekt suchen
    return this.http.get(this.node_url + "/api/" + typ + "/" + id);
  }

  updateSpecificTyp(dialog, id, typ) {
    // Einen Typen updaten
    return this.http.put(this.node_url + "/api/" + dialog + "/" + id, typ);
  }
}
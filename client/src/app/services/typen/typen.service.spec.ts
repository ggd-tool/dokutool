import { TestBed } from '@angular/core/testing';

import { TypenService } from './typen.service';

describe('TypenService', () => {
  let service: TypenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { MassnahmenService } from './massnahmen.service';

describe('MassnahmenService', () => {
  let service: MassnahmenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MassnahmenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

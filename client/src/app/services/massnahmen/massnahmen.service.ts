import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MassnahmenService {

  node_url = environment.apiServer;

  constructor(private http: HttpClient) { }

  getForm(typ) {
    // Formular/Dialog eines bestimmten Maßnahmen
    return this.http.get(this.node_url + '/api/pre/maßnahmen/' + typ);
  }

  getAllPrevMassnahmen(typ) {
    // Alle vorangegangenen Maßnahmen einer bestimmten Maßnahme
    return this.http.get(this.node_url + '/api/' + typ + '/prev');
  }

  getAllObjects() {
    // Alle Maßnahmen die es überhaupt gibt
    return this.http.get(this.node_url + '/api/pre/maßnahmen');
  }

  post(url, data) {
    // eine neue Maßnahme speichern
    return this.http.post(this.node_url + "/api" + url, data);
  }

  updateSpecificMassnahme(dialog, id, massnahme) {
    // Eine Maßnahme updaten
    return this.http.put(this.node_url + "/api/" + dialog + "/" + id, massnahme);
  }
}

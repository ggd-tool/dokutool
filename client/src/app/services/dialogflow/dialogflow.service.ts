import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DialogflowService {

  node_url = environment.apiServer;

  constructor(private http: HttpClient) { }
  
  UseAgent_text(request){
    return this.http.post(this.node_url + '/api/pre/dialogflow/text', request);
  }
  UseAgent_audio(request){
    return this.http.post(this.node_url + '/api/pre/dialogflow/audio', request);
  }
  showContext(){
    // Durch ein ungelösten Bug erhält man ein "--", wenn get verwendet wird 
    return this.http.post(this.node_url + '/api/pre/dialogflow/showContext', undefined);
  }
  createContext(request){
    return this.http.post(this.node_url + '/api/pre/dialogflow/createContext', request);
  }
  deleteAllContexts(){
    return this.http.delete(this.node_url + '/api/pre/dialogflow/deleteAllContexts');
  }
  deleteContext(idArray){
    return this.http.post(this.node_url + '/api/pre/dialogflow/deleteContext', idArray);
  }
  showEntities(){
    // Durch ein ungelösten Bug erhält man ein "--", wenn get verwendet wird 
    return this.http.post(this.node_url + '/api/pre/dialogflow/showEntities', undefined);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InputBase } from '../../inputs/input-base';
import { environment } from '../../../environments/environment';

let lastInputs;

@Injectable({
  providedIn: 'root'
})
export class InputControlService {

  node_url = environment.apiServer;

  constructor(private http: HttpClient) { }

  toFormGroup(inputs: InputBase<string>[]) {
    lastInputs = inputs;

    const group: any = {};
    // Neue Formgroup aus Inputs generieren - Funktioniert anscheinend momentan nicht mehr
    for (let i = 0; i < inputs.length; i++) {
      let input = inputs[i];
      let def = input.default;
      switch (input.controlType) {
        case 'checkbox':
          group[input.key] = new FormControl(def ? def : false);
          break;
        case 'radio':
          group[input.key] = new FormControl(def ? def : null);
          break;
        case 'datepicker':
          group[input.key] = new FormControl(new Date());
          break;
        case 'number':
          let pattern = new RegExp("^[0-9]*[.]?[0-9]*$");
          let validators = [Validators.pattern(pattern)];
          //Validator mit eventuellen min und max Werten zusammenbauen
          if (input.required) {
            validators.push(Validators.required);
          }
          if (input.min) {
            validators.push(Validators.min(input.min as any));
          }
          if (input.max) {
            validators.push(Validators.max(input.max as any));
          }
          group[input.key] = new FormControl(def || '', validators);
          break;
        case 'section':
          break;
        case 'array':
          group[input.key] = new FormControl();
          break;
        case 'multidropdown':
          let options = [];
          if ((input as any).options !== "ref") {
            if (input.options !== undefined && def) {
              let defaults = def.split(",");
              if (defaults[0] === "all") {
                for (let i = 0; i < input.options.length; i++) {
                  options.push(input.options[i]);
                };
              } else {
                for (let i = 0; i < input.options.length; i++) {
                  for (let j = 0; j < defaults.length; j++) {
                    if (defaults[j].trim() === input.options[i].key.trim()) {
                      options.push(input.options[i].key);
                    }
                    if (defaults[j].trim() === input.options[i].value.trim()) {
                      options.push(input.options[i].key);
                    }
                  };
                }
              }
              //Wenn dependsOn Fled optionen laden
              let values: any = [];
              if (input.dependLoader) {
                for (let k = 0; k < input.dependLoader.length; k++) {
                  let dependLoader = input.dependLoader[k];
                  if (dependLoader.split(".")[0].toLocaleLowerCase() !== "") {
                    if ((options as any).length !== 0) {
                      this.loadDependsOn((options as any), dependLoader).subscribe(data => {
                        for (let i = 0; i < lastInputs.length; i++) {
                          let searchInput = lastInputs[i];
                          if (searchInput.key === input.dependFor[k]) {
                            searchInput.options = data;
                            //falls default alle Optionen gesetzt werden sollen 
                            if (searchInput.default === "all") {
                              for (let i = 0; i < searchInput.options.length; i++) {
                                for (let j = 0; j < searchInput.options[i].data.length; j++) {
                                  values.push(searchInput.options[i].data[j].key);
                                }
                              }
                            }
                          }
                        }
                      })
                    }
                  };
                  group[input.dependFor[k]] = new FormControl(values);
                }

              }
            }
          }
          if (!group[input.key]) {
            group[input.key] = input.required ? new FormControl(options, Validators.required) : new FormControl(options);
          }
          break;
        case 'dropdown':
          let key;
          if (input.options !== undefined && def) {
            input.options.forEach(option => {
              if (option.value.trim() === def.trim()) {
                key = option.key;
              }
            })
          }
          group[input.key] = input.required ? new FormControl(key || def, Validators.required) : new FormControl(key || def);
          break;
        case 'range':
          group[input.key] = new FormControl([input.value, input.highValue]);
          break;
        case 'slider':
          group[input.key] = new FormControl(input.value);
          break;
        default:
          if (input.loadUsers) {
            // User-Dropdown
            group[input.key] = new FormControl();
          } else {
            // Alles andere
            group[input.key] = input.required ? new FormControl(def || '', Validators.required) : new FormControl(def || '');
          }
      }
    };
    return new FormGroup(group);
  }

  loadDependsOn(objectId: string, depend: string) {
    // Eine Maßnahmendialog updaten
    let url = this.node_url + "/api/pre/loadDepend/" + objectId + "/" + depend;
    return this.http.get(url);
  }

  //geladene options von dependsOn Dropdown zuweisen
  setDependValues(dependFor: string, controlType: string, key: string, data: any, repeat?: boolean, dependValues?: any) {
    lastInputs.forEach(input => {
      //Funktion für anpassung der Values in Repeat abändern
      if (repeat) {
        if (input.key === key) {
          input.options = data;
        }
      } else {
        //Wenn nicht über repeat geladen
        if (input.key === dependFor) {
          if (controlType === 'multidropdown') {
            let tmpOptions = [];
            if (input.options !== "ref") {
              //überprüfen ob group schon vorhaden, darf wegen disabled nicht überschrieben werden
              data.forEach(group => {
                input.options.forEach(element => {
                  if (group.group === element.group) {
                    tmpOptions.push(element);
                  }
                });
              });
              input.options = data;
              if (tmpOptions.length > 0) {
                tmpOptions.forEach(option => {
                  input.options.forEach((element, index) => {
                    if (element.group === option.group) {
                      input.options[index] = option;
                    }
                  });
                })
              }
            } else {
              input.options = data;
            }
          } else {
            input.options = data;
          }
          if (input.default === "all" && dependValues !== undefined) {
            input.options.forEach(option => {
              option.keys.forEach(key => {
                dependValues.push(key);
              });
            });
          }
        }
      }
    });
  }

  //Optionen die noch in selected Value des depends sind aber nicht mehr in den optionen stehen entfernen 
  removeFromDependDropdown(dependFor: string, dependValues: []) {
    if (dependValues.length > 0) {
      lastInputs.forEach(input => {
        if (input.key === dependFor) {
          if (input.options !== undefined) {
            for (let j = 0; j < dependValues.length; j++) {
              let remover = true;
              for (let i = 0; i < input.options.length; i++) {
                for (let l = 0; l < input.options[i].data.length; l++) {
                  if (input.options[i].data[l].key === dependValues[j]) {
                    remover = false;
                  }
                }
              }
              if (remover) {
                dependValues.splice(j, 1);
                j--;
              }
            };
          }
        }
      })
    }
  }

  getAllUsers() {
    // Alle User die es überhaupt gibt
    return this.http.get(this.node_url + '/api/pre/allUsers');
  }

  getObjectWithObjectinArray(typ, key, objectinarrayID) {
    return this.http.get(this.node_url + "/api/" + typ + "/sub/" + key + "/" + objectinarrayID);
  }

  findById(typ: string, id: string) {
    return this.http.get(this.node_url + "/api/pre/" + typ + "/" + id);
  }

  findInput(key: string) {
    for (let i = 0; i < lastInputs.length; i++) {
      let input = lastInputs[i];
      if (input.key === key) {
        return input;
      }
    };
  }
}

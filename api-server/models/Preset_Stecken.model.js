const mongoose = require('mongoose');

const preset_steckenModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                    },
                auftragsnummer: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Auftrag"}], 'default': '',
                    },
                feld: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Feld"}], 'default': '',
                    },
                beete: 
                    {
                        "type" : [],
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Preset_Stecken', preset_steckenModel);
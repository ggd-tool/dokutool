const mongoose = require('mongoose');

const düngerModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                        "unique": true
                    },
                harnstoff: 
                    {
                        "type": Number, 'default': "",
                    },
                no3: 
                    {
                        "type": Number, 'default': "",
                    },
                nh4: 
                    {
                        "type": Number, 'default': "",
                    },
                gesamt_n: 
                    {
                        "type": Number, 'default': "",
                    },
                p2o5: 
                    {
                        "type": Number, 'default': "",
                    },
                k2o: 
                    {
                        "type": Number, 'default': "",
                    },
                mgo: 
                    {
                        "type": Number, 'default': "",
                    },
                cao: 
                    {
                        "type": Number, 'default': "",
                    },
                so3: 
                    {
                        "type": Number, 'default': "",
                    },
                fe: 
                    {
                        "type": Number, 'default': "",
                    },
                mn: 
                    {
                        "type": Number, 'default': "",
                    },
                zn: 
                    {
                        "type": Number, 'default': "",
                    },
                b: 
                    {
                        "type": Number, 'default': "",
                    },
                cu: 
                    {
                        "type": Number, 'default': "",
                    },
                mo: 
                    {
                        "type": Number, 'default': "",
                    },
                al: 
                    {
                        "type": Number, 'default': "",
                    },
                dichte: 
                    {
                        "type": Number, 'default': "",
                    },
                für_05_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                für_10_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                für_15_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                für_20_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                für_1_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                für_5_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                für_100_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                für_200_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                flüssigdünger_100_ec: 
                    {
                        "type": Number, 'default': "",
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Dünger', düngerModel);
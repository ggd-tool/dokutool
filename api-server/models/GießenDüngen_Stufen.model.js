const mongoose = require('mongoose');

const gießendüngen_stufenModel = mongoose.Schema(
    {
                datum: 
                    {
                        "type": Date , 'default': null,
                        "required": "Datum ist erforderlich!",
                    },
                gießwagen_stufen: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Gießwagen_Stufen"}], 'default': '',
                        validate: v => Array.isArray(v) && v.length > 0,
                    },
                beete: 
                    {
                        "type" : [],
                    },
                einweghäufigkeit: 
                    {
                        "type": Number, 'default': "",
                    },
                stufe: 
                    {
                        "type": String , 'default': "",
                        "required": "Stufe ist erforderlich!",
                    },
                von_bis_in_: 
                    {
                        "type": [],
                    },
                stammlösungsbecken_düngemischung: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'StammlösungsbeckenDüngemischung', 'default': '',
                    },
                konzentration_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                konzentration_g_l: 
                    {
                        "type": Number, 'default': "",
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('GießenDüngen_Stufen', gießendüngen_stufenModel);
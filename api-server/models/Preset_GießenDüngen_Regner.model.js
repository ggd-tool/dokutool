const mongoose = require('mongoose');

const preset_gießendüngen_regnerModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                    },
                regner: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Regner"}], 'default': '',
                    },
                beete: 
                    {
                        "type" : [],
                    },
                zeit_in_min: 
                    {
                        "type": String, 'default': "",
                    },
                von_bis_in_: 
                    {
                        "type": [],
                    },
                stammlösungsbecken_düngemischung: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'StammlösungsbeckenDüngemischung', 'default': '',
                    },
                konzentration_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                konzentration_g_l: 
                    {
                        "type": Number, 'default': "",
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Preset_GießenDüngen_Regner', preset_gießendüngen_regnerModel);
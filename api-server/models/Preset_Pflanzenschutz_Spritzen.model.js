const mongoose = require('mongoose');

const preset_pflanzenschutz_spritzenModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                    },
                feld: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Feld"}], 'default': '',
                    },
                beete: 
                    {
                        "type" : [],
                    },
                ausbringung: 
                    {
                        "type": String , 'default': "",
                    },
                pflanzenschutzmittel: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'Pflanzenschutzmittel', 'default': '',
                    },
                mittelmenge_l_ha: 
                    {
                        "type": Number, 'default': "",
                    },
                wassermenge_l_m2: 
                    {
                        "type": Number, 'default': "",
                    },
                prozentanteil: 
                    {
                        "type": Number, 'default': "",
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Preset_Pflanzenschutz_Spritzen', preset_pflanzenschutz_spritzenModel);
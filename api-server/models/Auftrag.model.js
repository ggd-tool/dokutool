const mongoose = require('mongoose');

const auftragModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                        "unique": true
                    },
                pflanze: 
                    {
                        "type": String, 'default': "",
                        "required": "Pflanze ist erforderlich!",
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Auftrag', auftragModel);
const mongoose = require('mongoose');

const preset_sprühennebeln_regnerModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                    },
                regner: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Regner"}], 'default': '',
                    },
                beete: 
                    {
                        "type" : [],
                    },
                laufzeit_in_min: 
                    {
                        "type": Number, 'default': "",
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Preset_SprühenNebeln_Regner', preset_sprühennebeln_regnerModel);
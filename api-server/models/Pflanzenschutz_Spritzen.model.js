const mongoose = require('mongoose');

const pflanzenschutz_spritzenModel = mongoose.Schema(
    {
                datum: 
                    {
                        "type": Date , 'default': null,
                        "required": "Datum ist erforderlich!",
                    },
                feld: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Feld"}], 'default': '',
                        validate: v => Array.isArray(v) && v.length > 0,
                    },
                beete: 
                    {
                        "type" : [],
                    },
                ausbringung: 
                    {
                        "type": String , 'default': "",
                    },
                pflanzenschutzmittel: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'Pflanzenschutzmittel', 'default': '',
                        "required": "Pflanzenschutzmittel ist erforderlich!",
                    },
                mittelmenge_l_ha: 
                    {
                        "type": Number, 'default': "",
                    },
                wassermenge_l_m2: 
                    {
                        "type": Number, 'default': "",
                    },
                prozentanteil: 
                    {
                        "type": Number, 'default': "",
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Pflanzenschutz_Spritzen', pflanzenschutz_spritzenModel);
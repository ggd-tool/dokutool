const mongoose = require('mongoose');

const preset_stammlösung_ansetzenModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                    },
                stammlösungsbecken_düngemischung: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'StammlösungsbeckenDüngemischung', 'default': '',
                    },
                wassermenge_in_l: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Preset_Stammlösung_ansetzen', preset_stammlösung_ansetzenModel);
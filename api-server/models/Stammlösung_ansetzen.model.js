const mongoose = require('mongoose');

const stammlösung_ansetzenModel = mongoose.Schema(
    {
                datum: 
                    {
                        "type": Date , 'default': null,
                        "required": "Datum ist erforderlich!",
                    },
                stammlösungsbecken_düngemischung: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'StammlösungsbeckenDüngemischung', 'default': '',
                        "required": "Stammlösungsbecken-Düngemischung ist erforderlich!",
                    },
                wassermenge_in_l: 
                    {
                        "type": Number, 'default': "",
                        "required": "Wassermenge (in L) ist erforderlich!",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Stammlösung_ansetzen', stammlösung_ansetzenModel);
const mongoose = require('mongoose');

const preset_sprühennebeln_gießwagen_stufenModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                    },
                gießwagen_stufen: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Gießwagen_Stufen"}], 'default': '',
                    },
                beete: 
                    {
                        "type" : [],
                    },
                einweghäufigkeit: 
                    {
                        "type": Number, 'default': "",
                    },
                bewässerungssystem: 
                    {
                        "type": [],
                    },
                stufe: 
                    {
                        "type": String , 'default': "",
                    },
                von_bis_in_: 
                    {
                        "type": [],
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Preset_SprühenNebeln_Gießwagen_Stufen', preset_sprühennebeln_gießwagen_stufenModel);
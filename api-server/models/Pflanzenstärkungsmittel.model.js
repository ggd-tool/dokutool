const mongoose = require('mongoose');

const pflanzenstärkungsmittelModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                        "unique": true
                    },
                wirkstoffe: 
                    {
                        "type": [{"name": String}], 'default': [],
                    },
                bemerkung: 
                    {
                        "type": String, 'default': "",
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Pflanzenstärkungsmittel', pflanzenstärkungsmittelModel);
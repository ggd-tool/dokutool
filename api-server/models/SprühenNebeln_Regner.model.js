const mongoose = require('mongoose');

const sprühennebeln_regnerModel = mongoose.Schema(
    {
                datum: 
                    {
                        "type": Date , 'default': null,
                        "required": "Datum ist erforderlich!",
                    },
                regner: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Regner"}], 'default': '',
                        validate: v => Array.isArray(v) && v.length > 0,
                    },
                beete: 
                    {
                        "type" : [],
                    },
                laufzeit_in_min: 
                    {
                        "type": Number, 'default': "",
                        "required": "Laufzeit (in min) ist erforderlich!",
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('SprühenNebeln_Regner', sprühennebeln_regnerModel);
const mongoose = require('mongoose');

const platteModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                        "unique": true
                    },
                länge_zentimeter: 
                    {
                        "type": Number, 'default': "",
                        "required": "Länge (Zentimeter) ist erforderlich!",
                    },
                breite_zentimeter: 
                    {
                        "type": Number, 'default': "",
                        "required": "Breite (Zentimeter) ist erforderlich!",
                    },
                zellen: 
                    {
                        "type": Number, 'default': "",
                        "required": "Zellen ist erforderlich!",
                    },
                volumen_liter: 
                    {
                        "type": Number, 'default': "",
                        "required": "Volumen (Liter) ist erforderlich!",
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Platte', platteModel);
const mongoose = require('mongoose');

const pflanzenstärkung_gießen_stufenModel = mongoose.Schema(
    {
                datum: 
                    {
                        "type": Date , 'default': null,
                        "required": "Datum ist erforderlich!",
                    },
                gießwagen_stufen: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Gießwagen_Stufen"}], 'default': '',
                        validate: v => Array.isArray(v) && v.length > 0,
                    },
                beete: 
                    {
                        "type" : [],
                    },
                einweghäufigkeit: 
                    {
                        "type": Number, 'default': "",
                    },
                stufe: 
                    {
                        "type": String , 'default': "",
                        "required": "Stufe ist erforderlich!",
                    },
                von_bis_in_: 
                    {
                        "type": [],
                    },
                pflanzenstärkungsmittel: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'Pflanzenstärkungsmittel', 'default': '',
                        "required": "Pflanzenstärkungsmittel ist erforderlich!",
                    },
                mittelmenge_l_ha: 
                    {
                        "type": Number, 'default': "",
                    },
                wassermenge_l_m2: 
                    {
                        "type": Number, 'default': "",
                    },
                prozentanteil: 
                    {
                        "type": Number, 'default': "",
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Pflanzenstärkung_Gießen_Stufen', pflanzenstärkung_gießen_stufenModel);
const mongoose = require('mongoose');

const gießwagen_stufenlosModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                        "unique": true
                    },
                breite_meter: 
                    {
                        "type": Number, 'default': "",
                        "required": "Breite (Meter) ist erforderlich!",
                    },
                fahrlänge_meter: 
                    {
                        "type": Number, 'default': "",
                        "required": "Fahrlänge (Meter) ist erforderlich!",
                    },
                volumenstrom_m3_h: 
                    {
                        "type": Number, 'default': "",
                        "required": "Volumenstrom (m³/h) ist erforderlich!",
                    },
                bewässerungssysteme: 
                    {
                        "type" : [{ 'type': String }], 'default': [],
                        validate: v => Array.isArray(v) && v.length > 0,
                    },
                feld: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'Feld', 'default': '',
                        "required": "Feld ist erforderlich!",
                    },
                bei_1_meter_minute: 
                    {
                        "type": Number, 'default': "",
                        "required": "bei 1% (Meter/Minute) ist erforderlich!",
                    },
                bei_100_meter_minute: 
                    {
                        "type": Number, 'default': "",
                        "required": "bei 100% (Meter/Minute) ist erforderlich!",
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Gießwagen_Stufenlos', gießwagen_stufenlosModel);
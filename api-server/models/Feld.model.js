const mongoose = require('mongoose');

const feldModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                        "unique": true
                    },
                fläche_brutto_in_m2: 
                    {
                        "type": Number, 'default': "",
                        "required": "Fläche (Brutto in m²) ist erforderlich!",
                    },
                fläche_netto_in_m2: 
                    {
                        "type": Number, 'default': "",
                        "required": "Fläche (Netto in m²) ist erforderlich!",
                    },
                beete: 
                    {
                        "type": [{"name": String, "fläche_netto_in_m2": Number}], 'default': [],
                        "required": "Beete ist erforderlich!",
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Feld', feldModel);
const mongoose = require('mongoose');

const preset_gießendüngen_stufenlosModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                    },
                gießwagen_stufenlos: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Gießwagen_Stufenlos"}], 'default': '',
                    },
                beete: 
                    {
                        "type" : [],
                    },
                einweghäufigkeit: 
                    {
                        "type": Number, 'default': "",
                    },
                geschwindigkeit_: 
                    {
                        "type": Number,
                    },
                von_bis_in_: 
                    {
                        "type": [],
                    },
                stammlösungsbecken_düngemischung: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'StammlösungsbeckenDüngemischung', 'default': '',
                    },
                konzentration_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                konzentration_g_l: 
                    {
                        "type": Number, 'default': "",
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Preset_GießenDüngen_Stufenlos', preset_gießendüngen_stufenlosModel);
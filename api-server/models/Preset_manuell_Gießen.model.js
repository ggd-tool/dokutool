const mongoose = require('mongoose');

const preset_manuell_gießenModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                    },
                feld: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Feld"}], 'default': '',
                    },
                beete: 
                    {
                        "type" : [],
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Preset_manuell_Gießen', preset_manuell_gießenModel);
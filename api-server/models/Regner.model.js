const mongoose = require('mongoose');

const regnerModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                        "unique": true
                    },
                fläche_brutto_in_m2: 
                    {
                        "type": Number, 'default': "",
                        "required": "Fläche (Brutto in m²) ist erforderlich!",
                    },
                beregnungshöhe_mm: 
                    {
                        "type": Number, 'default': "",
                        "required": "Beregnungshöhe (mm) ist erforderlich!",
                    },
                zeit_minuten: 
                    {
                        "type": Number, 'default': "",
                        "required": "Zeit (Minuten) ist erforderlich!",
                    },
                feld: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'Feld', 'default': '',
                        "required": "Feld ist erforderlich!",
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Regner', regnerModel);
const mongoose = require('mongoose');

const pflanzenschutzmittelModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                        "unique": true
                    },
                zulassungsnummer: 
                    {
                        "type": String, 'default': "",
                        "required": "Zulassungsnummer ist erforderlich!",
                    },
                zugelassen_bis: 
                    {
                        "type": Date , 'default': null,
                        "required": "Zugelassen bis ist erforderlich!",
                    },
                anwendung_bis: 
                    {
                        "type": Date , 'default': null,
                        "required": "Anwendung bis ist erforderlich!",
                    },
                wirkstoffe: 
                    {
                        "type": [{"name": String, "wirkstoffgruppe": String}], 'default': [],
                        "required": "Wirkstoffe ist erforderlich!",
                    },
                gefahrenstoffeinstufung: 
                    {
                        "type": String, 'default': "",
                    },
                bienengefährdungsstufe: 
                    {
                        "type": String , 'default': "",
                    },
                indikation: 
                    {
                        "type": String, 'default': "",
                    },
                unter_glas: 
                    {
                        "type": Boolean,
                    },
                freiland: 
                    {
                        "type": Boolean,
                    },
                maximale_anzahl: 
                    {
                        "type": Number, 'default': "",
                    },
                wartezeit_tage: 
                    {
                        "type": Number, 'default': "",
                    },
                bis_50cm_liter_ha: 
                    {
                        "type": Number, 'default': "",
                    },
                für_50_125cm_liter_ha: 
                    {
                        "type": Number, 'default': "",
                    },
                ab_125cm_liter_ha: 
                    {
                        "type": Number, 'default': "",
                    },
                sf: 
                    {
                        "type": String, 'default': "",
                    },
                ng: 
                    {
                        "type": String, 'default': "",
                    },
                nw: 
                    {
                        "type": String, 'default': "",
                    },
                ns: 
                    {
                        "type": String, 'default': "",
                    },
                nt: 
                    {
                        "type": String, 'default': "",
                    },
                nz: 
                    {
                        "type": String, 'default': "",
                    },
                vz: 
                    {
                        "type": String, 'default': "",
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Pflanzenschutzmittel', pflanzenschutzmittelModel);
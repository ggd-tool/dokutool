const mongoose = require('mongoose');

const stammlösungsbeckendüngemischungModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                        "unique": true
                    },
                bemerkung: 
                    {
                        "type": String, 'default': "",
                    },
                fassungsvermögen: 
                    {
                        "type": Number, 'default': "",
                    },
                dünger: 
                    {
                        "type": [{"mittel": { "type": mongoose.Schema.Types.ObjectId, ref:"Dünger"}, "menge_kg_l": Number}], 'default': [],
                        "required": "Dünger ist erforderlich!",
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('StammlösungsbeckenDüngemischung', stammlösungsbeckendüngemischungModel);
const mongoose = require('mongoose');

const preset_pflanzenstärkung_gießen_stufenModel = mongoose.Schema(
    {
                name: 
                    {
                        "type": String, 'default': "",
                        "required": "Name ist erforderlich!",
                    },
                gießwagen_stufen: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Gießwagen_Stufen"}], 'default': '',
                    },
                beete: 
                    {
                        "type" : [],
                    },
                einweghäufigkeit: 
                    {
                        "type": Number, 'default': "",
                    },
                stufe: 
                    {
                        "type": String , 'default': "",
                    },
                von_bis_in_: 
                    {
                        "type": [],
                    },
                pflanzenstärkungsmittel: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'Pflanzenstärkungsmittel', 'default': '',
                    },
                mittelmenge_l_ha: 
                    {
                        "type": Number, 'default': "",
                    },
                wassermenge_l_m2: 
                    {
                        "type": Number, 'default': "",
                    },
                prozentanteil: 
                    {
                        "type": Number, 'default': "",
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Preset_Pflanzenstärkung_Gießen_Stufen', preset_pflanzenstärkung_gießen_stufenModel);
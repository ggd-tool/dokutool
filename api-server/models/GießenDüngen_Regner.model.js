const mongoose = require('mongoose');

const gießendüngen_regnerModel = mongoose.Schema(
    {
                datum: 
                    {
                        "type": Date , 'default': null,
                        "required": "Datum ist erforderlich!",
                    },
                regner: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Regner"}], 'default': '',
                    },
                beete: 
                    {
                        "type" : [],
                    },
                zeit_in_min: 
                    {
                        "type": String, 'default': "",
                        "required": "Zeit (in min) ist erforderlich!",
                    },
                von_bis_in_: 
                    {
                        "type": [],
                    },
                stammlösungsbecken_düngemischung: 
                    {
                        "type": mongoose.Schema.Types.ObjectId, ref:'StammlösungsbeckenDüngemischung', 'default': '',
                    },
                konzentration_ec: 
                    {
                        "type": Number, 'default': "",
                    },
                konzentration_g_l: 
                    {
                        "type": Number, 'default': "",
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('GießenDüngen_Regner', gießendüngen_regnerModel);
const mongoose = require('mongoose');

const pikierenzusammenpikierenModel = mongoose.Schema(
    {
                datum: 
                    {
                        "type": Date , 'default': null,
                        "required": "Datum ist erforderlich!",
                    },
                auftragsnummer: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Auftrag"}], 'default': '',
                        validate: v => Array.isArray(v) && v.length > 0,
                    },
                feld: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Feld"}], 'default': '',
                        validate: v => Array.isArray(v) && v.length > 0,
                    },
                beete: 
                    {
                        "type" : [],
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('Pikierenzusammenpikieren', pikierenzusammenpikierenModel);
const mongoose = require('mongoose');

const sprühennebeln_gießwagen_stufenlosModel = mongoose.Schema(
    {
                datum: 
                    {
                        "type": Date , 'default': null,
                        "required": "Datum ist erforderlich!",
                    },
                gießwagen_stufenlos: 
                    {
                        "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"Gießwagen_Stufenlos"}], 'default': '',
                        validate: v => Array.isArray(v) && v.length > 0,
                    },
                beete: 
                    {
                        "type" : [],
                    },
                einweghäufigkeit: 
                    {
                        "type": Number, 'default': "",
                    },
                bewässerungssystem: 
                    {
                        "type": [],
                    },
                geschwindigkeit_: 
                    {
                        "type": Number,
                    },
                von_bis_in_: 
                    {
                        "type": [],
                    },
                wasser_ec_ms_cm: 
                    {
                        "type": Number, 'default': "",
                    },
                bemerkung: 
                    {
                        "type": String , 'default': "",
                    },
                durchgeführt_von: 
                    {
                        "type": {
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                ,
                    },
    }, {timestamps: true} 
);

module.exports = mongoose.model('SprühenNebeln_Gießwagen_Stufenlos', sprühennebeln_gießwagen_stufenlosModel);
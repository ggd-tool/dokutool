const express = require('express');
const router = express.Router();

const DialogController = require('./pre-controllers/dialog.controller');

module.exports = function (app, keycloak) {

	// Objects - Routen
	router.get('/objects', keycloak.protect("realm:visitor"), DialogController.index);
	router.get('/objects/:typ', keycloak.protect("realm:visitor"), DialogController.show);

	// Maßnahmen - Routen
	router.get('/ma%C3%9Fnahmen', keycloak.protect("realm:visitor"), DialogController.index);
	router.get('/ma%C3%9Fnahmen/:typ', keycloak.protect("realm:visitor"), DialogController.show);

	// Preset - Routen
	router.get('/presets', keycloak.protect("realm:visitor"), DialogController.index);
	router.get('/presets/:typ', keycloak.protect("realm:visitor"), DialogController.show);
	router.get('/presets/preset/:presetName', keycloak.protect("realm:visitor"), DialogController.getByNameForDropdown);

	// Allgemein
	router.get('/ubersicht/:typ/:object', keycloak.protect("realm:visitor"), DialogController.showAll);
	router.get('/loadDepend/:id/:depend', keycloak.protect("realm:visitor"), DialogController.loadDepend);
	router.get('/:typ/:id', keycloak.protect("realm:visitor"), DialogController.loadById);
	router.get('/allUsers', keycloak.protect("realm:visitor"), DialogController.getAllUsers);
	router.put('/:typ/update/:id', keycloak.protect("realm:worker"), DialogController.update);
	router.get('/export', keycloak.protect("realm:worker"), DialogController.export);

	// Favoriten
	router.post('/updateFav', keycloak.protect("realm:worker"), DialogController.updateFavorites);
	router.get('/allFavs', keycloak.protect("realm:worker"), DialogController.getAllFavorites);

	// Kategorien
	router.post('/kategorien', keycloak.protect("realm:worker"), DialogController.addKategorien);
	router.get('/kategorien', keycloak.protect("realm:worker"), DialogController.getKategorien);
	router.delete('/kategorien/:id', keycloak.protect("realm:worker"), DialogController.deleteKategorien);
	
	app.use('/api/pre', router);
};
const mongoose = require('mongoose');

const objectsModel = mongoose.Schema(
    {
        name: {
            "type": String,
        },
        data: {
            "type": Array,
        },
        url: {
            "type": String,
        },
        kategorien: {
            "type": [ {"type": mongoose.Schema.Types.ObjectId, ref:"Kategorie"} ],
        }
    }, {timestamps: true} 
);

module.exports = mongoose.model('ObjectDialog', objectsModel);
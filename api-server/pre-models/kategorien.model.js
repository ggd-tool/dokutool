const mongoose = require('mongoose');

const kategorienModel = mongoose.Schema(
    {
        name: {
            "type": String,
            "unique": true
        },
        color: {
            "type": String
        },
        textColor: {
            "type": String
        }
    }, { timestamps: true }
);

module.exports = mongoose.model('Kategorie', kategorienModel);
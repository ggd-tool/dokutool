const mongoose = require('mongoose');

const favoritenModel = mongoose.Schema(
    {
        user: {
            "type": String
        },
        massnahme: {
            "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"massnahmenDialog"}], 'default': []
        },
        object: {
            "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"objectDialog"}], 'default': []
        },
        preset: {
            "type" : [{ 'type': mongoose.Schema.Types.ObjectId, ref:"presetDialog"}], 'default': []
        }
    }, { timestamps: true }
);

module.exports = mongoose.model('Favoriten', favoritenModel);
const { Regner } = require('../models-index');

const RegnerController = {
  async index(req, res){
  	const regner = await Regner.find().populate('feld');
  	res.send(regner);
  },
  async prev(req, res) {
    const regner = await Regner.find().populate('feld').sort('-createdAt').limit(3);
  	res.send(regner);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const regner = new Regner({
            'name': req.body.name,
            'fläche_brutto_in_m2': req.body.fläche_brutto_in_m2,
            'beregnungshöhe_mm': req.body.beregnungshöhe_mm,
            'zeit_minuten': req.body.zeit_minuten,
            'feld': req.body.feld,
        });
      await regner.save(); 
      res.send(regner);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const regner = await Regner.findById(req.params.id).populate('feld');
  	res.send(regner);
  },
  async update(req, res){
    const regner = await Regner.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(regner);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let regner;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  regner = await Regner.find().populate('feld').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async regner => {
        await Regner.countDocuments().then(count => {
          res.send({
            items: regner,
            total: count
          })
        })
      })
    }else{
      regner = await Regner.find().populate('feld');
    }
  	return regner;
  },
  async loadByID(id){
  	const regner = await Regner.findById(id).populate('feld');
  	return regner;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const regner = await Regner.find({ [searchSub]: req.params.id });
    res.send(regner[0]);
  }
};

module.exports = RegnerController;
const { Gießwagen_Stufen } = require('../models-index');

const Gießwagen_StufenController = {
  async index(req, res){
  	const gießwagen_stufen = await Gießwagen_Stufen.find().populate('feld');
  	res.send(gießwagen_stufen);
  },
  async prev(req, res) {
    const gießwagen_stufen = await Gießwagen_Stufen.find().populate('feld').sort('-createdAt').limit(3);
  	res.send(gießwagen_stufen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const gießwagen_stufen = new Gießwagen_Stufen({
            'name': req.body.name,
            'breite_meter': req.body.breite_meter,
            'fahrlänge_meter': req.body.fahrlänge_meter,
            'volumenstrom_m3_h': req.body.volumenstrom_m3_h,
            'bewässerungssysteme': req.body.bewässerungssysteme,
            'feld': req.body.feld,
            'stufe_1_meter_minute': req.body.stufe_1_meter_minute,
            'stufe_2_meter_minute': req.body.stufe_2_meter_minute,
        });
      await gießwagen_stufen.save(); 
      res.send(gießwagen_stufen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const gießwagen_stufen = await Gießwagen_Stufen.findById(req.params.id).populate('feld');
  	res.send(gießwagen_stufen);
  },
  async update(req, res){
    const gießwagen_stufen = await Gießwagen_Stufen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(gießwagen_stufen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let gießwagen_stufen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  gießwagen_stufen = await Gießwagen_Stufen.find().populate('feld').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async gießwagen_stufen => {
        await Gießwagen_Stufen.countDocuments().then(count => {
          res.send({
            items: gießwagen_stufen,
            total: count
          })
        })
      })
    }else{
      gießwagen_stufen = await Gießwagen_Stufen.find().populate('feld');
    }
  	return gießwagen_stufen;
  },
  async loadByID(id){
  	const gießwagen_stufen = await Gießwagen_Stufen.findById(id).populate('feld');
  	return gießwagen_stufen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const gießwagen_stufen = await Gießwagen_Stufen.find({ [searchSub]: req.params.id });
    res.send(gießwagen_stufen[0]);
  }
};

module.exports = Gießwagen_StufenController;
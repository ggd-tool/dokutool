const { SprühenNebeln_Gießwagen_Stufenlos } = require('../models-index');

const SprühenNebeln_Gießwagen_StufenlosController = {
  async index(req, res){
  	const sprühennebeln_gießwagen_stufenlos = await SprühenNebeln_Gießwagen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}});
  	res.send(sprühennebeln_gießwagen_stufenlos);
  },
  async prev(req, res) {
    const sprühennebeln_gießwagen_stufenlos = await SprühenNebeln_Gießwagen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).sort('-createdAt').limit(3);
  	res.send(sprühennebeln_gießwagen_stufenlos);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const sprühennebeln_gießwagen_stufenlos = new SprühenNebeln_Gießwagen_Stufenlos({
            'datum': req.body.datum,
            'gießwagen_stufenlos': req.body.gießwagen_stufenlos,
            'beete': req.body.beete,
            'einweghäufigkeit': req.body.einweghäufigkeit,
            'bewässerungssystem': req.body.bewässerungssystem,
            'geschwindigkeit_': req.body.geschwindigkeit_,
            'von_bis_in_': req.body.von_bis_in_,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await sprühennebeln_gießwagen_stufenlos.save(); 
      res.send(sprühennebeln_gießwagen_stufenlos);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const sprühennebeln_gießwagen_stufenlos = await SprühenNebeln_Gießwagen_Stufenlos.findById(req.params.id).populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}});
  	res.send(sprühennebeln_gießwagen_stufenlos);
  },
  async update(req, res){
    const sprühennebeln_gießwagen_stufenlos = await SprühenNebeln_Gießwagen_Stufenlos.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(sprühennebeln_gießwagen_stufenlos);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let sprühennebeln_gießwagen_stufenlos;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  sprühennebeln_gießwagen_stufenlos = await SprühenNebeln_Gießwagen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async sprühennebeln_gießwagen_stufenlos => {
        await SprühenNebeln_Gießwagen_Stufenlos.countDocuments().then(count => {
          res.send({
            items: sprühennebeln_gießwagen_stufenlos,
            total: count
          })
        })
      })
    }else{
      sprühennebeln_gießwagen_stufenlos = await SprühenNebeln_Gießwagen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}});
    }
  	return sprühennebeln_gießwagen_stufenlos;
  },
  async loadByID(id){
  	const sprühennebeln_gießwagen_stufenlos = await SprühenNebeln_Gießwagen_Stufenlos.findById(id).populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}});
  	return sprühennebeln_gießwagen_stufenlos;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const sprühennebeln_gießwagen_stufenlos = await SprühenNebeln_Gießwagen_Stufenlos.find({ [searchSub]: req.params.id });
    res.send(sprühennebeln_gießwagen_stufenlos[0]);
  }
};

module.exports = SprühenNebeln_Gießwagen_StufenlosController;
const { SprühenNebeln_Regner } = require('../models-index');

const SprühenNebeln_RegnerController = {
  async index(req, res){
  	const sprühennebeln_regner = await SprühenNebeln_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
  	res.send(sprühennebeln_regner);
  },
  async prev(req, res) {
    const sprühennebeln_regner = await SprühenNebeln_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).sort('-createdAt').limit(3);
  	res.send(sprühennebeln_regner);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const sprühennebeln_regner = new SprühenNebeln_Regner({
            'datum': req.body.datum,
            'regner': req.body.regner,
            'beete': req.body.beete,
            'laufzeit_in_min': req.body.laufzeit_in_min,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await sprühennebeln_regner.save(); 
      res.send(sprühennebeln_regner);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const sprühennebeln_regner = await SprühenNebeln_Regner.findById(req.params.id).populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
  	res.send(sprühennebeln_regner);
  },
  async update(req, res){
    const sprühennebeln_regner = await SprühenNebeln_Regner.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(sprühennebeln_regner);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let sprühennebeln_regner;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  sprühennebeln_regner = await SprühenNebeln_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async sprühennebeln_regner => {
        await SprühenNebeln_Regner.countDocuments().then(count => {
          res.send({
            items: sprühennebeln_regner,
            total: count
          })
        })
      })
    }else{
      sprühennebeln_regner = await SprühenNebeln_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
    }
  	return sprühennebeln_regner;
  },
  async loadByID(id){
  	const sprühennebeln_regner = await SprühenNebeln_Regner.findById(id).populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
  	return sprühennebeln_regner;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const sprühennebeln_regner = await SprühenNebeln_Regner.find({ [searchSub]: req.params.id });
    res.send(sprühennebeln_regner[0]);
  }
};

module.exports = SprühenNebeln_RegnerController;
const { Stammlösung_ansetzen } = require('../models-index');

const Stammlösung_ansetzenController = {
  async index(req, res){
  	const stammlösung_ansetzen = await Stammlösung_ansetzen.find().populate('stammlösungsbeckendüngemischung');
  	res.send(stammlösung_ansetzen);
  },
  async prev(req, res) {
    const stammlösung_ansetzen = await Stammlösung_ansetzen.find().populate('stammlösungsbeckendüngemischung').sort('-createdAt').limit(3);
  	res.send(stammlösung_ansetzen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const stammlösung_ansetzen = new Stammlösung_ansetzen({
            'datum': req.body.datum,
            'stammlösungsbecken_düngemischung': req.body.stammlösungsbecken_düngemischung,
            'wassermenge_in_l': req.body.wassermenge_in_l,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await stammlösung_ansetzen.save(); 
      res.send(stammlösung_ansetzen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const stammlösung_ansetzen = await Stammlösung_ansetzen.findById(req.params.id).populate('stammlösungsbeckendüngemischung');
  	res.send(stammlösung_ansetzen);
  },
  async update(req, res){
    const stammlösung_ansetzen = await Stammlösung_ansetzen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(stammlösung_ansetzen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let stammlösung_ansetzen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  stammlösung_ansetzen = await Stammlösung_ansetzen.find().populate('stammlösungsbeckendüngemischung').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async stammlösung_ansetzen => {
        await Stammlösung_ansetzen.countDocuments().then(count => {
          res.send({
            items: stammlösung_ansetzen,
            total: count
          })
        })
      })
    }else{
      stammlösung_ansetzen = await Stammlösung_ansetzen.find().populate('stammlösungsbeckendüngemischung');
    }
  	return stammlösung_ansetzen;
  },
  async loadByID(id){
  	const stammlösung_ansetzen = await Stammlösung_ansetzen.findById(id).populate('stammlösungsbeckendüngemischung');
  	return stammlösung_ansetzen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const stammlösung_ansetzen = await Stammlösung_ansetzen.find({ [searchSub]: req.params.id });
    res.send(stammlösung_ansetzen[0]);
  }
};

module.exports = Stammlösung_ansetzenController;
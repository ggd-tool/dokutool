const { Umstellen } = require('../models-index');

const UmstellenController = {
  async index(req, res){
  	const umstellen = await Umstellen.find().populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(umstellen);
  },
  async prev(req, res) {
    const umstellen = await Umstellen.find().populate({ path: 'feld'}).populate({ path: 'feld'}).sort('-createdAt').limit(3);
  	res.send(umstellen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const umstellen = new Umstellen({
            'datum': req.body.datum,
            'auftragsnummer': req.body.auftragsnummer,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await umstellen.save(); 
      res.send(umstellen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const umstellen = await Umstellen.findById(req.params.id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(umstellen);
  },
  async update(req, res){
    const umstellen = await Umstellen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(umstellen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let umstellen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  umstellen = await Umstellen.find().populate({ path: 'feld'}).populate({ path: 'feld'}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async umstellen => {
        await Umstellen.countDocuments().then(count => {
          res.send({
            items: umstellen,
            total: count
          })
        })
      })
    }else{
      umstellen = await Umstellen.find().populate({ path: 'feld'}).populate({ path: 'feld'});
    }
  	return umstellen;
  },
  async loadByID(id){
  	const umstellen = await Umstellen.findById(id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	return umstellen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const umstellen = await Umstellen.find({ [searchSub]: req.params.id });
    res.send(umstellen[0]);
  }
};

module.exports = UmstellenController;
const { Pikierenzusammenpikieren } = require('../models-index');

const PikierenzusammenpikierenController = {
  async index(req, res){
  	const pikierenzusammenpikieren = await Pikierenzusammenpikieren.find().populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(pikierenzusammenpikieren);
  },
  async prev(req, res) {
    const pikierenzusammenpikieren = await Pikierenzusammenpikieren.find().populate({ path: 'feld'}).populate({ path: 'feld'}).sort('-createdAt').limit(3);
  	res.send(pikierenzusammenpikieren);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const pikierenzusammenpikieren = new Pikierenzusammenpikieren({
            'datum': req.body.datum,
            'auftragsnummer': req.body.auftragsnummer,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await pikierenzusammenpikieren.save(); 
      res.send(pikierenzusammenpikieren);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const pikierenzusammenpikieren = await Pikierenzusammenpikieren.findById(req.params.id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(pikierenzusammenpikieren);
  },
  async update(req, res){
    const pikierenzusammenpikieren = await Pikierenzusammenpikieren.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(pikierenzusammenpikieren);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let pikierenzusammenpikieren;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  pikierenzusammenpikieren = await Pikierenzusammenpikieren.find().populate({ path: 'feld'}).populate({ path: 'feld'}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async pikierenzusammenpikieren => {
        await Pikierenzusammenpikieren.countDocuments().then(count => {
          res.send({
            items: pikierenzusammenpikieren,
            total: count
          })
        })
      })
    }else{
      pikierenzusammenpikieren = await Pikierenzusammenpikieren.find().populate({ path: 'feld'}).populate({ path: 'feld'});
    }
  	return pikierenzusammenpikieren;
  },
  async loadByID(id){
  	const pikierenzusammenpikieren = await Pikierenzusammenpikieren.findById(id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	return pikierenzusammenpikieren;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const pikierenzusammenpikieren = await Pikierenzusammenpikieren.find({ [searchSub]: req.params.id });
    res.send(pikierenzusammenpikieren[0]);
  }
};

module.exports = PikierenzusammenpikierenController;
const { Pflanzenstärkung_Gießen_Stufenlos } = require('../models-index');

const Pflanzenstärkung_Gießen_StufenlosController = {
  async index(req, res){
  	const pflanzenstärkung_gießen_stufenlos = await Pflanzenstärkung_Gießen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenstärkungsmittel');
  	res.send(pflanzenstärkung_gießen_stufenlos);
  },
  async prev(req, res) {
    const pflanzenstärkung_gießen_stufenlos = await Pflanzenstärkung_Gießen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenstärkungsmittel').sort('-createdAt').limit(3);
  	res.send(pflanzenstärkung_gießen_stufenlos);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const pflanzenstärkung_gießen_stufenlos = new Pflanzenstärkung_Gießen_Stufenlos({
            'datum': req.body.datum,
            'gießwagen_stufenlos': req.body.gießwagen_stufenlos,
            'beete': req.body.beete,
            'einweghäufigkeit': req.body.einweghäufigkeit,
            'geschwindigkeit_': req.body.geschwindigkeit_,
            'von_bis_in_': req.body.von_bis_in_,
            'pflanzenstärkungsmittel': req.body.pflanzenstärkungsmittel,
            'mittelmenge_l_ha': req.body.mittelmenge_l_ha,
            'wassermenge_l_m2': req.body.wassermenge_l_m2,
            'prozentanteil': req.body.prozentanteil,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await pflanzenstärkung_gießen_stufenlos.save(); 
      res.send(pflanzenstärkung_gießen_stufenlos);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const pflanzenstärkung_gießen_stufenlos = await Pflanzenstärkung_Gießen_Stufenlos.findById(req.params.id).populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenstärkungsmittel');
  	res.send(pflanzenstärkung_gießen_stufenlos);
  },
  async update(req, res){
    const pflanzenstärkung_gießen_stufenlos = await Pflanzenstärkung_Gießen_Stufenlos.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(pflanzenstärkung_gießen_stufenlos);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let pflanzenstärkung_gießen_stufenlos;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  pflanzenstärkung_gießen_stufenlos = await Pflanzenstärkung_Gießen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenstärkungsmittel').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async pflanzenstärkung_gießen_stufenlos => {
        await Pflanzenstärkung_Gießen_Stufenlos.countDocuments().then(count => {
          res.send({
            items: pflanzenstärkung_gießen_stufenlos,
            total: count
          })
        })
      })
    }else{
      pflanzenstärkung_gießen_stufenlos = await Pflanzenstärkung_Gießen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenstärkungsmittel');
    }
  	return pflanzenstärkung_gießen_stufenlos;
  },
  async loadByID(id){
  	const pflanzenstärkung_gießen_stufenlos = await Pflanzenstärkung_Gießen_Stufenlos.findById(id).populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenstärkungsmittel');
  	return pflanzenstärkung_gießen_stufenlos;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const pflanzenstärkung_gießen_stufenlos = await Pflanzenstärkung_Gießen_Stufenlos.find({ [searchSub]: req.params.id });
    res.send(pflanzenstärkung_gießen_stufenlos[0]);
  }
};

module.exports = Pflanzenstärkung_Gießen_StufenlosController;
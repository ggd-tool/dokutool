const { manuell_Gießen } = require('../models-index');

const manuell_GießenController = {
  async index(req, res){
  	const manuell_gießen = await manuell_Gießen.find().populate({ path: 'feld'});
  	res.send(manuell_gießen);
  },
  async prev(req, res) {
    const manuell_gießen = await manuell_Gießen.find().populate({ path: 'feld'}).sort('-createdAt').limit(3);
  	res.send(manuell_gießen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const manuell_gießen = new manuell_Gießen({
            'datum': req.body.datum,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await manuell_gießen.save(); 
      res.send(manuell_gießen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const manuell_gießen = await manuell_Gießen.findById(req.params.id).populate({ path: 'feld'});
  	res.send(manuell_gießen);
  },
  async update(req, res){
    const manuell_gießen = await manuell_Gießen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(manuell_gießen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let manuell_gießen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  manuell_gießen = await manuell_Gießen.find().populate({ path: 'feld'}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async manuell_gießen => {
        await manuell_Gießen.countDocuments().then(count => {
          res.send({
            items: manuell_gießen,
            total: count
          })
        })
      })
    }else{
      manuell_gießen = await manuell_Gießen.find().populate({ path: 'feld'});
    }
  	return manuell_gießen;
  },
  async loadByID(id){
  	const manuell_gießen = await manuell_Gießen.findById(id).populate({ path: 'feld'});
  	return manuell_gießen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const manuell_gießen = await manuell_Gießen.find({ [searchSub]: req.params.id });
    res.send(manuell_gießen[0]);
  }
};

module.exports = manuell_GießenController;
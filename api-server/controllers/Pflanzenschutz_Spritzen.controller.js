const { Pflanzenschutz_Spritzen } = require('../models-index');

const Pflanzenschutz_SpritzenController = {
  async index(req, res){
  	const pflanzenschutz_spritzen = await Pflanzenschutz_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenschutzmittel');
  	res.send(pflanzenschutz_spritzen);
  },
  async prev(req, res) {
    const pflanzenschutz_spritzen = await Pflanzenschutz_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenschutzmittel').sort('-createdAt').limit(3);
  	res.send(pflanzenschutz_spritzen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const pflanzenschutz_spritzen = new Pflanzenschutz_Spritzen({
            'datum': req.body.datum,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'ausbringung': req.body.ausbringung,
            'pflanzenschutzmittel': req.body.pflanzenschutzmittel,
            'mittelmenge_l_ha': req.body.mittelmenge_l_ha,
            'wassermenge_l_m2': req.body.wassermenge_l_m2,
            'prozentanteil': req.body.prozentanteil,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await pflanzenschutz_spritzen.save(); 
      res.send(pflanzenschutz_spritzen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const pflanzenschutz_spritzen = await Pflanzenschutz_Spritzen.findById(req.params.id).populate({ path: 'feld'}).populate('pflanzenschutzmittel');
  	res.send(pflanzenschutz_spritzen);
  },
  async update(req, res){
    const pflanzenschutz_spritzen = await Pflanzenschutz_Spritzen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(pflanzenschutz_spritzen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let pflanzenschutz_spritzen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  pflanzenschutz_spritzen = await Pflanzenschutz_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenschutzmittel').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async pflanzenschutz_spritzen => {
        await Pflanzenschutz_Spritzen.countDocuments().then(count => {
          res.send({
            items: pflanzenschutz_spritzen,
            total: count
          })
        })
      })
    }else{
      pflanzenschutz_spritzen = await Pflanzenschutz_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenschutzmittel');
    }
  	return pflanzenschutz_spritzen;
  },
  async loadByID(id){
  	const pflanzenschutz_spritzen = await Pflanzenschutz_Spritzen.findById(id).populate({ path: 'feld'}).populate('pflanzenschutzmittel');
  	return pflanzenschutz_spritzen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const pflanzenschutz_spritzen = await Pflanzenschutz_Spritzen.find({ [searchSub]: req.params.id });
    res.send(pflanzenschutz_spritzen[0]);
  }
};

module.exports = Pflanzenschutz_SpritzenController;
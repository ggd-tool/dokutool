const { Preset_GießenDüngen_Regner } = require('../models-index');

const Preset_GießenDüngen_RegnerController = {
  async index(req, res){
  	const preset_gießendüngen_regner = await Preset_GießenDüngen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	res.send(preset_gießendüngen_regner);
  },
  async prev(req, res) {
    const preset_gießendüngen_regner = await Preset_GießenDüngen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung').sort('-createdAt').limit(3);
  	res.send(preset_gießendüngen_regner);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_gießendüngen_regner = new Preset_GießenDüngen_Regner({
            'name': req.body.name,
            'regner': req.body.regner,
            'beete': req.body.beete,
            'zeit_in_min': req.body.zeit_in_min,
            'von_bis_in_': req.body.von_bis_in_,
            'stammlösungsbecken_düngemischung': req.body.stammlösungsbecken_düngemischung,
            'konzentration_ec': req.body.konzentration_ec,
            'konzentration_g_l': req.body.konzentration_g_l,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_gießendüngen_regner.save(); 
      res.send(preset_gießendüngen_regner);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_gießendüngen_regner = await Preset_GießenDüngen_Regner.findById(req.params.id).populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	res.send(preset_gießendüngen_regner);
  },
  async update(req, res){
    const preset_gießendüngen_regner = await Preset_GießenDüngen_Regner.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_gießendüngen_regner);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_gießendüngen_regner;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_gießendüngen_regner = await Preset_GießenDüngen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_gießendüngen_regner => {
        await Preset_GießenDüngen_Regner.countDocuments().then(count => {
          res.send({
            items: preset_gießendüngen_regner,
            total: count
          })
        })
      })
    }else{
      preset_gießendüngen_regner = await Preset_GießenDüngen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
    }
  	return preset_gießendüngen_regner;
  },
  async loadByID(id){
  	const preset_gießendüngen_regner = await Preset_GießenDüngen_Regner.findById(id).populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	return preset_gießendüngen_regner;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_gießendüngen_regner = await Preset_GießenDüngen_Regner.find({ [searchSub]: req.params.id });
    res.send(preset_gießendüngen_regner[0]);
  }
};

module.exports = Preset_GießenDüngen_RegnerController;
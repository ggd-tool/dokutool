const { Preset_manuell_Gießen } = require('../models-index');

const Preset_manuell_GießenController = {
  async index(req, res){
  	const preset_manuell_gießen = await Preset_manuell_Gießen.find().populate({ path: 'feld'});
  	res.send(preset_manuell_gießen);
  },
  async prev(req, res) {
    const preset_manuell_gießen = await Preset_manuell_Gießen.find().populate({ path: 'feld'}).sort('-createdAt').limit(3);
  	res.send(preset_manuell_gießen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_manuell_gießen = new Preset_manuell_Gießen({
            'name': req.body.name,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_manuell_gießen.save(); 
      res.send(preset_manuell_gießen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_manuell_gießen = await Preset_manuell_Gießen.findById(req.params.id).populate({ path: 'feld'});
  	res.send(preset_manuell_gießen);
  },
  async update(req, res){
    const preset_manuell_gießen = await Preset_manuell_Gießen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_manuell_gießen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_manuell_gießen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_manuell_gießen = await Preset_manuell_Gießen.find().populate({ path: 'feld'}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_manuell_gießen => {
        await Preset_manuell_Gießen.countDocuments().then(count => {
          res.send({
            items: preset_manuell_gießen,
            total: count
          })
        })
      })
    }else{
      preset_manuell_gießen = await Preset_manuell_Gießen.find().populate({ path: 'feld'});
    }
  	return preset_manuell_gießen;
  },
  async loadByID(id){
  	const preset_manuell_gießen = await Preset_manuell_Gießen.findById(id).populate({ path: 'feld'});
  	return preset_manuell_gießen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_manuell_gießen = await Preset_manuell_Gießen.find({ [searchSub]: req.params.id });
    res.send(preset_manuell_gießen[0]);
  }
};

module.exports = Preset_manuell_GießenController;
const { Preset_Pflanzenstärkung_Spritzen } = require('../models-index');

const Preset_Pflanzenstärkung_SpritzenController = {
  async index(req, res){
  	const preset_pflanzenstärkung_spritzen = await Preset_Pflanzenstärkung_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenstärkungsmittel');
  	res.send(preset_pflanzenstärkung_spritzen);
  },
  async prev(req, res) {
    const preset_pflanzenstärkung_spritzen = await Preset_Pflanzenstärkung_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenstärkungsmittel').sort('-createdAt').limit(3);
  	res.send(preset_pflanzenstärkung_spritzen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_pflanzenstärkung_spritzen = new Preset_Pflanzenstärkung_Spritzen({
            'name': req.body.name,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'ausbringung': req.body.ausbringung,
            'pflanzenstärkungsmittel': req.body.pflanzenstärkungsmittel,
            'mittelmenge_l_ha': req.body.mittelmenge_l_ha,
            'wassermenge_l_ha': req.body.wassermenge_l_ha,
            'prozentanteil': req.body.prozentanteil,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_pflanzenstärkung_spritzen.save(); 
      res.send(preset_pflanzenstärkung_spritzen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_pflanzenstärkung_spritzen = await Preset_Pflanzenstärkung_Spritzen.findById(req.params.id).populate({ path: 'feld'}).populate('pflanzenstärkungsmittel');
  	res.send(preset_pflanzenstärkung_spritzen);
  },
  async update(req, res){
    const preset_pflanzenstärkung_spritzen = await Preset_Pflanzenstärkung_Spritzen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_pflanzenstärkung_spritzen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_pflanzenstärkung_spritzen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_pflanzenstärkung_spritzen = await Preset_Pflanzenstärkung_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenstärkungsmittel').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_pflanzenstärkung_spritzen => {
        await Preset_Pflanzenstärkung_Spritzen.countDocuments().then(count => {
          res.send({
            items: preset_pflanzenstärkung_spritzen,
            total: count
          })
        })
      })
    }else{
      preset_pflanzenstärkung_spritzen = await Preset_Pflanzenstärkung_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenstärkungsmittel');
    }
  	return preset_pflanzenstärkung_spritzen;
  },
  async loadByID(id){
  	const preset_pflanzenstärkung_spritzen = await Preset_Pflanzenstärkung_Spritzen.findById(id).populate({ path: 'feld'}).populate('pflanzenstärkungsmittel');
  	return preset_pflanzenstärkung_spritzen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_pflanzenstärkung_spritzen = await Preset_Pflanzenstärkung_Spritzen.find({ [searchSub]: req.params.id });
    res.send(preset_pflanzenstärkung_spritzen[0]);
  }
};

module.exports = Preset_Pflanzenstärkung_SpritzenController;
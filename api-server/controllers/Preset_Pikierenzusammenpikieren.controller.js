const { Preset_Pikierenzusammenpikieren } = require('../models-index');

const Preset_PikierenzusammenpikierenController = {
  async index(req, res){
  	const preset_pikierenzusammenpikieren = await Preset_Pikierenzusammenpikieren.find().populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(preset_pikierenzusammenpikieren);
  },
  async prev(req, res) {
    const preset_pikierenzusammenpikieren = await Preset_Pikierenzusammenpikieren.find().populate({ path: 'feld'}).populate({ path: 'feld'}).sort('-createdAt').limit(3);
  	res.send(preset_pikierenzusammenpikieren);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_pikierenzusammenpikieren = new Preset_Pikierenzusammenpikieren({
            'name': req.body.name,
            'auftragsnummer': req.body.auftragsnummer,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_pikierenzusammenpikieren.save(); 
      res.send(preset_pikierenzusammenpikieren);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_pikierenzusammenpikieren = await Preset_Pikierenzusammenpikieren.findById(req.params.id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(preset_pikierenzusammenpikieren);
  },
  async update(req, res){
    const preset_pikierenzusammenpikieren = await Preset_Pikierenzusammenpikieren.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_pikierenzusammenpikieren);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_pikierenzusammenpikieren;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_pikierenzusammenpikieren = await Preset_Pikierenzusammenpikieren.find().populate({ path: 'feld'}).populate({ path: 'feld'}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_pikierenzusammenpikieren => {
        await Preset_Pikierenzusammenpikieren.countDocuments().then(count => {
          res.send({
            items: preset_pikierenzusammenpikieren,
            total: count
          })
        })
      })
    }else{
      preset_pikierenzusammenpikieren = await Preset_Pikierenzusammenpikieren.find().populate({ path: 'feld'}).populate({ path: 'feld'});
    }
  	return preset_pikierenzusammenpikieren;
  },
  async loadByID(id){
  	const preset_pikierenzusammenpikieren = await Preset_Pikierenzusammenpikieren.findById(id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	return preset_pikierenzusammenpikieren;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_pikierenzusammenpikieren = await Preset_Pikierenzusammenpikieren.find({ [searchSub]: req.params.id });
    res.send(preset_pikierenzusammenpikieren[0]);
  }
};

module.exports = Preset_PikierenzusammenpikierenController;
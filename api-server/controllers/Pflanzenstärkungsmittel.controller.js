const { Pflanzenstärkungsmittel } = require('../models-index');

const PflanzenstärkungsmittelController = {
  async index(req, res){
  	const pflanzenstärkungsmittel = await Pflanzenstärkungsmittel.find();
  	res.send(pflanzenstärkungsmittel);
  },
  async prev(req, res) {
    const pflanzenstärkungsmittel = await Pflanzenstärkungsmittel.find().sort('-createdAt').limit(3);
  	res.send(pflanzenstärkungsmittel);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const pflanzenstärkungsmittel = new Pflanzenstärkungsmittel({
            'name': req.body.name,
            'wirkstoffe': req.body.wirkstoffe,
            'bemerkung': req.body.bemerkung,
        });
      await pflanzenstärkungsmittel.save(); 
      res.send(pflanzenstärkungsmittel);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const pflanzenstärkungsmittel = await Pflanzenstärkungsmittel.findById(req.params.id);
  	res.send(pflanzenstärkungsmittel);
  },
  async update(req, res){
    const pflanzenstärkungsmittel = await Pflanzenstärkungsmittel.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(pflanzenstärkungsmittel);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let pflanzenstärkungsmittel;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  pflanzenstärkungsmittel = await Pflanzenstärkungsmittel.find().skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async pflanzenstärkungsmittel => {
        await Pflanzenstärkungsmittel.countDocuments().then(count => {
          res.send({
            items: pflanzenstärkungsmittel,
            total: count
          })
        })
      })
    }else{
      pflanzenstärkungsmittel = await Pflanzenstärkungsmittel.find();
    }
  	return pflanzenstärkungsmittel;
  },
  async loadByID(id){
  	const pflanzenstärkungsmittel = await Pflanzenstärkungsmittel.findById(id);
  	return pflanzenstärkungsmittel;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const pflanzenstärkungsmittel = await Pflanzenstärkungsmittel.find({ [searchSub]: req.params.id });
    res.send(pflanzenstärkungsmittel[0]);
  }
};

module.exports = PflanzenstärkungsmittelController;
const { Pflanzenschutzmittel } = require('../models-index');

const PflanzenschutzmittelController = {
  async index(req, res){
  	const pflanzenschutzmittel = await Pflanzenschutzmittel.find();
  	res.send(pflanzenschutzmittel);
  },
  async prev(req, res) {
    const pflanzenschutzmittel = await Pflanzenschutzmittel.find().sort('-createdAt').limit(3);
  	res.send(pflanzenschutzmittel);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const pflanzenschutzmittel = new Pflanzenschutzmittel({
            'name': req.body.name,
            'zulassungsnummer': req.body.zulassungsnummer,
            'zugelassen_bis': req.body.zugelassen_bis,
            'anwendung_bis': req.body.anwendung_bis,
            'wirkstoffe': req.body.wirkstoffe,
            'gefahrenstoffeinstufung': req.body.gefahrenstoffeinstufung,
            'bienengefährdungsstufe': req.body.bienengefährdungsstufe,
            'indikation': req.body.indikation,
            'unter_glas': req.body.unter_glas,
            'freiland': req.body.freiland,
            'maximale_anzahl': req.body.maximale_anzahl,
            'wartezeit_tage': req.body.wartezeit_tage,
            'bis_50cm_liter_ha': req.body.bis_50cm_liter_ha,
            'für_50_125cm_liter_ha': req.body.für_50_125cm_liter_ha,
            'ab_125cm_liter_ha': req.body.ab_125cm_liter_ha,
            'sf': req.body.sf,
            'ng': req.body.ng,
            'nw': req.body.nw,
            'ns': req.body.ns,
            'nt': req.body.nt,
            'nz': req.body.nz,
            'vz': req.body.vz,
        });
      await pflanzenschutzmittel.save(); 
      res.send(pflanzenschutzmittel);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const pflanzenschutzmittel = await Pflanzenschutzmittel.findById(req.params.id);
  	res.send(pflanzenschutzmittel);
  },
  async update(req, res){
    const pflanzenschutzmittel = await Pflanzenschutzmittel.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(pflanzenschutzmittel);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let pflanzenschutzmittel;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  pflanzenschutzmittel = await Pflanzenschutzmittel.find().skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async pflanzenschutzmittel => {
        await Pflanzenschutzmittel.countDocuments().then(count => {
          res.send({
            items: pflanzenschutzmittel,
            total: count
          })
        })
      })
    }else{
      pflanzenschutzmittel = await Pflanzenschutzmittel.find();
    }
  	return pflanzenschutzmittel;
  },
  async loadByID(id){
  	const pflanzenschutzmittel = await Pflanzenschutzmittel.findById(id);
  	return pflanzenschutzmittel;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const pflanzenschutzmittel = await Pflanzenschutzmittel.find({ [searchSub]: req.params.id });
    res.send(pflanzenschutzmittel[0]);
  }
};

module.exports = PflanzenschutzmittelController;
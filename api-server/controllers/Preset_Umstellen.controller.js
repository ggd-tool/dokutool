const { Preset_Umstellen } = require('../models-index');

const Preset_UmstellenController = {
  async index(req, res){
  	const preset_umstellen = await Preset_Umstellen.find().populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(preset_umstellen);
  },
  async prev(req, res) {
    const preset_umstellen = await Preset_Umstellen.find().populate({ path: 'feld'}).populate({ path: 'feld'}).sort('-createdAt').limit(3);
  	res.send(preset_umstellen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_umstellen = new Preset_Umstellen({
            'name': req.body.name,
            'auftragsnummer': req.body.auftragsnummer,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_umstellen.save(); 
      res.send(preset_umstellen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_umstellen = await Preset_Umstellen.findById(req.params.id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(preset_umstellen);
  },
  async update(req, res){
    const preset_umstellen = await Preset_Umstellen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_umstellen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_umstellen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_umstellen = await Preset_Umstellen.find().populate({ path: 'feld'}).populate({ path: 'feld'}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_umstellen => {
        await Preset_Umstellen.countDocuments().then(count => {
          res.send({
            items: preset_umstellen,
            total: count
          })
        })
      })
    }else{
      preset_umstellen = await Preset_Umstellen.find().populate({ path: 'feld'}).populate({ path: 'feld'});
    }
  	return preset_umstellen;
  },
  async loadByID(id){
  	const preset_umstellen = await Preset_Umstellen.findById(id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	return preset_umstellen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_umstellen = await Preset_Umstellen.find({ [searchSub]: req.params.id });
    res.send(preset_umstellen[0]);
  }
};

module.exports = Preset_UmstellenController;
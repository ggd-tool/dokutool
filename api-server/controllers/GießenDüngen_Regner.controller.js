const { GießenDüngen_Regner } = require('../models-index');

const GießenDüngen_RegnerController = {
  async index(req, res){
  	const gießendüngen_regner = await GießenDüngen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	res.send(gießendüngen_regner);
  },
  async prev(req, res) {
    const gießendüngen_regner = await GießenDüngen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung').sort('-createdAt').limit(3);
  	res.send(gießendüngen_regner);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const gießendüngen_regner = new GießenDüngen_Regner({
            'datum': req.body.datum,
            'regner': req.body.regner,
            'beete': req.body.beete,
            'zeit_in_min': req.body.zeit_in_min,
            'von_bis_in_': req.body.von_bis_in_,
            'stammlösungsbecken_düngemischung': req.body.stammlösungsbecken_düngemischung,
            'konzentration_ec': req.body.konzentration_ec,
            'konzentration_g_l': req.body.konzentration_g_l,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await gießendüngen_regner.save(); 
      res.send(gießendüngen_regner);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const gießendüngen_regner = await GießenDüngen_Regner.findById(req.params.id).populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	res.send(gießendüngen_regner);
  },
  async update(req, res){
    const gießendüngen_regner = await GießenDüngen_Regner.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(gießendüngen_regner);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let gießendüngen_regner;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  gießendüngen_regner = await GießenDüngen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async gießendüngen_regner => {
        await GießenDüngen_Regner.countDocuments().then(count => {
          res.send({
            items: gießendüngen_regner,
            total: count
          })
        })
      })
    }else{
      gießendüngen_regner = await GießenDüngen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
    }
  	return gießendüngen_regner;
  },
  async loadByID(id){
  	const gießendüngen_regner = await GießenDüngen_Regner.findById(id).populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	return gießendüngen_regner;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const gießendüngen_regner = await GießenDüngen_Regner.find({ [searchSub]: req.params.id });
    res.send(gießendüngen_regner[0]);
  }
};

module.exports = GießenDüngen_RegnerController;
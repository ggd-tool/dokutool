const { Stecken } = require('../models-index');

const SteckenController = {
  async index(req, res){
  	const stecken = await Stecken.find().populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(stecken);
  },
  async prev(req, res) {
    const stecken = await Stecken.find().populate({ path: 'feld'}).populate({ path: 'feld'}).sort('-createdAt').limit(3);
  	res.send(stecken);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const stecken = new Stecken({
            'datum': req.body.datum,
            'auftragsnummer': req.body.auftragsnummer,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await stecken.save(); 
      res.send(stecken);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const stecken = await Stecken.findById(req.params.id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(stecken);
  },
  async update(req, res){
    const stecken = await Stecken.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(stecken);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let stecken;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  stecken = await Stecken.find().populate({ path: 'feld'}).populate({ path: 'feld'}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async stecken => {
        await Stecken.countDocuments().then(count => {
          res.send({
            items: stecken,
            total: count
          })
        })
      })
    }else{
      stecken = await Stecken.find().populate({ path: 'feld'}).populate({ path: 'feld'});
    }
  	return stecken;
  },
  async loadByID(id){
  	const stecken = await Stecken.findById(id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	return stecken;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const stecken = await Stecken.find({ [searchSub]: req.params.id });
    res.send(stecken[0]);
  }
};

module.exports = SteckenController;
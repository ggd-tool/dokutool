const { Pflanzenschutz_Gießen_Stufen } = require('../models-index');

const Pflanzenschutz_Gießen_StufenController = {
  async index(req, res){
  	const pflanzenschutz_gießen_stufen = await Pflanzenschutz_Gießen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel');
  	res.send(pflanzenschutz_gießen_stufen);
  },
  async prev(req, res) {
    const pflanzenschutz_gießen_stufen = await Pflanzenschutz_Gießen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel').sort('-createdAt').limit(3);
  	res.send(pflanzenschutz_gießen_stufen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const pflanzenschutz_gießen_stufen = new Pflanzenschutz_Gießen_Stufen({
            'datum': req.body.datum,
            'gießwagen_stufen': req.body.gießwagen_stufen,
            'beete': req.body.beete,
            'einweghäufigkeit': req.body.einweghäufigkeit,
            'ausbringung': req.body.ausbringung,
            'stufe': req.body.stufe,
            'von_bis_in_': req.body.von_bis_in_,
            'pflanzenschutzmittel': req.body.pflanzenschutzmittel,
            'mittelmenge_l_ha': req.body.mittelmenge_l_ha,
            'wassermenge_l_m2': req.body.wassermenge_l_m2,
            'prozentanteil': req.body.prozentanteil,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await pflanzenschutz_gießen_stufen.save(); 
      res.send(pflanzenschutz_gießen_stufen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const pflanzenschutz_gießen_stufen = await Pflanzenschutz_Gießen_Stufen.findById(req.params.id).populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel');
  	res.send(pflanzenschutz_gießen_stufen);
  },
  async update(req, res){
    const pflanzenschutz_gießen_stufen = await Pflanzenschutz_Gießen_Stufen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(pflanzenschutz_gießen_stufen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let pflanzenschutz_gießen_stufen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  pflanzenschutz_gießen_stufen = await Pflanzenschutz_Gießen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async pflanzenschutz_gießen_stufen => {
        await Pflanzenschutz_Gießen_Stufen.countDocuments().then(count => {
          res.send({
            items: pflanzenschutz_gießen_stufen,
            total: count
          })
        })
      })
    }else{
      pflanzenschutz_gießen_stufen = await Pflanzenschutz_Gießen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel');
    }
  	return pflanzenschutz_gießen_stufen;
  },
  async loadByID(id){
  	const pflanzenschutz_gießen_stufen = await Pflanzenschutz_Gießen_Stufen.findById(id).populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel');
  	return pflanzenschutz_gießen_stufen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const pflanzenschutz_gießen_stufen = await Pflanzenschutz_Gießen_Stufen.find({ [searchSub]: req.params.id });
    res.send(pflanzenschutz_gießen_stufen[0]);
  }
};

module.exports = Pflanzenschutz_Gießen_StufenController;
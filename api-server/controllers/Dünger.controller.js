const { Dünger } = require('../models-index');

const DüngerController = {
  async index(req, res){
  	const dünger = await Dünger.find();
  	res.send(dünger);
  },
  async prev(req, res) {
    const dünger = await Dünger.find().sort('-createdAt').limit(3);
  	res.send(dünger);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const dünger = new Dünger({
            'name': req.body.name,
            'harnstoff': req.body.harnstoff,
            'no3': req.body.no3,
            'nh4': req.body.nh4,
            'gesamt_n': req.body.gesamt_n,
            'p2o5': req.body.p2o5,
            'k2o': req.body.k2o,
            'mgo': req.body.mgo,
            'cao': req.body.cao,
            'so3': req.body.so3,
            'fe': req.body.fe,
            'mn': req.body.mn,
            'zn': req.body.zn,
            'b': req.body.b,
            'cu': req.body.cu,
            'mo': req.body.mo,
            'al': req.body.al,
            'dichte': req.body.dichte,
            'für_05_ec': req.body.für_05_ec,
            'für_10_ec': req.body.für_10_ec,
            'für_15_ec': req.body.für_15_ec,
            'für_20_ec': req.body.für_20_ec,
            'für_1_ec': req.body.für_1_ec,
            'für_5_ec': req.body.für_5_ec,
            'für_100_ec': req.body.für_100_ec,
            'für_200_ec': req.body.für_200_ec,
            'flüssigdünger_100_ec': req.body.flüssigdünger_100_ec,
        });
      await dünger.save(); 
      res.send(dünger);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const dünger = await Dünger.findById(req.params.id);
  	res.send(dünger);
  },
  async update(req, res){
    const dünger = await Dünger.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(dünger);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let dünger;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  dünger = await Dünger.find().skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async dünger => {
        await Dünger.countDocuments().then(count => {
          res.send({
            items: dünger,
            total: count
          })
        })
      })
    }else{
      dünger = await Dünger.find();
    }
  	return dünger;
  },
  async loadByID(id){
  	const dünger = await Dünger.findById(id);
  	return dünger;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const dünger = await Dünger.find({ [searchSub]: req.params.id });
    res.send(dünger[0]);
  }
};

module.exports = DüngerController;
const { Preset_SprühenNebeln_Gießwagen_Stufenlos } = require('../models-index');

const Preset_SprühenNebeln_Gießwagen_StufenlosController = {
  async index(req, res){
  	const preset_sprühennebeln_gießwagen_stufenlos = await Preset_SprühenNebeln_Gießwagen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}});
  	res.send(preset_sprühennebeln_gießwagen_stufenlos);
  },
  async prev(req, res) {
    const preset_sprühennebeln_gießwagen_stufenlos = await Preset_SprühenNebeln_Gießwagen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).sort('-createdAt').limit(3);
  	res.send(preset_sprühennebeln_gießwagen_stufenlos);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_sprühennebeln_gießwagen_stufenlos = new Preset_SprühenNebeln_Gießwagen_Stufenlos({
            'name': req.body.name,
            'gießwagen_stufenlos': req.body.gießwagen_stufenlos,
            'beete': req.body.beete,
            'einweghäufigkeit': req.body.einweghäufigkeit,
            'bewässerungssystem': req.body.bewässerungssystem,
            'geschwindigkeit_': req.body.geschwindigkeit_,
            'von_bis_in_': req.body.von_bis_in_,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_sprühennebeln_gießwagen_stufenlos.save(); 
      res.send(preset_sprühennebeln_gießwagen_stufenlos);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_sprühennebeln_gießwagen_stufenlos = await Preset_SprühenNebeln_Gießwagen_Stufenlos.findById(req.params.id).populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}});
  	res.send(preset_sprühennebeln_gießwagen_stufenlos);
  },
  async update(req, res){
    const preset_sprühennebeln_gießwagen_stufenlos = await Preset_SprühenNebeln_Gießwagen_Stufenlos.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_sprühennebeln_gießwagen_stufenlos);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_sprühennebeln_gießwagen_stufenlos;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_sprühennebeln_gießwagen_stufenlos = await Preset_SprühenNebeln_Gießwagen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_sprühennebeln_gießwagen_stufenlos => {
        await Preset_SprühenNebeln_Gießwagen_Stufenlos.countDocuments().then(count => {
          res.send({
            items: preset_sprühennebeln_gießwagen_stufenlos,
            total: count
          })
        })
      })
    }else{
      preset_sprühennebeln_gießwagen_stufenlos = await Preset_SprühenNebeln_Gießwagen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}});
    }
  	return preset_sprühennebeln_gießwagen_stufenlos;
  },
  async loadByID(id){
  	const preset_sprühennebeln_gießwagen_stufenlos = await Preset_SprühenNebeln_Gießwagen_Stufenlos.findById(id).populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}});
  	return preset_sprühennebeln_gießwagen_stufenlos;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_sprühennebeln_gießwagen_stufenlos = await Preset_SprühenNebeln_Gießwagen_Stufenlos.find({ [searchSub]: req.params.id });
    res.send(preset_sprühennebeln_gießwagen_stufenlos[0]);
  }
};

module.exports = Preset_SprühenNebeln_Gießwagen_StufenlosController;
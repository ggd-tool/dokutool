const { Platte } = require('../models-index');

const PlatteController = {
  async index(req, res){
  	const platte = await Platte.find();
  	res.send(platte);
  },
  async prev(req, res) {
    const platte = await Platte.find().sort('-createdAt').limit(3);
  	res.send(platte);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const platte = new Platte({
            'name': req.body.name,
            'länge_zentimeter': req.body.länge_zentimeter,
            'breite_zentimeter': req.body.breite_zentimeter,
            'zellen': req.body.zellen,
            'volumen_liter': req.body.volumen_liter,
        });
      await platte.save(); 
      res.send(platte);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const platte = await Platte.findById(req.params.id);
  	res.send(platte);
  },
  async update(req, res){
    const platte = await Platte.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(platte);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let platte;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  platte = await Platte.find().skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async platte => {
        await Platte.countDocuments().then(count => {
          res.send({
            items: platte,
            total: count
          })
        })
      })
    }else{
      platte = await Platte.find();
    }
  	return platte;
  },
  async loadByID(id){
  	const platte = await Platte.findById(id);
  	return platte;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const platte = await Platte.find({ [searchSub]: req.params.id });
    res.send(platte[0]);
  }
};

module.exports = PlatteController;
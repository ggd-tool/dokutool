const { Pflanzenstärkung_Spritzen } = require('../models-index');

const Pflanzenstärkung_SpritzenController = {
  async index(req, res){
  	const pflanzenstärkung_spritzen = await Pflanzenstärkung_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenstärkungsmittel');
  	res.send(pflanzenstärkung_spritzen);
  },
  async prev(req, res) {
    const pflanzenstärkung_spritzen = await Pflanzenstärkung_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenstärkungsmittel').sort('-createdAt').limit(3);
  	res.send(pflanzenstärkung_spritzen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const pflanzenstärkung_spritzen = new Pflanzenstärkung_Spritzen({
            'datum': req.body.datum,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'ausbringung': req.body.ausbringung,
            'pflanzenstärkungsmittel': req.body.pflanzenstärkungsmittel,
            'mittelmenge_l_ha': req.body.mittelmenge_l_ha,
            'wassermenge_l_ha': req.body.wassermenge_l_ha,
            'prozentanteil': req.body.prozentanteil,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await pflanzenstärkung_spritzen.save(); 
      res.send(pflanzenstärkung_spritzen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const pflanzenstärkung_spritzen = await Pflanzenstärkung_Spritzen.findById(req.params.id).populate({ path: 'feld'}).populate('pflanzenstärkungsmittel');
  	res.send(pflanzenstärkung_spritzen);
  },
  async update(req, res){
    const pflanzenstärkung_spritzen = await Pflanzenstärkung_Spritzen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(pflanzenstärkung_spritzen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let pflanzenstärkung_spritzen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  pflanzenstärkung_spritzen = await Pflanzenstärkung_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenstärkungsmittel').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async pflanzenstärkung_spritzen => {
        await Pflanzenstärkung_Spritzen.countDocuments().then(count => {
          res.send({
            items: pflanzenstärkung_spritzen,
            total: count
          })
        })
      })
    }else{
      pflanzenstärkung_spritzen = await Pflanzenstärkung_Spritzen.find().populate({ path: 'feld'}).populate('pflanzenstärkungsmittel');
    }
  	return pflanzenstärkung_spritzen;
  },
  async loadByID(id){
  	const pflanzenstärkung_spritzen = await Pflanzenstärkung_Spritzen.findById(id).populate({ path: 'feld'}).populate('pflanzenstärkungsmittel');
  	return pflanzenstärkung_spritzen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const pflanzenstärkung_spritzen = await Pflanzenstärkung_Spritzen.find({ [searchSub]: req.params.id });
    res.send(pflanzenstärkung_spritzen[0]);
  }
};

module.exports = Pflanzenstärkung_SpritzenController;
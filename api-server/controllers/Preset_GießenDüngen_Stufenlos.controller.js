const { Preset_GießenDüngen_Stufenlos } = require('../models-index');

const Preset_GießenDüngen_StufenlosController = {
  async index(req, res){
  	const preset_gießendüngen_stufenlos = await Preset_GießenDüngen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	res.send(preset_gießendüngen_stufenlos);
  },
  async prev(req, res) {
    const preset_gießendüngen_stufenlos = await Preset_GießenDüngen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung').sort('-createdAt').limit(3);
  	res.send(preset_gießendüngen_stufenlos);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_gießendüngen_stufenlos = new Preset_GießenDüngen_Stufenlos({
            'name': req.body.name,
            'gießwagen_stufenlos': req.body.gießwagen_stufenlos,
            'beete': req.body.beete,
            'einweghäufigkeit': req.body.einweghäufigkeit,
            'geschwindigkeit_': req.body.geschwindigkeit_,
            'von_bis_in_': req.body.von_bis_in_,
            'stammlösungsbecken_düngemischung': req.body.stammlösungsbecken_düngemischung,
            'konzentration_ec': req.body.konzentration_ec,
            'konzentration_g_l': req.body.konzentration_g_l,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_gießendüngen_stufenlos.save(); 
      res.send(preset_gießendüngen_stufenlos);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_gießendüngen_stufenlos = await Preset_GießenDüngen_Stufenlos.findById(req.params.id).populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	res.send(preset_gießendüngen_stufenlos);
  },
  async update(req, res){
    const preset_gießendüngen_stufenlos = await Preset_GießenDüngen_Stufenlos.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_gießendüngen_stufenlos);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_gießendüngen_stufenlos;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_gießendüngen_stufenlos = await Preset_GießenDüngen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_gießendüngen_stufenlos => {
        await Preset_GießenDüngen_Stufenlos.countDocuments().then(count => {
          res.send({
            items: preset_gießendüngen_stufenlos,
            total: count
          })
        })
      })
    }else{
      preset_gießendüngen_stufenlos = await Preset_GießenDüngen_Stufenlos.find().populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
    }
  	return preset_gießendüngen_stufenlos;
  },
  async loadByID(id){
  	const preset_gießendüngen_stufenlos = await Preset_GießenDüngen_Stufenlos.findById(id).populate({ path: 'gießwagen_stufenlos', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	return preset_gießendüngen_stufenlos;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_gießendüngen_stufenlos = await Preset_GießenDüngen_Stufenlos.find({ [searchSub]: req.params.id });
    res.send(preset_gießendüngen_stufenlos[0]);
  }
};

module.exports = Preset_GießenDüngen_StufenlosController;
const { Preset_Pflanzenschutz_Gießen_Stufen } = require('../models-index');

const Preset_Pflanzenschutz_Gießen_StufenController = {
  async index(req, res){
  	const preset_pflanzenschutz_gießen_stufen = await Preset_Pflanzenschutz_Gießen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel');
  	res.send(preset_pflanzenschutz_gießen_stufen);
  },
  async prev(req, res) {
    const preset_pflanzenschutz_gießen_stufen = await Preset_Pflanzenschutz_Gießen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel').sort('-createdAt').limit(3);
  	res.send(preset_pflanzenschutz_gießen_stufen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_pflanzenschutz_gießen_stufen = new Preset_Pflanzenschutz_Gießen_Stufen({
            'name': req.body.name,
            'gießwagen_stufen': req.body.gießwagen_stufen,
            'beete': req.body.beete,
            'einweghäufigkeit': req.body.einweghäufigkeit,
            'ausbringung': req.body.ausbringung,
            'stufe': req.body.stufe,
            'von_bis_in_': req.body.von_bis_in_,
            'pflanzenschutzmittel': req.body.pflanzenschutzmittel,
            'mittelmenge_l_ha': req.body.mittelmenge_l_ha,
            'wassermenge_l_m2': req.body.wassermenge_l_m2,
            'prozentanteil': req.body.prozentanteil,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_pflanzenschutz_gießen_stufen.save(); 
      res.send(preset_pflanzenschutz_gießen_stufen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_pflanzenschutz_gießen_stufen = await Preset_Pflanzenschutz_Gießen_Stufen.findById(req.params.id).populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel');
  	res.send(preset_pflanzenschutz_gießen_stufen);
  },
  async update(req, res){
    const preset_pflanzenschutz_gießen_stufen = await Preset_Pflanzenschutz_Gießen_Stufen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_pflanzenschutz_gießen_stufen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_pflanzenschutz_gießen_stufen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_pflanzenschutz_gießen_stufen = await Preset_Pflanzenschutz_Gießen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_pflanzenschutz_gießen_stufen => {
        await Preset_Pflanzenschutz_Gießen_Stufen.countDocuments().then(count => {
          res.send({
            items: preset_pflanzenschutz_gießen_stufen,
            total: count
          })
        })
      })
    }else{
      preset_pflanzenschutz_gießen_stufen = await Preset_Pflanzenschutz_Gießen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel');
    }
  	return preset_pflanzenschutz_gießen_stufen;
  },
  async loadByID(id){
  	const preset_pflanzenschutz_gießen_stufen = await Preset_Pflanzenschutz_Gießen_Stufen.findById(id).populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('pflanzenschutzmittel');
  	return preset_pflanzenschutz_gießen_stufen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_pflanzenschutz_gießen_stufen = await Preset_Pflanzenschutz_Gießen_Stufen.find({ [searchSub]: req.params.id });
    res.send(preset_pflanzenschutz_gießen_stufen[0]);
  }
};

module.exports = Preset_Pflanzenschutz_Gießen_StufenController;
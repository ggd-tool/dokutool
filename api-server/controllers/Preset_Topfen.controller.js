const { Preset_Topfen } = require('../models-index');

const Preset_TopfenController = {
  async index(req, res){
  	const preset_topfen = await Preset_Topfen.find().populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(preset_topfen);
  },
  async prev(req, res) {
    const preset_topfen = await Preset_Topfen.find().populate({ path: 'feld'}).populate({ path: 'feld'}).sort('-createdAt').limit(3);
  	res.send(preset_topfen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_topfen = new Preset_Topfen({
            'name': req.body.name,
            'auftragsnummer': req.body.auftragsnummer,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_topfen.save(); 
      res.send(preset_topfen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_topfen = await Preset_Topfen.findById(req.params.id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(preset_topfen);
  },
  async update(req, res){
    const preset_topfen = await Preset_Topfen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_topfen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_topfen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_topfen = await Preset_Topfen.find().populate({ path: 'feld'}).populate({ path: 'feld'}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_topfen => {
        await Preset_Topfen.countDocuments().then(count => {
          res.send({
            items: preset_topfen,
            total: count
          })
        })
      })
    }else{
      preset_topfen = await Preset_Topfen.find().populate({ path: 'feld'}).populate({ path: 'feld'});
    }
  	return preset_topfen;
  },
  async loadByID(id){
  	const preset_topfen = await Preset_Topfen.findById(id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	return preset_topfen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_topfen = await Preset_Topfen.find({ [searchSub]: req.params.id });
    res.send(preset_topfen[0]);
  }
};

module.exports = Preset_TopfenController;
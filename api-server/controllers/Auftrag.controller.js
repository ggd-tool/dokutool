const { Auftrag } = require('../models-index');

const AuftragController = {
  async index(req, res){
  	const auftrag = await Auftrag.find();
  	res.send(auftrag);
  },
  async prev(req, res) {
    const auftrag = await Auftrag.find().sort('-createdAt').limit(3);
  	res.send(auftrag);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const auftrag = new Auftrag({
            'name': req.body.name,
            'pflanze': req.body.pflanze,
        });
      await auftrag.save(); 
      res.send(auftrag);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const auftrag = await Auftrag.findById(req.params.id);
  	res.send(auftrag);
  },
  async update(req, res){
    const auftrag = await Auftrag.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(auftrag);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let auftrag;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  auftrag = await Auftrag.find().skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async auftrag => {
        await Auftrag.countDocuments().then(count => {
          res.send({
            items: auftrag,
            total: count
          })
        })
      })
    }else{
      auftrag = await Auftrag.find();
    }
  	return auftrag;
  },
  async loadByID(id){
  	const auftrag = await Auftrag.findById(id);
  	return auftrag;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const auftrag = await Auftrag.find({ [searchSub]: req.params.id });
    res.send(auftrag[0]);
  }
};

module.exports = AuftragController;
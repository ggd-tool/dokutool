const { Preset_Stammlösung_ansetzen } = require('../models-index');

const Preset_Stammlösung_ansetzenController = {
  async index(req, res){
  	const preset_stammlösung_ansetzen = await Preset_Stammlösung_ansetzen.find().populate('stammlösungsbeckendüngemischung');
  	res.send(preset_stammlösung_ansetzen);
  },
  async prev(req, res) {
    const preset_stammlösung_ansetzen = await Preset_Stammlösung_ansetzen.find().populate('stammlösungsbeckendüngemischung').sort('-createdAt').limit(3);
  	res.send(preset_stammlösung_ansetzen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_stammlösung_ansetzen = new Preset_Stammlösung_ansetzen({
            'name': req.body.name,
            'stammlösungsbecken_düngemischung': req.body.stammlösungsbecken_düngemischung,
            'wassermenge_in_l': req.body.wassermenge_in_l,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_stammlösung_ansetzen.save(); 
      res.send(preset_stammlösung_ansetzen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_stammlösung_ansetzen = await Preset_Stammlösung_ansetzen.findById(req.params.id).populate('stammlösungsbeckendüngemischung');
  	res.send(preset_stammlösung_ansetzen);
  },
  async update(req, res){
    const preset_stammlösung_ansetzen = await Preset_Stammlösung_ansetzen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_stammlösung_ansetzen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_stammlösung_ansetzen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_stammlösung_ansetzen = await Preset_Stammlösung_ansetzen.find().populate('stammlösungsbeckendüngemischung').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_stammlösung_ansetzen => {
        await Preset_Stammlösung_ansetzen.countDocuments().then(count => {
          res.send({
            items: preset_stammlösung_ansetzen,
            total: count
          })
        })
      })
    }else{
      preset_stammlösung_ansetzen = await Preset_Stammlösung_ansetzen.find().populate('stammlösungsbeckendüngemischung');
    }
  	return preset_stammlösung_ansetzen;
  },
  async loadByID(id){
  	const preset_stammlösung_ansetzen = await Preset_Stammlösung_ansetzen.findById(id).populate('stammlösungsbeckendüngemischung');
  	return preset_stammlösung_ansetzen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_stammlösung_ansetzen = await Preset_Stammlösung_ansetzen.find({ [searchSub]: req.params.id });
    res.send(preset_stammlösung_ansetzen[0]);
  }
};

module.exports = Preset_Stammlösung_ansetzenController;
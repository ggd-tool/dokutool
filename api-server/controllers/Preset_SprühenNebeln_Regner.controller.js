const { Preset_SprühenNebeln_Regner } = require('../models-index');

const Preset_SprühenNebeln_RegnerController = {
  async index(req, res){
  	const preset_sprühennebeln_regner = await Preset_SprühenNebeln_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
  	res.send(preset_sprühennebeln_regner);
  },
  async prev(req, res) {
    const preset_sprühennebeln_regner = await Preset_SprühenNebeln_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).sort('-createdAt').limit(3);
  	res.send(preset_sprühennebeln_regner);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_sprühennebeln_regner = new Preset_SprühenNebeln_Regner({
            'name': req.body.name,
            'regner': req.body.regner,
            'beete': req.body.beete,
            'laufzeit_in_min': req.body.laufzeit_in_min,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_sprühennebeln_regner.save(); 
      res.send(preset_sprühennebeln_regner);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_sprühennebeln_regner = await Preset_SprühenNebeln_Regner.findById(req.params.id).populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
  	res.send(preset_sprühennebeln_regner);
  },
  async update(req, res){
    const preset_sprühennebeln_regner = await Preset_SprühenNebeln_Regner.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_sprühennebeln_regner);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_sprühennebeln_regner;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_sprühennebeln_regner = await Preset_SprühenNebeln_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_sprühennebeln_regner => {
        await Preset_SprühenNebeln_Regner.countDocuments().then(count => {
          res.send({
            items: preset_sprühennebeln_regner,
            total: count
          })
        })
      })
    }else{
      preset_sprühennebeln_regner = await Preset_SprühenNebeln_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
    }
  	return preset_sprühennebeln_regner;
  },
  async loadByID(id){
  	const preset_sprühennebeln_regner = await Preset_SprühenNebeln_Regner.findById(id).populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
  	return preset_sprühennebeln_regner;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_sprühennebeln_regner = await Preset_SprühenNebeln_Regner.find({ [searchSub]: req.params.id });
    res.send(preset_sprühennebeln_regner[0]);
  }
};

module.exports = Preset_SprühenNebeln_RegnerController;
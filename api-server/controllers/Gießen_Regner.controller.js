const { Gießen_Regner } = require('../models-index');

const Gießen_RegnerController = {
  async index(req, res){
  	const gießen_regner = await Gießen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
  	res.send(gießen_regner);
  },
  async prev(req, res) {
    const gießen_regner = await Gießen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).sort('-createdAt').limit(3);
  	res.send(gießen_regner);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const gießen_regner = new Gießen_Regner({
            'datum': req.body.datum,
            'regner': req.body.regner,
            'beete': req.body.beete,
            'zeit_in_min': req.body.zeit_in_min,
            'von_bis_in_': req.body.von_bis_in_,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await gießen_regner.save(); 
      res.send(gießen_regner);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const gießen_regner = await Gießen_Regner.findById(req.params.id).populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
  	res.send(gießen_regner);
  },
  async update(req, res){
    const gießen_regner = await Gießen_Regner.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(gießen_regner);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let gießen_regner;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  gießen_regner = await Gießen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async gießen_regner => {
        await Gießen_Regner.countDocuments().then(count => {
          res.send({
            items: gießen_regner,
            total: count
          })
        })
      })
    }else{
      gießen_regner = await Gießen_Regner.find().populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
    }
  	return gießen_regner;
  },
  async loadByID(id){
  	const gießen_regner = await Gießen_Regner.findById(id).populate({ path: 'regner', populate: {path: 'feld', model: 'Feld'}});
  	return gießen_regner;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const gießen_regner = await Gießen_Regner.find({ [searchSub]: req.params.id });
    res.send(gießen_regner[0]);
  }
};

module.exports = Gießen_RegnerController;
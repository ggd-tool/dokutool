const { Topfen } = require('../models-index');

const TopfenController = {
  async index(req, res){
  	const topfen = await Topfen.find().populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(topfen);
  },
  async prev(req, res) {
    const topfen = await Topfen.find().populate({ path: 'feld'}).populate({ path: 'feld'}).sort('-createdAt').limit(3);
  	res.send(topfen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const topfen = new Topfen({
            'datum': req.body.datum,
            'auftragsnummer': req.body.auftragsnummer,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await topfen.save(); 
      res.send(topfen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const topfen = await Topfen.findById(req.params.id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(topfen);
  },
  async update(req, res){
    const topfen = await Topfen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(topfen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let topfen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  topfen = await Topfen.find().populate({ path: 'feld'}).populate({ path: 'feld'}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async topfen => {
        await Topfen.countDocuments().then(count => {
          res.send({
            items: topfen,
            total: count
          })
        })
      })
    }else{
      topfen = await Topfen.find().populate({ path: 'feld'}).populate({ path: 'feld'});
    }
  	return topfen;
  },
  async loadByID(id){
  	const topfen = await Topfen.findById(id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	return topfen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const topfen = await Topfen.find({ [searchSub]: req.params.id });
    res.send(topfen[0]);
  }
};

module.exports = TopfenController;
const { Gießwagen_Stufenlos } = require('../models-index');

const Gießwagen_StufenlosController = {
  async index(req, res){
  	const gießwagen_stufenlos = await Gießwagen_Stufenlos.find().populate('feld');
  	res.send(gießwagen_stufenlos);
  },
  async prev(req, res) {
    const gießwagen_stufenlos = await Gießwagen_Stufenlos.find().populate('feld').sort('-createdAt').limit(3);
  	res.send(gießwagen_stufenlos);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const gießwagen_stufenlos = new Gießwagen_Stufenlos({
            'name': req.body.name,
            'breite_meter': req.body.breite_meter,
            'fahrlänge_meter': req.body.fahrlänge_meter,
            'volumenstrom_m3_h': req.body.volumenstrom_m3_h,
            'bewässerungssysteme': req.body.bewässerungssysteme,
            'feld': req.body.feld,
            'bei_1_meter_minute': req.body.bei_1_meter_minute,
            'bei_100_meter_minute': req.body.bei_100_meter_minute,
        });
      await gießwagen_stufenlos.save(); 
      res.send(gießwagen_stufenlos);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const gießwagen_stufenlos = await Gießwagen_Stufenlos.findById(req.params.id).populate('feld');
  	res.send(gießwagen_stufenlos);
  },
  async update(req, res){
    const gießwagen_stufenlos = await Gießwagen_Stufenlos.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(gießwagen_stufenlos);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let gießwagen_stufenlos;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  gießwagen_stufenlos = await Gießwagen_Stufenlos.find().populate('feld').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async gießwagen_stufenlos => {
        await Gießwagen_Stufenlos.countDocuments().then(count => {
          res.send({
            items: gießwagen_stufenlos,
            total: count
          })
        })
      })
    }else{
      gießwagen_stufenlos = await Gießwagen_Stufenlos.find().populate('feld');
    }
  	return gießwagen_stufenlos;
  },
  async loadByID(id){
  	const gießwagen_stufenlos = await Gießwagen_Stufenlos.findById(id).populate('feld');
  	return gießwagen_stufenlos;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const gießwagen_stufenlos = await Gießwagen_Stufenlos.find({ [searchSub]: req.params.id });
    res.send(gießwagen_stufenlos[0]);
  }
};

module.exports = Gießwagen_StufenlosController;
const { Preset_GießenDüngen_Stufen } = require('../models-index');

const Preset_GießenDüngen_StufenController = {
  async index(req, res){
  	const preset_gießendüngen_stufen = await Preset_GießenDüngen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	res.send(preset_gießendüngen_stufen);
  },
  async prev(req, res) {
    const preset_gießendüngen_stufen = await Preset_GießenDüngen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung').sort('-createdAt').limit(3);
  	res.send(preset_gießendüngen_stufen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_gießendüngen_stufen = new Preset_GießenDüngen_Stufen({
            'name': req.body.name,
            'gießwagen_stufen': req.body.gießwagen_stufen,
            'beete': req.body.beete,
            'einweghäufigkeit': req.body.einweghäufigkeit,
            'stufe': req.body.stufe,
            'von_bis_in_': req.body.von_bis_in_,
            'stammlösungsbecken_düngemischung': req.body.stammlösungsbecken_düngemischung,
            'konzentration_ec': req.body.konzentration_ec,
            'konzentration_g_l': req.body.konzentration_g_l,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_gießendüngen_stufen.save(); 
      res.send(preset_gießendüngen_stufen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_gießendüngen_stufen = await Preset_GießenDüngen_Stufen.findById(req.params.id).populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	res.send(preset_gießendüngen_stufen);
  },
  async update(req, res){
    const preset_gießendüngen_stufen = await Preset_GießenDüngen_Stufen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_gießendüngen_stufen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_gießendüngen_stufen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_gießendüngen_stufen = await Preset_GießenDüngen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_gießendüngen_stufen => {
        await Preset_GießenDüngen_Stufen.countDocuments().then(count => {
          res.send({
            items: preset_gießendüngen_stufen,
            total: count
          })
        })
      })
    }else{
      preset_gießendüngen_stufen = await Preset_GießenDüngen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
    }
  	return preset_gießendüngen_stufen;
  },
  async loadByID(id){
  	const preset_gießendüngen_stufen = await Preset_GießenDüngen_Stufen.findById(id).populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).populate('stammlösungsbeckendüngemischung');
  	return preset_gießendüngen_stufen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_gießendüngen_stufen = await Preset_GießenDüngen_Stufen.find({ [searchSub]: req.params.id });
    res.send(preset_gießendüngen_stufen[0]);
  }
};

module.exports = Preset_GießenDüngen_StufenController;
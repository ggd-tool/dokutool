const { Preset_SprühenNebeln_Gießwagen_Stufen } = require('../models-index');

const Preset_SprühenNebeln_Gießwagen_StufenController = {
  async index(req, res){
  	const preset_sprühennebeln_gießwagen_stufen = await Preset_SprühenNebeln_Gießwagen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}});
  	res.send(preset_sprühennebeln_gießwagen_stufen);
  },
  async prev(req, res) {
    const preset_sprühennebeln_gießwagen_stufen = await Preset_SprühenNebeln_Gießwagen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).sort('-createdAt').limit(3);
  	res.send(preset_sprühennebeln_gießwagen_stufen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_sprühennebeln_gießwagen_stufen = new Preset_SprühenNebeln_Gießwagen_Stufen({
            'name': req.body.name,
            'gießwagen_stufen': req.body.gießwagen_stufen,
            'beete': req.body.beete,
            'einweghäufigkeit': req.body.einweghäufigkeit,
            'bewässerungssystem': req.body.bewässerungssystem,
            'stufe': req.body.stufe,
            'von_bis_in_': req.body.von_bis_in_,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_sprühennebeln_gießwagen_stufen.save(); 
      res.send(preset_sprühennebeln_gießwagen_stufen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_sprühennebeln_gießwagen_stufen = await Preset_SprühenNebeln_Gießwagen_Stufen.findById(req.params.id).populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}});
  	res.send(preset_sprühennebeln_gießwagen_stufen);
  },
  async update(req, res){
    const preset_sprühennebeln_gießwagen_stufen = await Preset_SprühenNebeln_Gießwagen_Stufen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_sprühennebeln_gießwagen_stufen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_sprühennebeln_gießwagen_stufen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_sprühennebeln_gießwagen_stufen = await Preset_SprühenNebeln_Gießwagen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_sprühennebeln_gießwagen_stufen => {
        await Preset_SprühenNebeln_Gießwagen_Stufen.countDocuments().then(count => {
          res.send({
            items: preset_sprühennebeln_gießwagen_stufen,
            total: count
          })
        })
      })
    }else{
      preset_sprühennebeln_gießwagen_stufen = await Preset_SprühenNebeln_Gießwagen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}});
    }
  	return preset_sprühennebeln_gießwagen_stufen;
  },
  async loadByID(id){
  	const preset_sprühennebeln_gießwagen_stufen = await Preset_SprühenNebeln_Gießwagen_Stufen.findById(id).populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}});
  	return preset_sprühennebeln_gießwagen_stufen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_sprühennebeln_gießwagen_stufen = await Preset_SprühenNebeln_Gießwagen_Stufen.find({ [searchSub]: req.params.id });
    res.send(preset_sprühennebeln_gießwagen_stufen[0]);
  }
};

module.exports = Preset_SprühenNebeln_Gießwagen_StufenController;
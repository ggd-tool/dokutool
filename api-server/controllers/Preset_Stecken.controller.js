const { Preset_Stecken } = require('../models-index');

const Preset_SteckenController = {
  async index(req, res){
  	const preset_stecken = await Preset_Stecken.find().populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(preset_stecken);
  },
  async prev(req, res) {
    const preset_stecken = await Preset_Stecken.find().populate({ path: 'feld'}).populate({ path: 'feld'}).sort('-createdAt').limit(3);
  	res.send(preset_stecken);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const preset_stecken = new Preset_Stecken({
            'name': req.body.name,
            'auftragsnummer': req.body.auftragsnummer,
            'feld': req.body.feld,
            'beete': req.body.beete,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await preset_stecken.save(); 
      res.send(preset_stecken);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const preset_stecken = await Preset_Stecken.findById(req.params.id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	res.send(preset_stecken);
  },
  async update(req, res){
    const preset_stecken = await Preset_Stecken.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(preset_stecken);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let preset_stecken;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  preset_stecken = await Preset_Stecken.find().populate({ path: 'feld'}).populate({ path: 'feld'}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async preset_stecken => {
        await Preset_Stecken.countDocuments().then(count => {
          res.send({
            items: preset_stecken,
            total: count
          })
        })
      })
    }else{
      preset_stecken = await Preset_Stecken.find().populate({ path: 'feld'}).populate({ path: 'feld'});
    }
  	return preset_stecken;
  },
  async loadByID(id){
  	const preset_stecken = await Preset_Stecken.findById(id).populate({ path: 'feld'}).populate({ path: 'feld'});
  	return preset_stecken;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const preset_stecken = await Preset_Stecken.find({ [searchSub]: req.params.id });
    res.send(preset_stecken[0]);
  }
};

module.exports = Preset_SteckenController;
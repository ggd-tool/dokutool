const { StammlösungsbeckenDüngemischung } = require('../models-index');

const StammlösungsbeckenDüngemischungController = {
  async index(req, res){
  	const stammlösungsbeckendüngemischung = await StammlösungsbeckenDüngemischung.find().populate('dünger.mittel');
  	res.send(stammlösungsbeckendüngemischung);
  },
  async prev(req, res) {
    const stammlösungsbeckendüngemischung = await StammlösungsbeckenDüngemischung.find().populate('dünger.mittel').sort('-createdAt').limit(3);
  	res.send(stammlösungsbeckendüngemischung);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const stammlösungsbeckendüngemischung = new StammlösungsbeckenDüngemischung({
            'name': req.body.name,
            'bemerkung': req.body.bemerkung,
            'fassungsvermögen': req.body.fassungsvermögen,
            'dünger': req.body.dünger,
        });
      await stammlösungsbeckendüngemischung.save(); 
      res.send(stammlösungsbeckendüngemischung);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const stammlösungsbeckendüngemischung = await StammlösungsbeckenDüngemischung.findById(req.params.id).populate('dünger.mittel');
  	res.send(stammlösungsbeckendüngemischung);
  },
  async update(req, res){
    const stammlösungsbeckendüngemischung = await StammlösungsbeckenDüngemischung.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(stammlösungsbeckendüngemischung);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let stammlösungsbeckendüngemischung;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  stammlösungsbeckendüngemischung = await StammlösungsbeckenDüngemischung.find().populate('dünger.mittel').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async stammlösungsbeckendüngemischung => {
        await StammlösungsbeckenDüngemischung.countDocuments().then(count => {
          res.send({
            items: stammlösungsbeckendüngemischung,
            total: count
          })
        })
      })
    }else{
      stammlösungsbeckendüngemischung = await StammlösungsbeckenDüngemischung.find().populate('dünger.mittel');
    }
  	return stammlösungsbeckendüngemischung;
  },
  async loadByID(id){
  	const stammlösungsbeckendüngemischung = await StammlösungsbeckenDüngemischung.findById(id).populate('dünger.mittel');
  	return stammlösungsbeckendüngemischung;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const stammlösungsbeckendüngemischung = await StammlösungsbeckenDüngemischung.find({ [searchSub]: req.params.id });
    res.send(stammlösungsbeckendüngemischung[0]);
  }
};

module.exports = StammlösungsbeckenDüngemischungController;
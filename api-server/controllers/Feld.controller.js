const { Feld } = require('../models-index');

const FeldController = {
  async index(req, res){
  	const feld = await Feld.find();
  	res.send(feld);
  },
  async prev(req, res) {
    const feld = await Feld.find().sort('-createdAt').limit(3);
  	res.send(feld);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const feld = new Feld({
            'name': req.body.name,
            'fläche_brutto_in_m2': req.body.fläche_brutto_in_m2,
            'fläche_netto_in_m2': req.body.fläche_netto_in_m2,
            'beete': req.body.beete,
        });
      await feld.save(); 
      res.send(feld);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const feld = await Feld.findById(req.params.id);
  	res.send(feld);
  },
  async update(req, res){
    const feld = await Feld.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(feld);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let feld;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  feld = await Feld.find().skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async feld => {
        await Feld.countDocuments().then(count => {
          res.send({
            items: feld,
            total: count
          })
        })
      })
    }else{
      feld = await Feld.find();
    }
  	return feld;
  },
  async loadByID(id){
  	const feld = await Feld.findById(id);
  	return feld;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const feld = await Feld.find({ [searchSub]: req.params.id });
    res.send(feld[0]);
  }
};

module.exports = FeldController;
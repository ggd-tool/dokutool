const { Verkauf } = require('../models-index');

const VerkaufController = {
  async index(req, res){
  	const verkauf = await Verkauf.find().populate('auftrag');
  	res.send(verkauf);
  },
  async prev(req, res) {
    const verkauf = await Verkauf.find().populate('auftrag').sort('-createdAt').limit(3);
  	res.send(verkauf);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const verkauf = new Verkauf({
            'datum': req.body.datum,
            'auftragsnummer': req.body.auftragsnummer,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await verkauf.save(); 
      res.send(verkauf);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const verkauf = await Verkauf.findById(req.params.id).populate('auftrag');
  	res.send(verkauf);
  },
  async update(req, res){
    const verkauf = await Verkauf.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(verkauf);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let verkauf;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  verkauf = await Verkauf.find().populate('auftrag').skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async verkauf => {
        await Verkauf.countDocuments().then(count => {
          res.send({
            items: verkauf,
            total: count
          })
        })
      })
    }else{
      verkauf = await Verkauf.find().populate('auftrag');
    }
  	return verkauf;
  },
  async loadByID(id){
  	const verkauf = await Verkauf.findById(id).populate('auftrag');
  	return verkauf;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const verkauf = await Verkauf.find({ [searchSub]: req.params.id });
    res.send(verkauf[0]);
  }
};

module.exports = VerkaufController;
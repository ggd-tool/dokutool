const { SprühenNebeln_Gießwagen_Stufen } = require('../models-index');

const SprühenNebeln_Gießwagen_StufenController = {
  async index(req, res){
  	const sprühennebeln_gießwagen_stufen = await SprühenNebeln_Gießwagen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}});
  	res.send(sprühennebeln_gießwagen_stufen);
  },
  async prev(req, res) {
    const sprühennebeln_gießwagen_stufen = await SprühenNebeln_Gießwagen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).sort('-createdAt').limit(3);
  	res.send(sprühennebeln_gießwagen_stufen);
  },
  async store(req, res){
    try{
      for (let property in req.body) {
        if (req.body[property] === null || req.body[property] === '') {
          delete req.body[property];
        }
      }
      const sprühennebeln_gießwagen_stufen = new SprühenNebeln_Gießwagen_Stufen({
            'datum': req.body.datum,
            'gießwagen_stufen': req.body.gießwagen_stufen,
            'beete': req.body.beete,
            'einweghäufigkeit': req.body.einweghäufigkeit,
            'bewässerungssystem': req.body.bewässerungssystem,
            'stufe': req.body.stufe,
            'von_bis_in_': req.body.von_bis_in_,
            'wasser_ec_ms_cm': req.body.wasser_ec_ms_cm,
            'bemerkung': req.body.bemerkung,
            'durchgeführt_von': req.body.durchgeführt_von,
        });
      await sprühennebeln_gießwagen_stufen.save(); 
      res.send(sprühennebeln_gießwagen_stufen);
    } catch(error){
      res.send(error);
    }
  },
  async show(req, res){
  	const sprühennebeln_gießwagen_stufen = await SprühenNebeln_Gießwagen_Stufen.findById(req.params.id).populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}});
  	res.send(sprühennebeln_gießwagen_stufen);
  },
  async update(req, res){
    const sprühennebeln_gießwagen_stufen = await SprühenNebeln_Gießwagen_Stufen.findOneAndUpdate({ _id: req.params.id }, req.body);
  	res.send(sprühennebeln_gießwagen_stufen);
  },
  async remove(req, res){

  },
  async localIndex(req, res){
    let sprühennebeln_gießwagen_stufen;
    if(req){
      const pageSize = +req.query.pageSize;
      const currentPage = +req.query.page;
  	  sprühennebeln_gießwagen_stufen = await SprühenNebeln_Gießwagen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}}).skip(pageSize * (currentPage - 1)).limit(pageSize)
      .then(async sprühennebeln_gießwagen_stufen => {
        await SprühenNebeln_Gießwagen_Stufen.countDocuments().then(count => {
          res.send({
            items: sprühennebeln_gießwagen_stufen,
            total: count
          })
        })
      })
    }else{
      sprühennebeln_gießwagen_stufen = await SprühenNebeln_Gießwagen_Stufen.find().populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}});
    }
  	return sprühennebeln_gießwagen_stufen;
  },
  async loadByID(id){
  	const sprühennebeln_gießwagen_stufen = await SprühenNebeln_Gießwagen_Stufen.findById(id).populate({ path: 'gießwagen_stufen', populate: {path: 'feld', model: 'Feld'}});
  	return sprühennebeln_gießwagen_stufen;
  },
  async showFindSub(req, res) {
    let searchSub = req.params.key + "._id";
    const sprühennebeln_gießwagen_stufen = await SprühenNebeln_Gießwagen_Stufen.find({ [searchSub]: req.params.id });
    res.send(sprühennebeln_gießwagen_stufen[0]);
  }
};

module.exports = SprühenNebeln_Gießwagen_StufenController;
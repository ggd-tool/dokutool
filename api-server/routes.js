const express = require('express'),
	path = require('path'),
	rootPath = path.normalize(__dirname + '/../'),
	router = express.Router(),
	{ AuftragController,
    DüngerController,
    FeldController,
    GießenDüngen_RegnerController,
    GießenDüngen_StufenController,
    GießenDüngen_StufenlosController,
    Gießen_RegnerController,
    Gießwagen_StufenController,
    Gießwagen_StufenlosController,
    manuell_GießenController,
    PflanzenschutzmittelController,
    Pflanzenschutz_Gießen_StufenController,
    Pflanzenschutz_Gießen_StufenlosController,
    Pflanzenschutz_SpritzenController,
    PflanzenstärkungsmittelController,
    Pflanzenstärkung_Gießen_StufenController,
    Pflanzenstärkung_Gießen_StufenlosController,
    Pflanzenstärkung_SpritzenController,
    PikierenzusammenpikierenController,
    PlatteController,
    Preset_GießenDüngen_RegnerController,
    Preset_GießenDüngen_StufenController,
    Preset_GießenDüngen_StufenlosController,
    Preset_Gießen_RegnerController,
    Preset_manuell_GießenController,
    Preset_Pflanzenschutz_Gießen_StufenController,
    Preset_Pflanzenschutz_Gießen_StufenlosController,
    Preset_Pflanzenschutz_SpritzenController,
    Preset_Pflanzenstärkung_Gießen_StufenController,
    Preset_Pflanzenstärkung_Gießen_StufenlosController,
    Preset_Pflanzenstärkung_SpritzenController,
    Preset_PikierenzusammenpikierenController,
    Preset_SprühenNebeln_Gießwagen_StufenController,
    Preset_SprühenNebeln_Gießwagen_StufenlosController,
    Preset_SprühenNebeln_RegnerController,
    Preset_Stammlösung_ansetzenController,
    Preset_SteckenController,
    Preset_TopfenController,
    Preset_UmstellenController,
    RegnerController,
    SprühenNebeln_Gießwagen_StufenController,
    SprühenNebeln_Gießwagen_StufenlosController,
    SprühenNebeln_RegnerController,
    StammlösungsbeckenDüngemischungController,
    Stammlösung_ansetzenController,
    SteckenController,
    TopfenController,
    UmstellenController,
    VerkaufController,
     } = require('./index-controller');

module.exports = function(app, keycloak){

    // AuftragController - Routen
    router.get('/auftrag', keycloak.protect("realm:visitor"), AuftragController.index);
	router.get('/auftrag/prev', keycloak.protect("realm:visitor"), AuftragController.prev);
	router.post('/auftrag', keycloak.protect("realm:teamleader"), AuftragController.store);
	router.get('/auftrag/:id', keycloak.protect("realm:visitor"), AuftragController.show);
	router.get('/auftrag/sub/:key/:id', keycloak.protect("realm:visitor"), AuftragController.showFindSub);
	router.put('/auftrag/:id', keycloak.protect("realm:teamleader"), AuftragController.update);
	router.delete('/auftrag/:id', keycloak.protect("realm:teamleader"), AuftragController.remove);

    // DüngerController - Routen
    router.get('/d%C3%BCnger', keycloak.protect("realm:visitor"), DüngerController.index);
	router.get('/d%C3%BCnger/prev', keycloak.protect("realm:visitor"), DüngerController.prev);
	router.post('/d%C3%BCnger', keycloak.protect("realm:teamleader"), DüngerController.store);
	router.get('/d%C3%BCnger/:id', keycloak.protect("realm:visitor"), DüngerController.show);
	router.get('/d%C3%BCnger/sub/:key/:id', keycloak.protect("realm:visitor"), DüngerController.showFindSub);
	router.put('/d%C3%BCnger/:id', keycloak.protect("realm:teamleader"), DüngerController.update);
	router.delete('/d%C3%BCnger/:id', keycloak.protect("realm:teamleader"), DüngerController.remove);

    // FeldController - Routen
    router.get('/feld', keycloak.protect("realm:visitor"), FeldController.index);
	router.get('/feld/prev', keycloak.protect("realm:visitor"), FeldController.prev);
	router.post('/feld', keycloak.protect("realm:teamleader"), FeldController.store);
	router.get('/feld/:id', keycloak.protect("realm:visitor"), FeldController.show);
	router.get('/feld/sub/:key/:id', keycloak.protect("realm:visitor"), FeldController.showFindSub);
	router.put('/feld/:id', keycloak.protect("realm:teamleader"), FeldController.update);
	router.delete('/feld/:id', keycloak.protect("realm:teamleader"), FeldController.remove);

    // GießenDüngen_RegnerController - Routen
    router.get('/gie%C3%9Fend%C3%BCngen_regner', keycloak.protect("realm:visitor"), GießenDüngen_RegnerController.index);
	router.get('/gie%C3%9Fend%C3%BCngen_regner/prev', keycloak.protect("realm:visitor"), GießenDüngen_RegnerController.prev);
	router.post('/gie%C3%9Fend%C3%BCngen_regner', keycloak.protect("realm:teamleader"), GießenDüngen_RegnerController.store);
	router.get('/gie%C3%9Fend%C3%BCngen_regner/:id', keycloak.protect("realm:visitor"), GießenDüngen_RegnerController.show);
	router.get('/gie%C3%9Fend%C3%BCngen_regner/sub/:key/:id', keycloak.protect("realm:visitor"), GießenDüngen_RegnerController.showFindSub);
	router.put('/gie%C3%9Fend%C3%BCngen_regner/:id', keycloak.protect("realm:teamleader"), GießenDüngen_RegnerController.update);
	router.delete('/gie%C3%9Fend%C3%BCngen_regner/:id', keycloak.protect("realm:teamleader"), GießenDüngen_RegnerController.remove);

    // GießenDüngen_StufenController - Routen
    router.get('/gie%C3%9Fend%C3%BCngen_stufen', keycloak.protect("realm:visitor"), GießenDüngen_StufenController.index);
	router.get('/gie%C3%9Fend%C3%BCngen_stufen/prev', keycloak.protect("realm:visitor"), GießenDüngen_StufenController.prev);
	router.post('/gie%C3%9Fend%C3%BCngen_stufen', keycloak.protect("realm:teamleader"), GießenDüngen_StufenController.store);
	router.get('/gie%C3%9Fend%C3%BCngen_stufen/:id', keycloak.protect("realm:visitor"), GießenDüngen_StufenController.show);
	router.get('/gie%C3%9Fend%C3%BCngen_stufen/sub/:key/:id', keycloak.protect("realm:visitor"), GießenDüngen_StufenController.showFindSub);
	router.put('/gie%C3%9Fend%C3%BCngen_stufen/:id', keycloak.protect("realm:teamleader"), GießenDüngen_StufenController.update);
	router.delete('/gie%C3%9Fend%C3%BCngen_stufen/:id', keycloak.protect("realm:teamleader"), GießenDüngen_StufenController.remove);

    // GießenDüngen_StufenlosController - Routen
    router.get('/gie%C3%9Fend%C3%BCngen_stufenlos', keycloak.protect("realm:visitor"), GießenDüngen_StufenlosController.index);
	router.get('/gie%C3%9Fend%C3%BCngen_stufenlos/prev', keycloak.protect("realm:visitor"), GießenDüngen_StufenlosController.prev);
	router.post('/gie%C3%9Fend%C3%BCngen_stufenlos', keycloak.protect("realm:teamleader"), GießenDüngen_StufenlosController.store);
	router.get('/gie%C3%9Fend%C3%BCngen_stufenlos/:id', keycloak.protect("realm:visitor"), GießenDüngen_StufenlosController.show);
	router.get('/gie%C3%9Fend%C3%BCngen_stufenlos/sub/:key/:id', keycloak.protect("realm:visitor"), GießenDüngen_StufenlosController.showFindSub);
	router.put('/gie%C3%9Fend%C3%BCngen_stufenlos/:id', keycloak.protect("realm:teamleader"), GießenDüngen_StufenlosController.update);
	router.delete('/gie%C3%9Fend%C3%BCngen_stufenlos/:id', keycloak.protect("realm:teamleader"), GießenDüngen_StufenlosController.remove);

    // Gießen_RegnerController - Routen
    router.get('/gie%C3%9Fen_regner', keycloak.protect("realm:visitor"), Gießen_RegnerController.index);
	router.get('/gie%C3%9Fen_regner/prev', keycloak.protect("realm:visitor"), Gießen_RegnerController.prev);
	router.post('/gie%C3%9Fen_regner', keycloak.protect("realm:teamleader"), Gießen_RegnerController.store);
	router.get('/gie%C3%9Fen_regner/:id', keycloak.protect("realm:visitor"), Gießen_RegnerController.show);
	router.get('/gie%C3%9Fen_regner/sub/:key/:id', keycloak.protect("realm:visitor"), Gießen_RegnerController.showFindSub);
	router.put('/gie%C3%9Fen_regner/:id', keycloak.protect("realm:teamleader"), Gießen_RegnerController.update);
	router.delete('/gie%C3%9Fen_regner/:id', keycloak.protect("realm:teamleader"), Gießen_RegnerController.remove);

    // Gießwagen_StufenController - Routen
    router.get('/gie%C3%9Fwagen_stufen', keycloak.protect("realm:visitor"), Gießwagen_StufenController.index);
	router.get('/gie%C3%9Fwagen_stufen/prev', keycloak.protect("realm:visitor"), Gießwagen_StufenController.prev);
	router.post('/gie%C3%9Fwagen_stufen', keycloak.protect("realm:teamleader"), Gießwagen_StufenController.store);
	router.get('/gie%C3%9Fwagen_stufen/:id', keycloak.protect("realm:visitor"), Gießwagen_StufenController.show);
	router.get('/gie%C3%9Fwagen_stufen/sub/:key/:id', keycloak.protect("realm:visitor"), Gießwagen_StufenController.showFindSub);
	router.put('/gie%C3%9Fwagen_stufen/:id', keycloak.protect("realm:teamleader"), Gießwagen_StufenController.update);
	router.delete('/gie%C3%9Fwagen_stufen/:id', keycloak.protect("realm:teamleader"), Gießwagen_StufenController.remove);

    // Gießwagen_StufenlosController - Routen
    router.get('/gie%C3%9Fwagen_stufenlos', keycloak.protect("realm:visitor"), Gießwagen_StufenlosController.index);
	router.get('/gie%C3%9Fwagen_stufenlos/prev', keycloak.protect("realm:visitor"), Gießwagen_StufenlosController.prev);
	router.post('/gie%C3%9Fwagen_stufenlos', keycloak.protect("realm:teamleader"), Gießwagen_StufenlosController.store);
	router.get('/gie%C3%9Fwagen_stufenlos/:id', keycloak.protect("realm:visitor"), Gießwagen_StufenlosController.show);
	router.get('/gie%C3%9Fwagen_stufenlos/sub/:key/:id', keycloak.protect("realm:visitor"), Gießwagen_StufenlosController.showFindSub);
	router.put('/gie%C3%9Fwagen_stufenlos/:id', keycloak.protect("realm:teamleader"), Gießwagen_StufenlosController.update);
	router.delete('/gie%C3%9Fwagen_stufenlos/:id', keycloak.protect("realm:teamleader"), Gießwagen_StufenlosController.remove);

    // manuell_GießenController - Routen
    router.get('/manuell_gie%C3%9Fen', keycloak.protect("realm:visitor"), manuell_GießenController.index);
	router.get('/manuell_gie%C3%9Fen/prev', keycloak.protect("realm:visitor"), manuell_GießenController.prev);
	router.post('/manuell_gie%C3%9Fen', keycloak.protect("realm:teamleader"), manuell_GießenController.store);
	router.get('/manuell_gie%C3%9Fen/:id', keycloak.protect("realm:visitor"), manuell_GießenController.show);
	router.get('/manuell_gie%C3%9Fen/sub/:key/:id', keycloak.protect("realm:visitor"), manuell_GießenController.showFindSub);
	router.put('/manuell_gie%C3%9Fen/:id', keycloak.protect("realm:teamleader"), manuell_GießenController.update);
	router.delete('/manuell_gie%C3%9Fen/:id', keycloak.protect("realm:teamleader"), manuell_GießenController.remove);

    // PflanzenschutzmittelController - Routen
    router.get('/pflanzenschutzmittel', keycloak.protect("realm:visitor"), PflanzenschutzmittelController.index);
	router.get('/pflanzenschutzmittel/prev', keycloak.protect("realm:visitor"), PflanzenschutzmittelController.prev);
	router.post('/pflanzenschutzmittel', keycloak.protect("realm:teamleader"), PflanzenschutzmittelController.store);
	router.get('/pflanzenschutzmittel/:id', keycloak.protect("realm:visitor"), PflanzenschutzmittelController.show);
	router.get('/pflanzenschutzmittel/sub/:key/:id', keycloak.protect("realm:visitor"), PflanzenschutzmittelController.showFindSub);
	router.put('/pflanzenschutzmittel/:id', keycloak.protect("realm:teamleader"), PflanzenschutzmittelController.update);
	router.delete('/pflanzenschutzmittel/:id', keycloak.protect("realm:teamleader"), PflanzenschutzmittelController.remove);

    // Pflanzenschutz_Gießen_StufenController - Routen
    router.get('/pflanzenschutz_gie%C3%9Fen_stufen', keycloak.protect("realm:visitor"), Pflanzenschutz_Gießen_StufenController.index);
	router.get('/pflanzenschutz_gie%C3%9Fen_stufen/prev', keycloak.protect("realm:visitor"), Pflanzenschutz_Gießen_StufenController.prev);
	router.post('/pflanzenschutz_gie%C3%9Fen_stufen', keycloak.protect("realm:teamleader"), Pflanzenschutz_Gießen_StufenController.store);
	router.get('/pflanzenschutz_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:visitor"), Pflanzenschutz_Gießen_StufenController.show);
	router.get('/pflanzenschutz_gie%C3%9Fen_stufen/sub/:key/:id', keycloak.protect("realm:visitor"), Pflanzenschutz_Gießen_StufenController.showFindSub);
	router.put('/pflanzenschutz_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:teamleader"), Pflanzenschutz_Gießen_StufenController.update);
	router.delete('/pflanzenschutz_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:teamleader"), Pflanzenschutz_Gießen_StufenController.remove);

    // Pflanzenschutz_Gießen_StufenlosController - Routen
    router.get('/pflanzenschutz_gie%C3%9Fen_stufenlos', keycloak.protect("realm:visitor"), Pflanzenschutz_Gießen_StufenlosController.index);
	router.get('/pflanzenschutz_gie%C3%9Fen_stufenlos/prev', keycloak.protect("realm:visitor"), Pflanzenschutz_Gießen_StufenlosController.prev);
	router.post('/pflanzenschutz_gie%C3%9Fen_stufenlos', keycloak.protect("realm:teamleader"), Pflanzenschutz_Gießen_StufenlosController.store);
	router.get('/pflanzenschutz_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:visitor"), Pflanzenschutz_Gießen_StufenlosController.show);
	router.get('/pflanzenschutz_gie%C3%9Fen_stufenlos/sub/:key/:id', keycloak.protect("realm:visitor"), Pflanzenschutz_Gießen_StufenlosController.showFindSub);
	router.put('/pflanzenschutz_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:teamleader"), Pflanzenschutz_Gießen_StufenlosController.update);
	router.delete('/pflanzenschutz_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:teamleader"), Pflanzenschutz_Gießen_StufenlosController.remove);

    // Pflanzenschutz_SpritzenController - Routen
    router.get('/pflanzenschutz_spritzen', keycloak.protect("realm:visitor"), Pflanzenschutz_SpritzenController.index);
	router.get('/pflanzenschutz_spritzen/prev', keycloak.protect("realm:visitor"), Pflanzenschutz_SpritzenController.prev);
	router.post('/pflanzenschutz_spritzen', keycloak.protect("realm:teamleader"), Pflanzenschutz_SpritzenController.store);
	router.get('/pflanzenschutz_spritzen/:id', keycloak.protect("realm:visitor"), Pflanzenschutz_SpritzenController.show);
	router.get('/pflanzenschutz_spritzen/sub/:key/:id', keycloak.protect("realm:visitor"), Pflanzenschutz_SpritzenController.showFindSub);
	router.put('/pflanzenschutz_spritzen/:id', keycloak.protect("realm:teamleader"), Pflanzenschutz_SpritzenController.update);
	router.delete('/pflanzenschutz_spritzen/:id', keycloak.protect("realm:teamleader"), Pflanzenschutz_SpritzenController.remove);

    // PflanzenstärkungsmittelController - Routen
    router.get('/pflanzenst%C3%A4rkungsmittel', keycloak.protect("realm:visitor"), PflanzenstärkungsmittelController.index);
	router.get('/pflanzenst%C3%A4rkungsmittel/prev', keycloak.protect("realm:visitor"), PflanzenstärkungsmittelController.prev);
	router.post('/pflanzenst%C3%A4rkungsmittel', keycloak.protect("realm:teamleader"), PflanzenstärkungsmittelController.store);
	router.get('/pflanzenst%C3%A4rkungsmittel/:id', keycloak.protect("realm:visitor"), PflanzenstärkungsmittelController.show);
	router.get('/pflanzenst%C3%A4rkungsmittel/sub/:key/:id', keycloak.protect("realm:visitor"), PflanzenstärkungsmittelController.showFindSub);
	router.put('/pflanzenst%C3%A4rkungsmittel/:id', keycloak.protect("realm:teamleader"), PflanzenstärkungsmittelController.update);
	router.delete('/pflanzenst%C3%A4rkungsmittel/:id', keycloak.protect("realm:teamleader"), PflanzenstärkungsmittelController.remove);

    // Pflanzenstärkung_Gießen_StufenController - Routen
    router.get('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen', keycloak.protect("realm:visitor"), Pflanzenstärkung_Gießen_StufenController.index);
	router.get('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen/prev', keycloak.protect("realm:visitor"), Pflanzenstärkung_Gießen_StufenController.prev);
	router.post('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen', keycloak.protect("realm:teamleader"), Pflanzenstärkung_Gießen_StufenController.store);
	router.get('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:visitor"), Pflanzenstärkung_Gießen_StufenController.show);
	router.get('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen/sub/:key/:id', keycloak.protect("realm:visitor"), Pflanzenstärkung_Gießen_StufenController.showFindSub);
	router.put('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:teamleader"), Pflanzenstärkung_Gießen_StufenController.update);
	router.delete('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:teamleader"), Pflanzenstärkung_Gießen_StufenController.remove);

    // Pflanzenstärkung_Gießen_StufenlosController - Routen
    router.get('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos', keycloak.protect("realm:visitor"), Pflanzenstärkung_Gießen_StufenlosController.index);
	router.get('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos/prev', keycloak.protect("realm:visitor"), Pflanzenstärkung_Gießen_StufenlosController.prev);
	router.post('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos', keycloak.protect("realm:teamleader"), Pflanzenstärkung_Gießen_StufenlosController.store);
	router.get('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:visitor"), Pflanzenstärkung_Gießen_StufenlosController.show);
	router.get('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos/sub/:key/:id', keycloak.protect("realm:visitor"), Pflanzenstärkung_Gießen_StufenlosController.showFindSub);
	router.put('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:teamleader"), Pflanzenstärkung_Gießen_StufenlosController.update);
	router.delete('/pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:teamleader"), Pflanzenstärkung_Gießen_StufenlosController.remove);

    // Pflanzenstärkung_SpritzenController - Routen
    router.get('/pflanzenst%C3%A4rkung_spritzen', keycloak.protect("realm:visitor"), Pflanzenstärkung_SpritzenController.index);
	router.get('/pflanzenst%C3%A4rkung_spritzen/prev', keycloak.protect("realm:visitor"), Pflanzenstärkung_SpritzenController.prev);
	router.post('/pflanzenst%C3%A4rkung_spritzen', keycloak.protect("realm:teamleader"), Pflanzenstärkung_SpritzenController.store);
	router.get('/pflanzenst%C3%A4rkung_spritzen/:id', keycloak.protect("realm:visitor"), Pflanzenstärkung_SpritzenController.show);
	router.get('/pflanzenst%C3%A4rkung_spritzen/sub/:key/:id', keycloak.protect("realm:visitor"), Pflanzenstärkung_SpritzenController.showFindSub);
	router.put('/pflanzenst%C3%A4rkung_spritzen/:id', keycloak.protect("realm:teamleader"), Pflanzenstärkung_SpritzenController.update);
	router.delete('/pflanzenst%C3%A4rkung_spritzen/:id', keycloak.protect("realm:teamleader"), Pflanzenstärkung_SpritzenController.remove);

    // PikierenzusammenpikierenController - Routen
    router.get('/pikierenzusammenpikieren', keycloak.protect("realm:visitor"), PikierenzusammenpikierenController.index);
	router.get('/pikierenzusammenpikieren/prev', keycloak.protect("realm:visitor"), PikierenzusammenpikierenController.prev);
	router.post('/pikierenzusammenpikieren', keycloak.protect("realm:teamleader"), PikierenzusammenpikierenController.store);
	router.get('/pikierenzusammenpikieren/:id', keycloak.protect("realm:visitor"), PikierenzusammenpikierenController.show);
	router.get('/pikierenzusammenpikieren/sub/:key/:id', keycloak.protect("realm:visitor"), PikierenzusammenpikierenController.showFindSub);
	router.put('/pikierenzusammenpikieren/:id', keycloak.protect("realm:teamleader"), PikierenzusammenpikierenController.update);
	router.delete('/pikierenzusammenpikieren/:id', keycloak.protect("realm:teamleader"), PikierenzusammenpikierenController.remove);

    // PlatteController - Routen
    router.get('/platte', keycloak.protect("realm:visitor"), PlatteController.index);
	router.get('/platte/prev', keycloak.protect("realm:visitor"), PlatteController.prev);
	router.post('/platte', keycloak.protect("realm:teamleader"), PlatteController.store);
	router.get('/platte/:id', keycloak.protect("realm:visitor"), PlatteController.show);
	router.get('/platte/sub/:key/:id', keycloak.protect("realm:visitor"), PlatteController.showFindSub);
	router.put('/platte/:id', keycloak.protect("realm:teamleader"), PlatteController.update);
	router.delete('/platte/:id', keycloak.protect("realm:teamleader"), PlatteController.remove);

    // Preset_GießenDüngen_RegnerController - Routen
    router.get('/preset_gie%C3%9Fend%C3%BCngen_regner', keycloak.protect("realm:visitor"), Preset_GießenDüngen_RegnerController.index);
	router.get('/preset_gie%C3%9Fend%C3%BCngen_regner/prev', keycloak.protect("realm:visitor"), Preset_GießenDüngen_RegnerController.prev);
	router.post('/preset_gie%C3%9Fend%C3%BCngen_regner', keycloak.protect("realm:teamleader"), Preset_GießenDüngen_RegnerController.store);
	router.get('/preset_gie%C3%9Fend%C3%BCngen_regner/:id', keycloak.protect("realm:visitor"), Preset_GießenDüngen_RegnerController.show);
	router.get('/preset_gie%C3%9Fend%C3%BCngen_regner/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_GießenDüngen_RegnerController.showFindSub);
	router.put('/preset_gie%C3%9Fend%C3%BCngen_regner/:id', keycloak.protect("realm:teamleader"), Preset_GießenDüngen_RegnerController.update);
	router.delete('/preset_gie%C3%9Fend%C3%BCngen_regner/:id', keycloak.protect("realm:teamleader"), Preset_GießenDüngen_RegnerController.remove);

    // Preset_GießenDüngen_StufenController - Routen
    router.get('/preset_gie%C3%9Fend%C3%BCngen_stufen', keycloak.protect("realm:visitor"), Preset_GießenDüngen_StufenController.index);
	router.get('/preset_gie%C3%9Fend%C3%BCngen_stufen/prev', keycloak.protect("realm:visitor"), Preset_GießenDüngen_StufenController.prev);
	router.post('/preset_gie%C3%9Fend%C3%BCngen_stufen', keycloak.protect("realm:teamleader"), Preset_GießenDüngen_StufenController.store);
	router.get('/preset_gie%C3%9Fend%C3%BCngen_stufen/:id', keycloak.protect("realm:visitor"), Preset_GießenDüngen_StufenController.show);
	router.get('/preset_gie%C3%9Fend%C3%BCngen_stufen/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_GießenDüngen_StufenController.showFindSub);
	router.put('/preset_gie%C3%9Fend%C3%BCngen_stufen/:id', keycloak.protect("realm:teamleader"), Preset_GießenDüngen_StufenController.update);
	router.delete('/preset_gie%C3%9Fend%C3%BCngen_stufen/:id', keycloak.protect("realm:teamleader"), Preset_GießenDüngen_StufenController.remove);

    // Preset_GießenDüngen_StufenlosController - Routen
    router.get('/preset_gie%C3%9Fend%C3%BCngen_stufenlos', keycloak.protect("realm:visitor"), Preset_GießenDüngen_StufenlosController.index);
	router.get('/preset_gie%C3%9Fend%C3%BCngen_stufenlos/prev', keycloak.protect("realm:visitor"), Preset_GießenDüngen_StufenlosController.prev);
	router.post('/preset_gie%C3%9Fend%C3%BCngen_stufenlos', keycloak.protect("realm:teamleader"), Preset_GießenDüngen_StufenlosController.store);
	router.get('/preset_gie%C3%9Fend%C3%BCngen_stufenlos/:id', keycloak.protect("realm:visitor"), Preset_GießenDüngen_StufenlosController.show);
	router.get('/preset_gie%C3%9Fend%C3%BCngen_stufenlos/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_GießenDüngen_StufenlosController.showFindSub);
	router.put('/preset_gie%C3%9Fend%C3%BCngen_stufenlos/:id', keycloak.protect("realm:teamleader"), Preset_GießenDüngen_StufenlosController.update);
	router.delete('/preset_gie%C3%9Fend%C3%BCngen_stufenlos/:id', keycloak.protect("realm:teamleader"), Preset_GießenDüngen_StufenlosController.remove);

    // Preset_Gießen_RegnerController - Routen
    router.get('/preset_gie%C3%9Fen_regner', keycloak.protect("realm:visitor"), Preset_Gießen_RegnerController.index);
	router.get('/preset_gie%C3%9Fen_regner/prev', keycloak.protect("realm:visitor"), Preset_Gießen_RegnerController.prev);
	router.post('/preset_gie%C3%9Fen_regner', keycloak.protect("realm:teamleader"), Preset_Gießen_RegnerController.store);
	router.get('/preset_gie%C3%9Fen_regner/:id', keycloak.protect("realm:visitor"), Preset_Gießen_RegnerController.show);
	router.get('/preset_gie%C3%9Fen_regner/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_Gießen_RegnerController.showFindSub);
	router.put('/preset_gie%C3%9Fen_regner/:id', keycloak.protect("realm:teamleader"), Preset_Gießen_RegnerController.update);
	router.delete('/preset_gie%C3%9Fen_regner/:id', keycloak.protect("realm:teamleader"), Preset_Gießen_RegnerController.remove);

    // Preset_manuell_GießenController - Routen
    router.get('/preset_manuell_gie%C3%9Fen', keycloak.protect("realm:visitor"), Preset_manuell_GießenController.index);
	router.get('/preset_manuell_gie%C3%9Fen/prev', keycloak.protect("realm:visitor"), Preset_manuell_GießenController.prev);
	router.post('/preset_manuell_gie%C3%9Fen', keycloak.protect("realm:teamleader"), Preset_manuell_GießenController.store);
	router.get('/preset_manuell_gie%C3%9Fen/:id', keycloak.protect("realm:visitor"), Preset_manuell_GießenController.show);
	router.get('/preset_manuell_gie%C3%9Fen/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_manuell_GießenController.showFindSub);
	router.put('/preset_manuell_gie%C3%9Fen/:id', keycloak.protect("realm:teamleader"), Preset_manuell_GießenController.update);
	router.delete('/preset_manuell_gie%C3%9Fen/:id', keycloak.protect("realm:teamleader"), Preset_manuell_GießenController.remove);

    // Preset_Pflanzenschutz_Gießen_StufenController - Routen
    router.get('/preset_pflanzenschutz_gie%C3%9Fen_stufen', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_Gießen_StufenController.index);
	router.get('/preset_pflanzenschutz_gie%C3%9Fen_stufen/prev', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_Gießen_StufenController.prev);
	router.post('/preset_pflanzenschutz_gie%C3%9Fen_stufen', keycloak.protect("realm:teamleader"), Preset_Pflanzenschutz_Gießen_StufenController.store);
	router.get('/preset_pflanzenschutz_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_Gießen_StufenController.show);
	router.get('/preset_pflanzenschutz_gie%C3%9Fen_stufen/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_Gießen_StufenController.showFindSub);
	router.put('/preset_pflanzenschutz_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenschutz_Gießen_StufenController.update);
	router.delete('/preset_pflanzenschutz_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenschutz_Gießen_StufenController.remove);

    // Preset_Pflanzenschutz_Gießen_StufenlosController - Routen
    router.get('/preset_pflanzenschutz_gie%C3%9Fen_stufenlos', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_Gießen_StufenlosController.index);
	router.get('/preset_pflanzenschutz_gie%C3%9Fen_stufenlos/prev', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_Gießen_StufenlosController.prev);
	router.post('/preset_pflanzenschutz_gie%C3%9Fen_stufenlos', keycloak.protect("realm:teamleader"), Preset_Pflanzenschutz_Gießen_StufenlosController.store);
	router.get('/preset_pflanzenschutz_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_Gießen_StufenlosController.show);
	router.get('/preset_pflanzenschutz_gie%C3%9Fen_stufenlos/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_Gießen_StufenlosController.showFindSub);
	router.put('/preset_pflanzenschutz_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenschutz_Gießen_StufenlosController.update);
	router.delete('/preset_pflanzenschutz_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenschutz_Gießen_StufenlosController.remove);

    // Preset_Pflanzenschutz_SpritzenController - Routen
    router.get('/preset_pflanzenschutz_spritzen', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_SpritzenController.index);
	router.get('/preset_pflanzenschutz_spritzen/prev', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_SpritzenController.prev);
	router.post('/preset_pflanzenschutz_spritzen', keycloak.protect("realm:teamleader"), Preset_Pflanzenschutz_SpritzenController.store);
	router.get('/preset_pflanzenschutz_spritzen/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_SpritzenController.show);
	router.get('/preset_pflanzenschutz_spritzen/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenschutz_SpritzenController.showFindSub);
	router.put('/preset_pflanzenschutz_spritzen/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenschutz_SpritzenController.update);
	router.delete('/preset_pflanzenschutz_spritzen/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenschutz_SpritzenController.remove);

    // Preset_Pflanzenstärkung_Gießen_StufenController - Routen
    router.get('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_Gießen_StufenController.index);
	router.get('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen/prev', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_Gießen_StufenController.prev);
	router.post('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen', keycloak.protect("realm:teamleader"), Preset_Pflanzenstärkung_Gießen_StufenController.store);
	router.get('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_Gießen_StufenController.show);
	router.get('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_Gießen_StufenController.showFindSub);
	router.put('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenstärkung_Gießen_StufenController.update);
	router.delete('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufen/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenstärkung_Gießen_StufenController.remove);

    // Preset_Pflanzenstärkung_Gießen_StufenlosController - Routen
    router.get('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_Gießen_StufenlosController.index);
	router.get('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos/prev', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_Gießen_StufenlosController.prev);
	router.post('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos', keycloak.protect("realm:teamleader"), Preset_Pflanzenstärkung_Gießen_StufenlosController.store);
	router.get('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_Gießen_StufenlosController.show);
	router.get('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_Gießen_StufenlosController.showFindSub);
	router.put('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenstärkung_Gießen_StufenlosController.update);
	router.delete('/preset_pflanzenst%C3%A4rkung_gie%C3%9Fen_stufenlos/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenstärkung_Gießen_StufenlosController.remove);

    // Preset_Pflanzenstärkung_SpritzenController - Routen
    router.get('/preset_pflanzenst%C3%A4rkung_spritzen', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_SpritzenController.index);
	router.get('/preset_pflanzenst%C3%A4rkung_spritzen/prev', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_SpritzenController.prev);
	router.post('/preset_pflanzenst%C3%A4rkung_spritzen', keycloak.protect("realm:teamleader"), Preset_Pflanzenstärkung_SpritzenController.store);
	router.get('/preset_pflanzenst%C3%A4rkung_spritzen/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_SpritzenController.show);
	router.get('/preset_pflanzenst%C3%A4rkung_spritzen/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_Pflanzenstärkung_SpritzenController.showFindSub);
	router.put('/preset_pflanzenst%C3%A4rkung_spritzen/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenstärkung_SpritzenController.update);
	router.delete('/preset_pflanzenst%C3%A4rkung_spritzen/:id', keycloak.protect("realm:teamleader"), Preset_Pflanzenstärkung_SpritzenController.remove);

    // Preset_PikierenzusammenpikierenController - Routen
    router.get('/preset_pikierenzusammenpikieren', keycloak.protect("realm:visitor"), Preset_PikierenzusammenpikierenController.index);
	router.get('/preset_pikierenzusammenpikieren/prev', keycloak.protect("realm:visitor"), Preset_PikierenzusammenpikierenController.prev);
	router.post('/preset_pikierenzusammenpikieren', keycloak.protect("realm:teamleader"), Preset_PikierenzusammenpikierenController.store);
	router.get('/preset_pikierenzusammenpikieren/:id', keycloak.protect("realm:visitor"), Preset_PikierenzusammenpikierenController.show);
	router.get('/preset_pikierenzusammenpikieren/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_PikierenzusammenpikierenController.showFindSub);
	router.put('/preset_pikierenzusammenpikieren/:id', keycloak.protect("realm:teamleader"), Preset_PikierenzusammenpikierenController.update);
	router.delete('/preset_pikierenzusammenpikieren/:id', keycloak.protect("realm:teamleader"), Preset_PikierenzusammenpikierenController.remove);

    // Preset_SprühenNebeln_Gießwagen_StufenController - Routen
    router.get('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufen', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_Gießwagen_StufenController.index);
	router.get('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufen/prev', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_Gießwagen_StufenController.prev);
	router.post('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufen', keycloak.protect("realm:teamleader"), Preset_SprühenNebeln_Gießwagen_StufenController.store);
	router.get('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufen/:id', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_Gießwagen_StufenController.show);
	router.get('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufen/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_Gießwagen_StufenController.showFindSub);
	router.put('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufen/:id', keycloak.protect("realm:teamleader"), Preset_SprühenNebeln_Gießwagen_StufenController.update);
	router.delete('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufen/:id', keycloak.protect("realm:teamleader"), Preset_SprühenNebeln_Gießwagen_StufenController.remove);

    // Preset_SprühenNebeln_Gießwagen_StufenlosController - Routen
    router.get('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_Gießwagen_StufenlosController.index);
	router.get('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos/prev', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_Gießwagen_StufenlosController.prev);
	router.post('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos', keycloak.protect("realm:teamleader"), Preset_SprühenNebeln_Gießwagen_StufenlosController.store);
	router.get('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos/:id', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_Gießwagen_StufenlosController.show);
	router.get('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_Gießwagen_StufenlosController.showFindSub);
	router.put('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos/:id', keycloak.protect("realm:teamleader"), Preset_SprühenNebeln_Gießwagen_StufenlosController.update);
	router.delete('/preset_spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos/:id', keycloak.protect("realm:teamleader"), Preset_SprühenNebeln_Gießwagen_StufenlosController.remove);

    // Preset_SprühenNebeln_RegnerController - Routen
    router.get('/preset_spr%C3%BChennebeln_regner', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_RegnerController.index);
	router.get('/preset_spr%C3%BChennebeln_regner/prev', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_RegnerController.prev);
	router.post('/preset_spr%C3%BChennebeln_regner', keycloak.protect("realm:teamleader"), Preset_SprühenNebeln_RegnerController.store);
	router.get('/preset_spr%C3%BChennebeln_regner/:id', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_RegnerController.show);
	router.get('/preset_spr%C3%BChennebeln_regner/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_SprühenNebeln_RegnerController.showFindSub);
	router.put('/preset_spr%C3%BChennebeln_regner/:id', keycloak.protect("realm:teamleader"), Preset_SprühenNebeln_RegnerController.update);
	router.delete('/preset_spr%C3%BChennebeln_regner/:id', keycloak.protect("realm:teamleader"), Preset_SprühenNebeln_RegnerController.remove);

    // Preset_Stammlösung_ansetzenController - Routen
    router.get('/preset_stamml%C3%B6sung_ansetzen', keycloak.protect("realm:visitor"), Preset_Stammlösung_ansetzenController.index);
	router.get('/preset_stamml%C3%B6sung_ansetzen/prev', keycloak.protect("realm:visitor"), Preset_Stammlösung_ansetzenController.prev);
	router.post('/preset_stamml%C3%B6sung_ansetzen', keycloak.protect("realm:teamleader"), Preset_Stammlösung_ansetzenController.store);
	router.get('/preset_stamml%C3%B6sung_ansetzen/:id', keycloak.protect("realm:visitor"), Preset_Stammlösung_ansetzenController.show);
	router.get('/preset_stamml%C3%B6sung_ansetzen/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_Stammlösung_ansetzenController.showFindSub);
	router.put('/preset_stamml%C3%B6sung_ansetzen/:id', keycloak.protect("realm:teamleader"), Preset_Stammlösung_ansetzenController.update);
	router.delete('/preset_stamml%C3%B6sung_ansetzen/:id', keycloak.protect("realm:teamleader"), Preset_Stammlösung_ansetzenController.remove);

    // Preset_SteckenController - Routen
    router.get('/preset_stecken', keycloak.protect("realm:visitor"), Preset_SteckenController.index);
	router.get('/preset_stecken/prev', keycloak.protect("realm:visitor"), Preset_SteckenController.prev);
	router.post('/preset_stecken', keycloak.protect("realm:teamleader"), Preset_SteckenController.store);
	router.get('/preset_stecken/:id', keycloak.protect("realm:visitor"), Preset_SteckenController.show);
	router.get('/preset_stecken/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_SteckenController.showFindSub);
	router.put('/preset_stecken/:id', keycloak.protect("realm:teamleader"), Preset_SteckenController.update);
	router.delete('/preset_stecken/:id', keycloak.protect("realm:teamleader"), Preset_SteckenController.remove);

    // Preset_TopfenController - Routen
    router.get('/preset_topfen', keycloak.protect("realm:visitor"), Preset_TopfenController.index);
	router.get('/preset_topfen/prev', keycloak.protect("realm:visitor"), Preset_TopfenController.prev);
	router.post('/preset_topfen', keycloak.protect("realm:teamleader"), Preset_TopfenController.store);
	router.get('/preset_topfen/:id', keycloak.protect("realm:visitor"), Preset_TopfenController.show);
	router.get('/preset_topfen/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_TopfenController.showFindSub);
	router.put('/preset_topfen/:id', keycloak.protect("realm:teamleader"), Preset_TopfenController.update);
	router.delete('/preset_topfen/:id', keycloak.protect("realm:teamleader"), Preset_TopfenController.remove);

    // Preset_UmstellenController - Routen
    router.get('/preset_umstellen', keycloak.protect("realm:visitor"), Preset_UmstellenController.index);
	router.get('/preset_umstellen/prev', keycloak.protect("realm:visitor"), Preset_UmstellenController.prev);
	router.post('/preset_umstellen', keycloak.protect("realm:teamleader"), Preset_UmstellenController.store);
	router.get('/preset_umstellen/:id', keycloak.protect("realm:visitor"), Preset_UmstellenController.show);
	router.get('/preset_umstellen/sub/:key/:id', keycloak.protect("realm:visitor"), Preset_UmstellenController.showFindSub);
	router.put('/preset_umstellen/:id', keycloak.protect("realm:teamleader"), Preset_UmstellenController.update);
	router.delete('/preset_umstellen/:id', keycloak.protect("realm:teamleader"), Preset_UmstellenController.remove);

    // RegnerController - Routen
    router.get('/regner', keycloak.protect("realm:visitor"), RegnerController.index);
	router.get('/regner/prev', keycloak.protect("realm:visitor"), RegnerController.prev);
	router.post('/regner', keycloak.protect("realm:teamleader"), RegnerController.store);
	router.get('/regner/:id', keycloak.protect("realm:visitor"), RegnerController.show);
	router.get('/regner/sub/:key/:id', keycloak.protect("realm:visitor"), RegnerController.showFindSub);
	router.put('/regner/:id', keycloak.protect("realm:teamleader"), RegnerController.update);
	router.delete('/regner/:id', keycloak.protect("realm:teamleader"), RegnerController.remove);

    // SprühenNebeln_Gießwagen_StufenController - Routen
    router.get('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufen', keycloak.protect("realm:visitor"), SprühenNebeln_Gießwagen_StufenController.index);
	router.get('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufen/prev', keycloak.protect("realm:visitor"), SprühenNebeln_Gießwagen_StufenController.prev);
	router.post('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufen', keycloak.protect("realm:teamleader"), SprühenNebeln_Gießwagen_StufenController.store);
	router.get('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufen/:id', keycloak.protect("realm:visitor"), SprühenNebeln_Gießwagen_StufenController.show);
	router.get('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufen/sub/:key/:id', keycloak.protect("realm:visitor"), SprühenNebeln_Gießwagen_StufenController.showFindSub);
	router.put('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufen/:id', keycloak.protect("realm:teamleader"), SprühenNebeln_Gießwagen_StufenController.update);
	router.delete('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufen/:id', keycloak.protect("realm:teamleader"), SprühenNebeln_Gießwagen_StufenController.remove);

    // SprühenNebeln_Gießwagen_StufenlosController - Routen
    router.get('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos', keycloak.protect("realm:visitor"), SprühenNebeln_Gießwagen_StufenlosController.index);
	router.get('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos/prev', keycloak.protect("realm:visitor"), SprühenNebeln_Gießwagen_StufenlosController.prev);
	router.post('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos', keycloak.protect("realm:teamleader"), SprühenNebeln_Gießwagen_StufenlosController.store);
	router.get('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos/:id', keycloak.protect("realm:visitor"), SprühenNebeln_Gießwagen_StufenlosController.show);
	router.get('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos/sub/:key/:id', keycloak.protect("realm:visitor"), SprühenNebeln_Gießwagen_StufenlosController.showFindSub);
	router.put('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos/:id', keycloak.protect("realm:teamleader"), SprühenNebeln_Gießwagen_StufenlosController.update);
	router.delete('/spr%C3%BChennebeln_gie%C3%9Fwagen_stufenlos/:id', keycloak.protect("realm:teamleader"), SprühenNebeln_Gießwagen_StufenlosController.remove);

    // SprühenNebeln_RegnerController - Routen
    router.get('/spr%C3%BChennebeln_regner', keycloak.protect("realm:visitor"), SprühenNebeln_RegnerController.index);
	router.get('/spr%C3%BChennebeln_regner/prev', keycloak.protect("realm:visitor"), SprühenNebeln_RegnerController.prev);
	router.post('/spr%C3%BChennebeln_regner', keycloak.protect("realm:teamleader"), SprühenNebeln_RegnerController.store);
	router.get('/spr%C3%BChennebeln_regner/:id', keycloak.protect("realm:visitor"), SprühenNebeln_RegnerController.show);
	router.get('/spr%C3%BChennebeln_regner/sub/:key/:id', keycloak.protect("realm:visitor"), SprühenNebeln_RegnerController.showFindSub);
	router.put('/spr%C3%BChennebeln_regner/:id', keycloak.protect("realm:teamleader"), SprühenNebeln_RegnerController.update);
	router.delete('/spr%C3%BChennebeln_regner/:id', keycloak.protect("realm:teamleader"), SprühenNebeln_RegnerController.remove);

    // StammlösungsbeckenDüngemischungController - Routen
    router.get('/stamml%C3%B6sungsbeckend%C3%BCngemischung', keycloak.protect("realm:visitor"), StammlösungsbeckenDüngemischungController.index);
	router.get('/stamml%C3%B6sungsbeckend%C3%BCngemischung/prev', keycloak.protect("realm:visitor"), StammlösungsbeckenDüngemischungController.prev);
	router.post('/stamml%C3%B6sungsbeckend%C3%BCngemischung', keycloak.protect("realm:teamleader"), StammlösungsbeckenDüngemischungController.store);
	router.get('/stamml%C3%B6sungsbeckend%C3%BCngemischung/:id', keycloak.protect("realm:visitor"), StammlösungsbeckenDüngemischungController.show);
	router.get('/stamml%C3%B6sungsbeckend%C3%BCngemischung/sub/:key/:id', keycloak.protect("realm:visitor"), StammlösungsbeckenDüngemischungController.showFindSub);
	router.put('/stamml%C3%B6sungsbeckend%C3%BCngemischung/:id', keycloak.protect("realm:teamleader"), StammlösungsbeckenDüngemischungController.update);
	router.delete('/stamml%C3%B6sungsbeckend%C3%BCngemischung/:id', keycloak.protect("realm:teamleader"), StammlösungsbeckenDüngemischungController.remove);

    // Stammlösung_ansetzenController - Routen
    router.get('/stamml%C3%B6sung_ansetzen', keycloak.protect("realm:visitor"), Stammlösung_ansetzenController.index);
	router.get('/stamml%C3%B6sung_ansetzen/prev', keycloak.protect("realm:visitor"), Stammlösung_ansetzenController.prev);
	router.post('/stamml%C3%B6sung_ansetzen', keycloak.protect("realm:teamleader"), Stammlösung_ansetzenController.store);
	router.get('/stamml%C3%B6sung_ansetzen/:id', keycloak.protect("realm:visitor"), Stammlösung_ansetzenController.show);
	router.get('/stamml%C3%B6sung_ansetzen/sub/:key/:id', keycloak.protect("realm:visitor"), Stammlösung_ansetzenController.showFindSub);
	router.put('/stamml%C3%B6sung_ansetzen/:id', keycloak.protect("realm:teamleader"), Stammlösung_ansetzenController.update);
	router.delete('/stamml%C3%B6sung_ansetzen/:id', keycloak.protect("realm:teamleader"), Stammlösung_ansetzenController.remove);

    // SteckenController - Routen
    router.get('/stecken', keycloak.protect("realm:visitor"), SteckenController.index);
	router.get('/stecken/prev', keycloak.protect("realm:visitor"), SteckenController.prev);
	router.post('/stecken', keycloak.protect("realm:teamleader"), SteckenController.store);
	router.get('/stecken/:id', keycloak.protect("realm:visitor"), SteckenController.show);
	router.get('/stecken/sub/:key/:id', keycloak.protect("realm:visitor"), SteckenController.showFindSub);
	router.put('/stecken/:id', keycloak.protect("realm:teamleader"), SteckenController.update);
	router.delete('/stecken/:id', keycloak.protect("realm:teamleader"), SteckenController.remove);

    // TopfenController - Routen
    router.get('/topfen', keycloak.protect("realm:visitor"), TopfenController.index);
	router.get('/topfen/prev', keycloak.protect("realm:visitor"), TopfenController.prev);
	router.post('/topfen', keycloak.protect("realm:teamleader"), TopfenController.store);
	router.get('/topfen/:id', keycloak.protect("realm:visitor"), TopfenController.show);
	router.get('/topfen/sub/:key/:id', keycloak.protect("realm:visitor"), TopfenController.showFindSub);
	router.put('/topfen/:id', keycloak.protect("realm:teamleader"), TopfenController.update);
	router.delete('/topfen/:id', keycloak.protect("realm:teamleader"), TopfenController.remove);

    // UmstellenController - Routen
    router.get('/umstellen', keycloak.protect("realm:visitor"), UmstellenController.index);
	router.get('/umstellen/prev', keycloak.protect("realm:visitor"), UmstellenController.prev);
	router.post('/umstellen', keycloak.protect("realm:teamleader"), UmstellenController.store);
	router.get('/umstellen/:id', keycloak.protect("realm:visitor"), UmstellenController.show);
	router.get('/umstellen/sub/:key/:id', keycloak.protect("realm:visitor"), UmstellenController.showFindSub);
	router.put('/umstellen/:id', keycloak.protect("realm:teamleader"), UmstellenController.update);
	router.delete('/umstellen/:id', keycloak.protect("realm:teamleader"), UmstellenController.remove);

    // VerkaufController - Routen
    router.get('/verkauf', keycloak.protect("realm:visitor"), VerkaufController.index);
	router.get('/verkauf/prev', keycloak.protect("realm:visitor"), VerkaufController.prev);
	router.post('/verkauf', keycloak.protect("realm:teamleader"), VerkaufController.store);
	router.get('/verkauf/:id', keycloak.protect("realm:visitor"), VerkaufController.show);
	router.get('/verkauf/sub/:key/:id', keycloak.protect("realm:visitor"), VerkaufController.showFindSub);
	router.put('/verkauf/:id', keycloak.protect("realm:teamleader"), VerkaufController.update);
	router.delete('/verkauf/:id', keycloak.protect("realm:teamleader"), VerkaufController.remove);

	app.use('/api', router);
};
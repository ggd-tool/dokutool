const express = require("express");
const morgan = require("morgan");
const mongoose = require("mongoose");
const cors = require('cors');
const dotenv = require('dotenv');

// Environment laden aus .env
dotenv.config();

//Fix für deprecation Warnung kann/muss vielliecht mit neueren Mongoose Versionen entfernt werden
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

const Keycloak = require('./keycloak-connect/keycloak');
var keycloak = new Keycloak({}, {
    "realm": process.env.KC_REALM,
    "bearer-only": true,
    "auth-server-url": process.env.KC_AUTH_SERVER_URL,
    "ssl-required": "external",
    "resource": process.env.KC_CLIENT
});
const routes = require("./routes");
const preRoutes = require("./pre-routes");
const dialogdlowRoutes = require("./pre-dialogflow-routes")


// Port der API festlegen
const port = 5000;

// Mit MongoDB verbinden
// siehe Ordner ../mongo/docker-compose.yml für Konfiguration
mongoose
    .connect(process.env.MDB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        "auth": {
            "authSource": "admin"
        },
        "user": process.env.MDB_USER,
        "pass": process.env.MDB_PASS
    })
    .then(() => {
        // Wenn Verbindung zur DB hergestellt -> Server starten
        const app = express();
        app.use(cors())
        app.use(keycloak.middleware());
        if (process.env.M_PROD === "true") {
            app.use(morgan("short")); // configure morgan
        } else {
            app.use(morgan("dev")); // configure morgan
        }
        app.use(express.json());
        // Routen einbinden aus ./routes.js
        routes(app, keycloak);
        preRoutes(app, keycloak);
        dialogdlowRoutes(app, keycloak);

        app.listen(port, () => {
            console.log("Server wurde gestartet! Läuft auf Port", port);
        });
    })
    .catch((err) => {
        console.error("Server konnte nicht gestartet werden! Wurde die Datenbank gestartet?");
        throw err;
    });
    
const AuftragController = require('./controllers/Auftrag.controller');
const DüngerController = require('./controllers/Dünger.controller');
const FeldController = require('./controllers/Feld.controller');
const GießenDüngen_RegnerController = require('./controllers/GießenDüngen_Regner.controller');
const GießenDüngen_StufenController = require('./controllers/GießenDüngen_Stufen.controller');
const GießenDüngen_StufenlosController = require('./controllers/GießenDüngen_Stufenlos.controller');
const Gießen_RegnerController = require('./controllers/Gießen_Regner.controller');
const Gießwagen_StufenController = require('./controllers/Gießwagen_Stufen.controller');
const Gießwagen_StufenlosController = require('./controllers/Gießwagen_Stufenlos.controller');
const manuell_GießenController = require('./controllers/manuell_Gießen.controller');
const PflanzenschutzmittelController = require('./controllers/Pflanzenschutzmittel.controller');
const Pflanzenschutz_Gießen_StufenController = require('./controllers/Pflanzenschutz_Gießen_Stufen.controller');
const Pflanzenschutz_Gießen_StufenlosController = require('./controllers/Pflanzenschutz_Gießen_Stufenlos.controller');
const Pflanzenschutz_SpritzenController = require('./controllers/Pflanzenschutz_Spritzen.controller');
const PflanzenstärkungsmittelController = require('./controllers/Pflanzenstärkungsmittel.controller');
const Pflanzenstärkung_Gießen_StufenController = require('./controllers/Pflanzenstärkung_Gießen_Stufen.controller');
const Pflanzenstärkung_Gießen_StufenlosController = require('./controllers/Pflanzenstärkung_Gießen_Stufenlos.controller');
const Pflanzenstärkung_SpritzenController = require('./controllers/Pflanzenstärkung_Spritzen.controller');
const PikierenzusammenpikierenController = require('./controllers/Pikierenzusammenpikieren.controller');
const PlatteController = require('./controllers/Platte.controller');
const Preset_GießenDüngen_RegnerController = require('./controllers/Preset_GießenDüngen_Regner.controller');
const Preset_GießenDüngen_StufenController = require('./controllers/Preset_GießenDüngen_Stufen.controller');
const Preset_GießenDüngen_StufenlosController = require('./controllers/Preset_GießenDüngen_Stufenlos.controller');
const Preset_Gießen_RegnerController = require('./controllers/Preset_Gießen_Regner.controller');
const Preset_manuell_GießenController = require('./controllers/Preset_manuell_Gießen.controller');
const Preset_Pflanzenschutz_Gießen_StufenController = require('./controllers/Preset_Pflanzenschutz_Gießen_Stufen.controller');
const Preset_Pflanzenschutz_Gießen_StufenlosController = require('./controllers/Preset_Pflanzenschutz_Gießen_Stufenlos.controller');
const Preset_Pflanzenschutz_SpritzenController = require('./controllers/Preset_Pflanzenschutz_Spritzen.controller');
const Preset_Pflanzenstärkung_Gießen_StufenController = require('./controllers/Preset_Pflanzenstärkung_Gießen_Stufen.controller');
const Preset_Pflanzenstärkung_Gießen_StufenlosController = require('./controllers/Preset_Pflanzenstärkung_Gießen_Stufenlos.controller');
const Preset_Pflanzenstärkung_SpritzenController = require('./controllers/Preset_Pflanzenstärkung_Spritzen.controller');
const Preset_PikierenzusammenpikierenController = require('./controllers/Preset_Pikierenzusammenpikieren.controller');
const Preset_SprühenNebeln_Gießwagen_StufenController = require('./controllers/Preset_SprühenNebeln_Gießwagen_Stufen.controller');
const Preset_SprühenNebeln_Gießwagen_StufenlosController = require('./controllers/Preset_SprühenNebeln_Gießwagen_Stufenlos.controller');
const Preset_SprühenNebeln_RegnerController = require('./controllers/Preset_SprühenNebeln_Regner.controller');
const Preset_Stammlösung_ansetzenController = require('./controllers/Preset_Stammlösung_ansetzen.controller');
const Preset_SteckenController = require('./controllers/Preset_Stecken.controller');
const Preset_TopfenController = require('./controllers/Preset_Topfen.controller');
const Preset_UmstellenController = require('./controllers/Preset_Umstellen.controller');
const RegnerController = require('./controllers/Regner.controller');
const SprühenNebeln_Gießwagen_StufenController = require('./controllers/SprühenNebeln_Gießwagen_Stufen.controller');
const SprühenNebeln_Gießwagen_StufenlosController = require('./controllers/SprühenNebeln_Gießwagen_Stufenlos.controller');
const SprühenNebeln_RegnerController = require('./controllers/SprühenNebeln_Regner.controller');
const StammlösungsbeckenDüngemischungController = require('./controllers/StammlösungsbeckenDüngemischung.controller');
const Stammlösung_ansetzenController = require('./controllers/Stammlösung_ansetzen.controller');
const SteckenController = require('./controllers/Stecken.controller');
const TopfenController = require('./controllers/Topfen.controller');
const UmstellenController = require('./controllers/Umstellen.controller');
const VerkaufController = require('./controllers/Verkauf.controller');

module.exports = {
    AuftragController,
    DüngerController,
    FeldController,
    GießenDüngen_RegnerController,
    GießenDüngen_StufenController,
    GießenDüngen_StufenlosController,
    Gießen_RegnerController,
    Gießwagen_StufenController,
    Gießwagen_StufenlosController,
    manuell_GießenController,
    PflanzenschutzmittelController,
    Pflanzenschutz_Gießen_StufenController,
    Pflanzenschutz_Gießen_StufenlosController,
    Pflanzenschutz_SpritzenController,
    PflanzenstärkungsmittelController,
    Pflanzenstärkung_Gießen_StufenController,
    Pflanzenstärkung_Gießen_StufenlosController,
    Pflanzenstärkung_SpritzenController,
    PikierenzusammenpikierenController,
    PlatteController,
    Preset_GießenDüngen_RegnerController,
    Preset_GießenDüngen_StufenController,
    Preset_GießenDüngen_StufenlosController,
    Preset_Gießen_RegnerController,
    Preset_manuell_GießenController,
    Preset_Pflanzenschutz_Gießen_StufenController,
    Preset_Pflanzenschutz_Gießen_StufenlosController,
    Preset_Pflanzenschutz_SpritzenController,
    Preset_Pflanzenstärkung_Gießen_StufenController,
    Preset_Pflanzenstärkung_Gießen_StufenlosController,
    Preset_Pflanzenstärkung_SpritzenController,
    Preset_PikierenzusammenpikierenController,
    Preset_SprühenNebeln_Gießwagen_StufenController,
    Preset_SprühenNebeln_Gießwagen_StufenlosController,
    Preset_SprühenNebeln_RegnerController,
    Preset_Stammlösung_ansetzenController,
    Preset_SteckenController,
    Preset_TopfenController,
    Preset_UmstellenController,
    RegnerController,
    SprühenNebeln_Gießwagen_StufenController,
    SprühenNebeln_Gießwagen_StufenlosController,
    SprühenNebeln_RegnerController,
    StammlösungsbeckenDüngemischungController,
    Stammlösung_ansetzenController,
    SteckenController,
    TopfenController,
    UmstellenController,
    VerkaufController,
};
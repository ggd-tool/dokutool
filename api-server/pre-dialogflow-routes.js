const express = require('express');
const router = express.Router();

// Das Audio für Dialogflow wird durch Multer zum Backend gesendet.
const multer  = require('multer')
const upload = multer()
const audio = upload.fields([{ name: 'audio'}, { name: 'contexts'}]);

const excel = upload.single("excel")

const DialogflowController = require('./pre-controllers/dialogflow.controller');
const ImportController = require('./pre-controllers/import.controller');

module.exports = function (app, keycloak) {

	//Dialogflow
	router.post('/dialogflow/text',keycloak.protect("realm:worker"), DialogflowController.UseAgent_text);
	router.post('/dialogflow/audio', keycloak.protect("realm:worker"), audio, DialogflowController.UseAgent_audio);

	router.post('/dialogflow/showContext',keycloak.protect("realm:worker"), DialogflowController.showContext);// Durch ein ungelösten Bug erhält man ein "--", wenn get verwendet wird 
	router.post('/dialogflow/createContext',keycloak.protect("realm:worker"), DialogflowController.createContext);
	router.delete('/dialogflow/deleteAllContexts',keycloak.protect("realm:worker"), DialogflowController.deleteAllContexts);
	router.post('/dialogflow/deleteContext',keycloak.protect("realm:worker"), DialogflowController.deleteContext);

	router.post('/dialogflow/showEntities',keycloak.protect("realm:worker"), DialogflowController.showEntities);// Durch ein ungelösten Bug erhält man ein "--", wenn get verwendet wird 
    
	router.post('/excel/template',keycloak.protect("realm:worker") , ImportController.getImportTemplate);
	router.post('/excel/import',keycloak.protect("realm:worker"), excel, ImportController.import);

	app.use('/api/pre', router);
};
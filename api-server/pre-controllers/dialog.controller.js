const jwt_decode = require('jwt-decode');

const Favoriten = require('../pre-models/favoriten.model');
const Kategorien = require('../pre-models/kategorien.model');
const ObjectDialog = require('../pre-models/objectDialog.model');
const MassnahmenDialog = require('../pre-models/massnahmenDialog.model');
const PresetsDialog = require('../pre-models/presetsDialog.model');

const exportData = require('./export')

const controller = require('../index-controller');
const url = require('url');

let response = [];

var mysql = require('mysql');
var pool = mysql.createPool({
  connectionLimit: 1000,
  host: process.env.MSQL_HOST,
  user: process.env.MSQL_USER,
  password: process.env.MSQL_PASS,
  database: process.env.MSQL_DB
});

const DialogController = {
  //Gibt alle Objeckte des erfragten typ zurück
  async index(req, res) {
    const type = url.format({ pathname: req.originalUrl }).split('/')[3];
    let objects;
    switch (type) {
      case 'objects':
        objects = await ObjectDialog.find();
        break;
      case 'ma%C3%9Fnahmen':
        objects = await MassnahmenDialog.find();
        break;
      case 'presets':
        objects = await PresetsDialog.find();
        break;
    };
    res.send(objects);
  },
  //Objekte für Dropdowns, bzw. Multidropdowns laden
  async show(req, res) {
    const type = url.format({ pathname: req.originalUrl }).split('/')[3];
    let object;
    switch (type) {
      case 'objects':
        object = await ObjectDialog.findOne({ name: req.params.typ });
        break;
      case 'ma%C3%9Fnahmen':
        object = await MassnahmenDialog.findOne({ name: req.params.typ });
        break;
      case 'presets':
        if (req.params.typ.includes("Preset")) {
          object = await PresetsDialog.findOne({ name: req.params.typ });
        } else {
          object = await PresetsDialog.findOne({ name: "Preset_" + req.params.typ });
        }
        break;
    };
    let objectData;
    for (let i = 0; i < object.data.length; i++) {
      for (let j = 0; j < object.data[i].inputs.length; j++) {
        objectData = object.data[i].inputs[j];
        if ((objectData.type === 'dropdown' || objectData.type === 'multidropdown') && objectData.ref) {
          objectData.options = [];
          let all = await controller[formatForFiles(objectData.ref) + 'Controller'].localIndex();
          all.forEach(element => {
            let tmpElement = {};
            tmpElement.key = element._id;
            // Hier müssen wa nochmal gucken sonst nimmt der als label immer das attribut name
            tmpElement.value = element.name;
            objectData.options.push(tmpElement);
          });
        } else if (objectData.type === 'array') {
          // Überprüfen ob Arrays ein ref haben
          for (let k = 0; k < object.data[i].inputs[j].options.length; k++) {
            objectData = object.data[i].inputs[j].options[k];
            if ((objectData.type === 'dropdown' || objectData.type === 'multidropdown') && objectData.ref) {
              objectData.options = [];
              let all = await controller[formatForFiles(objectData.ref) + 'Controller'].localIndex();
              all.forEach(element => {
                let tmpElement = {};
                tmpElement.key = element._id;
                // Hier müssen wa nochmal gucken sonst nimmt der als label immer das attribut name
                tmpElement.value = element.name;
                objectData.options.push(tmpElement);
              });
            }
          }
        }
      }
    }
    res.send(object);
  },
  async update(req, res) {
    const typ = req.params.typ;
    const id = req.params.id;
    let object;
    switch (typ) {
      case 'typen':
        object = await ObjectDialog.findOneAndUpdate({ _id: id }, { kategorien: req.body.kategorien }, {
          new: true
        });
        break;
      case 'maßnahmen':
        object = await MassnahmenDialog.findOneAndUpdate({ _id: id }, { kategorien: req.body.kategorien }, {
          new: true
        });
        break;
      case 'presets':
        object = await PresetsDialog.findOneAndUpdate({ _id: id }, { kategorien: req.body.kategorien }, {
          new: true
        });
        break;
    };
    res.send(object);
  },

  //Lädt alle angelegten Typen/Objecte/Presets eines Vorgangs
  async showAll(req, res) {
    let objects;
    if (!req.query.pageSize) {
      let formated = formatForFiles(req.params.object);
      if (await controller[formated + 'Controller'] !== undefined) {
        objects = await controller[formated + 'Controller'].localIndex();
      }
      res.send(objects);
    } else {
      let formated = formatForFiles(req.params.object);
      if (await controller[formated + 'Controller'] !== undefined) {
        await controller[formated + 'Controller'].localIndex(req, res);
      }
    }
  },

  //Lädt alle presets zu einem bestimmten Namen
  async getByNameForDropdown(req, res) {
    let presets;
    let formated = formatForFiles(req.params.presetName);
    if (await controller[formated + 'Controller'] !== undefined) {
      presets = await controller[formated + 'Controller'].localIndex();
    }
    let formatedReturn = [];
    if (presets !== undefined) {
      for (let i = 0; i < presets.length; i++) {
        formatedReturn.push({ key: presets[i]._id, value: presets[i].name });
      }
    }
    res.send(formatedReturn);
  },

  //Laden für depend
  async loadDepend(req, res) {
    var objectId = require('mongoose').Types.ObjectId;
    let attributes = req.params.depend.split(".");
    let ids = req.params.id.split(",");
    let dropdown = [];
    for (let i = 0; i < ids.length; i++) {
      response = [];
      let object = await controller[formatForFiles(attributes[0]) + 'Controller'].loadByID(ids[i]);
      let currentObject = object[formatForFiles(attributes[1]).toLowerCase()];
      if (currentObject !== undefined) {
        if (currentObject.length === undefined) {
          currentObject = [currentObject];
        }
        if (attributes.length > 2) {
          for (let l = 0; l < currentObject.length; l++) {
            let element = currentObject[l];
            let name = object.name;
            for (let j = 2; j < attributes.length; j++) {
              name += " - " + element["name"];
              element = element[formatForFiles(attributes[j]).toLowerCase()];
              if (element.length && objectId.isValid(element[0].toString())) {
                for (let k = 0; k < element.length; k++) {
                  let current = element[k];
                  //Testen ob element objectId um es zu laden
                  if (objectId.isValid(current.toString())) {
                    //ersten Buchstaben Groß für controller
                    let capitalize = attributes[j].charAt(0).toUpperCase() + attributes[j].slice(1);
                    if (await controller[formatForFiles(capitalize) + 'Controller'] !== undefined) {
                      current = await controller[formatForFiles(capitalize) + 'Controller'].loadByID(current);
                    }
                  }
                  await DialogController.splitMultiId(current, attributes, j, name);
                }
                j = attributes.length;
              }
              if (j === attributes.length - 1) {
                response.push({ name: name, values: element });
              }
            }
          }

          //Elemente für Dropdown formatieren 
          response.forEach(element => {
            let tmpElement = { group: "", keys: [], data: [] };
            tmpElement.group = element.name;
            element.values.forEach(value => {
              let tmp = {};
              tmp.key = value._id;
              // Hier müssen wa nochmal gucken sonst nimmt der als label immer das attribut name
              tmp.value = value.name;
              tmpElement.keys.push(value._id);
              tmpElement.data.push(tmp);
            })
            dropdown.push(tmpElement);
          });
        } else {
          let data = [];
          let keys = [];
          for (let i = 0; i < currentObject.length; i++) {
            let element = currentObject[i];
            if (element._id) {
              keys.push(element._id);
              data.push({ value: element.name, key: element._id });
            } else {
              let dialog = await ObjectDialog.findOne({ name: attributes[0] });
              dialog.data.forEach(section => {
                section.inputs.forEach(input => {
                  if (formatForFiles(input.key).toLowerCase() === formatForFiles(attributes[1]).toLowerCase()) {
                    input.options.forEach(option => {
                      object[formatForFiles(attributes[1]).toLowerCase()].forEach(value => {
                        if (option.key === value) {
                          keys.push(option.key);
                          data.push(option);
                        }
                      });
                    });
                  }
                })
              })
              i = currentObject.length;
            }
          };
          dropdown.push({ group: object.name, data: data, keys: keys });
        }
      }
    }
    res.send(dropdown);
  },

  async splitMultiId(object, attributes, index, name) {
    var objectId = require('mongoose').Types.ObjectId;
    for (let j = index + 1; j < attributes.length; j++) {
      name += " - " + object["name"];
      object = object[formatForFiles(attributes[j]).toLowerCase()];
      if (object.length) {
        for (let k = 0; k < object.length; k++) {
          let current = object[k];
          //Testen ob element objectId um es zu laden
          if (current.length && objectId.isValid(current[0].toString())) {
            //ersten Buchstaben Groß für controller
            let capitalize = attributes[j].charAt(0).toUpperCase() + attributes[j].slice(1);
            if (await controller[formatForFiles(capitalize) + 'Controller'] !== undefined) {
              current = await controller[formatForFiles(capitalize) + 'Controller'].loadByID(current);
            }
          }
          await DialogController.splitMultiId(current, attributes, j);
        }
      } if (j === attributes.length - 1) {
        response.push({ name: name, values: object });
      }
    }
  },

  // Gibt alle User in einem Array zurück
  // Ein User hat die Eigenschaften: ID, EMAIL, FIRST_NAME, LAST_NAME und USERNAME
  // Ist eine Eigenschaft in Keycloak nicht hinterlegt dann ist sie hier null (ID immer vorhanden)
  async getAllUsers(req, res) {
    pool.query('SELECT ID, EMAIL, FIRST_NAME, LAST_NAME, USERNAME FROM user_entity WHERE REALM_ID = "Dokutool"', function (error, results, fields) {
      if (error) throw res.send(error);
      res.send(results);
    });
  },

  async loadById(req, res) {
    let typ = req.params.typ;
    if (await controller[formatForFiles(typ) + 'Controller'] !== undefined) {
      await controller[formatForFiles(typ) + 'Controller'].show(req, res);
    }
  },

  // Nutzer fügt hinzu oder löscht einen Favoriten
  async updateFavorites(req, res) {
    // keycloak Userid aus dem jwt token auslesen
    var userId = jwt_decode(req.kauth.grant.access_token.token).sub;

    // action - hinzufügen oder löschen
    var action = {};
    // type - massnahme, object oder preset
    var type = {};

    switch (req.body.type) {
      case 'massnahme':
        type = { 'massnahme': req.body.id };
        break;
      case 'object':
        type = { 'object': req.body.id };
        break;
      case 'preset':
        type = { 'preset': req.body.id };
        break;
    }

    if (req.body.status) {
      action = { $push: type };
    } else {
      action = { $pull: type };
    }

    // Abspeichern in der Datenbank
    var updatedFav = await Favoriten.updateOne(
      { user: userId },
      action,
      { upsert: true, new: true, setDefaultsOnInsert: true }
    );
    res.send(updatedFav);
  },

  // Alle Favoriten eines Nutzers eines bestimmten Typen (siehe updateFavorites) laden
  async getAllFavorites(req, res) {
    var userId = jwt_decode(req.kauth.grant.access_token.token).sub;

    try {
      const favs = await Favoriten.findOne({ user: userId });
      res.send(favs[req.query.type]);
    } catch (err) {
      res.send([]);
    }

  },

  async addKategorien(req, res) {
    try {
      const kategorie = new Kategorien({
        'name': req.body.name,
        'color': req.body.color,
        'textColor': req.body.textColor
      });
      await kategorie.save();
      res.send(kategorie);
    } catch (err) {
      res.send(err);
    }
  },

  async getKategorien(req, res) {
    try {
      const kategorien = await Kategorien.find();
      res.send(kategorien);
    } catch (err) {
      res.send([]);
    }
  },

  async deleteKategorien(req, res) {
    try {
      const kategorien = await Kategorien.findOneAndDelete({ _id: req.params.id });
      //Kategorien entfernen wenn angewählt
      await MassnahmenDialog.updateMany({ $pull: { kategorien: req.params.id } });
      await ObjectDialog.updateMany({ $pull: { kategorien: req.params.id } });
      await PresetsDialog.updateMany({ $pull: { kategorien: req.params.id } });
      res.send(kategorien);
    } catch (err) {
      res.send(err);
    }
  },

  // Bericht in Excel-Datei exportieren 
  async export(req, res) {
    //Zeitraum aus params extrahieren
    let start = req.query.start;
    let ende = req.query.ende;
    let users;
    //Liste der User zum übergeben laden damit nicht erneut verbindung hergestellt werden muss
    pool.query('SELECT ID, EMAIL, FIRST_NAME, LAST_NAME, USERNAME FROM user_entity WHERE REALM_ID = "Dokutool"', async function (error, results) {
      if (error) {
        throw res.send(error);
      } else {
        users = results;
        await exportData.excelExport(start, ende, users, res);
      }
    })
  }
};

//Namen für files wie z.B. Controller formatieren (keineklammern, / usw.) 
function formatForFiles(objectName) {
  objectName = objectName.toString();
  if (objectName.includes(" ")) {
    objectName = objectName.split(" ").join("_");
  }
  if (objectName.includes(".")) {
    objectName = objectName.split(".").join("");
  }
  if (objectName.includes("(")) {
    objectName = objectName.split("(").join("");;
  }
  if (objectName.includes(")")) {
    objectName = objectName.split(")").join("");;
  }
  if (objectName.includes("-")) {
    objectName = objectName.split("-").join("");;
  }
  return objectName;
}

module.exports = DialogController;
// imports
const xlsxPop = require('xlsx-populate');
const controller = require('../index-controller');
const models = require('../models-index');
const admZip = require('adm-zip');
let zip = new admZip();

module.exports = {
  excelExport: async function (start, ende, users, res) {
    //await exportSpritztagebuch(start, ende, users);
    await exportKulturtagebuch(start, ende, users);
    //collect data to send from zip
    let dataToSend = zip.toBuffer();
    res.send(dataToSend);
  }
}

async function exportKulturtagebuch(start, ende, users) {
  await xlsxPop.fromFileAsync('./templates/Kulturtagebuch.xlsx').then(async workbook => {
    let worksheet = workbook.sheet('Grunddaten');
    //default style
    let style = {
      type: "solid",
      color: {
        rgb: "ffffff"
      },
      border: true
    };
    // Alle Anwender einfüllen
    let anwenderValues = [];
    rangeToFill = worksheet.range(`J2:J${users.length + 1}`)
    users.forEach(user => {
      anwenderValues.push([`${user['FIRST_NAME']} ${user['LAST_NAME']}`]);
    })
    //Style fuer range setzen
    rangeToFill.style("fill", style);
    // Daten in Range schreiben
    rangeToFill.value(anwenderValues);
    // Alle Aufträge (Kulturen/Sätze) einfüllen
    let auftragValues = [];
    let auftraege = await controller['AuftragController'].localIndex();
    rangeToFill = worksheet.range(`D2:D${auftraege.length + 1}`);
    auftraege.forEach(auftrag => {
      auftragValues.push([`${auftrag['pflanze']} \[${auftrag['name']}\]`]);
    })
    //Style fuer range setzen
    rangeToFill.style("fill", style);
    // Daten in Range schreiben
    rangeToFill.value(auftragValues);
    // Alle Produkte (Dünger/Pflanzenschutzmittel/Pflanzenstärkungsmittel/Düngemischungen) einfüllen
    let produktValues = [];
    let psm = await controller['PflanzenschutzmittelController'].localIndex();
    let d = await controller['DüngerController'].localIndex();
    let pstm = await controller['PflanzenstärkungsmittelController'].localIndex();
    let slbd = await controller['StammlösungsbeckenDüngemischungController'].localIndex();
    rangeToFill = worksheet.range(`P2:P${psm.length + d.length + pstm.length + slbd.length + 1}`);
    psm.forEach(entity => {
      produktValues.push([entity['name']]);
    });
    d.forEach(entity => {
      produktValues.push([entity['name']]);
    });
    pstm.forEach(entity => {
      produktValues.push([entity['name']]);
    });
    slbd.forEach(entity => {
      produktValues.push([entity['name']]);
    });
    //Style fuer range setzen
    rangeToFill.style("fill", style);
    // Daten in Range schreiben
    rangeToFill.value(produktValues);
    // Alle Felder einfüllen
    let feldValues = [];
    let felder = await controller['FeldController'].localIndex();
    rangeToFill = worksheet.range(`A2:B${felder.length + 1}`);
    felder.forEach(entity => {
      feldValues.push([entity['name'], entity['fläche_brutto_in_m2']]);
    })
    //Style fuer range setzen
    rangeToFill.style("fill", style);
    // Daten in Range schreiben
    rangeToFill.value(feldValues);

    // Tagebuch
    worksheet = workbook.sheet('Tagebuch');
    //Bewässerungsmaßnahmen für Stufenlose GW sammeln und Kulturen ermitteln
    let bewaessernStufenlos = await collectAndCombine('GießenDüngen_Stufenlos', start, ende, 'gießwagen_stufenlos', 'Gießwagen_Stufenlos');
    //Bewässerungsmaßnahmen für GW mit Stufen sammeln und Kulturen ermitteln
    let bewaessernStufen = await collectAndCombine('GießenDüngen_Stufen', start, ende, 'gießwagen_stufen', 'Gießwagen_Stufen');
    //Bewässerungsmaßnahmen für Regner sammeln und Kulturen ermitteln
    let bewaessernRegner = await collectAndCombine('Gießen_Regner', start, ende, 'regner', 'Regner');
    //Bewässerungsmaßnahmen für manuell sammeln und Kulturen ermitteln
    let bewaessernManuell = await collectAndCombine('manuell_Gießen', start, ende, 'spritzen', '');
    //Pflanzenschutzmaßnahmen für GW mit Stufen sammeln und Kulturen ermitteln
    let schutzStufen = await collectAndCombine('Pflanzenschutz_Gießen_Stufen', start, ende, 'gießwagen_stufen', 'Gießwagen_Stufen');
    //Pflanzenschutzmaßnahmen für Stufenlose GW sammeln und Kulturen ermitteln
    let schutzStufenlos = await collectAndCombine('Pflanzenschutz_Gießen_Stufenlos', start, ende, 'gießwagen_stufenlos', 'Gießwagen_Stufenlos');
    //Pflanzenschutzmaßnahmen spritzen sammeln und Kulturen ermitteln
    let schutzSpritzen = await collectAndCombine('Pflanzenschutz_Spritzen', start, ende, 'spritzen', '');
    //Pflanzenstärkungmaßnahmen für GW mit Stufen sammeln und Kulturen ermitteln
    let starkungStufen = await collectAndCombine('Pflanzenstärkung_Gießen_Stufen', start, ende, 'gießwagen_stufen', 'Gießwagen_Stufen');
    //Pflanzenstärkungmaßnahmen für Stufenlose GW sammeln und Kulturen ermitteln
    let starkungStufenlos = await collectAndCombine('Pflanzenstärkung_Gießen_Stufenlos', start, ende, 'gießwagen_stufenlos', 'Gießwagen_Stufenlos');
    //Pflanzenstärkungmaßnahmen spritzen sammeln und Kulturen ermitteln
    let starkungSpritzen = await collectAndCombine('Pflanzenstärkung_Spritzen', start, ende, 'spritzen', '');
    //Bewässerungsmaßnahmen für Stufenlose GW sammeln und Kulturen ermitteln
    let spruhenStufenlos = await collectAndCombine('SprühenNebeln_Gießwagen_Stufenlos', start, ende, 'gießwagen_stufenlos', 'Gießwagen_Stufenlos');
    //Bewässerungsmaßnahmen für GW mit Stufen sammeln und Kulturen ermitteln
    let spruhenStufen = await collectAndCombine('SprühenNebeln_Gießwagen_Stufen', start, ende, 'gießwagen_stufen', 'Gießwagen_Stufen');
    //Bewässerungsmaßnahmen für Regner sammeln und Kulturen ermitteln
    let spruhenRegner = await collectAndCombine('SprühenNebeln_Regner', start, ende, 'regner', 'Regner');
    rangeToFill = worksheet.range(`A2:W${bewaessernStufen.length + bewaessernStufenlos.length + bewaessernRegner.length + bewaessernManuell.length + schutzStufen.length + schutzStufenlos.length + schutzSpritzen.length + starkungStufen.length + starkungStufenlos.length + starkungSpritzen.length + spruhenStufenlos.length + spruhenStufen.length + spruhenRegner.length + 1}`);
    rangeToFill.style("fill", style);
    rangeToFill.value(await format(bewaessernStufen, bewaessernStufenlos, bewaessernRegner, bewaessernManuell, schutzStufen, schutzStufenlos, schutzSpritzen, starkungStufen, starkungStufenlos, starkungSpritzen, spruhenStufenlos, spruhenStufen, spruhenRegner));
    // Ausgabe
    return workbook.outputAsync();
  }).then(data => {
    zip.addFile("Spritztagebuch.xlsx", data);
  });
}

async function format(bewaessernStufen, bewaessernStufenlos, bewaessernRegner, bewaessernManuell, schutzStufen, schutzStufenlos, schutzSpritzen, starkungStufen, starkungStufenlos, starkungSpritzen, spruhenStufenlos, spruhenStufen, spruhenRegner) {
  return new Promise(async (resolve, reject) => {
    let alleMassnahmen = [];
    //  BewässernStufen
    for (let i = 0; i < bewaessernStufen.length; i++) {
      let jahr = getWeekNumber(bewaessernStufen[i].datum)[0];
      let kw = getWeekNumber(bewaessernStufen[i].datum)[1].toString();
      let datum = bewaessernStufen[i].datum;
      let gießwagen_stufen = await findObjectById(bewaessernStufen[i].gießwagen_stufen, "Gießwagen_Stufen");
      let feld = await findObjectById(gießwagen_stufen.feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, bewaessernStufen[i]);
      let beetgröße = getBeeteToGröße(feld, bewaessernStufen[i]);
      let feldgrößeBeetGrößeVerhältnis = beetgröße / feld.fläche_netto_in_m2;
      let massnahmenname = 'Gießen-Düngen Stufen';
      let kultur = bewaessernStufen[i].kultur;
      let grund = bewaessernStufen[i].bemerkung;
      let produkt = await findObjectById(bewaessernStufen[i].stammlösungsbecken_düngemischung, "StammlösungsbeckenDüngemischung");
      let produktName = '';
      try {
        produktName = produkt.name;
      } catch (err) {
      }
      let konzentrationEC = (bewaessernStufen[i].konzentration_ec !== null) ? bewaessernStufen[i].konzentration_ec : '';
      let wasserEC = (bewaessernStufen[i].wasser_ec_ms_cm !== null) ? bewaessernStufen[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      try {
        wasseraufwandLAnw = calcwasseraufwandLAnw(gießwagen_stufen, bewaessernStufen[i], feldgrößeBeetGrößeVerhältnis);
        wasseraufwandmengeLHa = calcwasseraufwandmengeLHa(wasseraufwandLAnw, beetgröße);
        wasseraufwandmengeLm2 = calcwasseraufwandmengeLm2(wasseraufwandmengeLHa);
      } catch (err) { }
      try {
        konzentrationGL = bewaessernStufen[i].konzentration_g_l;
        if (konzentrationGL === null) {
          throw 'null';
        }
        produktmengeKGAnw = calcProduktmengeKGAnw(wasseraufwandLAnw, bewaessernStufen[i]);
        produktmengeKGHa = calcproduktmengeKGHa(produktmengeKGAnw, beetgröße);
      } catch (err) { }
      let applikationsart = '';
      let womit = 'Gießwagen (Stufen)';
      let user = bewaessernStufen[i].durchgeführt_von.FIRST_NAME + ' ' + bewaessernStufen[i].durchgeführt_von.LAST_NAME;
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      //console.log(arr);
      alleMassnahmen.push(arr);
    }
    // BewässernStufenlos
    for (let i = 0; i < bewaessernStufenlos.length; i++) {
      let jahr = getWeekNumber(bewaessernStufenlos[i].datum)[0];
      let kw = getWeekNumber(bewaessernStufenlos[i].datum)[1].toString();
      let datum = bewaessernStufenlos[i].datum;
      let gießwagen_stufenlos = await findObjectById(bewaessernStufenlos[i].gießwagen_stufenlos, "Gießwagen_Stufenlos");
      let feld = await findObjectById(gießwagen_stufenlos.feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, bewaessernStufenlos[i]);
      let beetgröße = getBeeteToGröße(feld, bewaessernStufenlos[i]);
      let feldgrößeBeetGrößeVerhältnis = beetgröße / feld.fläche_netto_in_m2;
      let massnahmenname = 'Gießen-Düngen Stufenlos';
      let kultur = bewaessernStufenlos[i].kultur;
      let grund = bewaessernStufenlos[i].bemerkung;
      let produkt = await findObjectById(bewaessernStufenlos[i].stammlösungsbecken_düngemischung, "StammlösungsbeckenDüngemischung");
      let produktName = '';
      try {
        produktName = produkt.name;
      } catch (err) {
      }
      let konzentrationEC = (bewaessernStufenlos[i].konzentration_ec !== null) ? bewaessernStufenlos[i].konzentration_ec : '';
      let wasserEC = (bewaessernStufenlos[i].wasser_ec_ms_cm !== null) ? bewaessernStufenlos[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      try {
        wasseraufwandLAnw = calcwasseraufwandLAnw(gießwagen_stufenlos, bewaessernStufenlos[i], feldgrößeBeetGrößeVerhältnis);
        wasseraufwandmengeLHa = calcwasseraufwandmengeLHa(wasseraufwandLAnw, beetgröße);
        wasseraufwandmengeLm2 = calcwasseraufwandmengeLm2(wasseraufwandmengeLHa);
      } catch (err) { }
      try {
        konzentrationGL = bewaessernStufenlos[i].konzentration_g_l;
        if (konzentrationGL === null) {
          throw 'null';
        }
        produktmengeKGAnw = calcProduktmengeKGAnw(wasseraufwandLAnw, bewaessernStufenlos[i]);
        produktmengeKGHa = calcproduktmengeKGHa(produktmengeKGAnw, beetgröße);
      } catch (err) { }
      let applikationsart = '';
      let womit = 'Gießwagen (Stufenlos)';
      let user = '';
      try {
        user = bewaessernStufen[i].durchgeführt_von.FIRST_NAME + ' ' + bewaessernStufen[i].durchgeführt_von.LAST_NAME;
      } catch (err) { }
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      //console.log(arr);
      alleMassnahmen.push(arr);
    }
    // BewässernRegner
    for (let i = 0; i < bewaessernRegner.length; i++) {
      let jahr = getWeekNumber(bewaessernRegner[i].datum)[0];
      let kw = getWeekNumber(bewaessernRegner[i].datum)[1].toString();
      let datum = bewaessernRegner[i].datum;
      let regner = await findObjectById(bewaessernRegner[i].regner, "Regner");
      let feld = await findObjectById(regner.feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, bewaessernRegner[i]);
      let beetgröße = getBeeteToGröße(feld, bewaessernRegner[i]);
      let massnahmenname = 'Gießen Regner';
      let kultur = bewaessernRegner[i].kultur;
      let grund = bewaessernRegner[i].bemerkung;
      let produktName = '';
      let konzentrationEC = '';
      let wasserEC = (bewaessernRegner[i].wasser_ec_ms_cm !== null) ? bewaessernRegner[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      let applikationsart = '';
      let womit = 'Regner';
      let user = '';
      try {
        user = bewaessernRegner[i].durchgeführt_von.FIRST_NAME + ' ' + bewaessernRegner[i].durchgeführt_von.LAST_NAME;
      } catch (err) { }
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      //console.log(arr);
      alleMassnahmen.push(arr);
    }
    // BewässernManuell
    for (let i = 0; i < bewaessernManuell.length; i++) {
      let jahr = getWeekNumber(bewaessernManuell[i].datum)[0];
      let kw = getWeekNumber(bewaessernManuell[i].datum)[1].toString();
      let datum = bewaessernManuell[i].datum;
      let feld = await findObjectById(bewaessernManuell[i].feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, bewaessernManuell[i]);
      let beetgröße = getBeeteToGröße(feld, bewaessernManuell[i]);
      let massnahmenname = 'Manuell gießen';
      let kultur = bewaessernManuell[i].kultur;
      let grund = bewaessernManuell[i].bemerkung;
      let produktName = '';
      let konzentrationEC = '';
      let wasserEC = (bewaessernManuell[i].wasser_ec_ms_cm !== null) ? bewaessernManuell[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      let applikationsart = '';
      let womit = 'per Hand';
      let user = '';
      try {
        user = bewaessernManuell[i].durchgeführt_von.FIRST_NAME + ' ' + bewaessernManuell[i].durchgeführt_von.LAST_NAME;
      } catch (err) { }
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      //console.log(arr);
      alleMassnahmen.push(arr);
    }
    // PflanzenschutzStufen
    for (let i = 0; i < schutzStufen.length; i++) {
      let jahr = getWeekNumber(schutzStufen[i].datum)[0];
      let kw = getWeekNumber(schutzStufen[i].datum)[1].toString();
      let datum = schutzStufen[i].datum;
      let gießwagen_stufen = await findObjectById(schutzStufen[i].gießwagen_stufen, "Gießwagen_Stufen");
      let feld = await findObjectById(gießwagen_stufen.feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, schutzStufen[i]);
      let beetgröße = getBeeteToGröße(feld, schutzStufen[i]);
      let feldgrößeBeetGrößeVerhältnis = beetgröße / feld.fläche_netto_in_m2;
      let massnahmenname = 'Pflanzenschutz (Stufen)';
      let kultur = schutzStufen[i].kultur;
      let grund = schutzStufen[i].bemerkung;
      let produkt = await findObjectById(schutzStufen[i].pflanzenschutzmittel, "Pflanzenschutzmittel");
      let produktName = '';
      try {
        produktName = produkt.name;
      } catch (err) {
      }
      let konzentrationEC = '';
      let wasserEC = (schutzStufen[i].wasser_ec_ms_cm !== null) ? schutzStufen[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      try {
        if (schutzStufen[i].wassermenge_l_m2) {
          wasseraufwandmengeLm2 = schutzStufen[i].wassermenge_l_m2;
          wasseraufwandmengeLHa = wasseraufwandmengeLm2 * 10000;
          wasseraufwandLAnw = wasseraufwandmengeLm2 * beetgröße;
        } else {
          wasseraufwandLAnw = calcwasseraufwandLAnw(gießwagen_stufen, schutzStufen[i], feldgrößeBeetGrößeVerhältnis);
          wasseraufwandmengeLHa = calcwasseraufwandmengeLHa(wasseraufwandLAnw, beetgröße);
          wasseraufwandmengeLm2 = calcwasseraufwandmengeLm2(wasseraufwandmengeLHa);
        }
      } catch (err) { }
      try {
        konzentrationGL = schutzStufen[i].prozentanteil;
        if (konzentrationGL === null) {
          if (schutzStufen[i].mittelmenge_l_ha !== null && wasseraufwandmengeLHa !== '') {
            konzentrationGL = schutzStufen[i].mittelmenge_l_ha / wasseraufwandmengeLHa;
          } else {
            throw 'null';
          }
        }
        produktmengeKGAnw = calcProduktmengeKGAnw(wasseraufwandLAnw, { konzentration_g_l: konzentrationGL });
        produktmengeKGHa = calcproduktmengeKGHa(produktmengeKGAnw, beetgröße);
      } catch (err) { }
      let applikationsart = getAusbringungToString(schutzStufen[i].ausbringung, gießwagen_stufen);
      let womit = 'Gießwagen (Stufen)';
      let user = schutzStufen[i].durchgeführt_von.FIRST_NAME + ' ' + schutzStufen[i].durchgeführt_von.LAST_NAME;
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      alleMassnahmen.push(arr);
    }
    // PflanzenschutzStufenlos
    for (let i = 0; i < schutzStufenlos.length; i++) {
      let jahr = getWeekNumber(schutzStufenlos[i].datum)[0];
      let kw = getWeekNumber(schutzStufenlos[i].datum)[1].toString();
      let datum = schutzStufenlos[i].datum;
      let gießwagen_stufenlos = await findObjectById(schutzStufenlos[i].gießwagen_stufenlos, "Gießwagen_Stufenlos");
      let feld = await findObjectById(gießwagen_stufenlos.feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, schutzStufenlos[i]);
      let beetgröße = getBeeteToGröße(feld, schutzStufenlos[i]);
      let feldgrößeBeetGrößeVerhältnis = beetgröße / feld.fläche_netto_in_m2;
      let massnahmenname = 'Pflanzenschutz (Stufenlos)';
      let kultur = schutzStufenlos[i].kultur;
      let grund = schutzStufenlos[i].bemerkung;
      let produkt = await findObjectById(schutzStufenlos[i].pflanzenschutzmittel, "Pflanzenschutzmittel");
      let produktName = '';
      try {
        produktName = produkt.name;
      } catch (err) {
      }
      let konzentrationEC = '';
      let wasserEC = (schutzStufenlos[i].wasser_ec_ms_cm !== null) ? schutzStufenlos[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      try {
        if (schutzStufenlos[i].wassermenge_l_m2) {
          wasseraufwandmengeLm2 = schutzStufenlos[i].wassermenge_l_m2;
          wasseraufwandmengeLHa = wasseraufwandmengeLm2 * 10000;
          wasseraufwandLAnw = wasseraufwandmengeLm2 * beetgröße;
        } else {
          wasseraufwandLAnw = calcwasseraufwandLAnw(gießwagen_stufenlos, schutzStufenlos[i], feldgrößeBeetGrößeVerhältnis);
          wasseraufwandmengeLHa = calcwasseraufwandmengeLHa(wasseraufwandLAnw, beetgröße);
          wasseraufwandmengeLm2 = calcwasseraufwandmengeLm2(wasseraufwandmengeLHa);
        }
      } catch (err) { }
      try {
        konzentrationGL = schutzStufenlos[i].prozentanteil;
        if (konzentrationGL === null) {
          if (schutzStufenlos[i].mittelmenge_l_ha !== null && wasseraufwandmengeLHa !== '') {
            konzentrationGL = schutzStufenlos[i].mittelmenge_l_ha / wasseraufwandmengeLHa;
          } else {
            throw 'null';
          }
        }
        produktmengeKGAnw = calcProduktmengeKGAnw(wasseraufwandLAnw, { konzentration_g_l: konzentrationGL });
        produktmengeKGHa = calcproduktmengeKGHa(produktmengeKGAnw, beetgröße);
      } catch (err) { }
      let applikationsart = getAusbringungToString(schutzStufenlos[i].ausbringung, gießwagen_stufenlos);
      let womit = 'Gießwagen (Stufen)';
      let user = schutzStufenlos[i].durchgeführt_von.FIRST_NAME + ' ' + schutzStufenlos[i].durchgeführt_von.LAST_NAME;
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      alleMassnahmen.push(arr);
    }
    // PflanzenschutzSpritzen
    for (let i = 0; i < schutzSpritzen.length; i++) {
      let jahr = getWeekNumber(schutzSpritzen[i].datum)[0];
      let kw = getWeekNumber(schutzSpritzen[i].datum)[1].toString();
      let datum = schutzSpritzen[i].datum;
      let feld = await findObjectById(schutzSpritzen[i].feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, schutzSpritzen[i]);
      let beetgröße = getBeeteToGröße(feld, schutzSpritzen[i]);
      let feldgrößeBeetGrößeVerhältnis = beetgröße / feld.fläche_netto_in_m2;
      let massnahmenname = 'Pflanzenschutz (Spritzen)';
      let kultur = schutzSpritzen[i].kultur;
      let grund = schutzSpritzen[i].bemerkung;
      let produkt = await findObjectById(schutzSpritzen[i].pflanzenschutzmittel, "Pflanzenschutzmittel");
      let produktName = '';
      try {
        produktName = produkt.name;
      } catch (err) {
      }
      let konzentrationEC = '';
      let wasserEC = (schutzSpritzen[i].wasser_ec_ms_cm !== null) ? schutzSpritzen[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      try {
        if (schutzSpritzen[i].wassermenge_l_m2) {
          wasseraufwandmengeLm2 = schutzSpritzen[i].wassermenge_l_m2;
          wasseraufwandmengeLHa = wasseraufwandmengeLm2 * 10000;
          wasseraufwandLAnw = wasseraufwandmengeLm2 * beetgröße;
        }
      } catch (err) { }
      try {
        konzentrationGL = schutzSpritzen[i].prozentanteil;
        if (konzentrationGL === null) {
          if (schutzSpritzen[i].mittelmenge_l_ha !== null && wasseraufwandmengeLHa !== '') {
            konzentrationGL = schutzSpritzen[i].mittelmenge_l_ha / wasseraufwandmengeLHa;
          } else {
            throw 'null';
          }
        }
        produktmengeKGAnw = calcProduktmengeKGAnw(wasseraufwandLAnw, { konzentration_g_l: konzentrationGL });
        produktmengeKGHa = calcproduktmengeKGHa(produktmengeKGAnw, beetgröße);
      } catch (err) { }
      let applikationsart = '';
      let womit = schutzSpritzen[i].ausbringung;
      let user = schutzSpritzen[i].durchgeführt_von.FIRST_NAME + ' ' + schutzSpritzen[i].durchgeführt_von.LAST_NAME;
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      alleMassnahmen.push(arr);
    }
    // PflanzenstärkungStufen
    for (let i = 0; i < starkungStufen.length; i++) {
      let jahr = getWeekNumber(starkungStufen[i].datum)[0];
      let kw = getWeekNumber(starkungStufen[i].datum)[1].toString();
      let datum = starkungStufen[i].datum;
      let gießwagen_stufen = await findObjectById(starkungStufen[i].gießwagen_stufen, "Gießwagen_Stufen");
      let feld = await findObjectById(gießwagen_stufen.feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, starkungStufen[i]);
      let beetgröße = getBeeteToGröße(feld, starkungStufen[i]);
      let feldgrößeBeetGrößeVerhältnis = beetgröße / feld.fläche_netto_in_m2;
      let massnahmenname = 'Pflanzenstärkung (Stufen)';
      let kultur = starkungStufen[i].kultur;
      let grund = starkungStufen[i].bemerkung;
      let produkt = await findObjectById(starkungStufen[i].pflanzenstärkungsmittel, "Pflanzenstärkungsmittel");
      let produktName = '';
      try {
        produktName = produkt.name;
      } catch (err) {
      }
      let konzentrationEC = '';
      let wasserEC = (starkungStufen[i].wasser_ec_ms_cm !== null) ? starkungStufen[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      try {
        if (starkungStufen[i].wassermenge_l_m2) {
          wasseraufwandmengeLm2 = starkungStufen[i].wassermenge_l_m2;
          wasseraufwandmengeLHa = wasseraufwandmengeLm2 * 10000;
          wasseraufwandLAnw = wasseraufwandmengeLm2 * beetgröße;
        } else {
          wasseraufwandLAnw = calcwasseraufwandLAnw(gießwagen_stufen, starkungStufen[i], feldgrößeBeetGrößeVerhältnis);
          wasseraufwandmengeLHa = calcwasseraufwandmengeLHa(wasseraufwandLAnw, beetgröße);
          wasseraufwandmengeLm2 = calcwasseraufwandmengeLm2(wasseraufwandmengeLHa);
        }
      } catch (err) { }
      try {
        konzentrationGL = starkungStufen[i].prozentanteil;
        if (konzentrationGL === null) {
          if (starkungStufen[i].mittelmenge_l_ha !== null) {
            konzentrationGL = starkungStufen[i].mittelmenge_l_ha / wasseraufwandmengeLHa;
          } else {
            throw 'null';
          }
        }
        produktmengeKGAnw = calcProduktmengeKGAnw(wasseraufwandLAnw, { konzentration_g_l: konzentrationGL });
        produktmengeKGHa = calcproduktmengeKGHa(produktmengeKGAnw, beetgröße);
      } catch (err) { }
      let applikationsart = '';
      let womit = 'Gießwagen (Stufen)';
      let user = starkungStufen[i].durchgeführt_von.FIRST_NAME + ' ' + starkungStufen[i].durchgeführt_von.LAST_NAME;
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      alleMassnahmen.push(arr);
    }
    // PflanzenstärkungStufenlos
    for (let i = 0; i < starkungStufenlos.length; i++) {
      let jahr = getWeekNumber(starkungStufenlos[i].datum)[0];
      let kw = getWeekNumber(starkungStufenlos[i].datum)[1].toString();
      let datum = starkungStufenlos[i].datum;
      let gießwagen_stufenlos = await findObjectById(starkungStufenlos[i].gießwagen_stufenlos, "Gießwagen_Stufenlos");
      let feld = await findObjectById(gießwagen_stufenlos.feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, starkungStufenlos[i]);
      let beetgröße = getBeeteToGröße(feld, starkungStufenlos[i]);
      let feldgrößeBeetGrößeVerhältnis = beetgröße / feld.fläche_netto_in_m2;
      let massnahmenname = 'Pflanzenstärkung (Stufenlos)';
      let kultur = starkungStufenlos[i].kultur;
      let grund = starkungStufenlos[i].bemerkung;
      let produkt = await findObjectById(starkungStufenlos[i].pflanzenstärkungsmittel, "Pflanzenstärkungsmittel");
      let produktName = '';
      try {
        produktName = produkt.name;
      } catch (err) {
      }
      let konzentrationEC = '';
      let wasserEC = (starkungStufenlos[i].wasser_ec_ms_cm !== null) ? starkungStufenlos[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      try {
        if (starkungStufenlos[i].wassermenge_l_m2) {
          wasseraufwandmengeLm2 = starkungStufenlos[i].wassermenge_l_m2;
          wasseraufwandmengeLHa = wasseraufwandmengeLm2 * 10000;
          wasseraufwandLAnw = wasseraufwandmengeLm2 * beetgröße;
        } else {
          wasseraufwandLAnw = calcwasseraufwandLAnw(gießwagen_stufenlos, starkungStufenlos[i], feldgrößeBeetGrößeVerhältnis);
          wasseraufwandmengeLHa = calcwasseraufwandmengeLHa(wasseraufwandLAnw, beetgröße);
          wasseraufwandmengeLm2 = calcwasseraufwandmengeLm2(wasseraufwandmengeLHa);
        }
      } catch (err) { }
      try {
        konzentrationGL = starkungStufenlos[i].prozentanteil;
        if (konzentrationGL === null) {
          if (starkungStufenlos[i].mittelmenge_l_ha !== null && wasseraufwandmengeLHa !== '') {
            konzentrationGL = starkungStufenlos[i].mittelmenge_l_ha / wasseraufwandmengeLHa;
          } else {
            throw 'null';
          }
        }
        produktmengeKGAnw = calcProduktmengeKGAnw(wasseraufwandLAnw, { konzentration_g_l: konzentrationGL });
        produktmengeKGHa = calcproduktmengeKGHa(produktmengeKGAnw, beetgröße);
      } catch (err) { }
      let applikationsart = '';
      let womit = 'Gießwagen (Stufen)';
      let user = starkungStufenlos[i].durchgeführt_von.FIRST_NAME + ' ' + starkungStufenlos[i].durchgeführt_von.LAST_NAME;
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      alleMassnahmen.push(arr);
    }
    // PflanzenstärkungSpritzen
    for (let i = 0; i < starkungSpritzen.length; i++) {
      let jahr = getWeekNumber(starkungSpritzen[i].datum)[0];
      let kw = getWeekNumber(starkungSpritzen[i].datum)[1].toString();
      let datum = starkungSpritzen[i].datum;
      let feld = await findObjectById(starkungSpritzen[i].feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, starkungSpritzen[i]);
      let beetgröße = getBeeteToGröße(feld, starkungSpritzen[i]);
      let feldgrößeBeetGrößeVerhältnis = beetgröße / feld.fläche_netto_in_m2;
      let massnahmenname = 'Pflanzenstärkung (Spritzen)';
      let kultur = starkungSpritzen[i].kultur;
      let grund = starkungSpritzen[i].bemerkung;
      let produkt = await findObjectById(starkungSpritzen[i].pflanzenstärkungsmittel, "Pflanzenstärkungsmittel");
      let produktName = '';
      try {
        produktName = produkt.name;
      } catch (err) {
      }
      let konzentrationEC = '';
      let wasserEC = (starkungSpritzen[i].wasser_ec_ms_cm !== null) ? starkungSpritzen[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      try {
        if (starkungSpritzen[i].wassermenge_l_m2) {
          wasseraufwandmengeLm2 = starkungSpritzen[i].wassermenge_l_m2;
          wasseraufwandmengeLHa = wasseraufwandmengeLm2 * 10000;
          wasseraufwandLAnw = wasseraufwandmengeLm2 * beetgröße;
        }
      } catch (err) { }
      try {
        konzentrationGL = starkungSpritzen[i].prozentanteil;
        if (konzentrationGL === null) {
          if (starkungSpritzen[i].mittelmenge_l_ha !== null && wasseraufwandmengeLHa !== '') {
            konzentrationGL = starkungSpritzen[i].mittelmenge_l_ha / wasseraufwandmengeLHa;
          } else {
            throw 'null';
          }
        }
        produktmengeKGAnw = calcProduktmengeKGAnw(wasseraufwandLAnw, { konzentration_g_l: konzentrationGL });
        produktmengeKGHa = calcproduktmengeKGHa(produktmengeKGAnw, beetgröße);
      } catch (err) { }
      let applikationsart = '';
      let womit = starkungSpritzen[i].ausbringung;
      let user = starkungSpritzen[i].durchgeführt_von.FIRST_NAME + ' ' + starkungSpritzen[i].durchgeführt_von.LAST_NAME;
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      alleMassnahmen.push(arr);
    }
    // SprühenStufenlos
    for (let i = 0; i < spruhenStufenlos.length; i++) {
      let jahr = getWeekNumber(spruhenStufenlos[i].datum)[0];
      let kw = getWeekNumber(spruhenStufenlos[i].datum)[1].toString();
      let datum = spruhenStufenlos[i].datum;
      let gießwagen_stufenlos = await findObjectById(spruhenStufenlos[i].gießwagen_stufenlos, "Gießwagen_Stufenlos");
      let feld = await findObjectById(gießwagen_stufenlos.feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, spruhenStufenlos[i]);
      let beetgröße = getBeeteToGröße(feld, spruhenStufenlos[i]);
      let feldgrößeBeetGrößeVerhältnis = beetgröße / feld.fläche_netto_in_m2;
      let massnahmenname = 'Sprühen-Nebeln Stufenlos';
      let kultur = spruhenStufenlos[i].kultur;
      let grund = spruhenStufenlos[i].bemerkung;
      let produktName = '';
      let konzentrationEC = '';
      let wasserEC = (spruhenStufenlos[i].wasser_ec_ms_cm !== null) ? spruhenStufenlos[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      try {
        wasseraufwandLAnw = calcwasseraufwandLAnw(gießwagen_stufenlos, spruhenStufenlos[i], feldgrößeBeetGrößeVerhältnis);
        wasseraufwandmengeLHa = calcwasseraufwandmengeLHa(wasseraufwandLAnw, beetgröße);
        wasseraufwandmengeLm2 = calcwasseraufwandmengeLm2(wasseraufwandmengeLHa);
      } catch (err) { }
      let applikationsart = '';
      let womit = 'Gießwagen (Stufenlos)';
      let user = '';
      try {
        user = spruhenStufenlos[i].durchgeführt_von.FIRST_NAME + ' ' + spruhenStufenlos[i].durchgeführt_von.LAST_NAME;
      } catch (err) { }
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      //console.log(arr);
      alleMassnahmen.push(arr);
    }
    //  SprühenStufen
    for (let i = 0; i < spruhenStufen.length; i++) {
      let jahr = getWeekNumber(spruhenStufen[i].datum)[0];
      let kw = getWeekNumber(spruhenStufen[i].datum)[1].toString();
      let datum = spruhenStufen[i].datum;
      let gießwagen_stufen = await findObjectById(spruhenStufen[i].gießwagen_stufen, "Gießwagen_Stufen");
      let feld = await findObjectById(gießwagen_stufen.feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, spruhenStufen[i]);
      let beetgröße = getBeeteToGröße(feld, spruhenStufen[i]);
      let feldgrößeBeetGrößeVerhältnis = beetgröße / feld.fläche_netto_in_m2;
      let massnahmenname = 'Sprühen-Nebeln Stufen';
      let kultur = spruhenStufen[i].kultur;
      let grund = spruhenStufen[i].bemerkung;
      let produktName = '';
      let konzentrationEC = '';
      let wasserEC = (spruhenStufen[i].wasser_ec_ms_cm !== null) ? spruhenStufen[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      try {
        wasseraufwandLAnw = calcwasseraufwandLAnw(gießwagen_stufen, spruhenStufen[i], feldgrößeBeetGrößeVerhältnis);
        wasseraufwandmengeLHa = calcwasseraufwandmengeLHa(wasseraufwandLAnw, beetgröße);
        wasseraufwandmengeLm2 = calcwasseraufwandmengeLm2(wasseraufwandmengeLHa);
      } catch (err) { }
      let applikationsart = '';
      let womit = 'Gießwagen (Stufen)';
      let user = spruhenStufen[i].durchgeführt_von.FIRST_NAME + ' ' + spruhenStufen[i].durchgeführt_von.LAST_NAME;
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      //console.log(arr);
      alleMassnahmen.push(arr);
    }
    // BewässernRegner
    for (let i = 0; i < spruhenRegner.length; i++) {
      let jahr = getWeekNumber(spruhenRegner[i].datum)[0];
      let kw = getWeekNumber(spruhenRegner[i].datum)[1].toString();
      let datum = spruhenRegner[i].datum;
      let regner = await findObjectById(spruhenRegner[i].regner, "Regner");
      let feld = await findObjectById(regner.feld, "Feld");
      let bezeichnung = feld.name;
      let unterteilung = getBeeteToString(feld, spruhenRegner[i]);
      let beetgröße = getBeeteToGröße(feld, spruhenRegner[i]);
      let massnahmenname = 'Sprühen-Nebeln Regner';
      let kultur = spruhenRegner[i].kultur;
      let grund = spruhenRegner[i].bemerkung;
      let produktName = '';
      let konzentrationEC = '';
      let wasserEC = (spruhenRegner[i].wasser_ec_ms_cm !== null) ? spruhenRegner[i].wasser_ec_ms_cm : '';
      let konzentrationGL = '';
      let wasseraufwandLAnw = '';
      let produktmengeKGAnw = '';
      let produktmengeKGHa = '';
      let wasseraufwandmengeLHa = '';
      let wasseraufwandmengeLm2 = '';
      let applikationsart = '';
      let womit = 'Regner';
      let user = '';
      try {
        user = spruhenRegner[i].durchgeführt_von.FIRST_NAME + ' ' + spruhenRegner[i].durchgeführt_von.LAST_NAME;
      } catch (err) { }
      let arr = [
        jahr,
        kw,
        datum,
        bezeichnung,
        unterteilung,
        beetgröße,
        massnahmenname,
        kultur,
        grund,
        produktName,
        konzentrationGL,
        konzentrationEC,
        wasserEC,
        produktmengeKGAnw,
        produktmengeKGHa,
        wasseraufwandLAnw,
        wasseraufwandmengeLHa,
        wasseraufwandmengeLm2,
        applikationsart,
        womit,
        '',
        '',
        user
      ];
      //console.log(arr);
      alleMassnahmen.push(arr);
    }
    resolve(alleMassnahmen);
  });
}

function calcwasseraufwandLAnw(gießwagen, massnahme, feldgrößeBeetGrößeVerhältnis) {
  try {
    let von_bis_in_ = (massnahme.von_bis_in_[1] - massnahme.von_bis_in_[0]) / 100;
    let geschwindigkeit = 0;
    try {
      if (gießwagen.bei_1_meter_minute !== undefined) {
        geschwindigkeit = ((gießwagen.bei_100_meter_minute - gießwagen.bei_1_meter_minute) / 99) * massnahme.geschwindigkeit_ + (1 / 99) * (-gießwagen.bei_100_meter_minute + 100 * gießwagen.bei_1_meter_minute);
      } else {
        (massnahme.stufe === 'stufe1') ? geschwindigkeit = gießwagen.stufe_1_meter_minute : geschwindigkeit = gießwagen.stufe_2_meter_minute;
      }
    } catch (err) { }
    return ((gießwagen.fahrlänge_meter * von_bis_in_ * massnahme.einweghäufigkeit) / geschwindigkeit / 60 * (gießwagen.volumenstrom_m3_h * feldgrößeBeetGrößeVerhältnis) * 1000);
  } catch (err) {
    console.log(err);
    return '';
  }
}

function calcProduktmengeKGAnw(wasseraufwandLAnw, massnahme) {
  try {
    return (wasseraufwandLAnw * massnahme.konzentration_g_l) / 1000;
  } catch (err) {
    return '';
  }
}

function calcproduktmengeKGHa(produktmengeKGAnw, größe) {
  try {
    return produktmengeKGAnw / größe * 10000;
  } catch {
    return '';
  }
}

function calcwasseraufwandmengeLHa(wasseraufwandLAnw, größe) {
  try {
    return wasseraufwandLAnw / größe * 10000;
  } catch {
    return '';
  }
}

function calcwasseraufwandmengeLm2(wasseraufwandmengeLHa) {
  try {
    return wasseraufwandmengeLHa / 10000;
  } catch {
    return '';
  }
}

function getBeeteToGröße(feld, massnahme) {
  let größe = 0;
  feld.beete.forEach((beet, i) => {
    if (massnahme.beete.includes(beet._id)) {
      größe += beet.fläche_netto_in_m2
    }
  });
  return größe;
}

function getAusbringungToString(ausbringung, gießwagen) {
  let str = '';
  ausbringung.forEach((art, i) => {
    str += art;
    if (i + 1 !== ausbringung.length) {
      str += ', ';
    }
  });
  return str;
}

function getBeeteToString(feld, massnahme) {
  let str = '';
  feld.beete.forEach((beet, i) => {
    if (massnahme.beete.includes(beet._id)) {
      str += beet.name
      if (i + 1 !== feld.beete.length) {
        str += ', ';
      }
    }
  });
  return str;
}

function getWeekNumber(d) {
  // Copy date so don't modify original
  d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
  // Set to nearest Thursday: current date + 4 - current day number
  // Make Sunday's day number 7
  d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
  // Get first day of year
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
  // Calculate full weeks to nearest Thursday
  var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
  // Return array of year and week number
  return [d.getUTCFullYear(), weekNo];
}


async function collectAndCombine(massnahme, start, ende, typ, controller) {
  let massnahmen = await filterForDateRange(massnahme, start, ende);
  let kulturen = await getKulturen(massnahmen, typ, controller);
  massnahmen = await addKultur(massnahmen, kulturen);
  return massnahmen;
}

async function findObjectById(id, modelName) {
  let object = await models[modelName].find({
    _id: id
  });
  return object[0];
}

//Datum als string in deutsches Format formatieren
function dateFormater(date) {
  date = new Date(date.toString());
  //Month +1 weil js monate von 0 zählt
  let monat = date.getMonth() + 1;
  let tag = date.getDate();
  let jahr = date.getFullYear();
  if (tag < 10) {
    tag = `0${tag}`;
  }
  if (monat < 10) {
    monat = `0${monat}`;
  }
  return `${tag}.${monat}.${jahr}`;
}
//Maßnahmen von bis für zeitraum filtern 
async function filterForDateRange(modelName, start, ende) {
  //ein Tag auf Enddatum rechnen da $lt diesen sonst nicht mit einbezieht
  let newEndDate = new Date(ende);
  newEndDate.setDate(newEndDate.getDate() + 1);
  let data = await models[modelName].find({
    datum: {
      $gte: new Date(start),
      $lt: newEndDate
    }
  })
  return data;
}
//Kulturen basierend auf der ID für das jeweilige Feld finden
async function getKulturen(massnahmen, typ, controller) {
  let kulturen = [];
  for (let i = 0; i < massnahmen.length; i++) {
    if (typ !== 'spritzen') {
      for (let j = 0; j < massnahmen[i][typ].length; j++) {
        let gw = await models[controller].findOne({
          _id: massnahmen[i][typ][j]
        });
        let feldID = gw.feld;
        await kulturenHelp(feldID, kulturen, massnahmen[i].datum)
      }
    } else {
      for (let j = 0; j < massnahmen[i].feld.length; j++) {
        let feldID = massnahmen[i].feld[j];
        await kulturenHelp(feldID, kulturen, massnahmen[i].datum);
      }
    }
  }
  return kulturen;
}
//Maßnahmen in denen Kulturen angesprochen werden bis zum ende des Datumsbereichs filtern
async function kulturenHelp(feldID, kulturen, datum) {
  let allMassnahmen = [];
  let umstellen = await models['Umstellen'].find({
    feld: [feldID],
    datum: {
      $lt: datum
    }
  });
  umstellen.forEach(massnahme => {
    allMassnahmen.push(massnahme);
  });
  let topfen = await models['Topfen'].find({
    feld: [feldID],
    datum: {
      $lt: datum
    }
  });
  topfen.forEach(massnahme => {
    allMassnahmen.push(massnahme);
  });
  let stecken = await models['Stecken'].find({
    feld: [feldID],
    datum: {
      $lt: datum
    }
  });
  stecken.forEach(massnahme => {
    allMassnahmen.push(massnahme);
  });
  let pikieren = await models['Pikierenzusammenpikieren'].find({
    feld: [feldID],
    datum: {
      $lt: datum
    }
  });
  pikieren.forEach(massnahme => {
    allMassnahmen.push(massnahme);
  });
  // älteste Maßnahme filtern
  let latest = await allMassnahmen.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime())[0];
  let kulturenTmp = [];
  if (latest !== undefined) {
    //Kulturnamen für dieses Feld finden
    for (let l = 0; l < latest.auftragsnummer.length; l++) {
      let auftragsnummer = latest.auftragsnummer[l];
      let verkauf = await models['Verkauf'].find({
        auftragsnummer: [auftragsnummer],
        datum: {
          $lt: datum,
          $gte: latest.datum
        }
      });
      if (verkauf.length === 0) {
        let auf = await models['Auftrag'].findOne({ _id: auftragsnummer });
        if (!kulturenTmp.includes(auf.pflanze)) {
          kulturenTmp.push(`${auf.pflanze} [${auf.name}]`);
        }
      } else {
        kulturenTmp.push("keine Kultur");
      }
    }
  } else {
    kulturenTmp.push("keine Kultur");
  }
  kulturen.push(kulturenTmp);
}
//jeweils passende Kultur zu der Maßnahme hinzufügen 
async function addKultur(massnahmen, kulturen) {
  for (let i = 0; i < massnahmen.length; i++) {
    massnahmen[i]["kultur"] = kulturen[i][0];
  }
  return massnahmen;
}
//Values jeweils für die einzelnen Ranges zurück geben (B-C, F-G, K-Q)
async function formatValues(massnahmen) {
  let firstRangeRows = [];
  let secondRangeRows = [];
  let thirdRangeRows = [];
  for (let j = 0; j < massnahmen.length; j++) {
    let massnahme = massnahmen[j].massnahmen;
    let typ = massnahmen[j].typ;
    let controller = massnahmen[j].controller;
    let anwendungMit;
    //Unterschiedliche Arten sind in Excel mit Nummer angegeben;
    if (controller === 'Gießwagen_Stufenlos') {
      anwendungMit = "5.";
    }
    if (controller === 'Gießwagen_Stufen') {
      anwendungMit = "4.";
    }
    //Werte die jeweils in eine Zeile müssen ermitteln und reihe befüllen 
    for (let i = 0; i < massnahme.length; i++) {
      let felder = [];
      let foramtedDate = dateFormater(massnahme[i].datum);
      let anwendungsArt;
      let wassermenge;
      let fullName = `${massnahme[i].durchgeführt_von['FIRST_NAME']} ${massnahme[i].durchgeführt_von['LAST_NAME']}`;
      let psm = await models["Pflanzenschutzmittel"].findOne({
        _id: massnahme[i].pflanzenschutzmittel
      });
      //unterscheidung, bei Gießwagen Feld über diesen finden bei spritzen ID schon so vorhanden
      if (typ !== 'spritzen') {
        anwendungsArt = 'gießen';
        wassermenge = massnahme[i].wassermenge_l_m2;
        for (let m = 0; m < massnahme[i][typ].length; m++) {
          let gw = massnahme[i][typ][m];
          let finder = await models[controller].findOne({
            _id: gw
          });
          let feld = await models["Feld"].findOne({
            _id: finder.feld
          });
          felder.push(feld);
        }
      } else {
        anwendungsArt = 'spritzen';
        wassermenge = massnahme[i].wassermenge_l_ha;
        //womit wurde Spritzen durchgeführt?
        switch (massnahme[i].ausbringung) {
          case "ruckenSpritze":
            anwendungMit = "1.";
            break;
          case "feldSpritze":
            anwendungMit = "2.";
            break;
          default:
            anwendungMit = "3."
            break;
        }
        for (let m = 0; m < massnahme[i].feld.length; m++) {
          let feld = massnahme[i].feld[m];
          let finder = await models["Feld"].findOne({
            _id: feld
          });
          felder.push(finder);
        };
      }
      //formatierte Werte sammeln und zuweisen
      await felder.forEach(feld => {
        firstRangeRows.push([massnahme[i].kultur, feld.name]);
        secondRangeRows.push([foramtedDate, psm.name]);
        thirdRangeRows.push([fullName, massnahme[i].bemerkung, massnahme[i].mittelmenge_l_ha, massnahme[i].prozentanteil, wassermenge, anwendungsArt, anwendungMit]);
      });
    }
  }
  return { firstRange: firstRangeRows, secondRange: secondRangeRows, thirdRange: thirdRangeRows };
}

//Füllt daten für DuengerMisch bei Bewässerung ein 
function fillMischung(worksheet, formatedMisch, nameRange, dataRange, index) {
  //Name
  let rangeToFill = worksheet.range(nameRange);
  rangeToFill.value(formatedMisch[index].name);
  //Daten
  rangeToFill = worksheet.range(dataRange);
  rangeToFill.value(formatedMisch[index].data);
}
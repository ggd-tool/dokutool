//https://www.npmjs.com/package/exceljs
const ExcelJS = require('exceljs') // there is another Excel package (xlsx-populate). I use this one, because it is better documentated and can load excel from a buffer

const ObjectDialog = require('../pre-models/objectDialog.model');
const controller = require('../index-controller');

const ImportController = {
    async getImportTemplate(req, res){
        try{
            // create Template Import Excel
            const workbook = new ExcelJS.Workbook();

            const addNewColumn = (tmpColumnID, tmpSheet, tmpLabel, tmpKey, tmpType, tmpRequired)=>{
                var columnKey = tmpSheet.getColumn(tmpColumnID)
                var rowKey = tmpSheet.getRow("3")

                var cellKey = tmpSheet.getCell(tmpColumnID +"3")
                var cellType = tmpSheet.getCell(tmpColumnID +"4")
                var cellLabel = tmpSheet.getCell(tmpColumnID +"5")
                
                // Style
                cellType.font = {size: 12}; 
                cellLabel.font = {size: 12};
                cellLabel.border = {
                    bottom: {style:'thin'},
                };
                rowKey.hidden = true
                columnKey.width = 20

                // value
                cellKey.value = CheckAndUpdateNameFromInput(tmpKey)
                cellType.value = tmpType

                var InputName
                if(tmpRequired){
                    InputName = "*" + tmpLabel
                }else{
                    InputName = tmpLabel
                }
                cellLabel.value = InputName

            }

            /// get all Objects
            const objectsArray = await ObjectDialog.find()

            for(const [indexObjectJSON, objectJSON] of objectsArray.entries()){
                const sheet = workbook.addWorksheet("Object " +(indexObjectJSON+1));
                
                // get cell to add Object Name
                const objectName = sheet.getCell('A1');
                // style
                objectName.font = {
                    size: 20
                }
                // value
                objectName.value = objectJSON.name
                
                var index = 0
                for(const [indexSection, section] of objectJSON.data.entries()){
                    // Insert an Sector Name above the Input Fields
                    const cellSector = sheet.getCell(createColumns(index) + "2")
                    cellSector.value = section.label
                    cellSector.font = {size: 16};

                    for(const [indexInput, input] of section.inputs.entries()){
                        const ColumnID = createColumns(index)
                        if(input.type){
                            switch(input.type){
                                case "array":
                                    // seperate each Element in the cell with /
                                    for(const [tmpIndex, option] of input.options.entries()){
                                        if(tmpIndex != 0){
                                            index++
                                        }
                                        let tmpColumnID = createColumns(index)
                                        
                                        let tmpType
                                        if(typeof option.options == "string" && option.ref){
                                            tmpType = input.type + "." + "ref " + option.ref
                                        }else{
                                            tmpType = input.type + "." + option.type
                                        }
                                        
                                        addNewColumn(tmpColumnID, sheet, input.label + "." + option.label, input.key + "." + option.key, tmpType, input.required)
                                    }
                                    break
                                case "datepicker":
                                    addNewColumn(ColumnID, sheet, input.label, input.key, input.type, input.required)
                                    // type Date
                                    for(let i=5; i<=15; i++){
                                        sheet.getCell(ColumnID + i).value = new Date();
                                        sheet.getCell(ColumnID + i).numFmt = 'dd/mm/yyyy\\ h:mm';
                                    }
                                    break;
                                case "dropdown":
                                    let tmpType
                                    if(typeof input.options == "object"){
                                        tmpType = input.type
                                        for(let i=5; i<=15; i++){
                                            let options = '"'
                                            for(const [tmpIndex, option] of input.options.entries()){
                                                if(tmpIndex!=0){
                                                    options += ","
                                                }
                                                options += option.value
                                            }
                                            options += '"'
                                            
                                            sheet.getCell(ColumnID + i).dataValidation = {
                                                type: 'list',
                                                allowBlank: true,
                                                formulae: [options]
                                            };
                                        }
                                    }else{
                                        if(typeof input.options == "string" && input.ref){
                                            tmpType = "ref " + input.ref
                                        }
                                    }
                                    addNewColumn(ColumnID, sheet, input.label, input.key, tmpType, input.required)
                                    break;
                                    
                                case "checkbox":
                                    // only true/false or 0/1
                                    addNewColumn(ColumnID, sheet, input.label, input.key, input.type, input.required)
                                    for(let i=5; i<=15; i++){
                                        sheet.getCell(ColumnID + i).dataValidation = {
                                            type: 'list',
                                            allowBlank: true,
                                            formulae: ['"true,false"'],
                                        };
                                    }
                                    break
                                case "multidropdown":
                                    addNewColumn(ColumnID, sheet, input.label, input.key, input.type, input.required)
                                    // seperate each Element in the cell with /
                                    // there is no dataValidation to create an multiselect cell 
                                    break
                                default:
                                    addNewColumn(ColumnID, sheet, input.label, input.key, input.type, input.required)
                                    break;
                            }
                        }
                        index++
                    }
                }
            }


            const buffer = await workbook.xlsx.writeBuffer();

            // create a JSON string that contains the data in the property "blob"
            const blob = JSON.stringify({ blob: buffer.toString("base64") });

            // Send the workbook.
            res.set('Content-Type', 'application/octet-stream');
            res.send(blob);
            
        }catch(e){
            console.log(e)
        }
    },
    async import(req, res){
        try{

            // used to store all Objects and its inputs from the Excel
            var object_from_Excel = {}

            // used to store all failed Rows
            var error_from_Excel = {}

            // used to count all success in Objects
            var success = []

            if(!req.file){
                res.status(400).send({
                    error: ["Sie haben keine Datei gesendet"]
                })
                return
            }
            
            const workbook = new ExcelJS.Workbook();

            // load from buffer
            await workbook.xlsx.load(req.file.buffer)

            // get all Objects
            const objectsArray = await ObjectDialog.find()

            /// dont use eachSheet, because it is annoying to integrate an async function in it.
            for(var index=0; index<workbook.worksheets.length; index++){
                const worksheet = workbook.worksheets[index]
                
                const objectName = worksheet.getCell("A1")
                var objectFound = objectsArray.filter(object => object.name === objectName.value)

                if(objectFound && objectFound.length != 0){
                    if(objectFound.length == 1){
                        object_from_Excel[objectName] = []

                        // the Objects in Excel
                        var indexRow = 6
                        while(true){
                            var indexCell = 1
                            const row = worksheet.getRow(indexRow)
                            const getRowName = row.getCell(1).value

                            if(!getRowName){
                                break;
                            }

                            var inputs_from_Excel = {}
                            while(true){
                                const keyCell = worksheet.getCell(createColumns(indexCell -1) + "3").value
                                const typeCell = worksheet.getCell(createColumns(indexCell -1) + "4").value
                                const NameCell = worksheet.getCell(createColumns(indexCell -1) + "5").value
                                const valueCell = row.getCell(indexCell).value
                                
                                indexCell++

                                // exctract from used Object all Sectors 
                                const ObjectsStructsFromDatabase = objectFound[0].data

                                if(!keyCell){
                                    break;
                                }
                                if(!valueCell && keyCell){
                                    continue
                                }

                                try {
                                    // check if Name already exist, because MongoDB don't always Check it.
                                    if(keyCell == "name"){
                                        const objects = await controller[CheckAndUpdateNameForIndexController(objectName) + 'Controller'].localIndex();
                                        let checkIfNameAlreadyExist = objects.filter(function(e) { return e.name === getRowName; }).length

                                        if(checkIfNameAlreadyExist != 0){
                                            throw {
                                                en: "The Name is already in use.",
                                                de: 'Der Name wird bereits verwendet.'
                                            }
                                        }
                                    }

                                    const convertValue = async (value, type, key) =>{
                                        let tmpValue

                                        if(type.search("ref") != -1){
                                            const split_type = type.split(" ")

                                            const objects = await controller[CheckAndUpdateNameForIndexController(split_type[1]) + 'Controller'].localIndex();
                        
                                            const found = objects.find(element => element.name == value);
                                            if(found){
                                                tmpValue = found._id.toString()
                                            }else{
                                                throw {
                                                    en: "Object '" + split_type[1] + "' with the Name '" + value + "' couldn't be found.", 
                                                    de: "Objekt '" + split_type[1] + "' mit dem Namen '" + value + "' konnte nicht gefunden werden."
                                                }
                                            }
                                        }else{
                                            switch(type){
                                                case "dropdown":
                                                    for(const sector of ObjectsStructsFromDatabase){
                                                        for(const inputs of sector.inputs){
                                                            if(key == inputs.key){
                                                                if(inputs.options){
                                                                    let option_key = ""

                                                                    let found_input_option = inputs.options.find(tmpInput => tmpInput.value == value)
                                                                    if(found_input_option){
                                                                        option_key = found_input_option.key
                                                                    }else{
                                                                        throw {
                                                                            en: "Option '" + tmpValue + "' couldn't be found",
                                                                            de: "Options '" + tmpValue + "' konnte nicht gefunden werden"
                                                                        }
                                                                    }

                                                                    tmpValue = option_key
                                                                }else{
                                                                    throw {
                                                                        en: "A new IF-Statement is for 'dropdown' in the Excel Import Function needed. Contact the IT-Support and send this Message",
                                                                        de: "Ein neuer IF-Statement für 'dropdown' wird benötigt, kontaktieren Sie den IT-Support und senden sie diese Nachricht"
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    break
                                                case "multidropdown": // multidropdown is in xml File an select and dropdown, which is multiple
                                                    for(const sector of ObjectsStructsFromDatabase){
                                                        for(const inputs of sector.inputs){
                                                            if(key == inputs.key){
                                                                if(inputs.options){
                                                                    let option_key_array = []

                                                                    let split_Value = value.split(";")
                                                                    for(const tmpValue of split_Value){
                                                                        let found_input_option = inputs.options.find(tmpInput => tmpInput.value == tmpValue)
                                                                        if(found_input_option){
                                                                            option_key_array.push(found_input_option.key)
                                                                        }else{
                                                                            throw {
                                                                                en: "Option '" + tmpValue + "' couldn't be found",
                                                                                de: "Options '" + tmpValue + "' konnte nicht gefunden werden"
                                                                            }
                                                                        }
                                                                    }

                                                                    tmpValue = option_key_array
                                                                }else{
                                                                    throw {
                                                                        en: "A new IF-Statement is for 'multidropdown' in the Excel Import Function needed. Contact the IT-Support and send this Message",
                                                                        de: "Eine neuer IF-Statement für 'multidropdown' wird benötigt, kontaktieren Sie den IT-Support und senden sie diese Nachricht"
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    break;
                                                case "datepicker":
                                                    let isNaN = Date.parse(value)
                                                    if(isNaN !== isNaN){ // it's NaN
                                                        throw {
                                                            en: "Date has an invalid Format", 
                                                            de: "Der Wert hat ein unzulässiges Datum Format."
                                                        }
                                                    }
                        
                                                    let date_MM_DD_YY = new Date(value)
                                                    let year = date_MM_DD_YY.getFullYear()
                                                    let month = date_MM_DD_YY.getMonth() -2
                                                    let day = date_MM_DD_YY.getDay()
                                                    let hours = date_MM_DD_YY.getHours()
                                                    let minutes = date_MM_DD_YY.getMinutes()
                                                    let seconds = date_MM_DD_YY.getSeconds()
                                                    let milliseconds = date_MM_DD_YY.getMilliseconds()
                                                    let date_DD_MM_YY = new Date(year, month, day, hours, minutes, seconds, milliseconds)
                                                    tmpValue = date_DD_MM_YY.toISOString()
                                                    break
                                                case "checkbox":
                                                    if (value == "true"){
                                                        tmpValue = true
                                                    }else if(value == "false"){
                                                        tmpValue = false
                                                    }else{
                                                        throw {
                                                            en: "Value can only be 'true' or 'false'.", 
                                                            de: "Der Wert darf nur 'true' oder 'false' sein."
                                                        }
                                                    }
                                                    break
                                                case "number":
                                                    let convertToString = value.toString()
                                                    let replaceComma = convertToString.replace(",", ".") // if there is an comma replace it
                                                    let convertToFloat = parseFloat(replaceComma) // converts an string to number
                                                    tmpValue = convertToFloat.toString() // go back to an string
                                                    break
                                                default:
                                                    tmpValue = value.toString()
                                                    break
                                            }
                                        }
                                        return tmpValue
                                    }

                                    if(!inputs_from_Excel){
                                        break
                                    }
    
                                    if(keyCell.indexOf(".") != -1){
                                        const split_KeyCell = keyCell.split(".")
                                        const split_TypeCell = typeCell.split(".")
                                        const split_valueCell = valueCell.toString().split(";")
    
                                        if(!inputs_from_Excel[split_KeyCell[0]]){
                                            inputs_from_Excel[split_KeyCell[0]] = new Array(split_valueCell.length)
                                        }else{
                                            if(split_valueCell.length != inputs_from_Excel[split_KeyCell[0]].length){
                                                throw {
                                                    en: "It contains an uneven number of Inputs. Please check and correct the Field and try it again.", 
                                                    de: "Es enthält eine ungerade Anzahl von Eingaben. Bitte überprüfen und korrigieren Sie das Feld und versuchen Sie es erneut."
                                                }
                                            }
                                        }
    
                                        for(const [index, value] of split_valueCell.entries()){
                                            if(!inputs_from_Excel[split_KeyCell[0]][index]){
                                                inputs_from_Excel[split_KeyCell[0]][index] = {}
                                            }
    
                                            var converted_value = await convertValue(value, split_TypeCell[1], split_KeyCell[1])
                                            inputs_from_Excel[split_KeyCell[0]][index][split_KeyCell[1]] = converted_value
                                        }
                                    }else{
                                        var converted_value = await convertValue(valueCell, typeCell, keyCell)
                                        inputs_from_Excel[keyCell] = converted_value
                                    }
                                }catch(e){
                                    if(!e.en && !e.de){
                                        console.log(e)
                                    }
                                    if(e.en){
                                        //console.log(e.en)
                                    }
                                    if(e.de){
                                        if(!error_from_Excel[objectName]){
                                            error_from_Excel[objectName] = []
                                        }
                                        error_from_Excel[objectName].push({
                                            "object": objectName.toString(), 
                                            "name": row.getCell(1).value, 
                                            "column": NameCell, 
                                            "error": e.de,
                                            "canBeCreated": false
                                        })
                                    }
                                    inputs_from_Excel = undefined
                                    break
                                }
                            }
                            
                            if(inputs_from_Excel){
                                object_from_Excel[objectName].push(inputs_from_Excel)
                            }
                            indexRow++
                        }

                        // create the Objects
                        let result = await CreateObject(objectName.toString(), object_from_Excel[objectName])
                        // Get all Errors while Creating
                        for(let tmp_error of result.errors){
                            if(!error_from_Excel[objectName]){
                                error_from_Excel[objectName] = []
                            }
                            error_from_Excel[objectName].push(tmp_error)
                        }
                        // Get the Number of success
                        success.push({
                            name: objectName.toString(), 
                            value: result.success
                        })

                    }else{
                        console.log(objectName.value + ": Object could be found, but there exist more than one in the Function 'ObjectDialog'.")

                        if(!error_from_Excel[objectName]){
                            error_from_Excel[objectName] = []
                        }
                        error_from_Excel[objectName].push(objectName.value + ": Object konnte gefunden werden, aber es gibt mehr als einen in der Funktion 'ObjectDialog'.")
                    }
                }else{
                    console.log(objectName.value + ": Object couldn't be found")

                    if(!error_from_Excel[objectName]){
                        error_from_Excel[objectName] = []
                    }
                    error_from_Excel[objectName].push({error: objectName.value + ": Object konnte nicht gefunden werden."})
                }
            }

            res.send({
                success: success,
                errors: error_from_Excel
            })

        }catch(e){
            console.log(e)
            res.status(500).send({
                error: ["Unerwarteter Error: Benachrichtigen sie den IT-Support"]
            })
        }
        
    }
}

module.exports = ImportController;

function createColumns(index){
    try{
        const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
        if(index >= alphabet.length){
            let indexDivide = Math.trunc(index / alphabet.length)
            let indexRest   = index % alphabet.length
            return createColumns(indexDivide-1) + alphabet[indexRest]
        }
        return alphabet[index]
    }catch(e){
        console.log("Error in the Funktion 'creacreateColumnsteID'")
        console.log(e)
    }
}

function CheckAndUpdateNameFromInput(name){
    name = name.replace(/ /g, "_") //' '
    name = name.replace(/\//g, "_") //'/'
    name = name.replace(/-/g, "_") //'-'
    name = name.replace(/\(/g, "") //(
    name = name.replace(/\)/g, "") //)
    name = name.replace(/\,/g, "") //,
    

    name = name.replace(/\%/g, "") //%
    name = name.replace(/\‰/g, "") //‰
    name = name.replace(/\°/g, "0") //°
    name = name.replace(/\²/g, "2") //²
    name = name.replace(/\³/g, "3") //³

    return name.toLowerCase()
}

function CheckAndUpdateNameForIndexController(name){
    name = name.toString();
    name = name.replace(/\ /g, "_") //' '
    name = name.replace(/\./g, "") //'.'
    name = name.replace(/\(/g, "") //'('
    name = name.replace(/\)/g, "") //')'
    name = name.replace(/\-/g, "") //'-'
    return name;
}

async function CreateObject(objectName, objectArray){
    var success = 0
    var errors = []
    
    for(const object of objectArray){
        try{
            const model_for_object = require('../models-index')[CheckAndUpdateNameForIndexController(objectName)]
            for (let property in object) {
                if (object[property] === null || object[property] === '' || object === undefined) {
                    delete object[property];
                }
            }
        
            const Object_Model = new model_for_object(object);
            await Object_Model.save();
            success++
        }catch(e){
            if(e.code){
                //console.log({"objectName": objectName, "name":object.name, error: "duplicate key error collection"})
                errors.push({
                    object: objectName,
                    name: object.name,
                    column: "name",
                    error: 'Der Name wird bereits verwendet.',
                    canBeCreated: false
                })
            }else{
                console.log("unexpected Error")
                console.log(e)
                errors.push({
                    object: objectName,
                    name: object.name,
                    column: null,
                    error: "Unerwarteter Error. Benachrichtigen sie den IT-Support.",
                    canBeCreated: false
                })
            }
        }
    }
    return {errors: errors, success: success}
}
// Imports the Dialogflow library
const dialogflow = require('@google-cloud/dialogflow');
// Imports protobuf Library; it changes struct to JSON. struct is just an complex form of JSON. Google uses struct.
const {struct} = require('pb-util');
// Imports jwt-decode, um die User ID aus keycloak zu entnehmen
const jwt_decode = require('jwt-decode');

/// Credentials for Dialogflow
const agent = require('../environment/agent.json');
const usedAgent = {credentials: agent }

// Instantiates a session client
const sessionClient = new dialogflow.SessionsClient(usedAgent);

// projectId: ID of the GCP project where Dialogflow agent is deployed
const projectId = usedAgent.credentials.project_id;
// languageCode: Indicates the language Dialogflow agent should use to detect intents
const languageCode = 'de';

const DialogflowController = {
    async UseAgent_audio(req, res){
        // Dialogflow akzeptiert nur single channel Audio Dateien
        // akzeptiert mp3, wav (Ich hab nicht alle Audio Formate getestet) (vielleicht findet man eine Dokumentation über unterstütze Audio Formate)
        // akzeptiert anscheinend nicht webm
        try{
            if(req.files == null){
                res.status(400)
                res.send("Sie haben eine leeres Audio abgesendet")
                return
            }

            // sessionId: String representing a random number or hashed user identifier
            // keycloak Userid aus dem jwt token auslesen
            const sessionId = jwt_decode(req.kauth.grant.access_token.token).sub;

            // The path to identify the agent that owns the created intent.
            const sessionPath = sessionClient.projectAgentSessionPath(
                projectId,
                sessionId
            );

            // Request wird erstellt und nach Dialogflow gesendet
            const request = {
                session: sessionPath,
                queryInput: {
                    audioConfig: {
                        languageCode: languageCode,
                    }
                },
                inputAudio: req.files.audio[0].buffer
            };

            await insert_Context_in_request(request, sessionPath, req)

            // ask for Audio response
            request.outputAudioConfig = {
                audioEncoding: 'OUTPUT_AUDIO_ENCODING_LINEAR_16',
            }

            const result = await send_request_to_Dialogflow_and_handle_result(request)
            
            res.send(result);
        }catch(e){
            console.log(e.name + ": " + e.message);
        }
    },
    async UseAgent_text(req, res){
        try{
            // sessionId: String representing a random number or hashed user identifier
            // keycloak Userid aus dem jwt token auslesen
            const sessionId = jwt_decode(req.kauth.grant.access_token.token).sub;

            const sessionPath = sessionClient.projectAgentSessionPath(
                projectId,
                sessionId
            );

            // queries: A set of sequential queries to be send to Dialogflow agent for Intent Detection
            const query = req.body.querytext;
            
            // Request wird erstellt
            const request = {
                session: sessionPath,
                queryInput: {
                    text: {
                        text: query,
                        languageCode: languageCode,
                    },
                },
            };

            await insert_Context_in_request(request, sessionPath, req)

            const result = await send_request_to_Dialogflow_and_handle_result(request)

            res.send(result)
        }catch(e){
            console.log(e)
            console.log(e.name + ": " + e.message);
        }
    },

    async showContext(req, res){
        try{
            // sessionId: String representing a random number or hashed user identifier
            // keycloak Userid aus dem jwt token auslesen
            const sessionId = jwt_decode(req.kauth.grant.access_token.token).sub;

            const sessionClient = new dialogflow.SessionsClient();
            const contextsClient = new dialogflow.ContextsClient(usedAgent);

            const sessionPath = sessionClient.projectAgentSessionPath(
                projectId,
                sessionId
            );
            
            const request = {parent: sessionPath}
            const contextlist = await contextsClient.listContexts(request)
            
            res.status(200).send(contextlist)
        }catch(e){
            console.log(e.name + ": " + e.message);
            res.status(500).end()
        }
    },
    async createContext(req, res){
        try{
            // sessionId: String representing a random number or hashed user identifier
            // keycloak Userid aus dem jwt token auslesen
            const sessionId = jwt_decode(req.kauth.grant.access_token.token).sub;

            // Instantiates a session client
            const sessionClient = new dialogflow.SessionsClient();
            const contextsClient = new dialogflow.ContextsClient(usedAgent);
            
            const sessionPath = sessionClient.projectAgentSessionPath(
                projectId,
                sessionId
            );

            var context = {
                name: sessionPath + "/contexts/"+ req.body.context.name,
                lifespanCount: req.body.context.lifespanCount | 5
            }

            if(req.body.context.parameters || req.body.context.parameters != {}){
                context.parameters = req.body.context.parameters
            }
            

            const request = {
                parent: sessionPath, 
                context: context
            }
            let result = await contextsClient.createContext(request)
            
            res.send()
        }catch(e){
            console.log(e.name + ": " + e.message);
        }
    },
    async deleteAllContexts(req, res){
        try{
            // sessionId: String representing a random number or hashed user identifier
            // keycloak Userid aus dem jwt token auslesen
            const sessionId = jwt_decode(req.kauth.grant.access_token.token).sub;

            // Instantiates a session client
            const sessionClient = new dialogflow.SessionsClient();
            const contextsClient = new dialogflow.ContextsClient(usedAgent);
            
            const sessionPath = sessionClient.projectAgentSessionPath(
                projectId,
                sessionId
            );

            const request = {parent: sessionPath}
            await contextsClient.deleteAllContexts(request)
            
            res.status(200).send()
        }catch(e){
            console.log(e.name + ": " + e.message);
        }
    },
    async deleteContext(req, res){
        try{
            // sessionId: String representing a random number or hashed user identifier
            // keycloak Userid aus dem jwt token auslesen
            const sessionId = jwt_decode(req.kauth.grant.access_token.token).sub;

            // Instantiates a session client
            const sessionClient = new dialogflow.SessionsClient();
            const contextsClient = new dialogflow.ContextsClient(usedAgent);
            
            const sessionPath = sessionClient.projectAgentSessionPath(
                projectId,
                sessionId
            );

            const contextNames = req.body

            const contextlist_result = await contextsClient.listContexts({parent: sessionPath})
            let contextlist = contextlist_result[0]

            for (let index = 0; index < contextlist.length; index++) {
                const element = contextlist[index];
                let tmp_context_name_split = element.name.split("/")
                let tmp_context_name = tmp_context_name_split[tmp_context_name_split.length -1]
                
                if(contextNames.indexOf(tmp_context_name) != -1){
                    let request = {name: sessionPath + "/contexts/" + tmp_context_name}
                    contextsClient.deleteContext(request)
                }
            }
            
            res.status(200).send()
        }catch(e){
            console.log(e.name + ": " + e.message);
        }
    },
    
    async showEntities(req, res){
        try{
            // sessionId: String representing a random number or hashed user identifier
            // keycloak Userid aus dem jwt token auslesen
            const sessionId = jwt_decode(req.kauth.grant.access_token.token).sub;

            const EntityClient = new dialogflow.SessionEntityTypesClient(usedAgent)
           
            const path = {"parent": "projects/" + usedAgent.credentials.project_id + "/agent/sessions/" + sessionId}
            
            let entitiesList = await EntityClient.listSessionEntityTypes(path);
            var answer = []
            for(let entity of entitiesList[0]){
                let entityname = entity.name.split("/")
                entityname = entityname[entityname.length -1]

                answer.push({
                    "entityname": entityname,
                    "entities": entity.entities
                })
            }
            res.status(200).send(answer);
            
        }catch(e){
            console.log(e.name + ": " + e.message);
        }
    },
}

async function send_request_to_Dialogflow_and_handle_result(request){
    // request wird nach Dialogflow gesendet
    const responses = await sessionClient.detectIntent(request);

    // get Intentname
    var intent = ""
    if(responses[0] || responses[0].queryResult || responses[0].queryResult.intent || responses[0].queryResult.intent.displayName){
        if(responses[0].queryResult.intent != null){
            intent = responses[0].queryResult.intent.displayName
        }
    }

    // get queryText
    var queryText = "[leere Frage]"
    if(responses[0].queryResult.queryText){
        queryText = responses[0].queryResult.queryText
    }

    // get fulfillmentMessage or fulfillmentText
    var fulfillment = "[leere Antwort]"
    if(responses[0].queryResult.fulfillmentMessages){
        if(responses[0].queryResult.fulfillmentMessages[0]){
            fulfillment = responses[0].queryResult.fulfillmentMessages[0].text.text[0]
        }
    }else{
        if(responses[0].queryResult.fulfillmentText || responses[0].queryResult.fulfillmentText != null){
            fulfillment = responses[0].queryResult.fulfillmentText
        }
    }
    
    // get Parameter either from "parameters" or from an Context
    var parameters = null;
    if(responses[0].queryResult.webhookPayload && responses[0].queryResult.webhookPayload != null){
        parameters = struct.decode(responses[0].queryResult.webhookPayload);
    }else{
        if(responses[0].queryResult.parameters && responses[0].queryResult.parameters != null){
            parameters = struct.decode(responses[0].queryResult.parameters);
        }
    }

    const result = {
        "parameters": parameters,
        "allRequiredParamsPresent": responses[0].queryResult.allRequiredParamsPresent,
        "intent": intent,
        "queryText": queryText,
        "fulfillment": fulfillment
    }

    if(responses[0].outputAudio){
        result.outputAudio = responses[0].outputAudio
    }

    return result
}  
async function insert_Context_in_request(request, sessionPath, req){
    request.queryParams = {}
    request.queryParams.contexts = []

    if (req.body.createContexts) {
        let contexts = []
        try{
            contexts = JSON.parse(req.body.createContexts)
        }catch(e){
            contexts = req.body.createContexts
        }
       
        for (const element of contexts) {
            request.queryParams.contexts.push({
                name: sessionPath + "/contexts/"+ element,
                lifespanCount: 5,
                parameters: {}
            })
        }
    }
    if (req.body.deleteContexts) {
        let contexts = []
        try{
            contexts = JSON.parse(req.body.deleteContexts)
        }catch(e){
            contexts = req.body.deleteContexts
        }

        for (const element of contexts) {
            request.queryParams.contexts.push({
                name: sessionPath + "/contexts/"+ element,
                lifespanCount: 0,
                parameters: {}
            })
        }
    }

    if (req.body.allowExperimentalIntents) {
        request.queryParams.contexts.push({
            name: sessionPath + "/contexts/" + "experimental-intents",
            lifespanCount: 5,
            parameters: {}
        })
    }else{
        request.queryParams.contexts.push({
            name: sessionPath + "/contexts/" + "experimental-intents",
            lifespanCount: 0,
            parameters: {}
        })
    }
}

module.exports = DialogflowController;
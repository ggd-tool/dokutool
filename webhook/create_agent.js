const Authorization = require('./controllers/controller.Authorization');
const DialogflowAgent = require('./controllers/controller.DialogflowAgent');

Authorization.getKeyCloakToken().then(async (token)=>{
    var access_token 
    if(token && token.access_token){
        access_token = token.access_token
    }else{
        console.log("access_token couldn't be found")
        return
    }
    
    await DialogflowAgent.clean_up()
    await DialogflowAgent.create_Entities(access_token)
    await DialogflowAgent.create_Necessary_Intents ()
    await DialogflowAgent.create_Selecting_Maßnahme_Intents ()
    await DialogflowAgent.create_Selecting_Presets_Intents(access_token)
    await DialogflowAgent.create_selecting_Massnahme_and_Preset_and_changes_Intents(access_token)
    await DialogflowAgent.create_Section_Intents()
    
    //await DialogflowAgent.create_Massnahmen_Intents()
    
    console.log("DONE")
})

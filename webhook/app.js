require('dotenv').config();
const port = process.env.PORT || 8082 

const express = require('express');
const app = express();

const Authorization = require('./controllers/controller.Authorization');
const Webhook = require('./controllers/controller.Webhook');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/',  (req, res) => { res.status(200).send('Server is running.') });

app.post('/webhook', Authorization.checkCredentials, 
//    Authorization.checkUserByID, 
    Webhook.handleWebhook);

app.listen(port, () => { console.log(`Server is running at Port: ${port}`) })

//https://www.google.com/search?client=firefox-b-d&q=dialogflow+webhook+nodejs
//https://medium.com/crowdbotics/how-to-create-a-chatbot-with-dialogflow-nodejs-and-webhooks-eecbbce97d8b


const axios = require('axios');
require('dotenv').config()


const Database = {
    async getScript(token){
        try {
            const url = process.env.DOKU_URL + "/api/pre/ma%C3%9Fnahmen"
            
            var answer
            await axios({
                method: 'get',
                url: url,
                headers: { Authorization: `Bearer ${token}` }
            }).then((response) => 
                answer = response.data
            ).catch(error => {
                console.log(error);
            });
            return answer
        } catch (e) {
            console.log("Error in the Funktion 'getScript'")
            console.log(e)
        }
    },

    async getMassnahme(object, token){ 
        try{
            const url = process.env.DOKU_URL + "/api/pre/ubersicht/Massnahmen/" + object
            
            var answer
            await axios({
                method: 'get',
                url: url,
                headers: { Authorization: `Bearer ${token}` }
            }).then((response) => 
                answer = response.data
            ).catch(error => {
                console.log(error);
            });
            return answer
        }catch(e){
            console.log("Error in the Funktion 'getMassnahme'")
            console.log(e)
        }
    },

    // not used Functions
    getDependsOnRecursiv(object, array){
        try{
            let tmpObject = {...object}
            let tmpArray = [...array]
            if(tmpArray.length == 1){
                return tmpObject[tmpArray.shift()]
            }
            return getDependsOnRecursiv(tmpObject[tmpArray.shift()], tmpArray)
        }catch(e){
            console.log("Error in the Funktion 'getDependsOnRecursiv'")
            console.log(e)
        }
    },
    async getPreset(preset, id, token){
        try{
            console.log("preset api")
            console.log(preset)
            var url = process.env.DOKU_URL + "/api/pre/" + preset + "/" + id
            
            var answer
            await axios({
                method: 'get',
                url: url,
                headers: { Authorization: `Bearer ${token}` }
            }).then((response) => 
                answer = response.data
            ).catch(error => {
                console.log(error);
            });
            return answer
            /**/
        }catch (error) {
            console.log("Error in the Funktion 'getPreset'")
            console.log(e)
        }
    },
    async createMassnahme(massnahme, objects, token){
        try{
            const url = process.env.DOKU_URL  + "/api/" + massnahme
            
            var answer
            await axios({
                method: 'post',
                url: url,
                headers: { Authorization: `Bearer ${token}` },
                data: objects
            }).then((response) => 
                answer = response.data
            ).catch(error => {
                console.log(error);
            });

            return answer
        }catch(e){
            console.log("Error in the Funktion 'createMassnahme'")
            console.log(e)
        }
    }
}

module.exports = Database;
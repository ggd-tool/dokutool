const axios = require('axios');
require('dotenv').config();

const Authorization = {
    // check if Request has Credentials for this API
    async checkCredentials(req, res, next){
        try{
            if(req.headers.authorization == null){
                throw "The authorization header ist leer."
            }
    
            var authorization_array = req.headers.authorization.split(" ")        
            if (authorization_array[0] == "Basic") {
                //Extract credentials
                let buff = new Buffer.from(authorization_array[1], 'base64');
                let credentials = buff.toString('ascii');
    
                //check credentials
                if(credentials == process.env.USERANDPASSWORD){
                    next(); // leitet weiter nach WebhookController.handlewebhook
                }else{
                    throw "Username oder Passwort stimmen nicht überein"
                }        
    
            } else {
                throw "The authorization header ist nicht Basic."
            }
            
        }catch(e){
            console.log("Error in the Funktion 'checkCredentials'")
            console.log(e)
            res.status(400).send(e)
        }
    }, 
    // check if User Exist and has all required rolls  
    async checkUserByID(req, res, next){
        // zukünftig eine ClientID erstellen, welcher für den Webhook bestimmt ist 
        var server       = process.env.KC_URL; // add your Keycloak-URL here (without /auth)
        var realm        = process.env.KC_REALM; // the name of the realm
        var grantType    = process.env.KC_GRAND; // the granttype, with password you can login as a normal user
        var clientId     = process.env.KC_CLIENTID; // the name of the client you created in Keycloak
        var clientSecret = process.env.KC_CLIENTSECRET; // the secret you copied earlier
        var username     = process.env.KC_USERNAME; // the username of the user you want to test with
        var password     = process.env.KC_PASSWORD; // the password of the user you want to test with

        // creating the request URL
        var url  = `${server}/auth/realms/${realm}/protocol/openid-connect/token`;
        // creating the body of the request
        var data = `grant_type=${grantType}&client_id=${clientId}&username=${username}&password=${password}&client_secret=${clientSecret}`;

        axios({
            method: 'post',
            url: url,
            data: data
        }).then((response) => {
            req.body.keycloak = response.data

            const session_split = req.body.session.split("/")
            const User_ID = session_split[session_split.length -1] 
            
            let url = `${server}/auth/admin/realms/${realm}/users/${User_ID}/role-mappings`
            axios({
                method: 'get',
                url: url,
                headers: {
                    "content-type": "application/json",
                    "accept": "application/json",
                    "authorization": `Bearer ${response.data.access_token}`
                }
            }).then((response)=>{
                var you_shall_pass = false

                const User_Rolls = response.data.realmMappings
                for(let user_role of User_Rolls){
                    if(user_role.name == "worker" || user_role.name == "configurator"){
                        you_shall_pass = true
                    }
                }

                if(you_shall_pass){
                    next()
                }else{
                    res.send({
                        "fulfillmentMessages": [
                            {
                                "text": {
                                    "text": [
                                        "Sie haben nicht die Berechtigung um die Spracheingabe zu verwenden"
                                    ]
                                }
                            }
                        ],
                    })
                }
            }).catch((error)=>{
                if(error.response.status == 403){
                    res.send({
                        "fulfillmentMessages": [
                            {
                                "text": {
                                    "text": [
                                        "Der Webhook wurde nicht richtig eingestellt. Melden sie es dem IT-Personal!"
                                    ]
                                }
                            }
                        ],
                    })
                }else{
                    var error_text = "Ein unerwarteter Fehler ist aufgetreten, sagen sie dem zuständigen IT-Personal bescheid."
                
                    try{
                        if(error.response.data.error == "User not found"){
                            error_text = "Ihr Benutzer konnte nicht gefunden werden. Melden Sie es dem zuständigen IT-Personal." 
                        }else{
                            error_text = error.response.status + ": " + error.response.data.error
                        }   
                    }catch(e){
                        console.log(error)
                    }
                    
                    res.send({
                        "fulfillmentMessages": [
                            {
                                "text": {
                                    "text": [
                                        error_text
                                    ]
                                }
                            }
                        ],
                    })
                }
            })
            
        }).catch(error => {
            console.log("Error in the Funktion 'getKeyCloakToken'")
            console.log(error);
            res.end(error.name + ": " + error.message);
        });
        
    },
    // get Token from KeyCloak (is not needed for app.js. Only for create_agent.js)
    async getKeyCloakToken(){
        // zukünftig eine ClientID erstellen, welcher für den Webhook bestimmt ist 
        // oder user token vom Dokutool hollen
        var server       = process.env.KC_URL; // add your Keycloak-URL here (without /auth)
        var realm        = process.env.KC_REALM; // the name of the realm
        var grantType    = process.env.KC_GRAND; // the granttype, with password you can login as a normal user
        var clientId     = process.env.KC_CLIENTID; // the name of the client you created in Keycloak
        var clientSecret = process.env.KC_CLIENTSECRET; // the secret you copied earlier
        var username     = process.env.KC_USERNAME; // the username of the user you want to test with
        var password     = process.env.KC_PASSWORD; // the password of the user you want to test with

        // creating the request URL
        var url  = `${server}/auth/realms/${realm}/protocol/openid-connect/token`;
        // creating the body of the request
        var data = `grant_type=${grantType}&client_id=${clientId}&username=${username}&password=${password}&client_secret=${clientSecret}`;
        return new Promise((resolve, reject)=>{
            axios({
                method: 'post',
                url: url,
                data: data
            }).then((response) => {
                resolve(response.data)
            }).catch(error => {
                console.log("Error in the Funktion 'getKeyCloakToken'")
                console.log(error);
                reject(error.name + ": " + error.message)
            });
        })
    },   
}

module.exports = Authorization;

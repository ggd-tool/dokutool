const fs = require('fs');

const Webhook = {

    async handleWebhook(req, res){
        try{
            /// get the access token for KeyCloak
            //const token = req.body.keycloak.access_token
            const parentSession = req.body.session
            const intent = req.body.queryResult.intent.displayName
            const contexts = req.body.queryResult.outputContexts
            const parameter = req.body.queryResult.parameters
 
            var answer = {
                "sessionEntityTypes": [],
                "outputContexts": [],
                "payload":{}
            }

            /// increase lifespan for all Contexte
            if(contexts.length != 0){
                for(let contextobject of contexts){
                    contextobject.lifespanCount = 5
                    answer.outputContexts.push(
                        contextobject
                    )
                }
            }
            
            if(req.body.queryResult.allRequiredParamsPresent){
                /// get JSON File
                var raw_overviewJSON = fs.readFileSync("intentsAndEntities.json", {encoding: 'utf-8'});
                const overviewJSON = JSON.parse(raw_overviewJSON)

                switch(intent){
                    case "select_massnahme":
                        const selected_Massnahme = parameter['selected_massnahme']
                        
                        if(selected_Massnahme && overviewJSON.massnahme[selected_Massnahme]){
                            /// delete all Contexts and replace them with the Context below
                            answer.outputContexts = []
                            if(contexts.length != 0){
                                for(let contextobject of contexts){
                                    contextobject.lifespanCount = 0
                                    answer.outputContexts.push(
                                        contextobject
                                    )
                                }
                            }

                            /// create selected "Maßnahme" Context
                            answer.outputContexts.push(
                                {
                                    "name": parentSession + "/contexts/" + overviewJSON.massnahme[selected_Massnahme],
                                    "lifespanCount": 5,
                                    "parameters": {}
                                }
                            )
                        }
                        break
                    case "fertig":
                        break    
                    case "Default Fallback Intent":
                        break
                    default:
                        let split_intent = intent.split("--")
                        let intent_first_id  = split_intent[0]
                        let intent_second_id = split_intent[1]

                        const get_parameters = () => {
                            /// collect all usefull parameters
                            /// in the Overview JSON is an collection of all parameters. 
                            /// These will be used to find the parameters inside the JSON
                            const keys = Object.keys(overviewJSON.id[intent_first_id].entities);
                            for(const [index, key] of keys.entries()){
                                if(!parameter[key]){
                                    continue
                                }
                                if(Array.isArray(parameter[key])){
                                    if(parameter[key].length == 0){
                                        continue
                                    }
                                }
                                answer.payload[overviewJSON.id[intent_first_id].entities[key].originalEntityName] = parameter[key]
                            }
                        }

                        switch(intent_second_id){
                            case "M-P-C":
                                /// delete all Contexts and replace them with the Context below
                                answer.outputContexts = []
                                if(contexts.length != 0){
                                    for(let contextobject of contexts){
                                        contextobject.lifespanCount = 0
                                        answer.outputContexts.push(
                                            contextobject
                                        )
                                    }
                                }

                                /// create selected "Maßnahme" Context 
                                answer.outputContexts.push({
                                    "name": parentSession + "/contexts/" + intent_first_id,
                                    "lifespanCount": 5,
                                    "parameters": {}
                                })
                                
                            case "PRESET":
                                /// send the selected Massnahme through the Payload to the User.
                                answer.payload["selected_massnahme"] = overviewJSON.id[intent_first_id].name

                                get_parameters()
                                break;
                            case "S":
                                /// create an Answer where all changed Inputs is mentiond
                                let fulfillmentMessages_for_S = "Sie haben "

                                let index = 0
                                const keys = Object.keys(overviewJSON.id[intent_first_id].entities);
                                for(const key of keys){
                                    if(!parameter[key]){
                                        continue
                                    }
                                    if(Array.isArray(parameter[key])){
                                        if(parameter[key].length == 0){
                                            continue
                                        }
                                    }

                                    if(0 < index){
                                        fulfillmentMessages_for_S += ", "
                                    }
                                    index++

                                    fulfillmentMessages_for_S += overviewJSON.id[intent_first_id].entities[key].originalEntityName
                                }

                                answer.fulfillmentMessages = [{
                                    "text": {
                                        "text": [
                                            fulfillmentMessages_for_S + " verändert."
                                        ]
                                    }
                                }]

                                get_parameters()
                                break;
                            case "M":
                                /// delete all Contexts and replace them with the Context below
                                answer.outputContexts = []
                                if(contexts.length != 0){
                                    for(let contextobject of contexts){
                                        contextobject.lifespanCount = 0
                                        answer.outputContexts.push(
                                            contextobject
                                        )
                                    }
                                }
                                /// create selected "Maßnahme" Context 
                                answer.outputContexts.push({
                                    "name": parentSession + "/contexts/" + intent_first_id,
                                    "lifespanCount": 5,
                                    "parameters": {}
                                })

                                /// send the selected Massnahme through the Payload to the User.
                                answer.payload["selected_massnahme"] = overviewJSON.id[intent_first_id].name

                                get_parameters()
                                break;
                        }
                        break
                }
            }
            
            res.status(200)
            res.send(answer)

        }catch(e){
            console.log("Error in the Funktion 'handleWebhook'")
            console.log(e)
            res.status(500)
            res.end(e.name + ": " + e.message)
        }
    },
}

function CheckAndUpdateNameForDatabase(name){
    try{
        name = name.replace(/ /g, "_") //' '
        name = name.replace(/\//g, "") //'/'
        name = name.replace(/\(/g, "") //(
        name = name.replace(/\)/g, "") //)

        name = name.replace(/\%/g, "") //%
        name = name.replace(/\°/g, "0") //°
        name = name.replace(/\²/g, "2") //²

        return name.toLowerCase()
    }catch(e){
        console.log("Error in the Funktion 'CheckAndUpdateNameForDatabase'")
    }
}

module.exports = Webhook;

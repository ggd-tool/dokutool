const Database = require('./function.Database');

const dialogflow = require('@google-cloud/dialogflow')
const fs = require('fs');
const xml2js = require('xml2js');

/// Credentials for Dialogflow
const agent = require('../environment/agent.json');
const usedAgent = {credentials: agent }

/// overview JSON
var intentsAndEntitiesJSONFile

/// define the Path to the Script Folder
const path_to_script = "../scripts/"
const path_to_massnahmen = path_to_script + "massnahmen"

/// get all XML Names in the "massnahmen" Folder 
const allMassnahmenFileNames = fs.readdirSync(path_to_massnahmen);

/// Define Clients
const entitiesClient = new dialogflow.EntityTypesClient(usedAgent)
const intentsClient = new dialogflow.IntentsClient(usedAgent)
const agentClient = new dialogflow.AgentsClient(usedAgent)

/// create Dialogflow Parent
const parent = "projects/" + usedAgent.credentials.project_id + "/agent"
const contextParent = parent + "/sessions/-/contexts/"

const DialogflowAgent = {
    async clean_up(){
        try{

            /// get all Intents
            var intents_list_object = await intentsClient.listIntents({
                parent: parent,
                languageCode: "de"
            });
            var intents_list_array = []
            let intent_path = parent + "/intents/"
            
            for(let intent of intents_list_object[0]){
                let intent_split = intent.name.split("/")
                let intent_id = intent_split[intent_split.length -1]

                intents_list_array.push({name: intent_path + intent_id})
            }
            /// Delete all Intents in Dialogflow
            if(intents_list_array.length != 0){
                await intentsClient.batchDeleteIntents({
                    parent: parent,
                    intents: intents_list_array
                });
            }
            
            /// get all Entities
            // https://googleapis.dev/nodejs/dialogflow/latest/v2.EntityTypesClient.html#listEntityTypes
            var entities_list_object = await entitiesClient.listEntityTypes({
                parent: parent,
                languageCode: "de"
            });
            var entities_list_array = []
            let entity_path = parent + "/entityTypes/"

            for(let entity of entities_list_object[0]){
                let entity_split = entity.name.split("/")
                let entity_id = entity_split[entity_split.length -1]

                entities_list_array.push(entity_path + entity_id)
            }
            /// Delete all Entities in Dialogflow
            if(entities_list_array.length != 0){
                await entitiesClient.batchDeleteEntityTypes({
                    parent: parent,
                    entityTypeNames: entities_list_array
                })
            }

            try{
                /// Delete the Overview JSON
                fs.unlinkSync("intentsAndEntities.json")
            }catch(e){
                console.log("No 'intentsAndEntities.json' File to delete")
            }
            
            
            /**/
            console.log("completed clean up")
        }catch(e){
            console.log("Error in the Funktion 'clean_up'")
            console.log(e)
        }
    },
    async create_Necessary_Intents(){
        try{

            var intents = []

            /// Add a Fallback Intent
            intents.push({
                "displayName": "Default Fallback Intent",
                "isFallback": true,
                "messages": [
                    {
                        "text": {
                            "text": [
                                "Ich verstehe deine Frage leider nicht.",
                                "Entschuldige bitte, ich habe deine Frage nicht verstanden.",
                                "Ich bin nicht so sicher, ob ich dich richtig verstanden habe.",
                                "Leider kann ich nicht verstehen, was du von mir möchtest.",
                                "Kannst du das noch mal anders formulieren?"
                            ]
                        },
                    }
                ],
                "webhookState": "WEBHOOK_STATE_ENABLED",
            });
            /// Add a confirm Intent
            intents.push({
                "displayName": "fertig",
                "inputContextNames": [
                    contextParent + "fertig"
                ],
                "trainingPhrases": [
                    {
                        "type": "EXAMPLE",
                        "parts": [
                            {
                                "text": "fertig",
                            }
                        ],
                    }
                ],
                "messages": [
                    {
                        "text": {
                            "text": [
                                "Maßnahme wird abgespeichert!"
                            ]
                        }
                    }
                ],
                "webhookState": "WEBHOOK_STATE_ENABLED"
            });

            /// create Intents
            await intentsClient.batchUpdateIntents({
                parent: parent,
                intentBatchInline: {
                    intents: intents
                },
            }).catch((e)=>{
                console.log(e)
            });

            console.log("Necessary Intents created")
            /**/
        }catch(e){
            console.log("Error in the Funktion 'create_Necessary_Intents'")
            console.log(e)
        }
    },
    async create_Selecting_Maßnahme_Intents(){
        try{

            /// load existing Overview JSON FIle or give an {} back
            intentsAndEntitiesJSONFile = OverviewJSON_load()
            
            var auswahlEntity = {
                "displayName": "select_massnahme",
                "kind": 1,
                "entities": []
            }
            var auswahlIntent = {
                displayName: "select_massnahme",
                parameters: [{
                    displayName: "selected_massnahme",
                    entityTypeDisplayName: "@select_massnahme",
                    value: "$select_massnahme",
                    mandatory: true,
                    prompts: [
                        "Bitte geben sie eine Massnahme oder Preset an!"
                    ]
                }],
                trainingPhrases: [],
                messages: [
                    {
                        "text": {
                            "text": ["Sie haben folgendes ausgewählt: $selected_massnahme.original"]
                        }
                    }
                ],
                inputContextNames: [
                    contextParent + "select_massnahme",
                ],
                webhookState: "WEBHOOK_STATE_ENABLED",
            }
            
            var intents = []
            var entities = []
            
            for(const [index, massnahme] of allMassnahmenFileNames.entries()){
                /// take the data from XML and parse it to JSON
                var dataAsJSON = get_XML_Massnahme_as_JSON(massnahme)

                // check if the Data is not empty
                if(dataAsJSON.massnahme == null || dataAsJSON.massnahme == undefined){
                    console.log("Error: XML '" + massnahme + "' empty or incorrect")
                    return 
                }

                /// Define a Unique ID for the Entities- and Intents Names
                const intentID = createID(index)

                OverviewJSON_add_ID(dataAsJSON.massnahme.$.name, intentID)

                /// pick all Massnahmen Names in a Array for the Entity auswahl
                auswahlEntity.entities.push({
                    "value": dataAsJSON.massnahme.$.name,
                    "synonyms": [dataAsJSON.massnahme.$.name]
                })
                
                /// create trainingPhrases
                auswahlIntent.trainingPhrases.push({
                    type: "EXAMPLE",
                    parts: [
                        {
                            "text": dataAsJSON.massnahme.$.name,
                            "entityType": "@select_massnahme",
                            "alias": "selected_massnahme"
                        }
                    ]
                })
            }

            entities.push(auswahlEntity)
            intents.push(auswahlIntent)

            OverviewJSON_save()

            /// create Entities
            await entitiesClient.batchUpdateEntityTypes({
                parent: parent,
                entityTypeBatchInline:{
                    entityTypes: entities
                }
            }).catch((e)=>{
                console.log(e)
            })
            /// create Intents
            await intentsClient.batchUpdateIntents({
                parent: parent,
                intentBatchInline: {
                    intents: intents
                },
            }).catch((e)=>{
                console.log(e)
            });

            console.log("Selecting Maßnahme Intent created")
            /**/
        }catch(e){
            console.log("Error in the Funktion 'create_Selecting_Maßnahme_Intents'")
            console.log(e)
        }
    },
    async create_Selecting_Presets_Intents(token){
        try{

            /// load existing Overview JSON FIle or give an {} back
            intentsAndEntitiesJSONFile = OverviewJSON_load()

            var intents = []

            for(let [index, massnahme] of allMassnahmenFileNames.entries()){
                /// take the data from XML and parse it to JSON
                let dataAsJSON = get_XML_Massnahme_as_JSON(massnahme);

                if(!dataAsJSON.massnahme.$.preset){
                    continue
                }

                // check if the Data is not empty
                if(dataAsJSON.massnahme == null || dataAsJSON.massnahme == undefined){
                    console.log("Error: XML '" + massnahme + "' empty or incorrect")
                    return
                }

                /// Define a Unique ID for the Entities- and Intents Names
                let intentID = createID(index)

                let entityName  = CheckAndUpdateNameForEntities(intentID + "PRESET" + dataAsJSON.massnahme.$.name)
                let intentName  = CheckAndUpdateNameForIntents(intentID + "--" + "PRESET" + "--" + dataAsJSON.massnahme.$.name)
                
                /// create a Overview JSON for the Intent
                OverviewJSON_create_intent(intentName, dataAsJSON.massnahme.$.name)
                OverviewJSON_add_ID_to_Intent(intentName, intentID)
                //OverviewJSON_add_object(intentName, "Preset_" + dataAsJSON.massnahme.$.name)

                let presets = await Database.getMassnahme("Preset_" + dataAsJSON.massnahme.$.name, token)

                if(!presets || presets.length == 0){
                    OverviewJSON_empty_object(intentName)
                    continue
                }

                let intent = {
                    displayName: intentName, //Preset
                    parameters: [{
                        displayName: entityName,
                        entityTypeDisplayName: "@" + entityName,
                        value: "$" + entityName,
                        prompts: [
                            'Bitte geben Sie ein Wert für einen Preset ein!'
                        ],
                        mandatory: true,
                        isList: false
                    }],
                    trainingPhrases: [],
                    messages: [
                        {
                            "text": {
                                "text": ["Sie haben folgenden Preset ausgewählt: $" + entityName + ".original"]
                            }
                        }
                    ],
                    inputContextNames: [
                        contextParent + intentID,
                        contextParent + "select_preset"
                    ],
                    outputContexts: [],
                    webhookState: "WEBHOOK_STATE_ENABLED",
                }

                /// create Trainingphrases:
                ///          <Massnahme>   <Preset>
                /// Example: Gießen Regner alles gießen
                var p_trainingPhrase
                for(const [index, preset] of presets.entries()){
                    p_trainingPhrase = {
                        "type": "EXAMPLE",
                        "parts": [{
                            "text": preset.name,
                            "entityType": "@" + entityName,
                            "alias": entityName
                        }]
                    }
                    intent.trainingPhrases.push(p_trainingPhrase)
                }

                if(p_trainingPhrase == undefined){
                    p_trainingPhrase = {
                        "type": "EXAMPLE",
                        "parts": []
                    }
                }
                
                /// add additional Trainingphrase <change>
                add_Changes_Trainingsphrases(dataAsJSON, intent, intentID, entityName, p_trainingPhrase)
                /// Result:
                ///          <Preset>     <Change>
                /// Example: alles gießen 5 min

                intents.push(intent)
            }

            /// create Intents
            // https://googleapis.dev/nodejs/dialogflow/latest/v2.IntentsClient.html#batchUpdateIntents
            await intentsClient.batchUpdateIntents({
                parent: parent,
                intentBatchInline: {
                    intents: intents
                },
            }).catch((e)=>{
                console.log(e)
            });
            /**/

            OverviewJSON_save()

            console.log("Selecting Presets Intent created")
            /**/
        }catch(e){
            console.log("Error in the Funktion 'create_Selecting_Presets_Intents'")
            console.log(e)
        }
    },
    
    async create_Entities(token){
        /// create Entities here once and save the Names and TrainingPhases into the overview JSON.
        /// When the Entities are needed, take it out of the overview JSON.

        /// load existing Overview JSON FIle or give an {} back
        intentsAndEntitiesJSONFile = OverviewJSON_load()

        var entities = []

        for(const [index, massnahme] of allMassnahmenFileNames.entries()){
            /// take the data from XML and parse it to JSON
            let dataAsJSON = get_XML_Massnahme_as_JSON(massnahme);

            // check if the Data is not empty
            if(dataAsJSON.massnahme == null || dataAsJSON.massnahme == undefined){
                console.log("Error: XML '" + massnahme + "' empty or incorrect")
                return
            }

            /// Define a Unique ID for the Entities- and Intents Names
            const intentID = createID(index)

            const intentName = CheckAndUpdateNameForIntents(intentID + "--" + dataAsJSON.massnahme.$.name)
            const intent_MPC_Name  = CheckAndUpdateNameForIntents(intentID + "--" + "M-P-C" + "--" + dataAsJSON.massnahme.$.name)

            OverviewJSON_create_intent(intentName, dataAsJSON.massnahme.$.name)
            OverviewJSON_create_intent(intent_MPC_Name, dataAsJSON.massnahme.$.name)
            OverviewJSON_add_ID_to_Intent(intentName, intentID)
            OverviewJSON_add_ID_to_Intent(intent_MPC_Name, intentID)
            OverviewJSON_add_ID(dataAsJSON.massnahme.$.name, intentID)

            /// create PresetEntitites
            if(dataAsJSON.massnahme.$.preset){
                /// get all Presets from the Database
                let presets = await Database.getMassnahme("Preset_" + dataAsJSON.massnahme.$.name, token)

                /// If this Massnahme has none Presets, skip it
                if(!presets || presets.length == 0){
                    OverviewJSON_empty_object(intentName)
                    OverviewJSON_empty_object(intent_MPC_Name)
                }
                /// Define a suitable Entity Name
                var entityName = CheckAndUpdateNameForEntities(intentID + "PRESET" + dataAsJSON.massnahme.$.name)
            
                /// collect all Information for the Entity inside this Variable 
                var entity = {
                    displayName: entityName,
                    kind: "KIND_MAP",
                    entities: []
                }
                
                presets.forEach((preset,index)=>{
                    entity.entities.push({
                        "value": preset._id,
                        "synonyms": [preset.name]
                    });
                });
                entities.push(entity)
            
            }

            for (const section of dataAsJSON.massnahme.section) {
                for (const input of section.input) {

                    if(input.$.dependsOn){
                        continue
                    }

                    /// Define a suitable Entity Name
                    var entityName = CheckAndUpdateNameForEntities(intentID + input.label[0])
                    
                    /// collect all Information for the Entity inside this Variable 
                    var entity = {
                        displayName: entityName
                    }

                    /// create Parts for trainingPhrases and push it in the overview JSON for later
                    /// later the Overview JSON provides the Parts for trainingphrases for the intents
                    var trainingPhrases_parts = []
                    const add_trainingPhrase_part = (text, entityType)=>{
                        trainingPhrases_parts.push({
                            text: text,
                            entityType: "@" + entityType,
                            alias: entityType,
                            //userDefined: true 
                        })
                    }
                    
                    switch(input.$.type){
                        // KIND_UNSPECIFIED:0
                        // KIND_MAP:1 (with Synonyms)
                        // KIND_LIST:2 (without Synonyms, but can use System and User Entities)
                        // KIND_REGEXP:3

                        //dependson is missing
                        case "number":
                        case "string":
                            entity.kind = "KIND_LIST"
                            entity.entities = []

                            let value = "@sys.number:number"
                            
                            if(input.unit){
                                switch(input.unit[0]){
                                    /// Values with single Units like(n: Number) "n minutes" or "n liter" can be easly extracted from sys.entities
                                    /// Values with multiple Units like(n: Number) "n liter per hektar", "n millisiemens per Zentimeter" can be extraced from 
                                    /// @sys.number and their Units as Text. Its easier to use just Text instead of sys.entities to define the Units, because i
                                    /// couldn'd find some Units doesn't exist inside of Dialogflow  
                                    case "min":
                                        entity.entities.push({"value": "@sys.duration"}) // gives {"amount": string, "unit": string}
                                        add_trainingPhrase_part("2 minuten", entityName)
                                        break;
                                    case "L":
                                        entity.entities.push({"value": value + "@sys.unit-volume-name"})
                                        add_trainingPhrase_part("1 liter", entityName)
                                        break;
                                    case "L/m²":
                                        //entity.entities.push({"value": value + " " + "@sys.unit-volume-name:unit1 pro @sys.unit-area-name:unit2"})// it works this way too
                                        entity.entities.push({"value": value + " " + "liter pro quadratmeter"})
                                        add_trainingPhrase_part("1 liter pro Quadratmeter", entityName)
                                        break;
                                    case "L/ha":
                                        //entity.entities.push({"value": value + " " + "@sys.unit-volume-name:unit1 pro @sys.unit-area-name:unit2"})// it works this way too
                                        entity.entities.push({"value": value + " " + "liter pro hektar"})
                                        entity.entities.push({"value": value + " " + "l pro hektar"})
                                        entity.entities.push({"value": value + " " + "liter pro ha"})
                                        entity.entities.push({"value": value + " " + "l pro ha"})
                                        add_trainingPhrase_part("1 liter pro Hektar", entityName)
                                        break;
                                    case "g/l":
                                        //entity.entities.push({"value": value + " " + "@sys.unit-weight-name:unit1 pro @sys.unit-volume-name:unit2"})// it works this way too
                                        entity.entities.push({"value": value + " " + "gramm pro liter"})
                                        entity.entities.push({"value": value + " " + "gramm pro l"})
                                        entity.entities.push({"value": value + " " + "g pro liter"})
                                        entity.entities.push({"value": value + " " + "g pro l"})
                                        add_trainingPhrase_part("1 gramm pro liter", entityName)
                                        break;
                                    case "mS/cm":
                                        // it doesn't works with sys.entities, because i couldn't find 
                                        entity.entities.push({"value": value + " " + "millisiemens pro zentimeter"})
                                        add_trainingPhrase_part("1 millisiemens pro zentimeter", entityName)
                                        break;
                                    case "%":
                                        entity.entities.push({"value": "@sys.percentage:number"})
                                        add_trainingPhrase_part("1 %", entityName)
                                        break;
                                    case "EC":
                                        entity.entities.push({"value": value + " " + "EC"})
                                        add_trainingPhrase_part("1 EC", entityName)
                                        break;
                                    default:
                                        entity.entities.push({"value": value + " " + input.unit[0]})
                                        add_trainingPhrase_part("1 " + input.unit[0], entityName)
                                        console.log("Couldn't find an suitable System Entity for an Unit!")
                                        break;
                                }
                            }else{
                                switch(input.label[0]){
                                    case "Einweghäufigkeit":
                                        entity.kind = "KIND_MAP"
                                        entity.entities.push(
                                            {value: 1,synonyms: ["1 mal","einmal"]},
                                            {value: 2,synonyms: ["2 mal", "zweimal"]},
                                            {value: 3,synonyms: ["3 mal","dreimal"]},
                                            {value: 4,synonyms: ["4 mal","viermal"]},
                                            {value: 5,synonyms: ["5 mal","fünfmal"]},
                                            {value: 6,synonyms: ["6 mal","sechsmal"]},
                                            {value: 7,synonyms: ["7 mal","siebenmal"]},
                                            {value: 8,synonyms: ["8 mal","achtmal"]},
                                            {value: 9,synonyms: ["9 mal","neunmal"]},
                                            {value: 10,synonyms: ["10 mal","zehnmal"]},
                                            {value: 11,synonyms: ["11 mal","elfmal"]},
                                            {value: 12,synonyms: ["12 mal","zwölfmal"]},
                                            {value: 13,synonyms: ["13 mal","dreizehnmal"]},
                                            {value: 14,synonyms: ["14 mal","vierzehnmal"]},
                                            {value: 15,synonyms: ["15 mal","fünfzehnmal"]},
                                            {value: 16,synonyms: ["16 mal","sechszehnmal"]},
                                            {value: 17,synonyms: ["17 mal","siebzehnmal"]},
                                            {value: 18,synonyms: ["18 mal","achtzehnmal"]},
                                            {value: 19,synonyms: ["19 mal","neunzehnmal"]},
                                            {value: 20,synonyms: ["20 mal","zwanzigmal"]}
                                        )
                                        add_trainingPhrase_part("zweimal", entityName)
                                        break;
                                    default:
                                        console.log("Entity/Input is a Number and doesn't has a Unit")
                                        break
                                }
                            }
                            break;                        
                        case "select":
                            entity.kind = "KIND_MAP"
                            entity.entities = [];
                            input.option.forEach((option, index)=>{
                                /// for creating the Entities.
                                entity.entities.push({"value": option.$.key}) 
                                if(index < 1){                               
                                    add_trainingPhrase_part(option._, entityName)
                                }
                            });
                            break
                        case "dropdown": 
                            entity.kind = "KIND_MAP"
                            entity.entities = [];

                            if(input.$.object){
                                /// Pick the Data from MongoDB and use it to create Entities
                                let objects = await Database.getMassnahme(input.$.object, token)

                                if(!objects || objects.length == 0){
                                    console.log("---------------"+dataAsJSON.massnahme.$.name+"---------------")
                                    console.log("Object/Typ hat keine Daten: " + input.label[0])
                                    console.log("----------------------------------------")
                                    OverviewJSON_empty_object(intentName)
                                    OverviewJSON_add_entity(intentID, entityName, input.label[0] , [])
                                }
                                
                                objects.forEach((object,index)=>{
                                    entity.entities.push({
                                        "value": object._id,
                                        "synonyms": [object.name]
                                    })
                                    if(input.$.multiple){
                                        if(index < 3 ){
                                            add_trainingPhrase_part(object.name, entityName)
                                        }
                                    }else{
                                        if(index < 1 ){
                                            add_trainingPhrase_part(object.name, entityName)
                                        }
                                    }
                                });
                            }

                            break;
                        case "singleSlider":
                            /// 1 to 100 => ^[1-9][0-9]?$|^100$
                            /// Regex didnt worked, when it is created in the Api
                            /// it works when it is manuell created
                            
                            /// for creating the Entity
                            entity.kind = "KIND_LIST"
                            entity.entities = [{"value": "@sys.percentage:value"}]
                            add_trainingPhrase_part("1%", entityName)
                            break;
                        case "rangeSlider":
                            entity.kind = "KIND_LIST"

                            /// for creating the Entity
                            entity.entities = [
                                {"value": "von @sys.percentage:von bis @sys.percentage:bis"}
                            ]
                            add_trainingPhrase_part("von 0% bis 100%", entityName)
                            break;
                        default:
                            console.log( dataAsJSON.massnahme.$.name )
                            console.log( input.label[0] )
                            console.log( input.$.type   )
                            console.log("-----------type nicht eingefügt-----------")
                            console.log("------------------------------------------")
                            continue;
                    }

                    entities.push(entity)

                    OverviewJSON_add_entity(intentID, entityName, input.label[0] , trainingPhrases_parts)
                    
                }
            }

            OverviewJSON_save()

        }
        /// create Entities
        await entitiesClient.batchUpdateEntityTypes({
            parent: parent,
            entityTypeBatchInline:{
                entityTypes: entities
            }
        }).catch((e)=>{
            console.log(e)
        })
        /**/

        console.log("Entities created")
    },
    async create_selecting_Massnahme_and_Preset_and_changes_Intents(token){
        try{

            /// load existing Overview JSON FIle or give an {} back
            intentsAndEntitiesJSONFile = OverviewJSON_load()
      
            var intents = []

            for(let [index, massnahme] of allMassnahmenFileNames.entries()){
                
                /// take the data from XML and parse it to JSON
                var dataAsJSON = get_XML_Massnahme_as_JSON(massnahme)
                
                // check if the Data is not empty
                if(dataAsJSON.massnahme == null || dataAsJSON.massnahme == undefined){
                    console.log("Error: XML '" + massnahme + "' empty or incorrect")
                    return 
                }
                
                if(!dataAsJSON.massnahme.$.preset){
                    continue
                }

                /// Define a Unique ID for the Entities- and Intents Names
                const intentID = createID(index)

                let entityPresetName  = CheckAndUpdateNameForEntities(intentID + "PRESET" + dataAsJSON.massnahme.$.name)
                // M = "Massnahme" you want to select, P = "Preset" you want to select, C = Changes you want to do
                let intent_MPC_Name  = CheckAndUpdateNameForIntents(intentID + "--" + "M-P-C" + "--" + dataAsJSON.massnahme.$.name)
                
                /// create a Overview JSON for the Intent
                //OverviewJSON_add_object(intent_MPC_Name, "Preset_" + dataAsJSON.massnahme.$.name)
                OverviewJSON_add_entity(intentID, entityPresetName, "Preset_" + dataAsJSON.massnahme.$.name)

                /// get all Presets from the Database
                let presets = await Database.getMassnahme("Preset_" + dataAsJSON.massnahme.$.name, token)

                /// If this Massnahme has none Presets, skip it
                if(!presets || presets.length == 0){
                    OverviewJSON_empty_object(intent_MPC_Name)
                    continue
                }
                
                let intent = {
                    displayName: intent_MPC_Name, //Preset
                    parameters: [{
                        displayName: entityPresetName,
                        entityTypeDisplayName: "@" + entityPresetName,
                        value: "$" + entityPresetName,
                        prompts: [
                            'Bitte geben Sie ein Wert für einen Preset ein!'
                        ],
                        mandatory: true,
                        isList: false
                    }],
                    trainingPhrases: [],
                    messages: [
                        {
                            "text": {
                                "text": ["Sie haben " + dataAsJSON.massnahme.$.name + " und $" + entityPresetName + ".original ausgewählt."],
                            },
                        }
                    ],
                    inputContextNames: [
                        contextParent + "select_massnahme"
                    ],
                    outputContexts: [],
                    webhookState: "WEBHOOK_STATE_ENABLED",
                }
                

                /// create Trainingphrases:
                ///          <Massnahme>   <Preset>
                /// Example: Gießen Regner alles gießen
                var m_p_trainingPhrase
                for(const preset of presets){
                    m_p_trainingPhrase = {
                        "type": "EXAMPLE",
                        "parts": [{
                            "text": dataAsJSON.massnahme.$.name
                        },{
                            "text": " ",
                        },{
                            "text": preset.name,
                            "entityType": "@" + entityPresetName,
                            "alias": entityPresetName
                        }]
                    }
                    intent.trainingPhrases.push(m_p_trainingPhrase)
                }

                if(m_p_trainingPhrase == undefined){
                    m_p_trainingPhrase = {
                        "type": "EXAMPLE",
                        "parts": []
                    }
                }

                /// add additional Trainingphrase <change>
                add_Changes_Trainingsphrases(dataAsJSON, intent, intentID, entityPresetName, m_p_trainingPhrase)
                /// Result:
                ///          <Massnahme>   <Preset>     <Change>
                /// Example: Gießen Regner alles gießen 5 min

                intents.push(intent)
            }
            

            /// create Intents
            // https://googleapis.dev/nodejs/dialogflow/latest/v2.IntentsClient.html#batchUpdateIntents
            await intentsClient.batchUpdateIntents({
                parent: parent,
                intentBatchInline: {
                    intents: intents
                },
            }).catch((e)=>{
                console.log(e)
            });
            /**/

            OverviewJSON_save()

            console.log("Selecting Maßnahme and Presets Intent created")
            /**/
        }catch(e){
            console.log("Error in the Funktion 'create_selecting_Massnahme_and_Preset_and_changes_Intents'")
            console.log(e)
        }
    },
    
    async create_Section_Intents(){
        try{

            /// load existing Overview JSON FIle or give an {} back
            intentsAndEntitiesJSONFile = OverviewJSON_load()
      
            var intents = []

            for(let [index_massnahme, massnahme] of allMassnahmenFileNames.entries()){
                
                /// take the data from XML and parse it to JSON
                var dataAsJSON = get_XML_Massnahme_as_JSON(massnahme)
                
                // check if the Data is not empty
                if(dataAsJSON.massnahme == null || dataAsJSON.massnahme == undefined){
                    console.log("Error: XML '" + massnahme + "' empty or incorrect")
                    return 
                }
                
                if(!dataAsJSON.massnahme.$.preset){
                    continue
                }

                /// Define a Unique ID for the Entities- and Intents Names
                const intentID = createID(index_massnahme)

                
                for(const [index_section, section] of dataAsJSON.massnahme.section.entries()){
                    // S = Sector
                    let intent_C_Name  = CheckAndUpdateNameForIntents(intentID + "--" + "S" + "--" + index_section + "--" + dataAsJSON.massnahme.$.name)

                    let intent = {
                        displayName: intent_C_Name,
                        parameters: [],
                        trainingPhrases: [],
                        messages: [{
                            "text": {
                                "text": []
                            }
                        }],
                        inputContextNames: [
                            contextParent + intentID,
                            contextParent + "customize_massnahme"
                        ],
                        outputContexts: [],
                        webhookState: "WEBHOOK_STATE_ENABLED",
                    }

                    let array_Entities = []
                    for(const input of section.input){
                        if(input.$.dependsOn){
                            continue
                        }
                        
                        var entityName = CheckAndUpdateNameForEntities(intentID + input.label[0])

                        let parameter = {
                            displayName: entityName,
                            entityTypeDisplayName: "@" + entityName,
                            value: "$" + entityName,
                            mandatory: false,
                            isList: input.$.multiple || false
                        }
                        intent.parameters.push(parameter)

                        array_Entities.push(entityName)
                    }

                    /// recursiv function to create all possible combination of Entities
                    /// EXAMPLE with 2 Entities:
                    /// <part1>
                    /// <part1> <part2>
                    /// <part2>
                    /// <part2> <part1>
                    /// the number of Trainingphrases increases with T(n+1) = (Tn + 1) x n
                    /// after 6 Entities its exceeds the number of Trainingphrases (Max 2000 Trainingphrases per Intent and only 10000 in total) 
                    /// Quelle: https://www.quora.com/This-sequence-is-killing-me-for-2-years-0-1-4-15-64-325-What-is-the-formula-of-this-sequence
                    const recursiv_create_trainingphrases = (array_Entities_Names, trainingPhrase = {
                        "type": "EXAMPLE",
                        "parts": []
                    })=>{
                        for(const [index_entities, entityName] of array_Entities_Names.entries()){
                            /// check if parts for the Trainingphrases exist
                            let entities = intentsAndEntitiesJSONFile.id[intentID].entities[entityName]

                            /// make a copy of the existing trainingphrase and Entity Array 
                            let copy_Trainingphrase = JSON.parse(JSON.stringify(trainingPhrase));
                            let copy_Entities =  [...array_Entities_Names]

                            if(entities && entities.parts && entities.parts.length != 0){
                                /// take the Part to add to the Trainingphrase
                                let part = entities.parts[0]
                                
                                /// add an Part to the copy of the Trainingphrase.
                                if(trainingPhrase.parts.length != 0){
                                    copy_Trainingphrase.parts.push({"text": " "})
                                }
                                copy_Trainingphrase.parts.push(part)

                                /// the new trainingphrases is added to the Intent
                                intent.trainingPhrases.push(copy_Trainingphrase)
                            }

                            /// Delete the used Part from the Entity Array
                            copy_Entities.splice(index_entities, 1);

                            /// If the Entity Array is Empty End this Recursion
                            /// If not then start a new one with "array_Entities_Names" -1
                            if(copy_Entities.length == 0){
                                return
                            }else{
                                recursiv_create_trainingphrases(copy_Entities, copy_Trainingphrase)
                            }
                        }
                    }

                    recursiv_create_trainingphrases(array_Entities)

                    intents.push(intent)
                }
            }
            

            /// create Intents
            // https://googleapis.dev/nodejs/dialogflow/latest/v2.IntentsClient.html#batchUpdateIntents
            await intentsClient.batchUpdateIntents({
                parent: parent,
                intentBatchInline: {
                    intents: intents
                },
            }).catch((e)=>{
                console.log(e)
            });
            /**/

            OverviewJSON_save()

            console.log("Customize Massnahme Intent created")
            /**/
        }catch(e){
            console.log("Error in the Funktion 'create_selecting_Massnahme_and_Preset_and_changes_Intents'")
            console.log(e)
        }
    },

    async create_Massnahmen_Intents(){
        try{

            /// load existing Overview JSON FIle or give an {} back
            intentsAndEntitiesJSONFile = OverviewJSON_load()
      
            var intents = []

            for(let [index_massnahme, massnahme] of allMassnahmenFileNames.entries()){
                
                /// take the data from XML and parse it to JSON
                var dataAsJSON = get_XML_Massnahme_as_JSON(massnahme)
                
                // check if the Data is not empty
                if(dataAsJSON.massnahme == null || dataAsJSON.massnahme == undefined){
                    console.log("Error: XML '" + massnahme + "' empty or incorrect")
                    return 
                }
                
                /// Define a Unique ID for the Entities- and Intents Names
                const intentID = createID(index_massnahme)
                
                // M = Massnahme
                let intent_M_Name  = CheckAndUpdateNameForIntents(intentID + "--" + "M" + "--" + dataAsJSON.massnahme.$.name)

                let intent = {
                    displayName: intent_M_Name,
                    parameters: [],
                    trainingPhrases: [],
                    messages: [{
                        "text": {
                            "text": ["Sie werden zur Maßnahme " + dataAsJSON.massnahme.$.name + " umgeleitet."]
                        }
                    }],
                    inputContextNames: [
                        contextParent + "select_massnahme",
                        contextParent + "experimental-intents"
                    ],
                    outputContexts: [],
                    webhookState: "WEBHOOK_STATE_ENABLED",
                }

                let array_Entities = []

                for(const [index_section, section] of dataAsJSON.massnahme.section.entries()){
                    for(const input of section.input){
                        if(input.$.dependsOn){
                            continue
                        }
                        
                        var entityName = CheckAndUpdateNameForEntities(intentID + input.label[0])

                        let parameter = {
                            displayName: entityName,
                            entityTypeDisplayName: "@" + entityName,
                            value: "$" + entityName,
                            mandatory: false,
                            isList: input.$.multiple || false
                        }
                        intent.parameters.push(parameter)

                        array_Entities.push(entityName)

                    }

                }

                /// recursiv function to create all possible combination of Entities
                /// EXAMPLE with 2 Entities:
                /// <part1>
                /// <part1> <part2>
                /// <part2>
                /// <part2> <part1>
                
                /// the number of Trainingphrases increases with each Entity T(n+1) = (Tn + 1) x n
                /// On 7 Entities its exceeds the number of Trainingphrases (Max 2000 Trainingphrases per Intent and only 10000 in total) 
                /// Quelle: https://www.quora.com/This-sequence-is-killing-me-for-2-years-0-1-4-15-64-325-What-is-the-formula-of-this-sequence

                // On 7 Entities the Recursiv Function don't work, because the Error shows up:
                // FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed - JavaScript heap out of memory

                const recursiv_create_trainingphrases = (array_Entities_Names, trainingPhrase = {
                    "type": "EXAMPLE",
                    "parts": []
                })=>{
                    for(const [index_entities, entityName] of array_Entities_Names.entries()){
                        /// check if parts for the Trainingphrases exist
                        let entities = intentsAndEntitiesJSONFile.id[intentID].entities[entityName]

                        /// make a copy of the existing trainingphrase and Entity Array 
                        let copy_Trainingphrase = JSON.parse(JSON.stringify(trainingPhrase));
                        let copy_Entities =  [...array_Entities_Names]

                        if(entities && entities.parts && entities.parts.length != 0){
                            /// take the Part to add to the Trainingphrase
                            let part = entities.parts[0]
                            
                            /// add an Part to the copy of the Trainingphrase.
                            if(trainingPhrase.parts.length != 0){
                                copy_Trainingphrase.parts.push({"text": " "})
                            }
                            copy_Trainingphrase.parts.push(part)

                            /// the new trainingphrases is added to the Intent
                            intent.trainingPhrases.push(copy_Trainingphrase)
                        }

                        /// Delete the used Part from the Entity Array
                        copy_Entities.splice(index_entities, 1);

                        /// If the Entity Array is Empty End this Recursion
                        /// If not then start a new one with "array_Entities_Names" -1
                        if(copy_Entities.length == 0){
                            return
                        }else{
                            recursiv_create_trainingphrases(copy_Entities, copy_Trainingphrase)
                        }
                    }
                }

                // iterative function to create most combination of Entities
                /// EXAMPLE with 3 Entities:
                /// <part1> <part2> <part3>
                /// <part1> <part3> <part2>
                /// <part2> <part1> <part3>
                /// <part2> <part3> <part1>
                /// <part3> <part1> <part2>
                /// <part3> <part2> <part1>
                /// the number of Trainingphrases increases with each Entity => n = The Number of Entities, f(n) = n!
                /// On 7 Entities it exceeds the number of Trainingphrases (Max 2000 Trainingphrases per Intent and only 10000 in total) 
                /// https://www.mathsisfun.com/combinatorics/combinations-permutations-calculator.html
                const iterativ_create_trainingphrases = (array_Entities_Names) =>{
                    const empty_trainingPhrase = {
                        "type": "EXAMPLE",
                        "parts": []
                    }
                    const create_trainingPhrasesFromArray = (Array_Entity)=>{
                        /// make a copy of the existing trainingphrase and Entity Array 
                        let copy_empty_Trainingphrase = JSON.parse(JSON.stringify(empty_trainingPhrase));

                        for(const [index_entities, entityName] of Array_Entity.entries()){
                            /// check if parts for the Trainingphrases exist
                            let entities = intentsAndEntitiesJSONFile.id[intentID].entities[entityName]
    
                            if(entities && entities.parts && entities.parts.length != 0){
                                /// take the Part to add to the Trainingphrase
                                let part = entities.parts[0]
                                
                                /// add an Part to the copy of the Trainingphrase.
                                if(copy_empty_Trainingphrase.parts.length != 0){
                                    copy_empty_Trainingphrase.parts.push({"text": " "})
                                }
                                copy_empty_Trainingphrase.parts.push(part)
                            }
                        }
                        
                        /// the new trainingphrases is added to the Intent
                        intent.trainingPhrases.push(copy_empty_Trainingphrase)
                    }
                    

                    if(array_Entities_Names.length == 1){
                        create_trainingPhrasesFromArray(array_Entities_Names)
                        return [array_Entities_Names]
                    }else if(array_Entities_Names.length == 0){
                        console.log("Error in Function 'iterativ_create_trainingphrases'. array_Entities_Names has the length 0.")
                    }

                    var all_combinations = []

                    const combinaten_reached_limits = (combinations_length)=>{
                        if(combinations_length == 2000){
                            return true
                        }
                        return false
                    }

                    for(const [index_Entitites, Entity] of array_Entities_Names.entries()){
                        const copy_Entities = [...array_Entities_Names]
                        copy_Entities.splice(index_Entitites, 1);
                        
                        /// the new trainingphrases is added to the Intent
                        all_combinations.push({array: [Entity], combinations: copy_Entities})
                    }

                    while(true){
                        var tmp_all_combinations = []
                        for(const [index_data, data] of all_combinations.entries()){

                            if(!data.combinations[0] || data.combinations[0] == null){
                                break
                            }
                            if(combinaten_reached_limits(tmp_all_combinations.length)){
                                break
                            }

                            for(const [index_combination, tmp_combinations] of data.combinations.entries()){
                                const copy_Array = [...data.array]
                                const copy_combinations = [...data.combinations]
                                // add one Entity
                                copy_Array.push(tmp_combinations)
                                /// Delete the used Part from the Entity Array
                                copy_combinations.splice(index_combination, 1);
                                // save new Array and reduced combination
                                tmp_all_combinations.push({array: copy_Array, combinations: copy_combinations})

                                if(combinaten_reached_limits(tmp_all_combinations.length)){
                                    break
                                }
                            }
                        }

                        if(tmp_all_combinations[0].combinations.length == 0 || combinaten_reached_limits(tmp_all_combinations.length)){
                            all_combinations = []
                            tmp_all_combinations.forEach((data)=>{
                                all_combinations.push(data.array)
                            })
                            break
                        }else{
                            all_combinations = JSON.parse(JSON.stringify(tmp_all_combinations));
                        }
                    }

                    for(const [index_Entitites, entity_array] of all_combinations.entries()){
                        create_trainingPhrasesFromArray(entity_array)
                    }
                }

                if(array_Entities.length <= 6 || true){
                    iterativ_create_trainingphrases(array_Entities)
                    //recursiv_create_trainingphrases(array_Entities)
                    intents.push(intent)
                }else{
                    console.log(dataAsJSON.massnahme.$.name)
                    console.log("Die Maßnahme überschreitet die 6!")
                }
            }

            console.log("uploading to Dialogflow")
            /// create Intents
            // https://googleapis.dev/nodejs/dialogflow/latest/v2.IntentsClient.html#batchUpdateIntents
            await intentsClient.batchUpdateIntents({
                parent: parent,
                intentBatchInline: {
                    intents: intents
                },
            }).catch((e)=>{
                console.log(e)
            });
            /**/

            OverviewJSON_save()

            console.log("Customize Massnahme Intent created")
            /**/
        }catch(e){
            console.log("Error in the Funktion 'create_selecting_Massnahme_and_Preset_and_changes_Intents'")
            console.log(e)
        }
    }
}
/**/

function add_Changes_Trainingsphrases(dataAsJSON, intent, intentID, entityPresetName, m_p_trainingPhrase){
    /// add additional Trainingphrase <change> (The Entity is "optional")
    for(const section of dataAsJSON.massnahme.section){
        for(const input of section.input){
            if(input.$.dependsOn){
                continue
            }
            
            var entityName = CheckAndUpdateNameForEntities(intentID + input.label[0])

            /// add the answer, when the Intent with this Entity/Input/<change> is selected.
            intent.messages[0].text.text.push("Sie haben " + dataAsJSON.massnahme.$.name + ", $" + entityPresetName + ".original und $" + entityName + ".original ausgewählt.")

            /// add the Parameters
            let parameter = {
                displayName: entityName,
                entityTypeDisplayName: "@" + entityName,
                value: "$" + entityName,
                mandatory: false,
                isList: input.$.multiple || false
            }
            intent.parameters.push(parameter)

            /// check if parts for the Trainingphrases exist
            let checkEntities = intentsAndEntitiesJSONFile.id[intentID].entities[entityName]
            if(!checkEntities || !checkEntities.parts || checkEntities.parts.length == 0){
                continue
            }

            /// add Trainingphrases
            let m_p_c_trainingPhrase
            for(const part of intentsAndEntitiesJSONFile.id[intentID].entities[entityName].parts){
                try {
                    m_p_c_trainingPhrase = JSON.parse(JSON.stringify(m_p_trainingPhrase));
                }catch(e){
                    console.log(e)
                }
                m_p_c_trainingPhrase.parts.push({"text": " "})
                m_p_c_trainingPhrase.parts.push(part)
                intent.trainingPhrases.push(m_p_c_trainingPhrase)
            }

        }
    }
}

function get_XML_Massnahme_as_JSON(massnahme){
    try{
        var dataAsXML = fs.readFileSync(path_to_massnahmen + '/' + massnahme, {encoding: 'utf-8'});
        var parser = new xml2js.Parser();
        var dataAsJSON;
        parser.parseString(dataAsXML, (err, result) => {
            dataAsJSON = result
        });
        return dataAsJSON   
    }catch(e){
        console.log("Error in the Funktion 'get_XML_Massnahme_as_JSON'")
        console.log(e)
    }
}

function CheckAndUpdateNameForEntities(name){
    try{
        // Valid Name:
        // - contain only A-Z, a-z, 0-9, _, -
        // - start with a letter
        name = name.replace(/\u00c4/g, "Ae") //Ä
        name = name.replace(/\u00e4/g, "ae") //ä
        name = name.replace(/\u00dc/g, "Ue") //Ü
        name = name.replace(/\u00fc/g, "ue") //ü
        name = name.replace(/\u00d6/g, "Oe") //Ö
        name = name.replace(/\u00f6/g, "oe") //ö
        name = name.replace(/\u00df/g, "ss") //ß
        
        name = name.replace(/ /g, "") //' '
        name = name.replace(/\//g, "") //'/'
        name = name.replace(/\(/g, "") //(
        name = name.replace(/\)/g, "") //)

        name = name.replace(/\%/g, "Prozent") //%
        name = name.replace(/\°/g, "0") //°
        name = name.replace(/\²/g, "2") //²

        name = name.substring(0, 30)

        return name   
    }catch(e){
        console.log("Error in the Funktion 'CheckAndUpdateNameForEntities'")
        console.log(e)
    }
}

function CheckAndUpdateNameForIntents(name){
    try{
        // Valid Name:
        // - contain only A-Z, a-z, 0-9, _, -
        // - start with a letter
        name = name.replace(/\u00c4/g, "Ae") //Ä
        name = name.replace(/\u00e4/g, "ae") //ä
        name = name.replace(/\u00dc/g, "Ue") //Ü
        name = name.replace(/\u00fc/g, "ue") //ü
        name = name.replace(/\u00d6/g, "Oe") //Ö
        name = name.replace(/\u00f6/g, "oe") //ö
        name = name.replace(/\u00df/g, "ss") //ß

        name = name.replace(/\°/g, "0") //°
        name = name.replace(/\²/g, "2") //²

        name = name.replace(/\//g, "-") //'/'
        name = name.replace(/\%/g, "Prozent") //%
        
        name = name.replace(/\ /g, "_") //" "
        name = name.replace(/\(/g, "") //(
        name = name.replace(/\)/g, "") //)

        // The Entity Name is limited to 30 Letters/Symbols
        name = name.substring(0, 30)

        return name
    }catch(e){
        console.log("Error in the Funktion 'CheckAndUpdateNameForIntents'")
        console.log(e)
    }    
}


function createID(index){
    try{
        const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
        if(index >= alphabet.length){
            let indexDivide = Math.trunc(index / alphabet.length)
            let indexRest   = index % alphabet.length
            return createID(indexDivide-1) + alphabet[indexRest]
        }
        return alphabet[index]
    }catch(e){
        console.log("Error in the Funktion 'createID'")
        console.log(e)
    }
}

function OverviewJSON_load(){
    try{
        /// get JSON File
        var raw_overviewJSON = fs.readFileSync("intentsAndEntities.json", {encoding: 'utf-8'});
        return JSON.parse(raw_overviewJSON)
    }catch(e){
        return {
            intents:{},
            id:{}
        }
    }
}
function OverviewJSON_save(){
    try{
        var dictstring = JSON.stringify(intentsAndEntitiesJSONFile);
        fs.writeFileSync("intentsAndEntities.json", dictstring);
    }catch(e){
        console.log("Error in the function 'OverviewJSON_save'")
        console.log(e)
    }
}

function OverviewJSON_create_intent(suitableIntentName, originalIntentName){
    try{
        intentsAndEntitiesJSONFile.intents[suitableIntentName] = {
            massnahmeName: originalIntentName,
            entitiesName: {},
            objects: [],
            dependsOn: []
        }
    }catch(e){
        console.log("Error in the function 'OverviewJSON_create_intent'")
        console.log(e)
    }
}
function OverviewJSON_add_ID_to_Intent(suitableIntentName, id){
    try{
        intentsAndEntitiesJSONFile.intents[suitableIntentName].id = id
    }catch(e){
        console.log("Error in the function 'OverviewJSON_add_ID_to_Intent'")
        console.log(e)
    }
}
function OverviewJSON_add_ID(MassnahmeName, id){
    try{
        if(intentsAndEntitiesJSONFile.id[id] == undefined){
            intentsAndEntitiesJSONFile.id[id] = {}
        }
        intentsAndEntitiesJSONFile.id[id]["name"] = MassnahmeName

        if(intentsAndEntitiesJSONFile.massnahme == undefined){
            intentsAndEntitiesJSONFile.massnahme = {}
        }
        intentsAndEntitiesJSONFile.massnahme[MassnahmeName] = id
    }catch(e){
        console.log("Error in the function 'OverviewJSON_add_ID'")
        console.log(e)
    }
}
function OverviewJSON_add_entity(id, suitableEntityName, originalEntityName, trainingPhrases_parts){
    try{
        if(intentsAndEntitiesJSONFile.id[id] == undefined){
            intentsAndEntitiesJSONFile.id[id] = {"entities":{}}
        }
        if(intentsAndEntitiesJSONFile.id[id]["entities"] == undefined){
            intentsAndEntitiesJSONFile.id[id]["entities"] = {}
        }
        intentsAndEntitiesJSONFile.id[id].entities[suitableEntityName] = {
            originalEntityName: originalEntityName,
            parts: trainingPhrases_parts
        }
    }catch(e){
        console.log("Error in the function 'OverviewJSON_add_entity'")
        console.log(e)
    }
}
function OverviewJSON_add_object(suitableIntentName, objectName){
    try{
        if(!intentsAndEntitiesJSONFile.intents[suitableIntentName] == undefined){
            intentsAndEntitiesJSONFile.intents[suitableIntentName] = {objects: []}
        }
        if(intentsAndEntitiesJSONFile.intents[suitableIntentName].objects == undefined){
            intentsAndEntitiesJSONFile.intents[suitableIntentName].objects = []
        }
        intentsAndEntitiesJSONFile.intents[suitableIntentName].objects.push(objectName)
    }catch(e){
        console.log("Error in the function 'OverviewJSON_add_object'")
        console.log(e)
    }
}
function OverviewJSON_empty_object(suitableIntentName){
    try{
        intentsAndEntitiesJSONFile.intents[suitableIntentName].emptyObject = true
    }catch(e){
        console.log("Error in the function 'OverviewJSON_empty_object'")
        console.log(e)
    }
}

module.exports = DialogflowAgent;

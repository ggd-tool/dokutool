const mongoose = require('mongoose');

const presetsModel = mongoose.Schema(
    {
        name: {
            "type": String,
            "unique": true
        },
        data: {
            "type": Array,
        },
        url: {
            "type": String,
        }
    }, {timestamps: true} 
);

module.exports = mongoose.model('PresetDialog', presetsModel);
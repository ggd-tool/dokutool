const mongoose = require('mongoose');

const massnahmenModel = mongoose.Schema(
    {
        name: {
            "type": String,
            "unique": true
        },
        data: {
            "type": Array,
        },
        preset: {
            "type": Boolean,
        },
        url: {
            "type": String,
        }
    }, {timestamps: true} 
);

module.exports = mongoose.model('MassnahmenDialog', massnahmenModel);
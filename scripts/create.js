// Import packages
const Handlebars = require("handlebars");
const xml2js = require('xml2js');
const fs = require('fs');
const path = require('path');
const parser = new xml2js.Parser({
    attrkey: "attr"
});

const createDialog = require('./createDialog');
const figlet = require('figlet');

// Import Templates
const templateModel = Handlebars.compile(fs.readFileSync("./templates/mongo-schema.template", "utf8"));
const templateController = Handlebars.compile(fs.readFileSync("./templates/api-controller.template", "utf8"));
const templateControllerIndex = Handlebars.compile(fs.readFileSync("./templates/api-controller-index.template", "utf8"));
const templateRoutes = Handlebars.compile(fs.readFileSync("./templates/api-routes.template", "utf8"));
const templateModelIndex = Handlebars.compile(fs.readFileSync("./templates/api-model-index.template", "utf8"));

//Import Assets
const masseinheiten = require('./assets/einheiten.json');

//globale Variablen
const dir = "../api-server/models";
const dirController = "../api-server/controllers";
const dirControllerIndex = "../api-server";
var objectName;
var populate = [];

//spielerei
figlet('- PLONK -', function (err, data) {
    if (err) {
        console.log('Something went wrong...');
        console.dir('\x1b[91m%s\x1b[0m', err);
        return;
    }
    console.log('\x1b[97m%s\x1b[0m', data);
    registerHelpers();
    initialize();
});

//XML Files anhand des uebergebenen Parameters einlesen
async function initialize() {
    if (process.argv[2] === "all") {
        process.argv[2] = 'massnahmen'
        let files = fs.readdirSync('./massnahmen/');
        files.forEach(file => {
            process.argv.push(file);
        })
    }
    if (process.argv[2] === 'objects' || process.argv[2] === 'massnahmen') {
        for (var i = 3; i < process.argv.length; i++) {
            //Unterscheidung Erstellung Objekt oder Maßnahme
            var xmlString;
            if (process.argv[2] === 'objects') {
                try {
                    xmlString = fs.readFileSync("./objects/" + process.argv[i], "utf8");
                } catch (error) {
                    console.log('\x1b[91m%s\x1b[0m', 'Es existiert keine Datei mit dem Namen ' + process.argv[i] + ' unter ./objects');
                    return;
                }
            }
            if (process.argv[2] === 'massnahmen') {
                try {
                    xmlString = fs.readFileSync("./massnahmen/" + process.argv[i], "utf8");
                } catch (error) {
                    console.log('\x1b[91m%s\x1b[0m', 'Es existiert keine Datei mit dem Namen ' + process.argv[i] + ' unter ./massnahmen');
                    return;
                }
            }
            //Schleife darf nicht immer neu aufgerufen werden, deshalb wird erneute Funktion benötigt die zum bau des Presets erneut aufgerufen werden kann 
            await createModel(xmlString, false)
        }
    } else {
        console.log('\x1b[91m%s\x1b[0m', 'Bitte geben Sie an ob Sie ein Objekt (objects) oder eine Maßnahme (massnahmen) erstellen möchten');
    }
}

//Erstellt model und ruft anschließend weiterfürhrende Funktionen auf
async function createModel(xmlString, preset) {
    var object = {};
    //Ergebnisse Parsen und SQL Statment erstellen
    // Alle Attributennamen für die templates speichern
    // Alle Referenzierungen für MongoDB templates speichern
    var attributeNames = [];
    parser.parseString(xmlString, function (error, result) {
        if (error === null) {
            //Unterscheidung Erstellung Objekt oder Maßnahme
            if (result.object) {
                object = result.object;
                if (object.section[0].input) {
                    object.section[0].input.splice(0, 0, {
                        attr: {
                            type: 'string',
                            required: 'true',
                            unique: 'true'
                        },
                        label: ['Name']
                    });
                } else {
                    object.section[0].input = [];
                    object.section[0].input.push({
                        attr: {
                            type: 'string',
                            required: 'true',
                            unique: 'true'
                        },
                        label: ['Name']
                    });
                }
            }
            if (result.massnahme) {
                object = result.massnahme;
                object.section[0].input.splice(0, 0, {
                    attr: {
                        type: 'datepicker',
                        required: 'true',
                    },
                    label: ['Datum']
                });
                object.section.push({
                    attr: {
                        name: 'Optionale Informationen'
                    },
                    input: [{
                        attr: {
                            type: 'textarea',
                            placeholder: 'Bemerkung hinterlassen'
                        },
                        label: ['Bemerkung']
                    },
                    {
                        attr: {
                            type: 'user-select',
                            hint: 'Welcher Nutzer hat die Maßnahme durchgeführt'
                        },
                        label: ['Durchgeführt von']
                    }
                    ]
                })
            }
            //Name des objekts für Preset ändern
            if (preset) {
                object.section[0].input.splice(0, 0, {
                    attr: {
                        type: 'string',
                        required: 'true'
                    },
                    label: ['Name']
                });
                objectName = "Preset_" + formatForFiles(object['attr'].name);
            } else {
                objectName = formatForFiles(object['attr'].name);
            }
            //Sections durchgehen
            object.section.forEach(element => {
                var splice = [];
                element.input.forEach((input, index) => {
                    //name fuer Spalte in der Datenbank festlegen
                    var attributeName;

                    attributeName = input.label;
                    if (preset) {
                        if (input['attr'].type !== 'datepicker') {
                            attributeName = formatForDatabase(attributeName);
                            attributeNames.push(attributeName);
                        }
                        //requireds ausschließen
                        if (input['attr'].required === 'true' && input.label[0] !== 'Name') {
                            input['attr'].required = undefined;
                        }
                    } else {
                        attributeName = formatForDatabase(attributeName);
                        attributeNames.push(attributeName);
                    }
                    //Unit überprüfen falls vorhanden
                    if (input.unit) {
                        var checkUnit = false;
                        masseinheiten.einheiten.forEach(einheit => {
                            if (input.unit[0] === einheit) {
                                checkUnit = true;
                            }
                        })
                        if (checkUnit === false) {
                            console.log('\x1b[91m%s\x1b[0m', "Es existiert keine Einheit mit dem Namen " + input.unit[0]);
                            return;
                        }
                    }
                    //Datepicker für Presets ignorieren
                    if (preset && input['attr'].type === "datepicker") {
                        splice.push(index);
                    }
                });
                //Nicht benötige Felder für Preset entfernen
                if (splice.length > 0) {
                    splice.forEach(index => {
                        element.input.splice(index, 1)
                    })
                }
            });
        };
    });

    //Dialoge in der DB erstellen
    if (preset) {
        await createDialog.create(object, 'preset', objectName);
    } else {
        await createDialog.create(object, process.argv[2], objectName);
    }

    // Daten verpacken
    // name - der Objektname, aus der .xml
    // nameLc - der Objektname, in Kleinbuchstaben, aus der .xml
    // jsonData - das mongoose Schema als String (nur JSON Teil)
    // attributeNames - Einzelnamen der Attribute
    // populate - Referenzierungen eines Objektes auf andere Objekte
    var data = {
        "object": object,
        "name": objectName,
        "formatedName": formatForFiles(objectName),
        "nameLc": formatForFiles(objectName.toLowerCase()),
        "attributeNames": attributeNames,
        "populate": populate
    }
    // Template befüllen
    var newModelFile = templateModel(data);
    var newControllerFile = templateController(data);
    //populates resetten
    populate = [];
    // Model als Datei in /models speichern
    // dir erstellen, falls noch nicht vorhanden
    createDirectory(dir);
    console.log('\x1b[32m%s\x1b[0m', 'Erstelle Model für ' + objectName);
    fs.writeFile(dir + "/" + objectName + ".model.js", newModelFile, (err) => {
        if (err) {
            throw err;
        }
    });
    let dataindex = createController(newControllerFile, object);
    await dataindex.then(async (res) => {
        await createRoutes(res, object);
        //Model und Routen für Preset erstellen falls zugelassen
        if (object['attr'].preset === 'true' && !preset) {
            await createModel(xmlString, true);
        }
    }, (err) => {
        console.log('\x1b[97m%s\x1b[0m', err);
    });
}

// Controller als Datei in /controllers speichern
// dir erstellen, falls noch nicht vorhanden
async function createController(newControllerFile, object) {
    return new Promise((resolve, reject) => {
        createDirectory(dirController);
        fs.writeFile(dirController + "/" + objectName + ".controller.js", newControllerFile, (err) => {
            if (err) {
                reject("");
                throw err;
            }
            // Controller-Index Datei generieren und speichern
            // dir erstellen, falls noch nicht vorhanden
            fs.readdir(path.resolve(__dirname, dirController), (err, files) => {
                if (err) {
                    reject("");
                    throw err;
                }
                // controllerName - Name des Controllers - entfernt das .js des Dateinamens
                // modelName - .model.js
                // lcName - modelName in Kleinbuchstaben
                // url - lcName aber als URL encodiert -> bspw. Umlaute müssen anders kodiert sein
                for (let i = 0; i < files.length; i++) {
                    files[i] = {
                        "controllerName": formatForFiles(files[i].substring(0, files[i].length - 14)) + "Controller",
                        "controllerImport": formatForFiles(files[i].substring(0, files[i].length - 14)) + ".controller",
                        "modelImport": formatForFiles(files[i].substring(0, files[i].length - 14)),
                        "modelName": formatForFiles(files[i].substring(0, files[i].length - 14)),
                        "lcName": formatForFiles(files[i].substring(0, files[i].length - 13).toLowerCase()),
                        "url": fixedEncodeURIComponent(files[i].substring(0, files[i].length - 14).toLowerCase())
                    };
                }

                const dataControllerIndex = {
                    controller: files
                }
                // Template befüllen
                // Controllerindex als Datei in / speichern
                // dir erstellen, falls noch nicht vorhanden
                // der Controllerindex enthält alle Controller in einem Objekt und ermöglicht es diese einfacher auszulesen und einzubinden
                let newControllerIndexFile = templateControllerIndex(dataControllerIndex);
                createDirectory(dirControllerIndex);
                fs.writeFile(dirControllerIndex + "/index-controller.js", newControllerIndexFile, (err) => {
                    if (err) {
                        reject("");
                        throw err;
                    }
                    console.log('Index für API Controller aktualisiert!');
                });
                resolve(dataControllerIndex);
            });
        });
    });
};

// Template befüllen
// Routen als Datei in / speichern
// dir erstellen, falls noch nicht vorhanden
// die Routen-Datei enthält alle Routen in einem Objekt und ermöglicht es diese einfacher auszulesen und einzubinden
async function createRoutes(dataControllerIndex) {
    let newRoutes = templateRoutes(dataControllerIndex);
    fs.writeFile(dirControllerIndex + "/routes.js", newRoutes, (err) => {
        if (err) {
            throw err;
        }
        console.log('Routen aktualisiert!');
    });

    // Template befüllen
    // ModelIndex als Datei in / speichern
    // dir erstellen, falls noch nicht vorhanden
    // der ModelIndex enthält alle Models in einem Objekt und ermöglicht es diese einfacher auszulesen und einzubinden
    let newModelIndexFile = templateModelIndex(dataControllerIndex);
    fs.writeFile(dirControllerIndex + "/models-index.js", newModelIndexFile, (err) => {
        if (err) {
            throw err;
        }
        console.log('Model-Index aktualisiert!');
    });
    return;
}

//falls übergebene dir nicht existiert diese anlegen
function createDirectory(dir) {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir)
    }
}

//überprüft ob angebebene Directory oder File existiert
function checkDirectory(dirFile) {
    if (fs.existsSync(dirFile)) {
        return true;
    } else {
        return false;
    }
}

//label fuer die Datenbank zu LowerCase umwandeln
//leerzeichen durch _ ersetzen
//Punkte entfernen
function formatForDatabase(attribute) {
    attribute = attribute.toString().toLowerCase();
    if (attribute.includes(" ")) {
        attribute = attribute.split(" ").join("_");
    }
    if (attribute.includes("-")) {
        attribute = attribute.split("-").join("_");
    }
    if (attribute.includes(",")) {
        attribute = attribute.split(",").join("");
    }
    if (attribute.includes("%")) {
        attribute = attribute.split("%").join("");
    }
    if (attribute.includes("‰")) {
        attribute = attribute.split("‰").join("");
    }
    if (attribute.includes(".")) {
        attribute = attribute.split(".").join("");
    }
    if (attribute.includes("(")) {
        attribute = attribute.split("(").join("");
    }
    if (attribute.includes(")")) {
        attribute = attribute.split(")").join("");
    }
    if (attribute.includes("/")) {
        attribute = attribute.split("/").join("_");
    }
    if (attribute.includes("²")) {
        attribute = attribute.split("²").join("2");
    }
    if (attribute.includes("³")) {
        attribute = attribute.split("³").join("3");
    }
    if (attribute.includes("°")) {
        attribute = attribute.split("°").join("");
    }
    return attribute;
}

//Namen für files wie z.B. Controller formatieren (keineklammern, / usw.) 
function formatForFiles(objectName) {
    objectName = objectName.toString();
    if (objectName.includes(" ")) {
        objectName = objectName.split(" ").join("_");
    }
    if (objectName.includes(".")) {
        objectName = objectName.split(".").join("");
    }
    if (objectName.includes("(")) {
        objectName = objectName.split("(").join("");;
    }
    if (objectName.includes(")")) {
        objectName = objectName.split(")").join("");;
    }
    if (objectName.includes("-")) {
        objectName = objectName.split("-").join("");;
    }
    return objectName;
}

function fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
}

function registerHelpers() {
    Handlebars.registerHelper("whatIsRequired", function (object, label) {
        if (object) {
            return object;
        } else {
            return label;
        }
    });
    Handlebars.registerHelper("formatForDatabase", function (label, arrayLabel) {
        if (arrayLabel) {
            return formatForDatabase(arrayLabel);
        } else {
            return formatForDatabase(label);
        }
    });
    Handlebars.registerHelper("typeDecoder", function (type, object, input, dependsOn, multiple, label) {
        switch (type) {
            //dropdown repräsentiert die Referenzierung eines anderen Typen
            case 'dropdown':
                //Ueberpruefung und anschliessende Referenzierung des Models fuer den Dropdown
                if (object !== undefined || dependsOn !== undefined) {
                    let file;
                    if (object) {
                        file = formatForFiles(object);
                    } else {
                        file = formatForFiles(dependsOn.split(".")[0]);
                    }
                    if (!checkDirectory(dir + '/' + file + ".model.js")) {
                        console.log('\x1b[91m%s\x1b[0m', "Es existiert noch kein Typ " + file + ". Bitte überprüfen Sie Groß-und Kleinschreibung und legen diesen Typ eventuell erst an bevor Sie ihn referenziern.");
                        return;
                    } else {
                        if (dependsOn) {
                            //Populates zuweisen populate({path: 'feld', populate: {path: 'gießwagen', model: 'Gießwagen'}});
                            let depends = dependsOn.split(".");
                            let populateString = `{ path: '${formatForDatabase(depends[0])}'`;
                            let populateIndex = 1;
                            for (let i = 1; i < depends.length; i++) {
                                let upperCase = depends[i].charAt(0).toUpperCase() + depends[i].slice(1);
                                if (checkDirectory(dir + '/' + upperCase + ".model.js")) {
                                    populateString += `, populate: {path: '${formatForDatabase(depends[i])}', model: '${upperCase}'`;
                                    populateIndex++;
                                }
                            }
                            for (let j = 0; j < populateIndex; j++) {
                                populateString += "}";
                            }
                            //gleiche Populates ausschließen 
                            let dontpush = false;
                            populate.forEach((popstring, index) => {
                                if (popstring.path === populateString.path) {
                                    dontpush = true;
                                    if (populateString.length > popstring.length) {
                                        populate[index] = populateString;
                                    }
                                }
                            })
                            if (!dontpush) {
                                populate.push(populateString);
                            }
                            return "[]";
                        } else {
                            if (!populate.includes(`'${formatForDatabase(file)}'`)) {
                                populate.push(`'${formatForDatabase(file)}'`);
                            }
                            if (input && input['attr'].multiple === 'true') {
                                return `[{ 'type': mongoose.Schema.Types.ObjectId, ref:"${formatForFiles(object)}"}], 'default': ''`;
                            } else {
                                return "mongoose.Schema.Types.ObjectId, ref:'" + formatForFiles(file) + "', 'default': ''";
                            }
                        }
                    }
                } else {
                    console.log('\x1b[91m%s\x1b[0m', "Bitte geben Sie ein Objekt an welches im Dropdown refernziert werden soll.");
                    return;
                }
            // Falls es sich um ein Dropdown mit options Handelt (select) soll nachher die ausgewählte Option als String gespeichert werden
            case 'select':
                if (multiple === true) {
                    return "[{ 'type': String }], 'default': []";
                } else {
                    return `String , 'default': ""`
                }

            // type für Arrays aus darauffolgenden inputs erstellen
            case 'array':
                var fields = "";
                input.forEach((arrayInputs, index) => {
                    if (arrayInputs['attr'].object) {
                        populate.push(`'${formatForDatabase(label) + "." + JSON.parse(JSON.stringify(arrayInputs.label).toLocaleLowerCase())[0]}'`);
                        fields += '"' + formatForDatabase(JSON.parse(JSON.stringify(arrayInputs.label).toLocaleLowerCase())) + '": ' + '{ "type": mongoose.Schema.Types.ObjectId, ref:"' +
                            arrayInputs['attr'].object.charAt(0).toUpperCase() + arrayInputs['attr'].object.slice(1) + '"}';
                        if (index !== input.length - 1) {
                            fields += ", ";
                        }
                    } else {
                        fields += '"' + formatForDatabase(JSON.parse(JSON.stringify(arrayInputs.label).toLocaleLowerCase())) + '": ' +
                            arrayInputs['attr'].type.charAt(0).toUpperCase() + arrayInputs['attr'].type.slice(1);
                        if (index !== input.length - 1) {
                            fields += ", ";
                        }
                    }
                })
                var array = "[{" + fields + "}], 'default': []";
                return array;
            case 'checkbox':
                return 'Boolean';
            case 'datepicker':
                return `Date , 'default': null`;
            case 'radio':
                return `String , 'default': ""`;
            case 'textarea':
                return `String , 'default': ""`;
            case 'user-select':
                return `{
                    'ID': String,
                    'FIRST_NAME': String,
                    'LAST_NAME': String,
                    'EMAIL': String,
                    'USERNAME': String
                },  
                'default': {}
                `;
            case 'rangeSlider':
                return "[]";
            case 'singleSlider':
                return 'Number';
            default:
                return type.charAt(0).toUpperCase() + type.slice(1) + `, 'default': ""`;
        }
    });
}
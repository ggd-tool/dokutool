// Import packages
const mongoose = require("mongoose");

//Fix für deprecation Warnung kann/muss vielliecht mit neueren Mongoose Versionen entfernt werden
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

// Modell für alle einzelnen Objektdialoge/Massnahmendialoge/Presetdialoge
const ObjectDialog = require("./mongoData/objectDialog.model");
const MassnahmenDialog = require("./mongoData/massnahmenDialog.model");
const PresetDialog = require("./mongoData/presetsDialog.model");

//Gibt an ob Presets in diesem Dialog ausgewählt werden können sollen
var preset = false;

// Object ist das geparste XML
async function create(object, typ, objectName) {
    var depends = [];
    var keys = [];
    // Mit MongoDB verbinden
    await mongoose
        .connect("mongodb://localhost:27017/dokutool", {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            "auth": {
                "authSource": "admin"
            },
            "user": "admin",
            "pass": "admin"
        })
        .then(async () => {
            if (object['attr'].preset === 'true' && typ !== 'preset') {
                preset = true
            }
            // Wenn Verbindung zur DB hergestellt
            // data ist eine Abbildung des Frontend Dialogs
            let data = [];
            // Durch Sektionen des XML iterieren und dem data-Array zuweisen
            object.section.forEach((section, index) => {
                // Neuen Sektions-Block hinterlegen und Standardvalues einfügen
                data[index] = {};
                data[index].label = section.attr.name;
                data[index].hint = (section.attr.hint) ? section.attr.hint : '';
                // Ein Array für die Inputs in einer Sektion anlegen
                data[index].inputs = [];
                section.input.forEach(input => {
                    // Tempöreres Input Objekt anlegen
                    let tmpInput = {};
                    // Label und Key festlegen
                    tmpInput.key = input.label[0].toLowerCase();
                    tmpInput.label = input.label[0];
                    tmpInput.placeholder = input['attr'].placeholder;
                    tmpInput.default = input['attr'].default;

                    if (input.help) {
                        tmpInput.hint = input.help[0];
                    }
                    if (input.attr.required) {
                        if (typ !== 'preset' || input.label[0] === "Name") {
                            tmpInput.required = true;
                        }
                    }
                    // Nach dem Inputtypen unterscheiden und Attribute dementsprechend setzen
                    switch (input.attr.type) {
                        case "string":
                            tmpInput.type = 'text';
                            if (input.attr.min) {
                                tmpInput.min = input.attr.min;
                            }
                            if (input.attr.max) {
                                tmpInput.max = input.attr.max;
                            }
                            break;
                        case "dropdown":
                            if (input.attr.multiple === 'true') {
                                tmpInput.type = 'multidropdown';
                            } else {
                                tmpInput.type = 'dropdown';
                                //Wenn dependsOn nachher immer multidropdown, wo allerdings nach auswahl einer option andere disabled werden
                                if (input.attr.dependsOn) {
                                    tmpInput.type = 'multidropdown';
                                    tmpInput.singleDepend = true;
                                }
                            }
                            tmpInput.options = 'ref';
                            if (input.attr.object) {
                                tmpInput.ref = input.attr.object;
                            } else {
                                tmpInput.dependsOn = input.attr.dependsOn;
                                if (!keys.includes(formatForDatabase(tmpInput.key))) {
                                    keys.push(formatForDatabase(tmpInput.key));
                                    depends.push(input.attr.dependsOn);
                                }
                            }
                            break;
                        case "checkbox":
                            tmpInput.type = 'checkbox';
                            break;
                        case "radio":
                            tmpInput.type = 'radio';
                            tmpInput.options = convertOptions(input.option);
                            break;
                        case "select":
                            if (input.attr.multiple === 'true') {
                                tmpInput.type = 'multidropdown';
                            } else {
                                tmpInput.type = 'dropdown';
                            }
                            tmpInput.options = convertOptions(input.option);
                            break;
                        case 'user-select':
                            tmpInput.type = 'user-dropdown';
                            break;
                        case "datepicker":
                            tmpInput.type = 'datepicker';
                            break;
                        case "number":
                            tmpInput.type = 'number';
                            if (input.attr.min) {
                                tmpInput.min = input.attr.min;
                            }
                            if (input.attr.max) {
                                tmpInput.max = input.attr.max;
                            }
                            break;
                        case "textarea":
                            tmpInput.type = 'textarea';
                            if (input.attr.min) {
                                tmpInput.min = input.attr.min;
                            }
                            if (input.attr.max) {
                                tmpInput.max = input.attr.max;
                            }
                            break;
                        case "array":
                            // Custom-Array Input
                            tmpInput.type = 'array';
                            tmpInput.options = [];
                            input['input'].forEach(element => {
                                let tmpOption = {};
                                tmpOption.key = element.label[0].toLowerCase();
                                tmpOption.label = element.label[0];

                                if (element.help) {
                                    tmpOption.hint = element.help[0];
                                }
                                if (element.attr.required) {
                                    tmpOption.required = true;
                                }
                                // Unterstützt als Sub-Inputs lediglich TextInputs und NumberInputs und Referenzierte Dropdowns
                                switch (element.attr.type) {
                                    case 'string':
                                        tmpOption.type = 'text';
                                        if (element.attr.min) {
                                            tmpOption.min = element.attr.min;
                                        }
                                        if (element.attr.max) {
                                            tmpOption.max = element.attr.max;
                                        }
                                        break;
                                    case 'number':
                                        tmpOption.type = 'number';
                                        if (element.attr.min) {
                                            tmpOption.min = element.attr.min;
                                        }
                                        if (element.attr.max) {
                                            tmpOption.max = element.attr.max;
                                        }
                                        break;
                                    case "dropdown":
                                        if (element.attr.multiple === 'true') {
                                            tmpOption.type = 'multidropdown';
                                        } else {
                                            tmpOption.type = 'dropdown';
                                        }
                                        tmpOption.options = 'ref';
                                        if (element.attr.object) {
                                            tmpOption.ref = element.attr.object;
                                        }
                                        break;
                                }
                                tmpInput.options.push(tmpOption);
                            });
                            break;
                        case 'singleSlider':
                        case 'rangeSlider':
                            if (input['attr'].highValue) {
                                tmpInput.type = 'rangeSlider';
                                tmpInput.highValue = parseInt(input['attr'].highValue);
                            } else {
                                tmpInput.type = 'singleSlider';
                            }
                            tmpInput.options = {
                                floor: parseInt(input['attr'].min),
                                ceil: parseInt(input['attr'].max),
                                step: 1,
                                showTicks: false,
                            };
                            if (input['attr'].steps) {
                                tmpInput.options.step = input['attr'].steps;
                            }
                            if (input['attr'].ticks === 'true') {
                                tmpInput.options.showTicks = true;
                            }
                            tmpInput.value = parseInt(input['attr'].value);
                            break;
                    }
                    // Temporäres Element zu den Inputs hinzufügen
                    data[index].inputs.push(tmpInput);
                });
            });
            //depends zuweisen
            for (let i = 0; i < data.length; i++) {
                let section = data[i];
                for (let j = 0; j < section.inputs.length; j++) {
                    let input = section.inputs[j];
                    for (let l = 0; l < depends.length; l++) {
                        let depend = depends[l];
                        if (input.ref === depend.split(".")[0]) {
                            if (!input.dependFor) {
                                input.dependFor = [];
                                input.dependLoader = [];
                            }
                            if (!input.dependFor.includes(keys[l])) {
                                input.dependFor.push(keys[l]);
                                input.dependLoader.push(depend);
                            }
                        }
                    };
                };
            };

            // Den Objektdialog, Massnahmendialog oder Prestdialog in der Datenbank speichern
            switch (typ) {
                case 'objects':
                    const newObjectDialog = new ObjectDialog({
                        'name': object['attr'].name,
                        'data': data,
                        'url': "/" + fixedEncodeURIComponent(objectName.toLowerCase()),
                    });
                    await newObjectDialog.save();
                    break;
                case 'massnahmen':
                    const newMassnahmenDialog = new MassnahmenDialog({
                        'name': object['attr'].name,
                        'data': data,
                        'preset': preset,
                        'url': "/" + fixedEncodeURIComponent(objectName.toLowerCase()),
                    });
                    await newMassnahmenDialog.save();
                    break;
                case 'preset':
                    const newPresetDialog = new PresetDialog({
                        'name': "Preset_" + object['attr'].name,
                        'data': data,
                        'url': "/" + fixedEncodeURIComponent(objectName.toLowerCase()),
                    });
                    await newPresetDialog.save();
                    break;
                default:
                    console.log('\x1b[91m%s\x1b[0m', "Es existiert kein Dialog zum typ: " + typ);
                    return;
                    break;
            }
        })
        .catch((err) => {
            if (err.code === 11000) {
                console.error('\x1b[33m%s\x1b[0m', "Warnung: Es existiert bereits ein Dialog für: " + object['attr'].name);
            } else {
                console.error('\x1b[91m%s\x1b[0m', err);
            }
        })
    mongoose.connection.close()
}

// Ein Array aus Optionen (wie gelesen aus dem XML) zu einem Frontend-kompatiblen Array konvertieren.
// Return: [{key="option1", value="Option 1"},...]
function convertOptions(options) {
    let convOptions = [];
    options.forEach(option => {
        let tmpOption = {};
        tmpOption.key = option.attr.key;
        tmpOption.value = option._;
        convOptions.push(tmpOption);
    });
    return convOptions;
}

function formatForDatabase(attribute) {
    attribute = attribute.toString().toLowerCase();
    if (attribute.includes(" ")) {
        attribute = attribute.split(" ").join("_");
    }
    if (attribute.includes(".")) {
        attribute = attribute.split(".").join("");
    }
    if (attribute.includes("(")) {
        attribute = attribute.split("_(")[0];
    }
    return attribute;
}

function fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
}


module.exports = {
    create
}
# README #

Set up Project

### How do I get set up? ###

* ### Schritt 1: Docker-Desktop installieren ###
    
    Download Docker-Desktop: https://www.docker.com/products/docker-desktop

    Folge den Schritten um Docker Desktop (Hyper-V) zu installieren

    Starte Docker Desktop

    Öffne ein Terminmal, wechsel in das Verzeichnis ./docker der App und führen den Befehl docker-compose up aus

    Auf eventuelle Nachfrage von Windows drücke auf "Share It"

    Keyclock ist nun unter localhost:8080 erreichbar, phpMyAdmin unter:8081, eine MySQL Datenbank läuft auf Port 3306 und eine MongoDB auf 27017

* ### Schritt 2: Node-Module installieren ###

    Führe npm install im Root Verzeichnis, als auch in ./client aus

* ### Schritt 3: MongoDB Compass installieren

    Download MongoDB Compass: https://www.mongodb.com/try/download/compass

    Folge den Schritten um MongoDB Compass zu installieren

    Öffne MongoDB Compass und Klicken auf Fill in connection fields individually:

    Hostname: localhost
    Port: 27017
    Authentication: Username/Password (admin/admin)

* ### Schritt 4: User in Keycloak anlegen ###

    Unter localhost:8080/auth/ links auf Admin Console klicken und dort mit admin admin anmelden

    Im Reiter links auf Useres klicken und dann rechts oben auf add User

    In disesem Screen einen Usernamen vergeben und die Email auf verified setzen, anschließend save drücken

    Danach unter dem Reiter Credentials ein Passwort vergeben und speichern
    
    Weiterhin muss dem User noch eine sogenannte Rolle gegeben werden. Klicke dazu auf den Reiter "Role Mappings".
    Zum testen sollte dem User am besten die Rolle "Configurator" zu geteilt werden. 
    Diese beinhaltet alle Rechte der dadrunter liegenden. Dazu die gewünschte Rolle auswählen und auf "Add selected" drücken. 

    Mit diesem Useraccount kann sich anschließend in der App angemeldet werden. 

    eventuell muss die neue realm Datei unter ./docker in Keycloak importiert werden

* ### Schritt 5: Modelle in das Backend laden ###

    Unter ./scripts befindet sich ein Script mit dem Namen create.js, mit diesem können dynamisch Backendmodelle und Routen aus xml Dateien erstellt werden

    Die dazugehörigen xml Dateien müssen unter ./scripts/objects bzw. ./scripts/massnahmen abgelget werden (Für Struktur siehe Beispieldateien)

    Das Script kann mit node create.js <objects/massnahmen> <objectName/Massnahmenname>.xml aufgerufen werden (mit node create all können alle Maßnahmen angelegt werden, da hier die Reihenfolge irrelevant ist)

* ### Schritt 6: Backend starten ###

    Das Backend kann unter ./api-server über node app.js gestartet werden und läuft danach auf Port 5000
    
* ### Schritt 7: Client tests ###

    Das Frontend unter ./client basiert auf Angular und ist mit AngularMaterial designed 

    Mit ng-serve kann das Frontend im Development-Modus unter localhost:4200 bearbeitet werden

### set up for Production ###

* ### Schritt 1: Downloade die neueste Version von Plonk von Github/Bitbucket. ###

* ### Schritt 2: Die Credentials/Environments anpassen. ###

    - In "/client/src/environments/environment.prod.ts" die Domain anpassen.

    - In "/api-server/.env" die Domain, Ports, Usernamen und Passwörter anpassen.
      (Usernamen und Passwörter können in docker-compose.yml gefunden und angepasst werden)

    - In "/docker/.env" die EXAMPLES mit den Unternehmen Namen ändern.
      (Docker Services dürfen nicht gleiche Namen bekommen)

    - In "/docker/docker-compose.yml" die Passwörter und Ports anpassen.
      Zusätzlich muss der Service nginx ausgeklammert werden.
      Und die Klammern für den Service api müssen entfernt werden.
      Die Ports für den Service api müssen angepasst werden.

    - In "/scipts/createDialog.js" müssen der Username und Passwort angepasst werden.

* ### Schritt 3: Zum Server uploaden ###

    - Verwende Filezilla um Plonk zum Server uploaden

* ### Schritt 4: Plonk einrichten ###

    - Installiere Docker für das jeweilige Betriebssystem im Server.

    - In "How do I get set up?" gehe die Schritte 1,2,4 und 5 durch.

    - Zusätzlich muss in Keycloak noch die zulässigen URL's und URI's angepasst werden.
      In einem Webbrowser besuche deine Website mit "www.EXAMPLE.de/auth" klicke links auf "Admin Console"
      dort meldest du dich an und gehst nach "Clients" und dann "dokutool-frontend".
      Dort passt du die URL's "Root URL", "* Valid Redirect URIs", "Admin URL" und "Web Origins" an.
      Verändere "localhost" zu deinem Domainnamen.

### Die Sprachsteuerung einrichten ###

* ### Schritt 1: Google Account erstellen ###

    - Die Sprachsteuerung nutzt Dialogflow, dafür wird ein Google Account benötigt.

* ### Schritt 2: Dialogflow einrichten ###

    - Erstellen sie einen Agenten über die Dialogflow Konsole.
    - Nutzen sie die Version Dialogflow ES.
    https://cloud.google.com/dialogflow/es/docs [Aufgerufen am 30.05.2022]

    - Nachdem erstellen eines Agenten in Dialogflow, muss nun ein JSON-Schlüssel angefordert werden, damit
    der Agent außerhalb von der Konsole bearbeitet werden kann.
    Folgen sie der Einleitung im Kapitel "Dienstkonto erstellen und Datei mit dem privaten Schlüssel herunterladen": 
    https://cloud.google.com/dialogflow/es/docs/quick/setup?hl=de#sa-create [Aufgerufen am 30.05.2022]

* ### Schritt 3: Webhook einrichten ###

    - Der Webhook benötigt einen Domainnamen mit einer https Verbindung.
    - Nachdem der Webhook über das Internet erreichbar ist, kann die Webaddresse in der Dialogflow-Konsole unter "Fulfillment" eingefügt werden. Falls sie ein Passwort verwenden muss, dies auch mit ein gegeben werden. (Testen sie die Erreichbarkeit des Webhooks mit Postman)
    - Der heruntergeladene Schlüssel muss im "Webhook" und im "api-server" unter "environment" gespeichert werden.
    - Die ".env" Datei muss noch eingestellt werden. Ein Account für einen Keycloak User und ein KeyCloak CLIENTSECRET für den Clienten "dokutool-postman" muss darin vorkommen.
    - Zuletzt wird der Agent mit seinen Entities, Intents und Kontexten generiert. Dazu muss nur "node .\create_agent.js" im Terminal ausgeführt werden.

* ### Schritt 3: Experimentelle Intents (Optional) ###

    - Das Projekt besitzt Experimentelle Intents, die Maßnahmen durch Parameter erkennen können. Jedoch 
    Funktioniert es nicht besonders gut, weshalb es ausgeklammert ist. Wenn sie es aber ausprobieren wollen, müssen 
    sie im Ordner "Webhook" die Datei "create_agent.js" öffnen und die Klammern für 
    "await DialogflowAgent.create_Massnahmen_Intents()" entfernen.
    - Wenn Sie die Experimentelle Intents verwenden wollen, muss der Befehl "node --max-old-space-size=8192 create_agent.js"
    im Terminal ausgeführt werden, weil die Anzahl an Trainingssätzen die Memory Kapizität überschreitet. 

### Alle derzeitigen XML-Dateien ins Dokumentationssystem laden ###

node create.js objects "Auftrag.xml"
node create.js objects "Dünger.xml"
node create.js objects "Feld.xml"
node create.js objects "Gießwagen(Stufenlos).xml"
node create.js objects "Gießwagen(Stufen).xml"
node create.js objects "Pflanzenschutzmittel.xml"  
node create.js objects "Pflanzenstärkungsmittel.xml"
node create.js objects "Platte.xml"
node create.js objects "Regner.xml"
node create.js objects "Stammlösungsbecken.xml"

node create.js massnahmen "Gießen(Regner).xml"  
node create.js massnahmen "Gießen(Stufen).xml"
node create.js massnahmen "Gießen(Stufenlos).xml"  
node create.js massnahmen "manuellGießen.xml"       
node create.js massnahmen "Pflanzenschutz(GießenStufen).xml"
node create.js massnahmen "Pflanzenschutz(GießenStufenlos).xml"
node create.js massnahmen "Pflanzenschutz(Spritzen).xml"     
node create.js massnahmen "Pflanzenstärkung(GießenStufen).xml"  
node create.js massnahmen "Pflanzenstärkung(GießenStufenlos).xml"  
node create.js massnahmen "Pflanzenstärkung(Spritzen).xml"      
node create.js massnahmen "Pikieren.xml"     
node create.js massnahmen "Sprühen-Nebeln(Regner).xml"          
node create.js massnahmen "Sprühen-Neblen(GWStufen).xml"    
node create.js massnahmen "Sprühen-Neblen(GWStufenlos).xml"   
node create.js massnahmen "Stammlösung.xml"
node create.js massnahmen "Stecken.xml"
node create.js massnahmen "Topfen.xml"
node create.js massnahmen "Umstellen.xml"
node create.js massnahmen "Verkauf.xml"

### Sonstiges ###

docker-compose up --force-recreate



